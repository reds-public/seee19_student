/*******************************************************************
 * reptar_sp6.c
 *
 * Copyright (c) 2019 HEIG-VD, REDS Institute
 *******************************************************************/

#include <common.h>

extern void app_startup(void);

__attribute__((__section__(".main"))) int main(int argc, char *argv[])
{
	app_startup();

	printf("Bienvenue dans SEEE\n");

	return 0;
}

