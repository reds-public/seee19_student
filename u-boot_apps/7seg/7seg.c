/********************************************************************
* 7seg.c
*
* Copyright (c) 2019 HEIG-VD, REDS Institute
*
* A character is encoded on 8 bits
*
* BIT0: Encode segment "a"
* BIT1: Encode segment "b"
* BIT2: Encode segment "c"
* BIT3: Encode segment "d"
* BIT4: Encode segment "e"
* BIT5: Encode segment "f"
* BIT6: Encode segment "g"
* BIT7: Encode segment "dot point"
*
* 0: fedcba => 00111111 => 0x3F
* 1: cb     => 00000110 => 0x06
* 2: gedba  => abged => 0x5B
* 3: abgcd  => 01001111 => 0x4F
* 4: fgbc   => 01100110 => 0x66
* 5: afgcd  => 01101101 => 0x6D
* 6: afgecd => 01111101 => 0x7D
* 7: abc    => 00000111 => 0x07
* 8: abcdefg=> 01111111 => 0x7F
* 9: afgbcd => 01101111 => 0x6F
*
*******************************************************************/

#include <common.h>
#include <errno.h>

/*  SP6 REGISTER */
#define SP6_REG_BASE		0x18000000

/*  7 segments display */
#define SP6_REG_7SEG1		(SP6_REG_BASE + 0x30)
#define SP6_REG_7SEG2		(SP6_REG_BASE + 0x32)
#define SP6_REG_7SEG3		(SP6_REG_BASE + 0x34)

#define N_CHARS 10
#define WAITING_COUNTER 500000000

extern void app_startup(void);

static uint16_t *register_addr[] = {
	(uint16_t *) SP6_REG_7SEG1,
	(uint16_t *) SP6_REG_7SEG2,
	(uint16_t *) SP6_REG_7SEG3
};

static const uint16_t char2seg[N_CHARS] = {
	0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F
};

static int sevenseg_putc(int index, uint8_t number)
{
	uint16_t c;

	if ((index < 0) || (index > 2))
		return -1;

	if ((number < 0) || (number > N_CHARS))
		return -EINVAL;

	c = char2seg[number];

	*((uint16_t *) register_addr[index]) = c;

	return 0;
}

void wait(int waiting) {
	uint32_t i;

	for (i = 0; i < waiting; i++)
		asm("nop");

}

__attribute__((__section__(".main"))) int main(int argc, char *argv[])
{
	uint16_t value = 0;
	app_startup();

	/*  initialize the seven segs */
	sevenseg_putc(0,0);
	sevenseg_putc(1,0);
	sevenseg_putc(2,0);

	while (1)
	{
		sevenseg_putc(0, value % N_CHARS);
		sevenseg_putc(1, (value + 1) % N_CHARS);
		sevenseg_putc(2, (value + 2) % N_CHARS);
		value++;
		wait(WAITING_COUNTER);
	}

	return (0);
}
