/*******************************************************************
 * reptar_sp6.c
 *
 * Copyright (c) 2019 HEIG-VD, REDS Institute
 *******************************************************************/

#include <common.h>
#include <cpu_gpio.h>

#define GPIO_LED_0	MASK_LED_0
#define GPIO_LED_1	MASK_LED_1
#define GPIO_LED_2	MASK_LED_2
#define GPIO_LED_3	MASK_LED_3

#define GPIO_SW_0	MASK_SW_0
#define GPIO_SW_1	MASK_SW_1
#define GPIO_SW_2	MASK_SW_2
#define GPIO_SW_3	MASK_SW_3
#define GPIO_SW_4	MASK_SW_4

extern void app_startup(void);

/* Write the value to a specified register  32bits wide */
void w32(uint32_t *reg, uint32_t value)
{
	volatile uint32_t *r = (volatile uint32_t *) reg;
	*r =  value;
}

/* Read the value to a specified register 32bits wide */
volatile uint32_t r32(uint32_t *reg)
{
	volatile uint32_t *r = (volatile uint32_t *) reg;
	return *r;
}

/* Set the specified gpio only works with leds gpios */
void gpio_set(uint32_t gpio, uint8_t new_value)
{
	uint32_t cur_value;
	uint32_t *reg_data;

	/*  Here we make sure to not erase other value 
	 *  by reading the value of the register. */
	/*  Depending on the gpio, the bank register is differnt */
	if (new_value == 1) {
		if (gpio == GPIO_LED_0 || gpio == GPIO_LED_1) {
			reg_data = (uint32_t *) GPIO_SETDATAOUT_REGISTER_BANK5;
			cur_value = r32(reg_data);
		} else if (gpio == GPIO_LED_2 || gpio == GPIO_LED_3) {
			reg_data = (uint32_t *) GPIO_SETDATAOUT_REGISTER_BANK4;
			cur_value = r32(reg_data);
		} else {
			printf("Invalid pin\n");
			return;
		}
	} else if (new_value == 0) {
		if (gpio == GPIO_LED_0 || gpio == GPIO_LED_1) {
			reg_data = (uint32_t *) GPIO_CLEARDATAOUT_REGISTER_BANK5;
			cur_value = r32(reg_data);
		} else if (gpio == GPIO_LED_2 || gpio == GPIO_LED_3) {
			reg_data = (uint32_t *) GPIO_CLEARDATAOUT_REGISTER_BANK4;
			cur_value = r32(reg_data);
		} else {
			printf("Invalid pin\n");
			return;
		}
	} else {
		printf("Invalid value set to gpio\n");
		return;
	}

	w32(reg_data, cur_value | gpio);
}

/* Set the specified gpio only works with buttons gpio */
uint8_t gpio_get(uint32_t gpio)
{
	uint32_t *reg_data;

	/*  Depending on the gpio, the bank register is differnt */
	if (gpio == GPIO_SW_3 || gpio == GPIO_SW_4) {
		reg_data = (uint32_t *) GPIO_DATAIN_REGISTER_BANK4;
	} else if (gpio == GPIO_SW_0 || gpio == GPIO_SW_1) {
		reg_data = (uint32_t *) GPIO_DATAIN_REGISTER_BANK5;
	} else if (gpio == GPIO_SW_2) {
		reg_data = (uint32_t *) GPIO_DATAIN_REGISTER_BANK6;
	} else {
		printf("Invalid pin\n");
		return 0;
	}

	return (uint8_t) ((r32(reg_data) & gpio) > 0);
}

__attribute__((__section__(".main"))) int main(int argc, char *argv[])
{
   	app_startup();

	while (!gpio_get(GPIO_SW_4)) {

		gpio_set(GPIO_LED_0, gpio_get(GPIO_SW_0));
		gpio_set(GPIO_LED_1, gpio_get(GPIO_SW_1));
		gpio_set(GPIO_LED_2, gpio_get(GPIO_SW_2));
		gpio_set(GPIO_LED_3, gpio_get(GPIO_SW_3));
	}

	return 0;
}

