/*******************************************************************
 * reptar_sp6.c
 *
 * Copyright (c) 2019 HEIG-VD, REDS Institute
 *******************************************************************/

/* Addresses and register definitions for the DM3730 microcontroller */

/*         |     GPIO    | BANK    |    BIT (0..31)
 * --------|-------------|---------|--------------
 * SW_0    |    140      |    5    |    12
 * SW_1    |    142      |    5    |    14
 * SW_2    |    167      |    6    |    7
 * SW_3    |    97       |    4    |    1
 * SW_4    |    126      |    4    |    30
 * LED_0   |    143      |    5    |    15
 * LED_1   |    141      |    5    |    13
 * LED_2   |    101      |    4    |    5
 * LED_3   |    102      |    4    |    6
 */ 


/*DATA IN REGISTERS ADDRESSES */
#define GPIO_DATAIN_REGISTER_BANK1 0x48310038
#define GPIO_DATAIN_REGISTER_BANK2 0x49050038 
#define GPIO_DATAIN_REGISTER_BANK3 0x49052038 
#define GPIO_DATAIN_REGISTER_BANK4 0x49054038     //SW_3, SW4     
#define GPIO_DATAIN_REGISTER_BANK5 0x49056038     //SW_0, SW_1
#define GPIO_DATAIN_REGISTER_BANK6 0x49058038     //SW_2

/*DATA OUT REGISTERS ADDRESSES */
#define GPIO_DATAOUT_REGISTER_BANK1 0x4831003C    
#define GPIO_DATAOUT_REGISTER_BANK2 0x4905003C
#define GPIO_DATAOUT_REGISTER_BANK3 0x4905203C
#define GPIO_DATAOUT_REGISTER_BANK4 0x4905403C    //LED_2, LED_3
#define GPIO_DATAOUT_REGISTER_BANK5 0x4905603C    //LED_0, LED_1
#define GPIO_DATAOUT_REGISTER_BANK6 0x4905803C 

/*SET_DATAOUT REGISTER ADDRESSES */
#define GPIO_SETDATAOUT_REGISTER_BANK1 0x48310094
#define GPIO_SETDATAOUT_REGISTER_BANK2 0x49050094
#define GPIO_SETDATAOUT_REGISTER_BANK3 0x49052094
#define GPIO_SETDATAOUT_REGISTER_BANK4 0x49054094    //LED_2
#define GPIO_SETDATAOUT_REGISTER_BANK5 0x49056094    //LED_0, LED_1
#define GPIO_SETDATAOUT_REGISTER_BANK6 0x49058094

/*CLEAR_DATAOUT REGISTER ADDRESSESS */
#define GPIO_CLEARDATAOUT_REGISTER_BANK1 0x48310090
#define GPIO_CLEARDATAOUT_REGISTER_BANK2 0x49050090
#define GPIO_CLEARDATAOUT_REGISTER_BANK3 0x49052090
#define GPIO_CLEARDATAOUT_REGISTER_BANK4 0x49054090    //LED_2
#define GPIO_CLEARDATAOUT_REGISTER_BANK5 0x49056090    //LED_0, LED_1
#define GPIO_CLEARDATAOUT_REGISTER_BANK6 0x49058090

/*LED GPIO MASK    (reptar boards 1-8)*/
#define MASK_LED_0    0x8000         //GPIO 143
#define MASK_LED_1    0x2000         //GPIO 141
#define MASK_LED_2    0x20           //GPIO 101
#define MASK_LED_3    0x40           //GPIO 102

/*SWITCHES GPIO MASK (reptar boards 1-8)*/
#define MASK_SW_0     0x1000         //GPIO 140
#define MASK_SW_1     0x4000         //GPIO 142
#define MASK_SW_2     0x80           //GPIO 167
#define MASK_SW_3     0x2            //GPIO 97
#define MASK_SW_4     0x40000000     //GPIO 126

