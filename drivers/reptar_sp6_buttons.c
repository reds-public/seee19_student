/*******************************************************************
 * Copyright (c) 2019 HEIG-VD, REDS Institute
 *******************************************************************/

#include <linux/err.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/slab.h>
#include <linux/gpio.h>

#include <asm/io.h>


#include "reptar_sp6.h"

static struct platform_device *reptar_sp6_btns;

struct reptar_sp6_buttons {
	struct reptar_sp6_buttons_platdata *pdata;
	int irq;
	uint16_t *btns_reg;
	uint16_t *irq_reg;
	struct input_dev *input;
	uint16_t current_button;
};

/* Threaded/deferred interrupt handler */
irqreturn_t reptar_sp6_buttons_irq_thread(int irq, void *dev_id)
{
	struct reptar_sp6_buttons *dev = (struct reptar_sp6_buttons *) dev_id;
	struct input_dev *input = dev->input;
	int pressed;
	unsigned int key;

	do {
	  pressed = fls(dev->current_button);

	  if (!pressed) {
		printk("ERROR: buttons IRQ without button pressed\n");
		return IRQ_HANDLED;
	  }

	  key = dev->pdata->keys[pressed-1];

	  /* Report key press and release */
	  input_report_key(input, key, 1);
	  input_sync(input);

	  input_report_key(input, key, 0);
	  input_sync(input);

	  dev->current_button &= ~(1 << (pressed-1));

	} while (dev->current_button);

	return IRQ_HANDLED;
}

/* Hard/immediate interrupt handler */
irqreturn_t reptar_sp6_buttons_irq(int irq, void *dev_id)
{

  /* to be completed */

  return IRQ_WAKE_THREAD;
}

static int reptar_sp6_buttons_probe(struct platform_device *pdev)
{
	struct reptar_sp6_buttons_platdata *pdata = pdev->dev.platform_data;
	struct reptar_sp6_buttons *btns;
	struct input_dev *input;
	struct platform_device *fpga_pdev;
	struct resource *res;
	int ret;
	int gpio;
	int i;

	/* Low-level initialization */
	btns = kzalloc(sizeof(struct reptar_sp6_buttons), GFP_KERNEL);
	if (btns == NULL) {
		dev_err(&pdev->dev, "No memory for device\n");
		return -ENOMEM;
	}

	platform_set_drvdata(pdev, btns);

	btns->pdata = pdata;

	fpga_pdev = to_platform_device(pdev->dev.parent);

	if (!fpga_pdev){
		printk("FAILED to get parent device\n");
		return -1;
	}

	res = platform_get_resource(fpga_pdev, IORESOURCE_MEM, 0);

	btns->btns_reg = ioremap(res->start + pdata->btns_reg_offset, sizeof(uint16_t));
	btns->irq_reg = ioremap(res->start + pdata->irq_reg_offset, sizeof(uint16_t));

	gpio = platform_get_irq(fpga_pdev, 0);
	btns->irq = gpio_to_irq(gpio);

	if (gpio_request_one(gpio, GPIOF_IN, "sp6_irq")) {
		dev_err(&pdev->dev, "Failed to alloc SP6 IRQ GPIO\n");
		return -1;
	}

	/* Register 2 interrupt handlers (top, bottom) */
	/* ... */

	platform_set_drvdata(pdev, btns);

	/* Registration as input device */

	input = input_allocate_device();
	if (!input) {
		dev_err(&pdev->dev, "failed to allocate input device\n");
		ret = -ENOMEM;
		return ret;
	}

	btns->input = input;
	input_set_drvdata(input, btns);

	input->name = pdev->name;
	input->dev.parent = &pdev->dev;

	for (i = 0; i < 8; i++)
		input_set_capability(input, EV_KEY, pdata->keys[i]);

	ret = input_register_device(input);
	if (ret) {
		dev_err(&pdev->dev, "Unable to register input device, error: %d\n", ret);
		return ret;
	}

	return 0;
}

static int reptar_sp6_buttons_remove(struct platform_device *pdev)
{
	struct platform_device *fpga_pdev;
	struct reptar_sp6_buttons *btns;

	btns = platform_get_drvdata(pdev);
	fpga_pdev = to_platform_device(pdev->dev.parent);

	disable_irq(btns->irq);

	input_unregister_device(btns->input);
	input_free_device(btns->input);

	gpio_free(platform_get_irq(fpga_pdev, 0));
	free_irq(btns->irq, btns);
	kfree(btns);

	return 0;
}

static struct platform_driver reptar_sp6_buttons_driver = {
	.probe		= reptar_sp6_buttons_probe,
	.remove		= reptar_sp6_buttons_remove,
	.driver		= {
		.name		= "reptar_sp6_buttons",
		.owner		= THIS_MODULE,
	},
};

/* static int __init reptar_sp6_buttons_init(void) */
int reptar_sp6_buttons_init(struct platform_device *parent_fpga)
{
	reptar_sp6_btns = platform_device_alloc("reptar_sp6_buttons", -1);
	platform_device_add_data(reptar_sp6_btns, &reptar_sp6_btns_pdata, sizeof(reptar_sp6_btns_pdata));

	/* Set link to parent device */
	reptar_sp6_btns->dev.parent = &(parent_fpga->dev);

	platform_device_add(reptar_sp6_btns);

	return platform_driver_register(&reptar_sp6_buttons_driver);
}

/* static void __exit reptar_sp6_buttons_exit(void) */
void reptar_sp6_buttons_exit(void)
{
	platform_device_unregister(reptar_sp6_btns);
	platform_driver_unregister(&reptar_sp6_buttons_driver);
}

