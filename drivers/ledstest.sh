#!/bin/sh

# Simple script to test SP6 LEDS on REPTAR board

# Test entries in sysfs
for i in 0 1 2 3 4 5
do
	if [ ! -e /sys/class/leds/sp6_led$i ] ; then
		echo "sp6_led$i entry not found in sysfs"
		exit -1	
	fi
done

echo "SP6 leds present in sysfs !"

echo "Turning on LEDS from right to left..."
sleep 1

for i in 0 1 2 3 4 5
do
	echo 1 > /sys/class/leds/sp6_led$i/brightness
	sleep 1
done

echo "Turning off LEDS from right to left..."
sleep 1

for i in 0 1 2 3 4 5
do
	echo 0 > /sys/class/leds/sp6_led$i/brightness
	sleep 1
done

echo "All LEDS ON..."
for i in 0 1 2 3 4 5
do
	echo 1 > /sys/class/leds/sp6_led$i/brightness
done
sleep 2

echo "All LEDS OFF..."
for i in 0 1 2 3 4 5
do
	echo 0 > /sys/class/leds/sp6_led$i/brightness
done
sleep 2

echo "End of test"

