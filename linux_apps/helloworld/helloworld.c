/*
 * =====================================================================================
 *
 *       Filename:  helloworld.c
 *
 *    Description:  Simple hello world.
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	printf("Hello, Bienvenue dans SEEE\n");
	return 0;
}
