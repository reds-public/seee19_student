
/*
 * soo_migration.h
 *
 */

#include <soo/soo.h>

void mig_restore_domain_migration_info(unsigned int ME_slotID, struct domain *domU);
void mig_restore_vcpu_migration_info(unsigned int ME_slotID, struct domain *domU);
void after_migrate_to_user(void);

int migration_init(soo_hyp_t *op);
int migration_final(soo_hyp_t *op);

int read_migration_structures(soo_hyp_t *op);
int write_migration_structures(soo_hyp_t *op);

int restore_migrated_domain(unsigned int ME_slotID);
int restore_injected_domain(unsigned int ME_slotID);

int inject_me(soo_hyp_t *op);

bool is_me_realtime(void);
