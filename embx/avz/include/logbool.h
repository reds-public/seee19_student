

/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2018: Daniel Rossier
 *
 */

#ifndef LOGBOOL_H
#define LOGBOOL_H

void dump_all_logbool(unsigned char key);


#endif /* LOGBOOL_H */

