/******************************************************************************
 * avz/console.h
 * 
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 *
 *
 */

#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <avz/spinlock.h>
#include <public/avz.h>

void console_init(void);
void printch(char c);

#endif /* __CONSOLE_H__ */
