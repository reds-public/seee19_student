/******************************************************************************
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA/AG, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef __SCHED_IF_H__
#define __SCHED_IF_H__

#include <avz/percpu.h>

struct schedule_data {
    spinlock_t    schedule_lock;  /* spinlock protecting curr        */
    struct timer  s_timer;        /* scheduling timer                */

    unsigned int current_dom;

} __cacheline_aligned;


struct task_slice {
	struct vcpu *task;
	u64 time;
};

struct scheduler {
	char *name;             /* full name for this scheduler      */

	void (*init)  (void);
  void (*sleep) (struct vcpu *);
	void (*wake)  (struct vcpu *);

	struct task_slice (*do_schedule) (void);

	struct schedule_data sched_data;
};

extern struct scheduler sched_flip;
extern struct scheduler sched_rt;
extern struct scheduler sched_agency;

#endif /* __SCHED_IF_H__ */
