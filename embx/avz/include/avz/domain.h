
#ifndef DOMAIN_H
#define DOMAIN_H

#include <public/avz.h>


struct vcpu *alloc_vcpu(struct domain *d, unsigned int cpu_id);

struct vcpu *alloc_idle_vcpu(unsigned int cpu_id);
struct vcpu *alloc_dom0_vcpu0(void);
void vcpu_reset(struct vcpu *v);


/*
 * Arch-specifics.
 */

/* Allocate/free a domain structure. */
struct domain *alloc_domain_struct(void);
void free_domain_struct(struct domain *d);

/* Allocate/free a VCPU structure. */
struct vcpu *alloc_vcpu_struct(struct domain *d);

void free_vcpu_struct(struct vcpu *v);
void vcpu_destroy(struct vcpu *v);

int arch_domain_create(struct domain *d);

void arch_domain_destroy(struct domain *d);

int domain_relinquish_resources(struct domain *d);

void dump_pageframe_info(struct domain *d);

void arch_dump_vcpu_info(struct vcpu *v);

void arch_dump_domain_info(struct domain *d);

void arch_vcpu_reset(struct vcpu *v);



#endif /* DOMAIN_H */
