#ifndef __ASM_CPU_OPS_H__
#define __ASM_CPU_OPS_H__

#ifndef __ASSEMBLY__
#define DECLARE_CPU_OP(gop, nop)	\
	typeof (nop) gop		\
	__attribute__((weak, alias(#nop)))

void cpu_halt(int mode);
void cpu_idle(void);

/*
 * MMU Operations
 */

void cpu_switch_ttb(unsigned long);
unsigned long cpu_get_ttb(void);


/*
 * Page operations
 */

void cpu_copy_page(void *dst, void *src, unsigned long size);
void cpu_clear_page(void *dst, unsigned long size);
#endif

#ifdef __ASSEMBLY__
#define DECLARE_CPU_OP(gop, nop)	 \
	.set gop, nop			;\
	.global gop			;
#endif

#endif

