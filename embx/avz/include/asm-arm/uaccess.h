/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 *
 */

#ifndef UACCESS_H
#define UACCESS_H

#include <avz/config.h>
#include <avz/string.h>

#include <asm/errno.h>


extern unsigned long __arch_copy_from_user(void *to, const void __user *from, unsigned long n);
extern unsigned long __arch_copy_to_user(void __user *to, const void *from, unsigned long n);


static inline unsigned long __copy_from_user(void *to, const void __user *from, unsigned long n)
{
	return __arch_copy_from_user(to, from, n);
}


static inline unsigned long __copy_to_user(void __user *to, const void *from, unsigned long n)
{
	return __arch_copy_to_user(to, from, n);
}

#define __copy_to_user_inatomic __copy_to_user
#define __copy_from_user_inatomic __copy_from_user

static inline unsigned long copy_from_user(void *to, const void __user *from, unsigned long n)
{
	return __arch_copy_from_user(to, from, n);
}

static inline unsigned long copy_to_user(void __user *to, const void *from, unsigned long n)
{
	return __arch_copy_to_user(to, from, n);
}


#endif /* UACCESS_H */
