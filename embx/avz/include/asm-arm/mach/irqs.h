
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2017: Daniel Rossier
 *
 */


#ifndef CONFIG_SPARSE_IRQ
#define NR_IRQS	256
#endif
