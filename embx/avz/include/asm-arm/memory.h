/*
 *  memory.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#ifndef __ASM_ARM_MEMORY_H
#define __ASM_ARM_MEMORY_H

/*
 * Allow for constants defined here to be used from assembly code
 * by prepending the UL suffix only with actual C code compilation.
 */
#ifndef __ASSEMBLY__

#define UL(x) (x##UL)
void flush_all(void);

#else
#define UL(x) (x)
#endif


#include <avz/compiler.h>
#include <asm/arch/memory.h>
#include <asm/sizes.h>


/*
 * Page offset: 3GB
 */
/* (DRE) Crucial !! */

#define PAGE_OFFSET	UL(0xff000000)
#define L_PAGE_OFFSET	UL(0xc0000000)

/*
 * Maximum size of Linux kernel linear mapping (within the 1 GB region)
 */
#define KERNEL_LINEAR_MAX_SIZE (PAGE_OFFSET - L_PAGE_OFFSET)

/*
 * Allow 16MB-aligned ioremap pages
 */
#define IOREMAP_MAX_ORDER	24


/*
 * Convert a physical address to a Page Frame Number and back
 */
#define	__phys_to_pfn(paddr)	((paddr) >> PAGE_SHIFT)
#define	__pfn_to_phys(pfn)	((pfn) << PAGE_SHIFT)

#ifndef __ASSEMBLY__
void make_heap_noncacheable(void);
#endif

#endif
