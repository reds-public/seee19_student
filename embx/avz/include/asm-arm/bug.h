#ifndef _ASMARM_BUG_H
#define _ASMARM_BUG_H

#include <avz/lib.h>


void dump_stack(void);

/* give file/line information */
#define BUG()		__bug(__FILE__, __LINE__)
#define WARN()		__warn(__FILE__, __LINE__)


#endif
