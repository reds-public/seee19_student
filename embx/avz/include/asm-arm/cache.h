/*
 *  linux/include/asm-arm/cache.h
 */
#ifndef __ASMARM_CACHE_H
#define __ASMARM_CACHE_H


#define L1_CACHE_SHIFT		6
#define L1_CACHE_BYTES		(1 << L1_CACHE_SHIFT)

#define __read_mostly __attribute__((__section__(".data.read_mostly")))

#define ____cacheline_aligned __attribute__((__aligned__(L1_CACHE_BYTES)))
#define __cacheline_aligned	____cacheline_aligned

/*
 * The maximum alignment needed for some critical structures
 * These could be inter-node cacheline sizes/L3 cacheline
 * size etc.  Define this in asm/cache.h for your arch
 */
#ifndef INTERNODE_CACHE_SHIFT
#define INTERNODE_CACHE_SHIFT L1_CACHE_SHIFT
#endif

#if !defined(____cacheline_internodealigned_in_smp)
#if defined(CONFIG_SMP)
#define ____cacheline_internodealigned_in_smp \
        __attribute__((__aligned__(1 << (INTERNODE_CACHE_SHIFT))))
#else
#define ____cacheline_internodealigned_in_smp
#endif
#endif

#ifndef __cacheline_aligned_in_smp
#ifdef CONFIG_SMP
#define __cacheline_aligned_in_smp __cacheline_aligned
#else
#define __cacheline_aligned_in_smp
#endif /* CONFIG_SMP */
#endif

#ifndef CONFIG_SMP
#define ____cacheline_aligned_in_smp
#else
#define ____cacheline_aligned_in_smp ____cacheline_aligned
#endif


#endif
