/*
 * arch/arm/mach-omap2/include/mach/system.h
 */

#include <asm/proc-fns.h>
#include <asm/arch/hardware.h>

static inline void arch_idle(void)
{
	cpu_do_idle();
}
