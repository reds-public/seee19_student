/* reptar.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#ifndef __MACH_REPTAR_H__
#define __MACH_REPTAR_H__



#define REPTAR_UART_VIRT	0xfb020000
#define REPTAR_UART_PHYS	0x49020000



#endif /* __MACH_REPTAR_H__ */
