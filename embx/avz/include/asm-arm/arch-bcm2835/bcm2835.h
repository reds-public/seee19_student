/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#ifndef BCM2835_H
#define BCM2835_H

#define BCM2836_INTC_PHYS	0x40000000
#define BCM2836_INTC_SIZE	0x200

#define BCM2835_UART0_VIRT	0xf8c15040
#define BCM2835_UART0_PHYS	0x3f215040

/*
 * From BCM2835 ARM peripherals documentation.
 *
 * Mini UART register offsets.
 */

#define MU_IO_REG 		0x00
#define MU_IER_REG 		0x04
#define MU_IIR_REG 		0x08
#define MU_LCR_REG 		0x0C
#define MU_MCR_REG 		0x10
#define MU_LSR_REG 		0x14
#define MU_MSR_REG 		0x18
#define MU_SCRATCH 		0x1C
#define MU_CNTL_REG 		0x20
#define MU_STAT_REG 		0x24
#define MU_BAUD_REG 		0x28

#define MU_IO_DATA 		(0xFF << 0)
#define MU_STAT_SP_AVAIL	 (1 << 0)
#define MU_STAT_RX_FIFO_FILL 	(0xF << 16)
#define MU_LSR_TX_EMPTY 	(1 << 5)
#define MU_LSR_DATA_READY 	(1 << 0)
/* TODO : all other bit offsets */


/*
 * The low 4 bits of this are the CPU's per-mailbox IRQ enables, and
 * the next 4 bits are the CPU's per-mailbox FIQ enables (which
 * override the IRQ bits).
 */
#define LOCAL_MAILBOX_INT_CONTROL0	0x050

/*
 * Mailbox write-to-set bits.  There are 16 mailboxes, 4 per CPU, and
 * these bits are organized by mailbox number and then CPU number.  We
 * use mailbox 0 for IPIs.  The mailbox's interrupt is raised while
 * any bit is set.
 */
#define LOCAL_MAILBOX0_SET0		0x080
#define LOCAL_MAILBOX3_SET0		0x08c
/* Mailbox write-to-clear bits. */
#define LOCAL_MAILBOX0_CLR0		0x0c0
#define LOCAL_MAILBOX3_CLR0		0x0cc

#ifndef __ASSEMBLY__

extern struct smp_operations    bcm2836_smp_ops;

struct bcm2836_arm_irqchip_intc {
	void *base;
};
extern struct bcm2836_arm_irqchip_intc intc;

int bcm2836_arm_irqchip_l1_intc_init(void *base);
void bcm2836_arm_irqchip_unmask_per_cpu_irq(unsigned int reg_offset, unsigned int bit, int cpu);
void bcm2836_arm_irqchip_send_ipi(const struct cpumask *mask, unsigned int ipi);

#endif /* __ASSEMBLY__ */


#endif /* BCM2835_H */
