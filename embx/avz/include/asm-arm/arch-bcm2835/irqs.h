#define IRQ_LOCALTIMER		29
#define IRQ_LOCALWDOG		30

#define IRQ_ARCH_ARM_TIMER	3

#ifndef CONFIG_SPARSE_IRQ
#define NR_IRQS	256

#define NR_PIRQS NR_IRQS

#define IRQ_TIMER  			34
#define IRQ_TIMER_RT			35

#define IRQ_MERIDA_AUDIO	46

#endif
