#ifndef INIT_H
#define INIT_H

/* For assembly routines */
#define __HEAD          .section        ".head.text","ax"
#define __INIT          .section        ".init.text","ax"
#define __FINIT         .previous

#define __INITDATA      .section        ".init.data","aw",%progbits
#define __INITRODATA    .section        ".init.rodata","a",%progbits
#define __FINITDATA     .previous

#define __DEVINIT        .section       ".devinit.text", "ax"
#define __DEVINITDATA    .section       ".devinit.data", "aw"
#define __DEVINITRODATA  .section       ".devinit.rodata", "a"

#define __CPUINIT        .section       ".cpuinit.text", "ax"
#define __CPUINITDATA    .section       ".cpuinit.data", "aw"
#define __CPUINITRODATA  .section       ".cpuinit.rodata", "a"

#define __MEMINIT        .section       ".meminit.text", "ax"
#define __MEMINITDATA    .section       ".meminit.data", "aw"
#define __MEMINITRODATA  .section       ".meminit.rodata", "a"

#endif /* INIT_H */
