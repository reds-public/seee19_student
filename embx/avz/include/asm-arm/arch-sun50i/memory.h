/* include/asm-arm/arch-goldfish/memory.h
**
** Copyright (C) 2007 Google, Inc.
**
** This software is licensed under the terms of the GNU General Public
** License version 2, as published by the Free Software Foundation, and
** may be copied, distributed, and modified under those terms.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
*/

#ifndef __ASM_ARCH_MEMORY_H
#define __ASM_ARCH_MEMORY_H

/*
 * Physical DRAM offset.
 */

#define CONFIG_DRAM_BASE	(0x40000000)
#define CONFIG_DRAM_SIZE	(0x3ff00000) /* 1 GB (-1 MB) */
 
/*
 * Maximum size of Linux kernel linear mapping (within the 1 GB region)
 */
#define KERNEL_LINEAR_MAX_SIZE (PAGE_OFFSET - L_PAGE_OFFSET)

/*
 * Physical DRAM offset.
 */

#define PHYS_OFFSET CONFIG_DRAM_BASE


#endif
