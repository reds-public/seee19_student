/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#ifndef A64_H
#define A64_H

#define SUNXI_GIC_DIST_PHYS 	0x01c81000
#define SUNXI_GIC_DIST_SIZE 	0x1000

#define SUNXI_GIC_CPU_PHYS 	0x01c82000
#define SUNXI_GIC_CPU_SIZE 	0x1000

#define SUNXI_UART0_VIRT	0xf8c28000
#define SUNXI_UART0_PHYS	0x01c28000

#define SUNXI_TIMER0		0x01c20c00
#define SUNXI_TIMER_HS0		0x01c60000

#define IRQ_SUNXI_TIMER0	(32 + 18)
#define IRQ_SUNXI_TIMER1	(32 + 19)
#define IRQ_SUNXI_HS_TIMER0	(32 + 81)

#define SUNXI_CPUCFG_PHYS	0x01c25c00
#define SUNXI_CPUCFG_SIZE	0x400

#define SUNXI_PMU_PHYS		0x01c25400
#define SUNXI_PMU_SIZE		0x400

extern struct smp_operations    sun50i_smp_ops;



#endif /* A64_H */
