/******************************************************************************
 * config.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#ifndef __ARM_CONFIG_H__
#define __ARM_CONFIG_H__

#include <asm/arch/memory.h>
#include <linux/autoconf.h>

#define AGENCY_CPU		0
#define AGENCY_RT_CPU     	1

#ifdef CONFIG_MACH_REPTAR
#define	NR_CPUS			1
#else
#define	NR_CPUS			4
#endif

#define ME_STANDARD_CPU 	2
#define ME_RT_CPU		3


/* (DRE) Define the top of usable memory by the hypervisor and guest... */
#define	TOP_PHYS_MEMORY		(CONFIG_DRAM_BASE + CONFIG_DRAM_SIZE - 1)

/*
 * Allow for constants defined here to be used from assembly code
 * by prepending the UL suffix only with actual C code compilation.
 */
#ifndef __ASSEMBLY__
#define UL(x) (x##UL)
#else
#define UL(x) (x)
#endif

/* align addr on a size boundary - adjust address up/down if needed */
#define ALIGN_UP(addr,size) (((addr)+((size)-1))&(~((size)-1)))
#define ALIGN_DOWN(addr,size) ((addr)&(~((size)-1)))

/* We keep the STACK_SIZE to 8192 in order to have a similar stack_size as guest OS in SVC mode */
#define STACK_ORDER 1
#define STACK_SIZE  (PAGE_SIZE << STACK_ORDER)

#define HEAP_MAX_SIZE_MB 	(2)

/* Hypervisor owns top 64MB of virtual address space. */
#define HYPERVISOR_VIRT_START   0xFF000000

#define HYPERVISOR_SIZE	0x00c00000  /* 12 MB */
#define HYPERVISOR_PHYS_START CONFIG_DRAM_BASE

#endif /* __ARM_CONFIG_H__ */
