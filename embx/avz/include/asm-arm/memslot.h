/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA/AG, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef __ASM_MEMMAP_HEADER
#define __ASM_MEMMAP_HEADER

#include <asm/page.h>
#include <asm/arch/memory.h>
#include <avz/libelf.h>

#include <virtshare/soo.h>
#include <soo/soo.h>


/* Number of possible MEs in the local SOO */
#define MEMSLOT_BASE	  2
#define MEMSLOT_NR	  (MEMSLOT_BASE + MAX_ME_DOMAINS)

/*
 * Memslot management
 *
 * Describes how domains are mapped in physical memory
 */
typedef struct {
	unsigned long start;  /* Kernel physical start address */
	unsigned int size;
	unsigned int busy; /* Indicate if a memslot is available or not */

	struct elf_binary elf;

	unsigned int atags_pointer; /* physical address */

} memslot_entry_t;

extern memslot_entry_t memslot[];

/**
 * We put all the guest domains in ELF format on top of memory so
 * that the domain_build will be able to elf-parse and load to their final destination.
 */
void loadAgency(void);
void loadME(unsigned int slotID, uint8_t *img, uint8_t *dest, dtb_feat_t *dtb_feat);


#endif /* !__ASM_MEMMAP_HEADER */
