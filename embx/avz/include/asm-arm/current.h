#ifndef _ASMARM_CURRENT_H
#define _ASMARM_CURRENT_H

#include <virtshare/avz.h>
#include <avz/percpu.h>
#include <asm/page.h>

struct vcpu;
extern struct domain *dom0;

DECLARE_PER_CPU(struct vcpu *, curr_vcpu);

struct cpu_info {
	struct vcpu *cur_vcpu;
	ulong saved_regs[2];
};

static inline struct cpu_info *current_cpu_info(void)
{
	register unsigned long sp asm("r13");
	return (struct cpu_info *) (sp & ~(STACK_SIZE - 1));
}

static inline struct vcpu *get_current(void)
{
  return current_cpu_info()->cur_vcpu;
}
/*
 * Sometimes, with the realtime agency support, we need to refer to the current domain as the primary (dom0) domain and
 * not the subdomain itself.
 */

#define current_primary ((get_current()->domain->domain_id == DOMID_AGENCY_RT) ? dom0->vcpu[0] : get_current())

#define current get_current()

static inline void set_current(struct vcpu *v)
{
    current_cpu_info()->cur_vcpu = v;
}

#define guest_cpu_user_regs()	(&current->arch.guest_context.user_regs)


/* XXX *#%(ing circular header dependencies force this to be a macro */
/* If the vcpu is running, its state is still on the stack, and the vcpu
 * structure's copy is obsolete. If the vcpu isn't running, the vcpu structure
 * holds the only copy. This routine always does the right thing. */
#define vcpu_regs(v) ({                 \
    struct cpu_user_regs *regs;         \
    if (v == current)                   \
        regs = guest_cpu_user_regs();   \
    else                                \
        regs = &v->arch.ctxt;           \
    regs;                               \
})

#endif /* _ASMARM_CURRENT_H */
