/*
 * This was automagically generated from arch/arm/tools/mach-types!
 * Do NOT edit
 */

#ifndef __ASM_ARM_MACH_TYPE_H
#define __ASM_ARM_MACH_TYPE_H

#include <linux/config.h>

#ifndef __ASSEMBLY__
/* The type of machine we're running on */
extern unsigned int __machine_arch_type;
#endif

/* see arch/arm/kernel/arch.c for a description of these */
#define MACH_TYPE_VEXPRESS             2272
#define MACH_TYPE_SUN50I               9878
#define MACH_TYPE_REPTAR               9876

#ifdef CONFIG_MACH_VEXPRESS
# ifdef machine_arch_type
#  undef machine_arch_type
#  define machine_arch_type	__machine_arch_type
# else
#  define machine_arch_type	MACH_TYPE_VEXPRESS
# endif
# define machine_is_vexpress()	(machine_arch_type == MACH_TYPE_VEXPRESS)
#else
# define machine_is_vexpress()	(0)
#endif

#ifdef CONFIG_MACH_SUN50I
# ifdef machine_arch_type
#  undef machine_arch_type
#  define machine_arch_type	__machine_arch_type
# else
#  define machine_arch_type	MACH_TYPE_SUN50I
# endif
# define machine_is_sun50i()	(machine_arch_type == MACH_TYPE_SUN50I)
#else
# define machine_is_sun50i()	(0)
#endif

#ifdef CONFIG_MACH_REPTAR
# ifdef machine_arch_type
#  undef machine_arch_type
#  define machine_arch_type	__machine_arch_type
# else
#  define machine_arch_type	MACH_TYPE_REPTAR
# endif
# define machine_is_reptar()	(machine_arch_type == MACH_TYPE_REPTAR)
#else
# define machine_is_reptar()	(0)
#endif

/*
 * These have not yet been registered
 */

#ifndef machine_arch_type
#define machine_arch_type	__machine_arch_type
#endif

#endif
