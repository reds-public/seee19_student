
/*
 *
 * SOO callback prototypes
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015 SOOtech Ltd, Switzerland
 *
 * Authors: Daniel Rossier, Baptiste Delporte
 * Emails: {daniel.rossier, baptiste.delporte}@sootech.ch
 *
 */

#ifndef AVZ_SOO_H
#define AVZ_SOO_H

#include <avz/types.h>

/* Device tree features */
#define ME_FEAT_ROOT		"/me_features"

#define	ME_FEAT_REALTIME	"realtime" /* 0 means non-realtime / 1 means realtime */

typedef struct {
	bool realtime;
} dtb_feat_t;

int soo_pre_activate(unsigned int slotID);
int soo_cooperate(unsigned int slotID);
void shutdown_ME(unsigned int ME_slotID);

ME_state_t get_ME_state(unsigned int ME_slotID);
void set_ME_state(unsigned int ME_slotID, ME_state_t state);

void set_dom_realtime(struct domain *d, bool realtime);
bool is_dom_realtime(struct domain *d);

#endif /* SOO_H */
