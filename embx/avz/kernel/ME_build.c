/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 *
 */

#include <avz/config.h>
#include <avz/lib.h>
#include <avz/percpu.h>
#include <avz/sched.h>
#include <avz/init.h>
#include <avz/ctype.h>
#include <avz/console.h>

#include <avz/domain.h>
#include <avz/errno.h>
#include <asm/processor.h>
#include <asm/cputype.h>
#include <asm/pgtable.h>
#include <asm/mach/map.h>
#include <asm/memslot.h>
#include <public/arch-arm.h>
#include <avz/mm.h>

#include <asm/mm.h>
#include <avz/libelf.h>

#include <asm/cacheflush.h>

#include <virtshare/debug/logbool.h>

#define round_pgup(_p)    (((_p)+(PAGE_SIZE-1))&PAGE_MASK)

#define L_TEXT_OFFSET	0x8000

#define bucket_from_port(d,p) \
    ((d)->evtchn[(p)/EVTCHNS_PER_BUCKET])
#define port_is_valid(d,p)    \
    (((p) >= 0) && ((p) < MAX_EVTCHNS) && \
     (bucket_from_port(d,p) != NULL))

extern unsigned int __atags_pointer __initdata; /* defined in kernel/setup.c */
extern unsigned int __boot_params __initdata;

/* adapted from common/elf.c */
#define RM_MASK(a,l) ((a) & ((1UL << (l)) - 1))

extern unsigned long total_pages;

start_info_t *domU_start_info;

extern struct page_info *alloc_chunk(struct domain *d, unsigned long max_pages);

/*
 * setup_page_table_guestOS() is setting up the 1st-level page table within the domain.
 */
extern int setup_page_table_guestOS(struct vcpu *v, unsigned long v_start, unsigned long map_size, unsigned long p_start, unsigned long vpt_start);

extern char hypercall_start[];

extern start_info_t *dom0_start_info;

extern void pre_ret_to_user(void);

extern long __evtchn_close(struct domain *d1, int port1);

void prep_to_reboot_domU(struct domain *d) {
  unsigned long v_start;
  unsigned long alloc_spfn;
  unsigned long vpt_start;
  unsigned long vstack_end;
  unsigned long *domain_stack;
  struct start_info *si = NULL;

  struct cpu_user_regs *domain_context;
  struct cpu_user_regs *regs = &d->vcpu[0]->arch.guest_context.user_regs;
  int i;

  /* Unbouncing VIRQs */

  for (i = 0; i < NR_EVTCHN; i++)
    (void)__evtchn_close(d, i);

  /* Switch to the hypervisor page table */
  save_ptbase(d->vcpu[0]);
  write_ptbase(idle_domain[smp_processor_id()]->vcpu[0]);

  /* Reinit guest page tables */
  si = (start_info_t *) d->arch.vstartinfo_start;

  vpt_start = si->pt_base;
  alloc_spfn = si->min_mfn;
  v_start = vpt_start - L_TEXT_OFFSET + 0x4000;
  vstack_end = d->arch.vstack_end;
  setup_page_table_guestOS(d->vcpu[0], v_start, vstack_end-v_start, (alloc_spfn << PAGE_SHIFT), vpt_start);

  /* Parsing and loading the ELF image of the guest */
  /* Copy the OS image and free temporary buffer. */
  printk("Re-loading domU ELF image located at: %p, dest: %p... ", memslot[d->domain_id+1].elf.image_base, memslot[d->domain_id+1].elf.dest_base);
  elf_load_binary(&(memslot[d->domain_id+1].elf));
  printk("done.\n");

  /* Re-switch to ME page table */
  save_ptbase(idle_domain[smp_processor_id()]->vcpu[0]);
  write_ptbase(d->vcpu[0]);

  /* Put the frame which will be restored later */
  domain_stack = (unsigned long *) d->arch.domain_stack;

  domain_stack += (STACK_SIZE - sizeof(struct cpu_user_regs))/sizeof(unsigned long);
  domain_context = (struct cpu_user_regs *)domain_stack;

  domain_context->r2 = si->atags_pointer;
  domain_context->r12 = d->arch.vstartinfo_start;
  domain_context->r13 = d->arch.vstack_end;
  domain_context->r15 = d->arch.entry_main;

  domain_context->psr = 0x93;  /* IRQs disabled initially */

  regs->r13 = (unsigned long) domain_stack;
  regs->r14 = (unsigned long) pre_ret_to_user;

  d->vcpu[0]->arch.guest_context.sys_regs.guest_ttbr0 = pagetable_get_paddr(d->vcpu[0]->arch.guest_table);

#ifdef CONFIG_CPU_V7
  d->vcpu[0]->arch.guest_context.sys_regs.guest_ttbr1 = pagetable_get_paddr(d->vcpu[0]->arch.guest_table);
  d->vcpu[0]->arch.guest_context.sys_regs.guest_context_id = 0;
#endif

}

/*
 * construct_ME sets up a new Mobile Entity.
 */
int construct_ME(struct domain *d) {
	int rc;
	unsigned int slotID;
	struct vcpu *v;
	unsigned long vstartinfo_start;
	unsigned long v_start;
	unsigned long align_offset = 0;
	unsigned long alloc_spfn;
	unsigned long vpt_start;
	unsigned long vstack_start, vstack_end;
	struct start_info *si = NULL;

	unsigned int target_vaddr;

	unsigned long nr_pages;
	unsigned long entry_main;

	/*
	 * Switch back to the normal page table which covers the user space along.
	 * This is required to access the user space elf binary image.
	 */

	slotID = d->domain_id + 1;

	switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

	printk("***************************** Loading Mobile Entity (ME) *****************************\n");

	if (memslot[slotID].size == 0)
		panic("No domU image supplied\n");

	printk("Parsing ME (%luB) at 0x%lx\n", memslot[slotID].elf.size, (unsigned long) memslot[slotID].elf.image_base);

	/* Preserve the destination virtual address */
	target_vaddr = (unsigned int) memslot[slotID].elf.dest_base;

	elf_init(&(memslot[slotID].elf), memslot[slotID].elf.image_base, memslot[slotID].elf.size);

	elf_parse_binary(&(memslot[slotID].elf));

	entry_main = (unsigned long) elf_uval(&(memslot[slotID].elf), (memslot[slotID].elf).ehdr, e_entry);

	printk("### ELF image information for ME domain #%d\n", slotID-1);

	printk("\timage =       \t0x%p\n", memslot[slotID].elf.image_base);
	printk("\tsize =        \t%lu\n", memslot[slotID].elf.size);

	printk("\tpstart =      \t0x%lx\n", (unsigned long) memslot[slotID].elf.pstart);
	printk("\tpend =        \t0x%lx\n", (unsigned long) memslot[slotID].elf.pend);
	printk("\treloc_offset =\t0x%lx\n", (unsigned long) memslot[slotID].elf.reloc_offset);
	printk("\tentry_main =  \t0x%lx\n", entry_main);

	switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

	/* The following page will contain start_info information */
	vstartinfo_start = (unsigned long) alloc_heap_page();

	d->max_pages = ~0U;
	d->tot_pages = 0;

	nr_pages = memslot[slotID].size >> PAGE_SHIFT;
	printk("Max dom size %d\n", memslot[slotID].size);

	printk("Domain length = %lu pages.\n", nr_pages);

	ASSERT(d);

	v = d->vcpu[0];
	BUG_ON(d->vcpu[0] == NULL);

	ASSERT(v);

	d->tot_pages = memslot[slotID].size >> PAGE_SHIFT;
	alloc_spfn = memslot[slotID].start >> PAGE_SHIFT;

	clear_bit(_VPF_down, &v->pause_flags);

	 /* Align load address to 1MB boundary. */
	v_start = memslot[slotID].elf.pstart & ~((1UL << __L2_PAGETABLE_SHIFT) - 1);
	printk("Domain virtual start address: %lx\n", v_start);

	align_offset = memslot[slotID].elf.pstart - v_start;   /* Align offset if any */
	memslot[slotID].elf.dest_base = (char *) (target_vaddr + align_offset);

	vpt_start = v_start + L_TEXT_OFFSET - 0x4000;  /* according to Linux guest OS in head.S */

	/* vstack is used when the guest has not initialized its own stack yet; put right after _end of the guest OS*/
	vstack_start = PGD_ALIGN(memslot[slotID].elf.pend);
	vstack_end = vstack_start + PAGE_SIZE;

	setup_page_table_guestOS(v, v_start, vstack_end-v_start, (alloc_spfn << PAGE_SHIFT), vpt_start);

	/** Loading the domain... **/
	/* Destination is based upon running page table, i.e. hypervisor page table */

	switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

	printk("Domain image destination: %p\n", memslot[slotID].elf.dest_base);

	/* Parsing and loading the ELF image of the guest */
	/* Copy the OS image and free temporary buffer. */
	printk("Loading domain ELF image located at: %p, dest: %p... ", memslot[slotID].elf.image_base, memslot[slotID].elf.dest_base);
	elf_load_binary(&(memslot[slotID].elf));
	printk("done.\n");

	switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

	/* Lets switch to the page table of our new domain - required for sharing page info & p2m table setup...*/

	save_ptbase(idle_domain[smp_processor_id()]->vcpu[0]);

	write_ptbase(v);

	si = (start_info_t *) vstartinfo_start;
	domU_start_info = si;

	memset(si, 0, PAGE_SIZE);

	si->domID = d->domain_id;

	si->nr_pages = d->tot_pages;
	si->min_mfn = alloc_spfn;

	si->shared_info = d->shared_info;
	si->hypercall_addr = (unsigned long) hypercall_start;

        si->logbool_ht_set_addr = (unsigned long) ht_set;

	/*
	 * si->atags_pointer must be physical address
	 * cur_domain->atags_pointer is a virtual address
	 */

	si->atags_pointer = memslot[slotID].atags_pointer;

	printk("ME ATAGS pointer: 0x%lx (phys)\n", si->atags_pointer);

	/* HW details on the CPU: processor ID, cache ID and ARM architecture version */

	si->printch = printch;

	si->pt_base = vpt_start;

	write_ptbase(current);

	d->arch.vstartinfo_start = vstartinfo_start;
	d->arch.vstack_end = vstack_end;
	d->arch.entry_main = entry_main;

	new_thread(v, (unsigned long) entry_main, si->atags_pointer, vstack_end, vstartinfo_start);

	rc = 0;

	BUG_ON(rc != 0);
	return 0;
}

