/******************************************************************************
 * Arch-specific domctl.c
 * 
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#if 0
#define DEBUG
#endif

#include <avz/config.h>
#include <avz/types.h>
#include <avz/lib.h>
#include <avz/mm.h>

#include <avz/sched.h>
#include <avz/domain.h>
#include <avz/event.h>
#include <avz/errno.h>

#include <asm/uaccess.h>
#include <asm/irq.h>
#include <asm/processor.h>
#include <asm/hypercall.h> /* for arch_do_domctl */

#include <public/domctl.h>

#include <virtshare/debug.h>

static DEFINE_SPINLOCK(domctl_lock);

extern long arch_do_domctl(struct domctl *op, domctl_t *args);

bool_t domctl_lock_acquire(void)
{

	/*
	 * Trylock here is paranoia if we have multiple privileged domains. Then
	 * we could have one domain trying to pause another which is spinning
	 * on domctl_lock -- results in deadlock.
	 */
	if (spin_trylock(&domctl_lock))
		return 1;

	return 0;
}

void domctl_lock_release(void)
{
	spin_unlock(&domctl_lock);
}

extern void __domain_restart(int stackptr);

long do_domctl(domctl_t *args)
{
	long ret = 0;
	struct domctl curop, *op = &curop;

	if (copy_from_user(op, args, sizeof(domctl_t)))
			return -EFAULT;

	if (!domctl_lock_acquire())
		return 0;

	switch ( op->cmd )
	{

	case DOMCTL_pauseME:
	{
		struct domain *d = domains[op->domain];

		ret = -ESRCH;
		if (d != NULL)
		{

			ret = -EINVAL;
			if (d != current->domain)
			{
				domain_pause_by_systemcontroller(d);
				ret = 0;
			}
		}
	}
	break;


	default:
		ret = arch_do_domctl(op, args);
		break;
	}

	domctl_lock_release();

	return ret;
}

long arch_do_domctl(struct domctl *op,  domctl_t *args)
{
    long ret = 0;
    struct start_info *si;

    switch (op->cmd)
    {
   
      case DOMCTL_unpauseME:
      {
        struct domain *d = domains[op->domain];
        ret = -ESRCH;

        if (d == NULL)
        	break;

        si = (struct start_info *) d->arch.vstartinfo_start;
     //   printk("## SOFTIRQ unpause : %x\n", *((unsigned int *) 0xff03c2e4));
        /* Retrieve info from hypercall parameter structure */
        si->store_mfn = op->u.unpause_ME.store_mfn;

        DBG("%s: unpausing ME\n", __FUNCTION__);

        domain_unpause_by_systemcontroller(d);

        ret = 0;
      }
      break;

    default:
        ret = -ENOSYS;
        break;
    }

    return ret;
}

