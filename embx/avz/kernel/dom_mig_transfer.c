/******************************************************************************
 * dom_mig_transfer.c
 *
 * Implementation of Live Domain Migration
 *    This file contains the transfer part of the migration
 * 
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 * - Jun. 2016: Baptiste Delporte
 * - January 2018: Baptiste Delporte
 *
 *
 */

#if 0
#define DEBUG
#endif

#include <asm/uaccess.h>
#include <asm/irq.h>
#include <asm/page.h>
#include <asm/memslot.h>
#include <asm/mach/map.h>

#include <avz/lib.h>
#include <avz/smp.h>

#include <avz/types.h>
#include <avz/console.h>
#include <avz/migration.h>
#include <avz/domain.h>
#include <avz/xmalloc.h>

#include <virtshare/avz.h>
#include <virtshare/debug.h>
#include <virtshare/soo.h>
#include <virtshare/debug/logbool.h>

#include <soo/soo.h>

#include <soo_migration.h>
/*
 * Structures to store domain and vcpu infos. Must be here and not locally in function,
 * since the maximum stack size is 8 KB
 */
static struct domain_migration_info dom_info = {0};
static struct vcpu_migration_info vcpu_info = {0};

/* PFN offset of the target platform */
long pfn_offset = 0;

/*******************************************************************************
    MIGRATION INTERNALS
 *******************************************************************************/

/* Start of ME RAM in virtual address space of idle domain (extern) */
unsigned long vaddr_start_ME = 0;

/*------------------------------------------------------------------------------
switch_domain_address_space
------------------------------------------------------------------------------*/
void switch_domain_address_space(struct domain *from, struct domain *to)
{
	save_ptbase(from->vcpu[0]);
	write_ptbase(to->vcpu[0]);
}

/**
 * Initiate the migration process of a ME.
 */
int migration_init(soo_hyp_t *op) {
	unsigned int slotID = *((unsigned int *) op->p_val1);
	soo_personality_t pers = *((soo_personality_t *) op->p_val2);
	struct domain *domME = domains[slotID - 1];

	DBG("slotID=%d, pers=%d\n", slotID, pers);

	switch (pers) {
	case SOO_PERSONALITY_INITIATOR:

		/* Initiator's side: the ME must be suspended during the migration */
		domain_pause_by_systemcontroller(domME);

		DBG0("ME paused OK\n");

		break;

	case SOO_PERSONALITY_SELFREFERENT:

		DBG("Self-referent\n");

		/* Create a domain context including the ME descriptor before the ME gets injected. */
		switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

		domME = domain_create(slotID-1, false, true);
		if (!domME)
			panic("Error creating the ME");

		domains[slotID-1] = domME;

		/* Initialize the ME descriptor */
		set_ME_state(slotID, ME_state_booting);

		/* Set the size of this ME in its own descriptor */
		domME->shared_info->dom_desc.u.ME.size = memslot[slotID].size;

		/* Now set the pfn base of this ME; this will be useful for the Agency Core subsystem */
		domME->shared_info->dom_desc.u.ME.pfn = phys_to_pfn(memslot[slotID].start);

		/* Switch back to dom0 address space */
		switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

		break;

	case SOO_PERSONALITY_TARGET:
		/* Target's side: nothing to do in particular */
		DBG("Target\n");

		/* Create the basic domain context including the ME descriptor (in its shared info page) */

		/* Switch to idle domain address space which has a full mapping of the RAM */
		switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

		/* Create new ME domain */
		domME = domain_create(slotID-1, is_me_realtime(), false);

		domains[slotID-1] = domME;

		if (domME == NULL) {
			printk("Error creating the ME\n");

			/* Switch back address space */
			switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);
			panic("Failure during domain creation ...\n");
		}

		/* Pre-init the basic information related to the ME */
		domME->shared_info->dom_desc.u.ME.size = memslot[slotID].size;
		domME->shared_info->dom_desc.u.ME.pfn = phys_to_pfn(memslot[slotID].start);

		/* Switch back to dom0 address space */
		switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

		break;
	}

	/* Used for future restore operation */
	vaddr_start_ME  = (unsigned long) __lva(memslot[slotID].start);

	if (pers == SOO_PERSONALITY_INITIATOR)
		DBG("Initiator: Preparing to copy in ME_slotID %d: ME @ paddr 0x%08x (mapped @ vaddr 0x%08x in hypervisor)\n", slotID, (unsigned int) memslot[slotID].start, (unsigned int) vaddr_start_ME);

	return 0;
}

/*------------------------------------------------------------------------------
build_domain_migration_info
build_vcpu_migration_info
    Build the structures holding the key info to be migrated over
------------------------------------------------------------------------------*/
static void build_domain_migration_info(unsigned int ME_slotID, struct domain *me, struct domain_migration_info *mig_info)
{
	/* Domain ID */
	mig_info->domain_id = me->domain_id;

	/* Event channel info */

	memcpy(mig_info->evtchn, me->evtchn, sizeof(me->evtchn));

	mig_info->nr_pirqs = me->nr_pirqs;

	/* Shared info */

	memcpy(mig_info->evtchn_pending, me->shared_info->evtchn_pending, sizeof(me->shared_info->evtchn_pending));
	memcpy(mig_info->evtchn_mask, me->shared_info->evtchn_mask, sizeof(me->shared_info->evtchn_mask));

	/* Keep the a local clocksource reference */
	mig_info->clocksource_ref = me->shared_info->clocksource_ref;

	/* Get the start_info structure */
	memcpy(mig_info->start_info_page, (void *) me->arch.vstartinfo_start, PAGE_SIZE);

	/* ME_desc */
	memcpy(&mig_info->dom_desc, &me->shared_info->dom_desc, sizeof(dom_desc_t));

	/* Update the state for the ME instance which will migrate. The resident ME keeps its current state. */
	mig_info->dom_desc.u.ME.state = ME_state_migrating;

	/* Domain start pfn */
	mig_info->start_pfn = phys_to_pfn(memslot[ME_slotID].start);

	mig_info->pause_count = me->pause_count;

	dmb();

}

static void build_vcpu_migration_info(unsigned int ME_slotID, struct domain *me, struct vcpu_migration_info *mig_info)
{
	mig_info->processor = me->vcpu[0]->processor;
	mig_info->need_periodic_timer = me->vcpu[0]->need_periodic_timer;

	/* Pause */
	mig_info->pause_flags     = me->vcpu[0]->pause_flags;

	memcpy(&(mig_info->pause_count), &(me->vcpu[0]->pause_count), sizeof(me->vcpu[0]->pause_count));
	/* VIRQ mapping */
	memcpy(mig_info->virq_to_evtchn, me->vcpu[0]->virq_to_evtchn, sizeof((me->vcpu[0]->virq_to_evtchn)));

	/* Arch - including usr regs, vfp, etc. */
	memcpy(&(mig_info->arch), &(me->vcpu[0]->arch), sizeof(me->vcpu[0]->arch));

	/* Internal fields of vcpu_info_t structure */
	mig_info->evtchn_upcall_pending = me->vcpu[0]->vcpu_info->evtchn_upcall_pending;

	memcpy(&(mig_info->arch_info), &(me->vcpu[0]->vcpu_info->arch), sizeof(me->vcpu[0]->vcpu_info->arch));
	memcpy(&(mig_info->time), &(me->vcpu[0]->vcpu_info->time), sizeof(me->vcpu[0]->vcpu_info->time));

	dmb();
}

/**
 * Read the migration info structures.
 */
int read_migration_structures(soo_hyp_t *op) {
	unsigned int ME_slotID = *((unsigned int *) op->p_val1);
	struct domain *domME = domains[ME_slotID-1];
	unsigned long paddr;

	/* Gather all the info we need into structures */
	build_domain_migration_info(ME_slotID, domME, &dom_info);
	build_vcpu_migration_info(ME_slotID, domME, &vcpu_info);

	/* Switch to idle domain address space */
	switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

	/* Copy structures to buffer */
	paddr = op->paddr;

	memcpy((void *) __lva(paddr), &dom_info, sizeof(dom_info));
	paddr += sizeof(dom_info);
	memcpy((void *) __lva(paddr), &vcpu_info, sizeof(vcpu_info));
	paddr += sizeof(vcpu_info);

	dmb();

	/* Switch back to dom0 address space before updating size pointer! */
	switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

	/* Update op->size with valid data size */
	*((unsigned int *) op->p_val2) = sizeof(dom_info) + sizeof(vcpu_info);

	return 0;
}

/*
 * Get the info to know if the ME is realtime or not (used after migrating).
 */
bool is_me_realtime(void)
{
	return dom_info.dom_desc.realtime;
}


/*------------------------------------------------------------------------------
restore_domain_migration_info
restore_vcpu_migration_info
    Restore the migration info in the new ME structure
    Those function are actually exported and called in domain_migrate_restore.c
    They were kept in this file because they are the symmetric functions of
    build_domain_migration_info() and build_vcpu_migration_info()
------------------------------------------------------------------------------*/
extern char hypercall_start[];

static void restore_domain_migration_info(unsigned int ME_slotID, struct domain *me, struct domain_migration_info *mig_info)
{
	unsigned long vstartinfo_start;
	struct start_info *si;
	int i;

	DBG("%s\n", __FUNCTION__);

	memcpy(me->evtchn, mig_info->evtchn, sizeof(me->evtchn));

	/*
	 * We reconfigure the inter-domain event channel so that we unbind the link to the previous
	 * remote domain (the agency in most cases), but we keep the state as it is since we do not
	 * want that the local event channel gets changed.
	 *
	 * Re-binding is performed during the resuming via vbus (backend side) OR
	 * if the ME gets killed, the event channel will be closed without any effect to a remote domain.
	 */

	for (i = 0; i < NR_EVTCHN; i++)
		if (me->evtchn[i].state == ECS_INTERDOMAIN)
			me->evtchn[i].interdomain.remote_dom = NULL;

	me->nr_pirqs = mig_info->nr_pirqs;

	/* Shared info */

	memcpy(me->shared_info->evtchn_pending, mig_info->evtchn_pending, sizeof((me->shared_info->evtchn_pending)));
	memcpy(me->shared_info->evtchn_mask, mig_info->evtchn_mask, sizeof((me->shared_info->evtchn_mask)));

	/* Retrieve the clocksource reference */
	me->shared_info->clocksource_ref = mig_info->clocksource_ref;

	me->tot_pages = memslot[ME_slotID].size >> PAGE_SHIFT;

	/* Restore start_info structure (allocated in the heap of hypervisor) */
	vstartinfo_start = (unsigned long) alloc_heap_page();

	memcpy((struct start_info *) vstartinfo_start, mig_info->start_info_page, PAGE_SIZE);
	si = (start_info_t *) vstartinfo_start;

	/* Restoring ME descriptor */
	memcpy(&me->shared_info->dom_desc, &mig_info->dom_desc, sizeof(dom_desc_t));

	/* Update the pfn of the ME in its host Smart Object */
	me->shared_info->dom_desc.u.ME.pfn = phys_to_pfn(memslot[ME_slotID].start);

	/* start pfn can differ from the initiator according to the physical memory layout */
	si->min_mfn = phys_to_pfn(memslot[ME_slotID].start);
	si->nr_pages = me->tot_pages;

	pfn_offset = si->min_mfn - mig_info->start_pfn;

	si->hypercall_addr = (unsigned long) hypercall_start;

	si->domID = me->domain_id;

	si->printch = printch;

	/* Re-init the startinfo_start address */
	me->arch.vstartinfo_start = vstartinfo_start;

	me->pause_count = mig_info->pause_count;

	dmb();
}

static void restore_vcpu_migration_info(unsigned int ME_slotID, struct domain *me, struct vcpu_migration_info *mig_info)
{
	DBG("%s\n", __FUNCTION__);

	me->vcpu[0]->processor 		   = mig_info->processor;

	me->vcpu[0]->need_periodic_timer = mig_info->need_periodic_timer;

	/* Pause */
	me->vcpu[0]->pause_flags     = mig_info->pause_flags;

	memcpy(&(me->vcpu[0]->pause_count), &(mig_info->pause_count), sizeof(me->vcpu[0]->pause_count));
	/* VIRQ mapping */
	memcpy(me->vcpu[0]->virq_to_evtchn, mig_info->virq_to_evtchn, sizeof((me->vcpu[0]->virq_to_evtchn)));

	/* Arch */
	memcpy(&(me->vcpu[0]->arch), &(mig_info->arch), sizeof(me->vcpu[0]->arch));

	/* Internal fields of vcpu_info_t structure */
	me->vcpu[0]->vcpu_info->evtchn_upcall_pending = mig_info->evtchn_upcall_pending;

	memcpy(&(me->vcpu[0]->vcpu_info->arch), &(mig_info->arch_info), sizeof(me->vcpu[0]->vcpu_info->arch));
	memcpy(&(me->vcpu[0]->vcpu_info->time), &(mig_info->time), sizeof(me->vcpu[0]->vcpu_info->time));

	dmb();
}

/**
 * Write the migration info structures.
 */
int write_migration_structures(soo_hyp_t *op) {
	unsigned long paddr;
	uint32_t crc32;

	crc32 = *((uint32_t *) op->p_val1);

	/* Switch to idle domain address space */
	switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

	/* Get the migration info structures */
	paddr = op->paddr;
	memcpy(&dom_info, (void *) __lva(paddr), sizeof(dom_info));

	dom_info.dom_desc.u.ME.crc32 = crc32;

	paddr += sizeof(dom_info);
	memcpy(&vcpu_info, (void *) __lva(paddr), sizeof(vcpu_info));

	dmb();

	/* Switch back to dom0 address space */
	switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

	return 0;
}

/*
 * Inject a ME within a SOO device. This is the only possibility to load a ME within a Smart Object.
 *
 * Returns 0 in case of success, -1 otherwise.
 */
int inject_me(soo_hyp_t *op)
{
	int rc = 0;
	unsigned int slotID;
	unsigned char *imgPtr;
	dtb_feat_t dtb_feat;

	DBG("%s: Preparing ME injection, source image = %lx\n", __func__, op->vaddr);

	/* Find the size required for allocating the domain memory */
	imgPtr = (unsigned char *) op->vaddr;
	imgPtr += 4; /* Skip the size of the binary image */

	slotID = *((unsigned int *) op->p_val1);

	loadME(slotID, (unsigned char *) op->vaddr, (unsigned char *) *((unsigned int *) op->p_val2), &dtb_feat);

	switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

	DBG("ME realtime feature (0 = non-realtime, 1 = realtime): %d\n", dtb_feat.realtime);

	/* Finalize the domain creation. */
	finalize_domain_create(domains[slotID-1], dtb_feat.realtime);

	if (construct_ME(domains[slotID-1]) != 0)
		panic("Could not set up ME guest OS\n");

	domains[slotID-1]->shared_info->dom_desc.u.ME.crc32 = *((uint32_t *) op->paddr);

	/* Switch back to dom0 address space */
	switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

	return rc;
}

/*******************************************************************************
    EXPORTED FUNCTIONS
 *******************************************************************************/

void mig_restore_domain_migration_info(unsigned int ME_slotID, struct domain *me)
{
	return restore_domain_migration_info(ME_slotID, me, &dom_info);
}

void mig_restore_vcpu_migration_info(unsigned int ME_slotID, struct domain *me)
{
	return restore_vcpu_migration_info(ME_slotID, me, &vcpu_info);
}
