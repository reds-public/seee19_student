/******************************************************************************
 * console.c
 * 
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 *
 *
 */

#include <avz/stdarg.h>
#include <avz/config.h>
#include <avz/init.h>
#include <avz/lib.h>
#include <avz/errno.h>
#include <avz/event.h>
#include <avz/spinlock.h>
#include <avz/console.h>
#include <avz/serial.h>
#include <avz/softirq.h>
#include <avz/keyhandler.h>
#include <avz/mm.h>
#include <avz/delay.h>

#include <virtshare/console.h>

#include <asm/uaccess.h>
#include <asm/domain.h>
#include <asm/current.h>
#include <asm/debugger.h>
#include <asm/io.h>
#include <asm/div64.h>

#define AVZ_BANNER		"*********** SOO - Agency Virtualizer Technology - Sootech SA, Switzerland ***********\n\n\n"

DEFINE_SPINLOCK(console_lock);

extern void printch(char c);

void serial_puts(const char *s)
{

	char c;

	while ((c = *s++) != '\0')
		printch(c);

}

static void sercon_puts(const char *s)
{
	serial_puts(s);
}

/* (DRE) Perform hypercall to process char addressed to the keyhandler mechanism */
void process_char(char ch) {

	struct cpu_user_regs regs;

	register unsigned int r0 __asm__("r0");
	register unsigned int r1 __asm__("r1");
	register unsigned int r2 __asm__("r2");
	register unsigned int r3 __asm__("r3");
	register unsigned int r4 __asm__("r4");
	register unsigned int r5 __asm__("r5");
	register unsigned int r6 __asm__("r6");
	register unsigned int r7 __asm__("r7");
	register unsigned int r8 __asm__("r8");
	register unsigned int r9 __asm__("r9");
	register unsigned int r10 __asm__("r10");
	register unsigned int r11 __asm__("r11");
	register unsigned int r12 __asm__("r12");
	register unsigned int r13 __asm__("r13");
	register unsigned int r14 __asm__("r14");
	register unsigned int r15;

	asm("mov %0, pc":"=r"(r15));

	regs.r0 = r0;
	regs.r1 = r1;
	regs.r2 = r2;
	regs.r3 = r3;
	regs.r4 = r4;
	regs.r5 = r5;
	regs.r6 = r6;
	regs.r7 = r7;
	regs.r8 = r8;
	regs.r9 = r9;
	regs.r10 = r10;
	regs.r11 = r11;
	regs.r12 = r12;
	regs.r13 = r13;
	regs.r14 = r14;
	regs.r15 = r15;

	handle_keypress(ch, &regs);
}

long do_console_io(int cmd, int count, char *buffer)
{
	char kbuf;
	char __buffer[CONSOLEIO_BUFFER_SIZE];
	int ret = 0;

	switch (cmd) {

	case CONSOLEIO_process_char:
		if (copy_from_user(&kbuf, buffer, sizeof(char)))
			BUG();

		process_char(kbuf);
		ret = 1;
		break;

	case CONSOLEIO_write_string:
		spin_lock(&console_lock);

		ret = strlen(buffer);

		if (copy_from_user(__buffer, buffer, ret))
			BUG();

		printk("%s", buffer);

		spin_unlock(&console_lock);
		break;

	default:
		BUG();
		break;
	}

	return ret;
}


/*
 * *****************************************************
 * *************** GENERIC CONSOLE I/O *****************
 * *****************************************************
 */

static void __putstr(const char *str)
{
	sercon_puts(str);
}

void printk(const char *fmt, ...)
{
	static char   buf[1024];

	va_list       args;
	char         *p, *q;

	va_start(args, fmt);
	(void)vsnprintf(buf, sizeof(buf), fmt, args);
	va_end(args);

	p = buf;

	while ((q = strchr(p, '\n')) != NULL)
	{
		*q = '\0';
		__putstr(p);
		__putstr("\n");
		p = q + 1;
	}

	if (*p != '\0')
		__putstr(p);

}

void printk_buffer(void *buffer, int n) {
	int i;

	for (i = 0 ; i < n ; i++)
		printk("%02x ", ((char *) buffer)[i]);
	printk("\n");
}

void printk_buffer_separator(void *buffer, int n, char separator) {
	int i;

	for (i = 0 ; i < n ; i++)
		printk("%02x%c", ((char *) buffer)[i], separator);
	printk("\n");
}

void __init console_init(void)
{
	__putstr(AVZ_BANNER);

	printk(" SOO Agency Virtualizer Technology -- v2016.0.2\n");

	printk(" Copyright (c) 2016 Sootech SA, Switzerland\n");
	printk(" http://www.sootech.ch\n\n");


}



