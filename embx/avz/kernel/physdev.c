/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA/AG, Switzerland
 * 
 * This code is under the property of Sootech SA and must not be shared.
 *
 */


#if 0
#define DEBUG
#endif

#include <avz/types.h>

#include <logbool.h>

#include <virtshare/avz.h>

#include <avz/config.h>
#include <avz/init.h>
#include <avz/lib.h>
#include <avz/types.h>
#include <avz/sched.h>
#include <avz/irq.h>
#include <avz/event.h>

#include <asm/uaccess.h>
#include <asm/current.h>
#include <asm/page.h>
#include <asm/domain.h>
#include <asm/hypercall.h>
#include <asm/mach/arch.h>
#include <asm/hardware/gic.h>
#include <asm/smp.h>

#include <virtshare/avz.h>
#include <virtshare/physdev.h>
#include <virtshare/debug.h>
#include <virtshare/avz.h>

#include <soo/soo.h>

#include <asm/system.h>

int do_physdev_op(int cmd, void *args)
{
	int val;

	switch (cmd) {

	case PHYSDEVOP_dump_page:
	{
		if (copy_from_user(&val, args, sizeof(int)) != 0)
			BUG();

		 switch_mm(NULL, idle_domain[smp_processor_id()]->vcpu[0], USE_SYSTEM_PGTABLE);

		 dump_page(val);

		 switch_mm(idle_domain[smp_processor_id()]->vcpu[0], NULL, USE_NORMAL_PGTABLE);

		 break;
	}

	case PHYSDEVOP_dump_logbool:
	{
		/* Do not care about the parameter */
		dump_all_logbool(' ');

		break;
	}

	default:
		BUG();
		break;
	}

	return 0;
}
