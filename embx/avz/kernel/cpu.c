/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 */

#include <avz/config.h>
#include <avz/cpumask.h>
#include <avz/errno.h>
#include <avz/smp.h>

#ifdef CONFIG_SMP

void set_cpu_online(unsigned int cpu, int online)
{
	if (online)
		cpu_set(cpu, cpu_online_map);
	else
		cpu_clear(cpu, cpu_online_map);
}

int __cpuinit cpu_up(unsigned int cpu)
{
	int ret = 0;

	if (!cpu_possible(cpu)) {
		printk(KERN_ERR "can't online cpu %d because it is not "
				"configured as may-hotadd at boot time\n", cpu);
		return -EINVAL;
	}

	if (cpu_online(cpu) || !cpu_present(cpu))
		return -EINVAL;

	/* Arch-specific enabling code. */
	ret = __cpu_up(cpu);

	BUG_ON(!cpu_online(cpu));

	return ret;
}

#endif /* CONFIG_SMP */

/*
 * cpu_bit_bitmap[] is a special, "compressed" data structure that
 * represents all NR_CPUS bits binary values of 1<<nr.
 *
 * It is used by cpumask_of() to get a constant address to a CPU
 * mask value that has a single bit set only.
 */

/* cpu_bit_bitmap[0] is empty - so we can back into it */
#define MASK_DECLARE_1(x) [x+1][0] = 1UL << (x)
#define MASK_DECLARE_2(x) MASK_DECLARE_1(x), MASK_DECLARE_1(x+1)
#define MASK_DECLARE_4(x) MASK_DECLARE_2(x), MASK_DECLARE_2(x+2)
#define MASK_DECLARE_8(x) MASK_DECLARE_4(x), MASK_DECLARE_4(x+4)

const unsigned long cpu_bit_bitmap[BITS_PER_LONG+1][BITS_TO_LONGS(NR_CPUS)] = {

    MASK_DECLARE_8(0),  MASK_DECLARE_8(8),
    MASK_DECLARE_8(16), MASK_DECLARE_8(24),
#if BITS_PER_LONG > 32
    MASK_DECLARE_8(32), MASK_DECLARE_8(40),
    MASK_DECLARE_8(48), MASK_DECLARE_8(56),
#endif
};
