/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 *
 */

#include <avz/config.h>
#include <avz/lib.h>
#include <avz/percpu.h>
#include <avz/sched.h>
#include <avz/init.h>
#include <avz/ctype.h>
#include <avz/console.h>

#include <avz/domain.h>
#include <avz/errno.h>
#include <asm/processor.h>
#include <asm/cputype.h>
#include <asm/pgtable.h>
#include <asm/mach/map.h>
#include <asm/memslot.h>

#include <public/arch-arm.h>
#include <avz/mm.h>
#include <asm/mm.h>
#include <avz/libelf.h>
#include <asm/cacheflush.h>

#include <virtshare/debug/logbool.h>

#define round_pgup(_p)    (((_p)+(PAGE_SIZE-1))&PAGE_MASK)

#define L_TEXT_OFFSET	0x8000

extern unsigned int __atags_pointer __initdata; /* defined in kernel/setup.c */
extern unsigned int __boot_params __initdata;

extern unsigned long total_pages;

unsigned long mapcache_pages;

start_info_t *dom0_start_info;
struct domain *agency_rt_domain;

/*
 * setup_page_table_guestOS() is setting up the 1st-level and 2nd-level page tables within the domain.
 */
int setup_page_table_guestOS(struct vcpu *v, unsigned long v_start, unsigned long map_size, unsigned long p_start, unsigned long vpt_start) {

	struct map_desc map;
	unsigned long addr, new_pt;
	struct vcpu temp_vcpu;

	ASSERT(v);

	/* Make sure that the size is 1 MB-aligned */
	map_size = ALIGN_UP(map_size, SECTION_SIZE);

	printk("*** Setup page table for guest OS: ***\n");
	printk("   v_start          : 0x%lx\n", v_start);
	printk("   map size (bytes) : 0x%lx\n", map_size);
	printk("   phys address     : 0x%lx\n", p_start);
	printk("   vpt_start        : 0x%lx\n", vpt_start);

	new_pt = (unsigned long) __lva(vpt_start-v_start+p_start);

	/* copy page table of idle domain to guest domain */
	memcpy((unsigned long *) new_pt, swapper_pg_dir, sizeof(swapper_pg_dir));

	/* guest start address (phys/virtual addr) */
	v->arch.guest_pstart = p_start;
	v->arch.guest_vstart = v_start;

	/* guest page table address (phys addr) */
	v->arch.guest_table = mk_pagetable(vpt_start - v_start + p_start);
	v->arch.guest_vtable = (pte_t *) vpt_start;

	/* We have to change the ptes of the page table in our new page table :-) */
	addr = (unsigned long) pgd_offset_priv((pde_t *) new_pt, vpt_start);

	((pde_t *) addr)->l2 &= ~SECTION_MASK; /* Reset the pfn */
	((pde_t *) addr)->l2 |= (vpt_start-v_start+p_start) & SECTION_MASK;

	flush_all();

	/* Immediately switch to our newly created page table in order to get the guest address space */
	save_ptbase(&temp_vcpu);

	write_ptbase(v);

	/* Clear the area below the I/Os, but preserve of course the page table itself which is located within the first MB */
	for (addr = v_start + SECTION_SIZE; addr < VMALLOC_END; addr += SECTION_SIZE)
		pmd_clear(pgd_offset_priv((pde_t *) vpt_start, addr));

	/* And now, re-do the mapping including the remaining sections of the guest domain */
	map.pfn = phys_to_pfn(p_start);
	map.virtual = v_start;
	map.length = map_size;
	map.type = MT_MEMORY_RWX;

	create_mapping(&map, (pde_t *) vpt_start);

	write_ptbase(&temp_vcpu);

	return 0;
}

extern char hypercall_start[];

extern struct page_info *set_guest_pages(struct domain *d, unsigned long guest_paddr, unsigned int guest_size, unsigned int flags);

int construct_agency(struct domain *d) {
	int rc;

	struct vcpu *v;
	unsigned long vstartinfo_start;
	unsigned long v_start;
	unsigned long align_offset = 0;
	unsigned long alloc_spfn;
	unsigned long vpt_start;
	unsigned long vstack_start, vstack_end;
	struct start_info *si = NULL;

	unsigned int target_vaddr;

	unsigned long nr_pages;
	unsigned long entry_main;


	printk("***************************** Loading SOO Agency Domain *****************************\n");

	/* The agency is always in slot 1 */

	if (memslot[1].size == 0)
		panic("No Dom0 image supplied\n");

	printk("Parsing agency (%luB) at 0x%lx\n", memslot[1].elf.size, (unsigned long) memslot[1].elf.image_base);

	/* Preserve the destination virtual address */
	target_vaddr = (unsigned int) memslot[1].elf.dest_base;

	elf_init(&(memslot[1].elf), memslot[1].elf.image_base, memslot[1].elf.size);

	elf_parse_binary(&(memslot[1].elf));

	entry_main = (unsigned long) elf_uval(&(memslot[1].elf), (memslot[1].elf).ehdr, e_entry);

	printk("AVZ: ELF image information for agency domain\n");

	printk("   image        : 0x%p\n", 	memslot[1].elf.image_base);
	printk("   size         : %lu\n", 	memslot[1].elf.size);
	printk("   pstart       : 0x%lx\n", (unsigned long) memslot[1].elf.pstart);
	printk("   pend         : 0x%lx\n", (unsigned long) memslot[1].elf.pend);
	printk("   reloc_offset : 0x%lx\n", (unsigned long) memslot[1].elf.reloc_offset);
	printk("   entry_main   : 0x%lx\n", entry_main);

	/* The following page will contain start_info information */
	vstartinfo_start = (unsigned long) alloc_heap_page();

	d->max_pages = ~0U;
	d->tot_pages = 0;

	nr_pages = memslot[1].size >> PAGE_SHIFT;

	printk("Max dom size %d\n", memslot[1].size);

	printk("Domain length = %lu pages.\n", nr_pages);

	ASSERT(d);

	v = d->vcpu[0];
	BUG_ON(d->vcpu[0] == NULL);

	ASSERT(v);

	d->tot_pages = memslot[1].size >> PAGE_SHIFT;
	alloc_spfn = memslot[1].start >> PAGE_SHIFT;

	clear_bit(_VPF_down, &v->pause_flags);

	/* Align load address to 1MB boundary. */
	v_start = memslot[1].elf.pstart & ~((1UL << __L2_PAGETABLE_SHIFT) - 1);
	printk("Domain virtual start address: %lx\n", v_start);

	align_offset = memslot[1].elf.pstart - v_start;   /* Align offset if any */
	memslot[1].elf.dest_base = (char *) (target_vaddr + align_offset);	/* Adjust the physical start of the domain image, pages before must be mapped */

	vpt_start = v_start + L_TEXT_OFFSET - 0x4000;  /* according to Linux guest OS in head.S */

	/* vstack is used when the guest has not initialized its own stack yet; put right after _end of the guest OS*/
	vstack_start = PGD_ALIGN(memslot[1].elf.pend);
	vstack_end = vstack_start + PAGE_SIZE;

	setup_page_table_guestOS(v, v_start, vstack_end-v_start, (alloc_spfn << PAGE_SHIFT), vpt_start);

	/** Loading the domain... **/
	/* Destination is based upon running page table, i.e. hypervisor page table */

	printk("Domain image destination: %p\n", memslot[1].elf.dest_base);

	/* Parsing and loading the ELF image of the guest */
	/* Copy the OS image and free temporary buffer. */
	printk("Loading domain ELF image located at: %p, dest: %p... ", memslot[1].elf.image_base, memslot[1].elf.dest_base);
	elf_load_binary(&(memslot[1].elf));
	printk("done.\n");

	/* Lets switch to the page table of our new domain - required for sharing page info */

	save_ptbase(idle_domain[smp_processor_id()]->vcpu[0]);

	write_ptbase(v);

	si = (start_info_t *) vstartinfo_start;

	dom0_start_info = si;
	memset(si, 0, PAGE_SIZE);

	si->domID = 0;

	si->nr_pages = d->tot_pages;

	si->min_mfn = alloc_spfn;

	/* Propagate the virtual address of the shared info page for this domain */
	si->shared_info = d->shared_info;

	si->hypercall_addr = (unsigned long) hypercall_start;

	si->logbool_ht_set_addr = (unsigned long) ht_set;

	/*
	 * si->atags_pointer must be physical address
	 * cur_domain->atags_pointer is a virtual address
	 */

	si->atags_pointer = memslot[1].atags_pointer;

	printk("Agency ATAGS pointer: 0x%lx (phys)\n", si->atags_pointer);

	/* HW details on the CPU: processor ID, cache ID and ARM architecture version */

	si->printch = printch;
	si->pt_base = vpt_start;

	write_ptbase(current);

	d->arch.vstartinfo_start = vstartinfo_start;
	d->arch.vstack_end = vstack_end;
	d->arch.entry_main = entry_main;

#if 0
	{
		unsigned long domain_stack;
		extern unsigned long *hypervisor_stack;
		extern unsigned long *pseudo_usr_mode;
		static unsigned long *__hyp_stack = (unsigned long *) &hypervisor_stack;
		static unsigned long *__pseudo_usr_mode = (unsigned long *) &pseudo_usr_mode;

	  /* Set up a new domain stack */
	  domain_stack = (unsigned long) setup_dom_stack(agency_rt_domain->vcpu[0]);

	  /* Store the stack address for further needs in hypercalls/interrupt context */
	  __hyp_stack[AGENCY_RT_CPU] = domain_stack;

	  /* We set the realtime domain in pseudo-usr mode since the primary domain will start it, not us. */
	  __pseudo_usr_mode[AGENCY_RT_CPU] = 1;

	  /*
	   * Keep a reference in the primary agency domain to its subdomain. Indeed, there is only one shared info page mapped
	   * in the guest.
	   */
	  dom0->shared_info->subdomain_shared_info = agency_rt_domain->shared_info;
	}
#endif /* 0 */

	new_thread(v, (unsigned long) entry_main, si->atags_pointer, vstack_end, vstartinfo_start);

	rc = 0;

	BUG_ON(rc != 0);
	return 0;
}

