#ifndef __LIBELF_PRIVATE_H__
#define __LIBELF_PRIVATE_H_


#include <avz/config.h>
#include <avz/types.h>
#include <avz/string.h>
#include <avz/lib.h>
#include <avz/libelf.h>
#include <asm/byteorder.h>


#define elf_msg(elf, fmt, args ... ) \
   if (elf->verbose) printk(fmt, ## args )
#define elf_err(elf, fmt, args ... ) \
   printk(fmt, ## args )

#define strtoull(str, end, base) simple_strtoull(str, end, base)
#define bswap_16(x) swab16(x)
#define bswap_32(x) swab32(x)
#define bswap_64(x) swab64(x)

#endif /* __LIBELF_PRIVATE_H_ */

