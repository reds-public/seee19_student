/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 *
 *
 */

#include <fdt_support.h>

#include <avz/kernel.h>
#include <asm/memslot.h>
#include <asm/mach/map.h>

#include <soo/soo.h>

/*
 * Parse a concatened image containing the kernel, (possible) devtree and (possible) initrd
 * and relocate the components to their target destination.
 *
 * <domID>	target domain
 * <imgptr>	beginning of the concatened image
 * <userspace>  0 means target slot is accessible through the hypervisor page table, 1 means we are using intermediate user space (case of injection)
 *
 * Returns the current position
 */
unsigned char *parse_and_relocate_domain(unsigned int slotID, unsigned char *imgptr, unsigned char *dest) {
	unsigned long devtree_size;
	unsigned int initrd_start, initrd_size;
	unsigned int initrd_start_phys;
	unsigned int vaddr;

	/* Get the size of the memory required by the domain */
	memslot[slotID].size = *((unsigned long *) imgptr);
	imgptr += 4;

	vaddr = (unsigned int) dest;

	memslot[slotID].elf.dest_base = (char *) vaddr;

	/* Get the size of the domain */
	memslot[slotID].elf.size = *((unsigned long *) imgptr);
	imgptr += 4;

	/* If the domain was not empty, i.e. we are not at the end of the list. */
	if (memslot[slotID].elf.size > 0) {

		/* Update the base address of the elf image */
		memslot[slotID].elf.image_base = (void *) imgptr;

		imgptr += memslot[slotID].elf.size;

		/* Now, place the device tree to the expected offset */
		/* Read the size of device tree */

		devtree_size = *((unsigned long *) imgptr);
		imgptr += 4;


		memslot[slotID].atags_pointer = vaddr + min(memslot[slotID].size / 2, (unsigned int) 128 * 1024 * 1024);

		if (devtree_size > 0) {
			memcpy((unsigned char *) memslot[slotID].atags_pointer, imgptr, devtree_size);
			imgptr += devtree_size;
		}

		/* And finally, the initrd */
		initrd_size = *((unsigned long *) imgptr);
		imgptr += 4;

		if (initrd_size > 0) {

			/* Address of initrd is determined according to remarks found in Qemu 2.1.50... */

			/* We want to put the initrd far enough into RAM that when the
			 * kernel is uncompressed it will not clobber the initrd. However
			 * on boards without much RAM we must ensure that we still leave
			 * enough room for a decent sized initrd, and on boards with large
			 * amounts of RAM we must avoid the initrd being so far up in RAM
			 * that it is outside lowmem and inaccessible to the kernel.
			 * So for boards with less  than 256MB of RAM we put the initrd
			 * halfway into RAM, and for boards with 256MB of RAM or more we put
			 * the initrd at 128MB.
			 */

			/* The device tree comes right after the initrd (according to Qemu) */
			initrd_start = memslot[slotID].atags_pointer;

			memslot[slotID].atags_pointer += initrd_size;
			memslot[slotID].atags_pointer = ALIGN_UP(memslot[slotID].atags_pointer, PAGE_SIZE);

			memcpy((unsigned char *) memslot[slotID].atags_pointer, (unsigned char *) initrd_start, devtree_size);


			memcpy((unsigned char *) initrd_start, (unsigned char *) imgptr, initrd_size);
			imgptr += initrd_size;

			fdt_resize((unsigned char *) memslot[slotID].atags_pointer);

			/*
			 * Update the device tree
			 * The address of initrd start has to be converted into a physical address.
			 */
			initrd_start_phys = initrd_start - vaddr + memslot[slotID].start;

			fdt_initrd((unsigned char *) memslot[slotID].atags_pointer, initrd_start_phys, initrd_start_phys + initrd_size, 1);

		} else {

			/* In this case (no device tree), we move the device tree to end of RAM to ensure,
			 * the elf will not clobber the device tree (case of small ME).
			 */
			unsigned int dest_devtree;

			dest_devtree = vaddr + memslot[slotID].size - devtree_size;
			memcpy((unsigned char *) dest_devtree, (unsigned char *) memslot[slotID].atags_pointer, devtree_size);
			memslot[slotID].atags_pointer = dest_devtree;

		}

		/* Finally, atags_pointer itself is changed to physical for the following ... */
		memslot[slotID].atags_pointer = memslot[slotID].atags_pointer - vaddr + memslot[slotID].start;

	}

	return imgptr;
}

/**
 * We put all the guest domains in ELF format on top of memory so
 * that the domain_build will be able to elf-parse and load to their final destination.
 */
void loadAgency(void)
{
	/* Source is right after the hypervisor code in phys. memory */
	unsigned char *current_position = (unsigned char *) __lva(__pa(&_end));
	unsigned int target_addr;
	unsigned long size;

	/* Relocating domain images */

	/* The top of memory cannot exceed the virtual address of the hypervisor */
	target_addr = (unsigned int) __lva(TOP_PHYS_MEMORY);
	target_addr = ((target_addr >= VMALLOC_END) ? VMALLOC_END-1 : target_addr);

	/* Get the size of the full contents */
	size = *((unsigned long *) current_position);
	current_position += 4;

	target_addr = ALIGN_DOWN(target_addr-size, 4);

	/* Copy the contents to the target address to avoid interfering with domain memory */
	memcpy((void *)(target_addr), current_position, size);

	memslot[1].start = CONFIG_DRAM_BASE + HYPERVISOR_SIZE;

	parse_and_relocate_domain(1, (unsigned char *) target_addr, (unsigned char *) __lva(memslot[1].start));

}

/*
 * The concatened image must be out of domains because of elf parser
 *
 * <img> represents the original binary image as injected in the user application
 * <target_dom> represents the target memory area
 */
void loadME(unsigned int slotID, uint8_t *img, uint8_t *dest, dtb_feat_t *dtb_feat) {
	u32 realtime;
	unsigned int vaddr;

	/* Skip the size of the full contents */
	img += 4;

	parse_and_relocate_domain(slotID, img, dest);

	/* Now, try to retrieve some feature such as realtime */
	vaddr = memslot[slotID].atags_pointer - memslot[slotID].start + (unsigned int) dest;
	realtime = fdt_getprop_u32_default((unsigned char *) vaddr, "/me_features", "realtime", 0);

	dtb_feat->realtime = ((realtime == 1) ? true : false);

}

