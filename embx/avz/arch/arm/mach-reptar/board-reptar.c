/*
 * CompuLab CM-T35/CM-T3730 modules support
 *
 * Copyright (C) 2009-2011 CompuLab, Ltd.
 * Authors: Mike Rapoport <mike@compulab.co.il>
 *	    Igor Grinberg <grinberg@compulab.co.il>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 */

#include <avz/kernel.h>
#include <avz/init.h>
#include <avz/console.h>
#include <avz/percpu.h>

#include <asm/mach/map.h>

#include <asm/setup.h>
#include <asm/irq.h>

#include <asm/mach-types.h>

#include <asm/mach/arch.h>
#include <asm/arch/reptar.h>

DEFINE_PER_CPU(spinlock_t, intc_lock);

static struct map_desc reptar_io_desc[] __initdata = {
	{
		.virtual = 	REPTAR_UART_VIRT,
		.pfn = __phys_to_pfn(REPTAR_UART_PHYS),
		.length = SZ_4K,
		.type = MT_DEVICE,
	},
};

static void __init reptar_map_io(void) {
	iotable_init(reptar_io_desc, ARRAY_SIZE(reptar_io_desc));
}

void arm_timer_init(int cpu) {

}

void ll_entry_irq(void) {

}

/******************************************************************
*	Init methods 
******************************************************************/

static void __init reptar_init(void)
{
	printk("reptar initializing board...\n");

}

struct clocksource *system_timer_clocksource;
struct clock_event_device *system_timer_clockevent;

MACHINE_START(REPTAR, "Reptar Board")
	.boot_params    = 0x80000100,
	.map_io		= reptar_map_io,
	.init_machine   = reptar_init,
MACHINE_END
