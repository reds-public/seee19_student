/******************************************************************************
 * hypervisor.c
 *
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */


#include <linux/version.h>
#include <linux/cpumask.h>
#include <linux/mm_types.h>
#include <linux/delay.h>

#include <asm/pgtable.h>

#include <virtshare/avz.h>
#include <virtshare/hypervisor.h>
#include <virtshare/evtchn.h>
#include <virtshare/physdev.h>
#include <virtshare/schedop.h>
#include <virtshare/domctl.h>

void avz_ME_unpause(domid_t domain_id, uint32_t store_mfn)
{
	struct domctl op;
	int ret;

	printk("Trying to unpause ME domain %d...", domain_id);

	op.cmd = DOMCTL_unpauseME;

	op.domain = domain_id;

	op.u.unpause_ME.store_mfn = store_mfn;

	ret = hypercall_trampoline(__HYPERVISOR_domctl, (long) &op, 0 ,0 ,0);

	if (ret == -ESRCH) {
		printk("no further ME !\n");
		return ;
	}
	else
		printk("done.\n");

	BUG_ON(ret< 0);
}

void avz_ME_pause(domid_t domain_id)
{
	struct domctl op;
	int ret;

	printk("Trying to pause domain %d...", domain_id);

	op.cmd = DOMCTL_pauseME;
	op.domain = domain_id;

	ret = hypercall_trampoline(__HYPERVISOR_domctl, (long) &op, 0, 0, 0);

	if (ret == -ESRCH) {
		printk("no further ME !\n");
		return ;
	} else
		printk("done.\n");

	BUG_ON(ret< 0);
}

int avz_dump_page(unsigned int pfn)
{
	int ret;

	ret = hypercall_trampoline(__HYPERVISOR_physdev_op, PHYSDEVOP_dump_page, (long) &pfn, 0, 0);
	BUG_ON(ret < 0);

	return 0;
}

int avz_dump_logbool(void)
{
	int ret;

	ret = hypercall_trampoline(__HYPERVISOR_physdev_op, PHYSDEVOP_dump_logbool, 0 ,0, 0);
	BUG_ON(ret < 0);

	return 0;
}

int avz_sched_yield(void)
{
	int ret;

	ret = hypercall_trampoline(__HYPERVISOR_sched_op, SCHEDOP_yield, 0, 0, 0);
	BUG_ON(ret < 0);

	return ret;
}

extern int irq_masked[256];
extern void mask_irq(struct irq_desc *desc);
extern void unmask_irq(struct irq_desc *desc);

void printsp(unsigned int sp) {
	lprintk("sp: %x\n", sp);
}
/*
 * This hypercall allows REPTAR domains to be switched adequately.
 */
int avz_sched_schedule(void)
{
	int ret;

#ifdef CONFIG_SOO_AGENCY
	int i;
	struct irq_desc *desc;

		/* Mask all IRQs except the timer */
		for (i = 0; i < 256; i++) {
			if (i != 53) /* timer IRQ (hwirq 37 mapped into the IRQ domain) */
			{
				desc = irq_to_desc(i);
				if (desc != NULL) {
					if ((!irqd_irq_disabled(&desc->irq_data)) && (!irqd_irq_masked(&desc->irq_data))) {
						mask_irq(desc);
						irq_masked[i] = 1;
					}
				}
			}
		}

#endif

	ret = hypercall_trampoline(__HYPERVISOR_sched_op, SCHEDOP_schedule, 0, 0, 0);
	BUG_ON(ret < 0);

#ifdef CONFIG_SOO_AGENCY
		for (i = 0; i < 256; i++) {
			if (i != 53) /* timer IRQ (hwirq 37 mapped into the IRQ domain) */
			{
				desc = irq_to_desc(i);
				if (desc != NULL) {
					if (irq_masked[i]) {
						unmask_irq(desc);
						irq_masked[i] = 0;
					}
				}
			}
		}
#endif

	return ret;
}

int avz_sched_deadline(u64 delta_ns)
{
	int ret;
	deadline_args_t deadline_args;

	deadline_args.delta_ns = delta_ns;

	ret = hypercall_trampoline(__HYPERVISOR_sched_op, SCHEDOP_deadline, (long) &deadline_args, 0, 0);
	BUG_ON(ret < 0);

	return 0;
}

int avz_sched_sleep_ns(u64 delta_ns)
{
	int ret;
	deadline_args_t deadline_args;

	deadline_args.delta_ns = delta_ns;

	ret = hypercall_trampoline(__HYPERVISOR_sched_op, SCHEDOP_sleep, (long) &deadline_args, 0, 0);
	BUG_ON(ret < 0);

	return 0;
}

int avz_sched_sleep_us(u64 delta_us)
{
	return avz_sched_sleep_ns((u64) (1000 * delta_us));
}

int avz_sched_sleep_ms(u64 delta_ms)
{
	return avz_sched_sleep_us((u64) (1000 * delta_ms));
}

int avz_printk(char *buffer)
{
	int len;

	len = hypercall_trampoline(__HYPERVISOR_console_io, CONSOLEIO_write_string, 1, (long) buffer, 0);
	if (len < 0)
		BUG();

	return len;
}

