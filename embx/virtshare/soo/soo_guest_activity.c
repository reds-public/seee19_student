
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 * - June 2016: Baptiste Delporte
 * 
 */

#if 0
#define DEBUG
#endif

#include <linux/version.h>
#include <linux/device.h>
#include <linux/semaphore.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>

#if (LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0))
#include <linux/sched/signal.h>
#include <linux/sched/debug.h>
#endif

#include <asm/cacheflush.h>

#include <virtshare/vbus.h>
#include <virtshare/vbstore.h>

#include <virtshare/soo.h>
#include <virtshare/guest_api.h>
#include <virtshare/console.h>

#include <virtshare/guest_api.h>

#ifdef CONFIG_SOO_AGENCY

#if defined(CONFIG_SOOLINK_PLUGIN_WLAN)
#include <soolink/plugin/wlan.h>
#endif /* CONFIG_SOOLINK_PLUGIN_WLAN */

#ifdef CONFIG_SOOLINK_PLUGIN_LOOPBACK
#include <soolink/plugin/loopback.h>
#endif /* CONFIG_SOOLINK_PLUGIN_LOOPBACK */

#ifdef CONFIG_SOOLINK_PLUGIN_ETHERNET
#include <soolink/plugin/ethernet.h>
#endif /* CONFIG_SOOLINK_PLUGIN_ETHERNET */

#ifdef CONFIG_SOOLINK_PLUGIN_BLUETOOTH
#include <soolink/plugin/bluetooth.h>
#endif /* CONFIG_SOOLINK_PLUGIN_BLUETOOTH */

#include <soo/core/device_access.h>

#endif /* CONFIG_SOO_AGENCY */

#define MAX_PENDING_UEVENT		10

/*
 * Used to keep track of the target domain for a certain (outgoing) dc_event.
 * Value -1 means no dc_event in progress.
 */
atomic_t dc_outgoing_domID[DC_EVENT_MAX];

/*
 * Used to store the domID issuing a (incoming) dc_event
 */
atomic_t dc_incoming_domID[DC_EVENT_MAX];

static struct completion dc_stable_lock[DC_EVENT_MAX];

static DEFINE_SEMAPHORE(sooeventd_lock);
DEFINE_SEMAPHORE(usr_feedback);

struct list_head uevents;
static char uevent_str[80];

static unsigned int current_pending_uevent = 0;
static volatile pending_uevent_request_t pending_uevent_req[MAX_PENDING_UEVENT];

#ifdef CONFIG_SOO_ME

int __pfn_offset = 0;

#endif /* CONFIG_SOO_ME */

void dc_stable(int dc_event)
{
	/* It may happen that the thread which performs the down did not have time to perform the call and is not suspended.
	 * In this case, complete() will increment the count and wait_for_completion() will go straightforward.
	 */
	atomic_set(&dc_outgoing_domID[dc_event], -1);
	atomic_set(&dc_incoming_domID[dc_event], -1);

	complete(&dc_stable_lock[dc_event]);
}


/*
 * Sends a ping event to a remote domain in order to get synchronized.
 * Various types of event (dc_event) can be sent.
 *
 * To perform a ping from the RT domain, please use rtdm_do_sync_agency() in rtdm_vbus.c
 *
 * As for the domain table, the index 0 is for the agency and the indexes 1..MAX_DOMAINS
 * are for the MEs. If a ME_slotID is provided, the proper index is given by ME_slotID - 1.
 *
 * @domID: the target domain
 * @dc_event: type of event used in the synchronization
 */
void do_sync_dom(int domID, dc_event_t dc_event)
{
	/* Ping the remote domain to perform the task associated to the DC event */
	DBG("%s: ping domain %d...\n", __func__, domID);

	/* Make sure a previous transaction is not ongoing. */
	while (atomic_cmpxchg(&dc_outgoing_domID[dc_event], -1, domID) != -1) { }

	set_dc_event(domID, dc_event);

#ifdef CONFIG_SOO_AGENCY
	notify_remote_via_evtchn(dc_evtchn[domID]);
#else
	notify_remote_via_evtchn(dc_evtchn);
#endif

	/* Wait for the response from the outgoing domain */
	wait_for_completion(&dc_stable_lock[dc_event]);
}

/*
 * Tell a specific domain that we are now in a stable state regarding the dc_event actions.
 * Typically, this comes after receiving a dc_event leading to a dc_event related action.
 */
void tell_dc_stable(int dc_event)  {
	int domID;

	domID = atomic_read(&dc_incoming_domID[dc_event]);

	BUG_ON(domID == -1);

	DBG("vbus_stable: now pinging domain %d back\n", domID);

	if (domID == DOMID_AGENCY_RT)
		while (atomic_cmpxchg(&avz_shared_info->subdomain_shared_info->dc_event, DC_NO_EVENT, dc_event) != DC_NO_EVENT) ;
	else
		set_dc_event(domID, dc_event);

	/* Ping the remote domain to perform the task associated to the DC event */
	DBG("%s: ping domain %d...\n", __func__, dc_incoming_domID[dc_event]);

	atomic_set(&dc_incoming_domID[dc_event], -1);

#ifdef CONFIG_SOO_AGENCY
		notify_remote_via_evtchn(dc_evtchn[domID]);
#else
		notify_remote_via_evtchn(dc_evtchn);
#endif
}


/*
 * Prepare a remote ME to react to a ping event.
 * @domID: the target ME
 */
int set_dc_event(unsigned int domID, dc_event_t dc_event)
{
	soo_hyp_dc_event_t dc_event_args;
	int rc;

	DBG("%s(%d, %d)\n", __func__, domID, dc_event);

	dc_event_args.domID = domID;
	dc_event_args.dc_event = dc_event;

	rc = soo_hypercall(AVZ_DC_SET, NULL, NULL, &dc_event_args, NULL);
	if (rc != 0) {
		lprintk("%s: Failed to set directcomm event from hypervisor (%d)\n", __func__, rc);
		BUG();
	}

	return 0;
}

int sooeventd_resume(void)
{
	/* Increment the semaphore */
	up(&sooeventd_lock);

	return 0;
}


void wait_for_usr_feedback(void)
{
	down(&usr_feedback);
}

void usr_feedback_ready(void)
{
	up(&usr_feedback);
}

int soo_uevent(struct device *dev, struct kobj_uevent_env *env)
{
	return add_uevent_var(env, uevent_str);
}

/*
 * Configure the uevent which will be retrieved from the user space with the sooeventd utility
 */
void set_uevent(unsigned int uevent_type, unsigned int ME_slotID)
{
	char strSlotID[3];

	switch (uevent_type) {

	case ME_FORCE_TERMINATE:
		strcpy(uevent_str, "SOO_CALLBACK=FORCE_TERMINATE:");
		break;

	case ME_POST_ACTIVATE:
		strcpy(uevent_str, "SOO_CALLBACK=POST_ACTIVATE:");
		break;

	case ME_PRE_SUSPEND:
		strcpy(uevent_str, "SOO_CALLBACK=PRE_SUSPEND:");
		break;

	case ME_LOCALINFO_UPDATE:
		strcpy(uevent_str, "SOO_CALLBACK=LOCALINFO_UPDATE:");
		break;

	}

	sprintf(strSlotID, "%d", ME_slotID);
	strcat(uevent_str, strSlotID);

	/* uevent entries should be processed by means of netlink sockets.
	 * However, at the moment, we just read the /sys file, and we do not
	 * get the right size of the string. So, that's why we put a delimiter at the end of our string.
	 */

	strcat(uevent_str, ":");
}

/*
 * Check if an uevent already exists for a specific slotID.
 */
static bool check_uevent(unsigned int uevent_type, unsigned int slotID) {
	soo_uevent_t *cur;

	list_for_each_entry(cur, &uevents, list)
		if ((cur->uevent_type == uevent_type) && (cur->slotID == slotID))
			return true;

	return false;
}

/*
 * Propagate the cooperate callback in the user space if necessary.
 */
static void add_soo_uevent(unsigned int uevent_type, unsigned int slotID)
{
	soo_uevent_t *cur;

	/* Consider only one uevent_type for a given slotID */
	if (!check_uevent(uevent_type, slotID)) {

		cur = (soo_uevent_t *) kmalloc(sizeof(soo_uevent_t), GFP_ATOMIC);

		cur->uevent_type = uevent_type;
		cur->slotID = slotID;

		/* Insert the thread at the end of the list */
		list_add_tail(&cur->list, &uevents);

#ifdef CONFIG_SOO_ME
		/* Resume the eventd daemon */
		sooeventd_resume();
#endif
	}
}

/*
 * queue_uevent is used by both agency and ME.
 *
 * At the agency side, most of callback processing is performed by the agency (CPU #0), hence
 * in the context of a hypercall. It means that during the final upcall in the agency,
 * the uevent will be inserted in the
 */
void queue_uevent(unsigned int uevent_type, unsigned int slotID)
{
	pending_uevent_req[current_pending_uevent].slotID = slotID;
	pending_uevent_req[current_pending_uevent].uevent_type = uevent_type;
	pending_uevent_req[current_pending_uevent].pending = true;

	current_pending_uevent++;
}

/*
 * SOO Migration hypercall
 *
 * Mandatory arguments:
 * - cmd: hypercall
 * - vaddr: a virtual address used within the hypervisor
 * - paddr: a physical address used within the hypervisor
 * - p_val1: a (virtual) address to a first value
 * - p_val2: a (virtual) address to a second value
 */

int soo_hypercall(int cmd, void *vaddr, void *paddr, void *p_val1, void *p_val2)
{
	soo_hyp_t op;
	int ret, i;

	op.cmd = cmd;
	op.vaddr = (unsigned long) vaddr;
	op.paddr = (unsigned long) paddr;
	op.p_val1 = p_val1;
	op.p_val2 = p_val2;

#ifdef CONFIG_SOO_AGENCY
	/* AVZ_DC_SET and AVZ_GET_DOM_DESC are the only hypercalls that are allowed for CPU 1. */
	if ((smp_processor_id() == 1) && (cmd != AVZ_DC_SET) && (cmd != AVZ_GET_DOM_DESC)) {
		lprintk("%s: trying to unauthorized hypercall %d from the realtime CPU.\n", __func__, cmd);
		panic("Unauthorized.\n");
	}
#endif

	ret = hypercall_trampoline(__HYPERVISOR_soo_hypercall, (long) &op, 0, 0, 0);
	if (ret < 0)
		goto out;

	/* Complete possible pending uevent request done during the work performed at the hypervisor level. */
	DBG("current_pending_uevent=%d\n", current_pending_uevent);
	for (i = 0; i < current_pending_uevent; i++)
		if (pending_uevent_req[i].pending) {
			add_soo_uevent(pending_uevent_req[i].uevent_type, pending_uevent_req[i].slotID);
			pending_uevent_req[i].pending = false;
		}

	current_pending_uevent = 0;

out:
	return ret;

}

void dump_threads(void)
{
	struct task_struct *p;

	for_each_process(p) {

		lprintk("  Backtrace for process pid: %d\n\n", p->pid);

		show_stack(p, NULL);
	}
}

/*
 * Agency side
 */
#ifdef CONFIG_SOO_AGENCY

/*
 * Check for any available uevent in the agency.
 * This version is sequential.
 * Returns 0 if no uevent is present, 1 if any.
 */
int pick_next_uevent(void)
{
	soo_uevent_t *cur;

	/*
	 * If a uevent is available, it will be retrieved and released to the user space.
	 * If no uevent is available, the thread is suspended.
	 */

	if (list_empty(&uevents))
		return 0;

	cur = list_first_entry(&uevents, soo_uevent_t, list);

	/* Process usr space notification & tasks */
	/* Prepare a uevent for further activities in user space if any... */
	set_uevent(cur->uevent_type, cur->slotID);

	list_del(&cur->list);

	kfree(cur);

	return 1;
}

int get_ME_state(unsigned int ME_slotID)
{
	int rc;
	int val;

	val = ME_slotID;

	rc = soo_hypercall(AVZ_GET_ME_STATE, NULL, NULL, &val, NULL);
	if (rc != 0) {
		printk("%s: failed to get the ME state from the hypervisor (%d)\n", __func__, rc);
		return rc;
	}

	return val;
}

/*
 * Setting the ME state to the specific ME_slotID.
 * The hypercall args is passed by 2 contiguous (unsigned) int, the first one is
 * used for slotID, the second for the state
 */
int set_ME_state(unsigned int ME_slotID, ME_state_t state)
{
	int rc;
	int _state[2];

	_state[0] = ME_slotID;
	_state[1] = state;

	rc = soo_hypercall(AVZ_SET_ME_STATE, NULL, NULL, _state, NULL);
	if (rc != 0) {
		printk("%s: failed to set the ME state from the hypervisor (%d)\n", __func__, rc);
		return rc;
	}

	return rc;
}

void perform_task(dc_event_t dc_event)
{
	DBG("%s agency, avz_shared_info->dc_event=%d, &=0x%08x\n", __func__, dc_event, avz_shared_info);

	switch (dc_event) {

	case DC_TRIGGER_DEV_PROBE:
		vbus_probe_backend(atomic_read(&dc_incoming_domID[dc_event]));

		tell_dc_stable(DC_TRIGGER_DEV_PROBE);
		break;

	default:
		lprintk("%s: CPU %d wrong *unexpected* DC event %d\n", __func__, smp_processor_id(), dc_event);
	}

}

/*
 * do_soo_activity() may be called from the hypervisor as a DOMCALL, but not necessary.
 * The function may also be called as a deferred work during the ME kernel execution.
 */
int do_soo_activity(void *arg)
{
	int rc = 0;
	soo_domcall_arg_t *args = (soo_domcall_arg_t *) arg;
	agency_ctl_args_t agency_ctl_args;

	switch (args->cmd) {

	case CB_DUMP_BACKTRACE: /* DOMCALL */

		dump_threads();

		break;

	case CB_DUMP_VBSTORE: /* DOMCALL */

		vbs_dump();

		break;

	case CB_AGENCY_CTL: /* DOMCALL */

		/* Prepare the arguments to pass to the agency ctl */
		memcpy(&agency_ctl_args, &args->u.agency_ctl_args, sizeof(agency_ctl_args_t));

		rc = agency_ctl(&agency_ctl_args);

		/* Copy the agency ctl args back to retrieve the results (if relevant) */
		memcpy(&args->u.agency_ctl_args, &agency_ctl_args, sizeof(agency_ctl_args_t));

		break;

	}

	return rc;
}

/*
 * Agency ctl operations
 */

int agency_ctl(agency_ctl_args_t *agency_ctl_args)
{

	return 0;
}

/*
 * Retrieve the agency descriptor.
 */
int get_agency_desc(agency_desc_t *agency_desc)
{
	int rc;
	dom_desc_t dom_desc;
	unsigned int slotID;

	slotID = 1;  /* Agency slot */

	rc = soo_hypercall(AVZ_GET_DOM_DESC, NULL, NULL, &slotID, &dom_desc);
	if (rc != 0) {
		printk("%s: failed to retrieve the SOO descriptor for slot ID %d.\n", __func__, rc);
		return rc;
	}

	memcpy(agency_desc, &dom_desc.u.agency, sizeof(agency_desc_t));

	return 0;
}

/**
 * Retrieve the ME descriptor including the SPID, the state and the SPAD.
 */
int get_ME_desc(unsigned int slotID, ME_desc_t *ME_desc) {
	int rc;
	dom_desc_t dom_desc;

	rc = soo_hypercall(AVZ_GET_DOM_DESC, NULL, NULL, &slotID, &dom_desc);
	if (rc != 0) {
		printk("%s: failed to retrieve the SOO descriptor for slot ID %d.\n", __func__, rc);
		return rc;
	}

	memcpy(ME_desc, &dom_desc.u.ME, sizeof(ME_desc_t));

	return 0;
}

/*
 * Retrieve the SPID of a ME.
 *
 * Return 0 if success.
 */
int get_ME_spid(unsigned int slotID, unsigned char *spid) {
	int rc;
	ME_desc_t ME_desc;

	rc = get_ME_desc(slotID, &ME_desc);
	if (rc)
		return rc;

	memcpy(spid, ME_desc.spid, SPID_SIZE);

	return 0;
}

#endif /* CONFIG_SOO_AGENCY */

/*
 * ME side
 */

#ifdef CONFIG_SOO_ME


/*
 * Set the pfn offset after migration
 */
void set_pfn_offset(int pfn_offset)
{
	__pfn_offset = pfn_offset;
}

int get_pfn_offset(void)
{
	return __pfn_offset;
}

/*
 * Get the state of a ME.
 */
int get_ME_state(void)
{
	return avz_shared_info->dom_desc.u.ME.state;
}

void set_ME_state(int state)
{
	avz_shared_info->dom_desc.u.ME.state = state;
}

void perform_task(dc_event_t dc_event)
{

	tell_dc_stable(dc_event);
}


/*
 * do_soo_activity() may be called from the hypervisor as a DOMCALL, but not necessary.
 * The function may also be called as a deferred work during the ME kernel execution.
 */
int do_soo_activity(void *arg)
{

	return 0;
}

/*
 * Agency ctl operations
 */

int agency_ctl(agency_ctl_args_t *agency_ctl_args)
{
	int rc;

	agency_ctl_args->slotID = ME_domID()+1;

	rc = soo_hypercall(AVZ_AGENCY_CTL, NULL, NULL, agency_ctl_args, NULL);
	if (rc != 0) {
		printk("Failed to set directcomm event from hypervisor (%d)\n", rc);
		return rc;
	}

	return 0;
}

ME_desc_t *get_ME_desc(void)
{
	return (ME_desc_t *) &avz_shared_info->dom_desc.u.ME;
}

agency_desc_t *get_agency_desc(void)
{
	return (agency_desc_t *) &avz_shared_info->dom_desc.u.agency;
}


#endif /* CONFIG_SOO_ME */

void flush_all(void)
{

	/* Flush all cache */
	flush_cache_all();
	flush_tlb_all();
}

void soo_guest_activity_init(void)
{
	unsigned int i;

	sema_init(&sooeventd_lock, 0);
	sema_init(&usr_feedback, 0);

	for (i = 0; i < DC_EVENT_MAX; i++) {
		atomic_set(&dc_outgoing_domID[i], -1);
		atomic_set(&dc_incoming_domID[i], -1);

		init_completion(&dc_stable_lock[i]);
	}

	for (i = 0; i < MAX_PENDING_UEVENT; i++)
		pending_uevent_req[i].pending = false;

	INIT_LIST_HEAD(&uevents);

}

