/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2017: Daniel Rossier
 * - January 2018: Baptiste Delporte
 *
 */

#if 0
#define DEBUG
#endif

#include <virtshare/soo.h>
#include <virtshare/console.h>
#include <virtshare/debug.h>

static soo_personality_t personality = SOO_PERSONALITY_INITIATOR;

soo_personality_t soo_get_personality(void) {
	return personality;
}

void soo_set_personality(soo_personality_t pers) {
	personality = pers;
	DBG("Set personality: %d\n", pers);
}
