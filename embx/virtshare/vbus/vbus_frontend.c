/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 * - June 2016: Baptiste Delporte
 * - March 2018: Baptiste Delporte
 */

#if 0
#define DEBUG
#endif

#define DPRINTK(fmt, args...)	pr_debug("vbus_probe (%s:%d) " fmt ".\n",	__func__, __LINE__, ##args)

#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/fcntl.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/sched.h>

#include <asm/page.h>
#include <asm/pgtable.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/soo.h>
#include <virtshare/gnttab.h>
#include <virtshare/vbstore.h>
#include <virtshare/vbstore_me.h>

#include <virtshare/debug.h>
#include <virtshare/console.h>

/* List of frontend */
struct list_head frontends;

/*
 * Walk through the list of frontend devices and perform an action.
 * When the action returns 1, we stop the walking.
 */
void frontend_for_each(void *data, int (*fn)(struct vbus_device *, void *)) {
	struct list_head *pos, *q;
	struct vbus_device *dev;

	list_for_each_safe(pos, q, &frontends)
	{
		dev = list_entry(pos, struct vbus_device, list);

		if (fn(dev, data) == 1)
			return ;
	}
}

void add_new_dev(struct vbus_device *dev) {
	list_add_tail(&dev->list, &frontends);
}

/* device/domID/<type>/<id> => <type>-<id> */
static int frontend_bus_id(char bus_id[VBUS_ID_SIZE], const char *nodename)
{
	/* device/ */
	nodename = strchr(nodename, '/');
	/* domID/ */
	nodename = strchr(nodename+1, '/');
	if (!nodename || strlen(nodename + 1) >= VBUS_ID_SIZE) {
		printk("vbus: bad frontend %s\n", nodename);
		BUG();
	}

	strncpy(bus_id, nodename + 1, VBUS_ID_SIZE);
	if (!strchr(bus_id, '/')) {
		printk("vbus: bus_id %s no slash\n", bus_id);
		BUG();
	}
	*strchr(bus_id, '/') = '-';
	return 0;
}

static void backend_changed(struct vbus_watch *watch)
{
	DBG("Backend changed now: node = %s\n", watch->node);

	vbus_otherend_changed(watch);
}

static char root_name[15];
static char initial_rootname[15];

static struct vbus_type vbus_frontend = {
		.root = "device",
		.get_bus_id = frontend_bus_id,
		.otherend_changed = backend_changed,
};


static int remove_dev(struct vbus_device *dev, void *data)
{
	if (dev->drv == NULL) {
		/* Skip if driver is NULL, i.e. probe failed */
		return 0;
	}

	/* Remove it from the main list */
	list_del(&dev->list);

	/* Removal from vbus namespace */
	vbus_dev_remove(dev);

	return 0;
}

/*
 * Remove a device or all devices present on the bus (if path = NULL)
 */
void remove_devices(void)
{
	frontend_for_each(NULL, remove_dev);
}

static int __device_shutdown(struct vbus_device *dev, void *data)
{
	/* Removal from vbus namespace */
	vbus_dev_shutdown(dev);

	return 0;
}

void devices_shutdown(void) {
	frontend_for_each(NULL, __device_shutdown);
}

/*
 * In frontend drivers, otherend_id refering to the agency or *realtime* agency is equal to 0.
 */
static void read_backend_details(struct vbus_device *vdev)
{
	vbus_read_otherend_details(vdev, "backend-id", "backend");
}

/*
 * The drivers/vbus_fron have to be registered *before* any registered frontend devices.
 */
void vbus_register_frontend(struct vbus_driver *drv)
{
	DBG("Registering driver %s\n", drv->name);

	drv->read_otherend_details = read_backend_details;
	DBG("__vbus_register_frontend\n");

	vbus_register_driver_common(drv);
}

/*
 * Probe a new device on the frontend bus.
 * Typically called by vbstore_dev_init()
 */
int vdev_probe(char *node) {
	char *type, *pos;
	char target[VBS_KEY_LENGTH];

	DBG("%s: probing device: %s\n", __func__, node);

	strcpy(target, node);
	pos = target;
	type = strsep(&pos, "/");    /* "device/" */
	type = strsep(&pos, "/");    /* "device/<domid>/" */
	type = strsep(&pos, "/");    /* "/device/<domid>/<type>" */

	vbus_dev_changed(node, type, &vbus_frontend);

	return 0;
}

void vbus_probe_frontend_init(void)
{
	DBG0("Initializing...\n");

	/* Preserve the initial root node name */
	strcpy(initial_rootname, vbus_frontend.root);

	/* Re-adjust the root node name with the dom ID */

	sprintf(root_name, "%s/%d", vbus_frontend.root, ME_domID());

	vbus_frontend.root = root_name;

	INIT_LIST_HEAD(&frontends);
}




