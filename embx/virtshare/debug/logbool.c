/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2018: Daniel Rossier
 *
 * The logbool hashtable functions provides a helpful mechanism to track the code execution along function call paths.
 * The dump of logbool entries can be obtained by means of a specific keyhandler.
 */


#include <virtshare/debug/logbool.h>

ht_set_t __ht_set;

void ht_set(logbool_hashtable_t *hashtable, char *key, logbool_t value) {

	/* Trampoline function for AVZ logbool hashtable support. */
	__ht_set(hashtable, key, value);
}
