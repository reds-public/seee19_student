/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2018: Baptiste Delporte
 *
 */

#if defined(CONFIG_SOO_AGENCY)

#include <linux/types.h>
#include <linux/ktime.h>
#include <linux/slab.h>

#include <virtshare/debug.h>
#include <virtshare/console.h>

#include <virtshare/debug/time.h>
#include <virtshare/debug/bandwidth.h>

#include <arm_neon.h>

void ll_bandwidth_compute(s64 *delays, size_t size, uint32_t *div, uint32_t *result) {
	uint32_t i;
	s64 sum = 0;
	float div_f, result_f;

	for (i = 0 ; i < N_BANDWIDTH_DELAYS ; i++)
		sum += delays[i];

	div_f = (float) ((uint32_t) sum / N_BANDWIDTH_DELAYS);
	result_f = (float) size / (div_f * 1E-9); /* ns > s */
	result_f *= 8.0; /* B > bits */
	result_f /= 1E6; /* bps > Mbps */

	*div = (uint32_t) div_f;
	*result = (uint32_t) result_f;
}

#endif /* CONFIG_SOO_AGENCY */
