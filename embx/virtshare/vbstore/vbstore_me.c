/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2015-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2015-2018: Daniel Rossier
 * - March 2018: Baptiste Delporte
 *
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/of.h>

#include <virtshare/avz.h>
#include <virtshare/soo.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbstore.h>
#include <virtshare/evtchn.h>
#include <virtshare/guest_api.h>

#include <virtshare/debug.h>

static void vbs_me_rmdir(const char *dir, const char *node) {
	vbus_rm(VBT_NIL, dir, node);
}

static void vbs_me_mkdir(const char *dir, const char *node) {
	vbus_mkdir(VBT_NIL, dir, node);
}

static void vbs_me_write(const char *dir, const char *node, const char *string) {
	vbus_write(VBT_NIL, dir, node, string);
}

/*
 * The following vbstore node creation does not require to be within a transaction
 * since the backend has no visibility on these nodes until it gets the DC_TRIGGER_DEV_PROBE event.
 *
 * <realtime> tells if the device is realtime.
 */
static void vbstore_dev_init(unsigned int domID, const char *devname, bool realtime) {

	char rootname[VBS_KEY_LENGTH];  /* Root name depending on domID */
	char propname[VBS_KEY_LENGTH];  /* Property name depending on domID */
	char devrootname[VBS_KEY_LENGTH];
	unsigned int dir_exists = 0; /* Data used to check if a directory exists */

	DBG("%s: creating vbstore entries for domain %d and dev %s\n", __func__, domID, devname);

	/*
	 * We must check here if the /backend/% entry exists.
	 * If not, this means that there is no backend available for this virtual interface. If so, just abort.
	 */
	strcpy(devrootname, "backend/");
	strcat(devrootname, devname);
	dir_exists = vbus_directory_exists(VBT_NIL, devrootname, "");
	if (!dir_exists) {
		DBG0("Cannot find backend node: %s\n", devrootname);
		return ;
	}

	/* Virtualized interface of dev config */
	sprintf(propname, "%d", domID);
	vbs_me_mkdir("device", propname);

	strcpy(devrootname, "device/%d");

	sprintf(rootname, devrootname, domID);
	vbs_me_mkdir(rootname, devname);

	strcat(devrootname, "/");
	strcat(devrootname, devname);

	sprintf(rootname, devrootname, domID);   /* "/device/%d/vuart"  */
	vbs_me_mkdir(rootname, "0");

	strcat(devrootname, "/0");    /*  "/device/%d/vuart/0"   */

	sprintf(rootname, devrootname, domID);
	vbs_me_write(rootname, "state", "1");  /* = VBusStateInitialising */

	switch (realtime) {

	case true:
		vbs_me_write(rootname, "realtime", "1");
		break;

	case false:
		vbs_me_write(rootname, "realtime", "0");
	}

	vbs_me_write(rootname, "backend-id", "0");

	strcpy(devrootname, "backend/");
	strcat(devrootname, devname);
	strcat(devrootname, "/%d/0");    /* "backend/vuart/%d/0" */

	sprintf(propname, devrootname, domID);
	vbs_me_write(rootname, "backend", propname);

	/* Create the vbstore entries for the backend side */

	sprintf(propname, "%d", domID);
	strcpy(devrootname, "backend/");
	strcat(devrootname, devname);  /* "/backend/vuart"  */

	vbs_me_mkdir(devrootname, propname);

	strcat(devrootname, "/%d");   /* "/backend/vuart/%d" */

	sprintf(rootname, devrootname, domID);
	vbs_me_mkdir(rootname, "0");

	strcat(devrootname, "/0");   /* "/backend/vuart/%d/state/1" */
	sprintf(rootname, devrootname, domID);
	vbs_me_write(rootname, "state", "1");

	switch (realtime) {

	case true:
		vbs_me_write(rootname, "realtime", "1");

		/* The two next entries are used to synchronize RT and non-RT vbus/vbstore. */
		vbs_me_write(rootname, "sync_backfront", "0");

		vbs_me_write(rootname, "sync_backfront_rt", "0");

		break;

	case false:
		vbs_me_write(rootname, "realtime", "0");
	}

	strcpy(devrootname, "device/%d/");
	strcat(devrootname, devname);
	strcat(devrootname, "/0");  /* "device/%d/vuart/0" */

	sprintf(propname, devrootname, domID);
	vbs_me_write(rootname, "frontend", propname);

	sprintf(propname, "%d", domID);
	vbs_me_write(rootname, "frontend-id", propname);

	/* Now, we create the corresponding device on the frontend side */
	strcpy(devrootname, "device/%d/");
	strcat(devrootname, devname);
	strcat(devrootname, "/0");  /* "device/%d/vuart/0" */
	sprintf(propname, devrootname, domID);

	/* Create device structure and gets ready to begin interactions with the backend */
	vdev_probe(propname);
}

static void vbstore_dev_remove(unsigned int domID, const char *devname) {

	char propname[VBS_KEY_LENGTH];  /* Property name depending on domID */
	char devrootname[VBS_KEY_LENGTH];
	unsigned int dir_exists = 0; /* Data used to check if a directory exists */

	DBG("%s: removing vbstore entries for domain %d\n", __func__, domID);

	/*
	 * We must check here if the /backend/% entry exists.
	 * If not, this means that there is no backend available for this virtual interface. If so, just abort.
	 */
	strcpy(devrootname, "backend/");
	strcat(devrootname, devname);
	dir_exists = vbus_directory_exists(VBT_NIL, devrootname, "");
	if (!dir_exists) {
		DBG0("Cannot find backend node: %s\n", devrootname);
		return ;
	}


	/* Remove virtualized interface of vuart config */
	sprintf(propname, "%d/", domID);

	strcpy(devrootname, "device/");
	strcat(devrootname, propname);
	strcat(devrootname, devname); /* "/device/vuart/" */

	vbs_me_rmdir(devrootname, "0");

	/* Remove the vbstore entries for the backend side */

	sprintf(propname, "/%d", domID);

	strcpy(devrootname, "backend/");
	strcat(devrootname, devname);   /* "/backend/vuart/" */
	strcat(devrootname, propname);  /* "/backend/vuart/2" */

	vbs_me_rmdir(devrootname, "0");
}

/*
 * Remove all vbstore entries related to this ME.
 */
void remove_vbstore_entries(void) {
	struct device_node *fdt_node;

	fdt_node = of_find_compatible_node(NULL, NULL, "vuart,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: removing vuart from vbstore...\n", __func__);
		vbstore_dev_remove(ME_domID(), "vuart");
	}

	fdt_node = of_find_compatible_node(NULL, NULL, "vdummy,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: removing vdummy from vbstore...\n", __func__);
		vbstore_dev_remove(ME_domID(), "vdummy");
	}

	fdt_node = of_find_compatible_node(NULL, NULL, "vfb,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: removing vfb from vbstore...\n", __func__);
		vbstore_dev_remove(ME_domID(), "vfb");
	}

	fdt_node = of_find_compatible_node(NULL, NULL, "vsp6,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: removing vsp6 from vbstore...\n", __func__);
		vbstore_dev_remove(ME_domID(), "vsp6");
	}
}

/*
 * Populate vbstore with all necessary properties required by the frontend drivers.
 */
void vbstore_devices_populate(void) {
	struct device_node *fdt_node;

	DBG0("Populate vbstore\n");

	fdt_node = of_find_compatible_node(NULL, NULL, "vuart,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: init vuart...\n", __func__);
		vbstore_dev_init(ME_domID(), "vuart", false);
	}

	fdt_node = of_find_compatible_node(NULL, NULL, "vdummy,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: init vdummy...\n", __func__);
		vbstore_dev_init(ME_domID(), "vdummy", false);
	}

	fdt_node = of_find_compatible_node(NULL, NULL, "vfb,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: init vfb...\n", __func__);
		vbstore_dev_init(ME_domID(), "vfb", false);
	}

	fdt_node = of_find_compatible_node(NULL, NULL, "vsp6,frontend");
	if (of_device_is_available(fdt_node)) {
		DBG("%s: init vsp6...\n", __func__);
		vbstore_dev_init(ME_domID(), "vsp6", false);
	}
}

void vbstore_trigger_dev_probe(void) {
	DBG("%s: sending DC_TRIGGER_DEV_PROBE to the agency...\n", __func__);

	/* Trigger the probe on the backend side. */
	do_sync_dom(DOMID_AGENCY, DC_TRIGGER_DEV_PROBE);
}

/*
 * Prepare the vbstore entries used by this ME.
 */
void vbstore_me_init(void) {

	vbus_probe_frontend_init();

	/* Regarding MEs, avz_start_info->store_mfn is mapped statically at 0xffe01000
	 * in trap_init() earlier in the startup sequence */

	__intf = (vbstore_intf_t *) HYPERVISOR_VBSTORE_VADDR;

	/* Initialize the interface to vbstore. */

	vbus_vbstore_init();

}

void vbstore_init_dev_populate(void) {

	/*
	 * Now, the ME requests to be paused by setting its state to ME_state_preparing. As a consequence,
	 * the agency will pause it.
	 */

	DBG0("Now ready to register vbstore entries\n");

	/* Now, we are ready to register vbstore entries */
	vbstore_devices_populate();

	vbstore_trigger_dev_probe();
}



