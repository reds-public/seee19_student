
/*
 * Copyright (C) 2014-2016 Daniel Rossier <daniel.rossier@heig-vd.ch>
 * Copyright (C) 2016 Baptiste Delporte <bonel@bonel.neth>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/irqdesc.h>
#include <linux/irqnr.h>
#include <linux/irqdomain.h>
#include <linux/sched.h>
#include <linux/kernel_stat.h>
#include <linux/random.h>
#include <linux/bootmem.h>
#include <linux/kthread.h>
#include <linux/ipipe_base.h>

#include <asm/atomic.h>
#include <asm/ptrace.h>
#include <asm/irqflags.h>

#include <asm-generic/irq_regs.h>

#include <virtshare/hypervisor.h>
#include <virtshare/evtchn.h>
#include <virtshare/event_channel.h>
#include <virtshare/physdev.h>
#include <virtshare/console.h>
#include <virtshare/debug.h>
#include <virtshare/debug/dbgvar.h>

/* Convenient shorthand for packed representation of an unbound IRQ. */
#define IRQ_UNBOUND	mk_irq_info(IRQT_UNBOUND, 0, 0)

#undef NR_IRQS
#define NR_IRQS (NR_PIRQS + NR_VIRTIRQS)
/*
 * This lock protects updates to the following mapping and reference-count
 * arrays. The lock does not need to be acquired to read the mapping tables.
 */
static DEFINE_SPINLOCK(irq_mapping_update_lock);

#if defined(CONFIG_IRQ_DOMAIN) && defined(CONFIG_SOO_AGENCY)
extern struct irq_domain *__irq_domain;
#endif

static int irq_bindcount[NR_IRQS];		/* Reference counts for bindings to IRQs. */

typedef struct {
	int evtchn_to_irq[NR_EVTCHN];	/* evtchn -> IRQ */
	u32 irq_to_evtchn[NR_IRQS];	/* IRQ -> evtchn */
	bool valid[NR_EVTCHN]; /* Indicate if the event channel can be used for notification for example */
} evtchn_info_t;

static DEFINE_PER_CPU(evtchn_info_t, evtchn_info);

DEFINE_PER_CPU(bool, in_upcall_progress);

/*
 * Accessors for packed IRQ information.
 */

/*
 * Get the evtchn from a irq_data structure which contains a (unique) CPU bound to the IRQ processing.
 */
inline unsigned int evtchn_from_irq_and_cpu(unsigned int irq, unsigned int cpu) {
	return per_cpu(evtchn_info, cpu).irq_to_evtchn[irq];
}

inline unsigned int evtchn_from_irq(int irq)
{
	return evtchn_from_irq_and_cpu(irq, smp_processor_id());
}

inline unsigned int evtchn_from_irq_data(struct irq_data *irq_data)
{
	return evtchn_from_irq(irq_data->irq);
}

/* __evtchn_from_irq : helpful for irq_chip handlers */
#define __evtchn_from_irq   evtchn_from_irq_data

static inline unsigned evtchn_is_masked(volatile shared_info_t *sh, unsigned int b) {
	return test_bit(b, sh->evtchn_mask);
}

void dump_evtchn_pending(void) {

	int i;
	unsigned char *ptr_pending, *ptr_mask;
	volatile shared_info_t *s = avz_shared_info;

#if 0
	lprintk("   Evtchn info in Agency/ME domain %d\n\n", ME_domID());
	for (i = 0; i < NR_EVTCHN; i++) {

		lprintk("e:%d m:%d p:%d  ", i, test_bit(i, s->evtchn_mask), test_bit(i, s->evtchn_pending));
	}
	lprintk("\n\n");
#endif

	ptr_pending = (unsigned char *) &s->evtchn_pending;
	ptr_mask =(unsigned char *) &s->evtchn_mask;

	lprintk("%s: evtchn pending: ", __func__);
	for (i = 0; i < 32; i++) {
		lprintk("%.2lx ", (unsigned long) *ptr_pending);
		ptr_pending++;
	}
	lprintk("\n");

	lprintk("%s: evtchn mask   : ", __func__);
	for (i = 0; i < 32; i++) {
		lprintk("%.2lx ", (unsigned long) *ptr_mask);
		ptr_mask++;
	}
	lprintk("\n");

}
EXPORT_SYMBOL(dump_evtchn_pending);


/* NB. Interrupts are disabled on entry. */
extern asmlinkage void asm_do_IRQ(unsigned int irq, struct pt_regs *regs);
static struct irq_chip virtirq_chip;

/*
 * evtchn_do_upcall
 *
 * This is the main entry point for processing IRQs and VIRQs for this domain.
 *
 * The function runs with IRQs OFF during the whole execution of the function. As such, this function is executed in top half processing of the IRQ.
 * - All pending event channels are processed one after the other, according to their bit position within the bitmap.
 * - Checking pending IRQs in AVZ (low-level IRQ) is performed at the end of the loop so that we can react immediately if some new IRQs have been generated
 *   and are present in the GIC.
 *
 *  Ipipe behaviour with interrupts.
 * -> if ipipe_request() is called (for example from rtdm_request_irq(), for a PIRQ, it is OK, for a VIRQ, it is necessary to install a ack function of level type (ipipe_level_handler -> see timer)
 *
 * -> For VIRQ_TIMER_IRQ, avoid change the bind_virq_to_irqhandler.....
 *
 */

asmlinkage void evtchn_do_upcall(struct pt_regs *regs)
{
	unsigned int   evtchn;
	int            l1, irq;
	volatile shared_info_t *s = avz_shared_info;
	volatile vcpu_info_t *vcpu_info = &s->vcpu_info;

	int loopmax = 0;
	int at_least_one_processed;

	BUG_ON(!hard_irqs_disabled());

	/* It may happen than an hypercall is executed during a top half ISR. Since the hypercall will
	 * call this function along its return path.
	 */

	if (per_cpu(in_upcall_progress, smp_processor_id()))
		return ;

	/* Reptar scheme: entering here from the hypervisor means we mostly go downwards via
	 * a timer IRQ, meaning the guest can be safely considered with IRQs ON.
	 */
#if 0
	/* Check if the (local) IRQs are off. In this case, pending events are not processed at this time,
	 * but will be once the local IRQs will be re-enabled (either by the GIC loop or an active assert
	 * of the IRQ line).
	 */

	if (irqs_disabled_flags(regs->uregs[16])) /* uregs[16] matches with SPSR */
		return ;
#endif

	per_cpu(in_upcall_progress, smp_processor_id()) = true;

retry:

	l1 = xchg(&vcpu_info->evtchn_upcall_pending, 0);

	evtchn = find_first_bit((void *) &s->evtchn_pending, NR_EVTCHN);

	do {
		at_least_one_processed = 0; /* If all interrupts are masked, we avoid to loop at infinity */

		while (evtchn < NR_EVTCHN) {
#warning to be validated...
			//BUG_ON(!evtchn_info.valid[evtchn]);

			loopmax++;

			if (loopmax > 500)   /* Probably something wrong ;-) */
				lprintk("%s: Warning trying to process evtchn: %d IRQ: %d for quite a long time (dom ID: %d) on CPU %d / masked: %d...\n",
						__func__, evtchn, per_cpu(evtchn_info, smp_processor_id()).evtchn_to_irq[evtchn], ME_domID(), smp_processor_id(), evtchn_is_masked(s, evtchn));

			if (!evtchn_is_masked(s, evtchn)) {

				at_least_one_processed = 1;

				irq = per_cpu(evtchn_info, smp_processor_id()).evtchn_to_irq[evtchn];

				clear_evtchn(evtchn_from_irq(irq));

				asm_do_IRQ(irq, regs);

				BUG_ON(!hard_irqs_disabled());

			}
			evtchn = find_next_bit((void *) &s->evtchn_pending, NR_EVTCHN, evtchn+1);
		}

	} while (at_least_one_processed);

	if (vcpu_info->evtchn_upcall_pending)
		goto retry;

	per_cpu(in_upcall_progress, smp_processor_id()) = false;
}

static int find_unbound_irq(void)
{
	int irq;

	for (irq = 0; irq < NR_IRQS; irq++)
		if (irq_bindcount[irq] == 0)
			break;

	if (irq == NR_IRQS)
		panic("No available IRQ to bind to: increase NR_IRQS!\n");

	return irq;
}

bool in_upcall_process(void) {
	return per_cpu(in_upcall_progress, smp_processor_id());
}

static int bind_evtchn_to_irq(unsigned int evtchn)
{
	int irq;
	int cpu = smp_processor_id();

	spin_lock(&irq_mapping_update_lock);

	if ((irq = per_cpu(evtchn_info, cpu).evtchn_to_irq[evtchn]) == -1) {
		irq = find_unbound_irq();
		per_cpu(evtchn_info, cpu).evtchn_to_irq[evtchn] = irq;
		per_cpu(evtchn_info, cpu).irq_to_evtchn[irq] = evtchn;
		per_cpu(evtchn_info, cpu).valid[evtchn] = true;

	}

	irq_bindcount[irq]++;

	spin_unlock(&irq_mapping_update_lock);

	return irq;
}

int unbind_domain_evtchn(unsigned int domID, unsigned int evtchn)
{
	struct evtchn_bind_interdomain bind_interdomain;
	int err;

	bind_interdomain.remote_dom = domID;
	bind_interdomain.local_evtchn = evtchn;

	err = hypercall_trampoline(__HYPERVISOR_event_channel_op, EVTCHNOP_unbind_domain, (long) &bind_interdomain, 0, 0);

	evtchn_info.valid[evtchn] = false;

	return err;
}

static int bind_interdomain_evtchn_to_irq(unsigned int remote_domain, unsigned int remote_evtchn)
{
	struct evtchn_bind_interdomain bind_interdomain;
	int err;

	bind_interdomain.remote_dom  = remote_domain;
	bind_interdomain.remote_evtchn = remote_evtchn;

	err = hypercall_trampoline(__HYPERVISOR_event_channel_op, EVTCHNOP_bind_interdomain, (long) &bind_interdomain, 0, 0);

	return err ? : bind_evtchn_to_irq(bind_interdomain.local_evtchn);
}

int bind_existing_interdomain_evtchn(unsigned local_evtchn, unsigned int remote_domain, unsigned int remote_evtchn)
{
	struct evtchn_bind_interdomain bind_interdomain;
	int err;

	bind_interdomain.local_evtchn = local_evtchn;
	bind_interdomain.remote_dom  = remote_domain;
	bind_interdomain.remote_evtchn = remote_evtchn;

	err = hypercall_trampoline(__HYPERVISOR_event_channel_op, EVTCHNOP_bind_existing_interdomain, (long) &bind_interdomain, 0, 0);

	return err ? : bind_evtchn_to_irq(bind_interdomain.local_evtchn);
}
EXPORT_SYMBOL_GPL(bind_existing_interdomain_evtchn);

static void unbind_from_irq(unsigned int irq)
{
	evtchn_close_t op;
	int evtchn = evtchn_from_irq(irq);
	int cpu = smp_processor_id();
	int ret;

	spin_lock(&irq_mapping_update_lock);

	if (--irq_bindcount[irq] == 0) {
		op.evtchn = evtchn;

		ret = hypercall_trampoline(__HYPERVISOR_event_channel_op, EVTCHNOP_close, (long) &op, 0, 0);
		BUG_ON(ret != 0);

		per_cpu(evtchn_info, cpu).evtchn_to_irq[evtchn] = -1;
		per_cpu(evtchn_info, cpu).valid[evtchn] = false;
	}

	spin_unlock(&irq_mapping_update_lock);
}


int bind_evtchn_to_irq_handler(unsigned int evtchn, irq_handler_t handler, irq_handler_t thread_fn, unsigned long irqflags, const char *devname, void *dev_id)
{
	unsigned int irq;
	int retval;

	irq = bind_evtchn_to_irq(evtchn);
	retval = request_threaded_irq(irq, handler, thread_fn, irqflags, devname, dev_id);
	if (retval != 0) {
		unbind_from_irq(irq);
		return retval;
	}

	return irq;
}
EXPORT_SYMBOL_GPL(bind_evtchn_to_irq_handler);

int bind_interdomain_evtchn_to_irqhandler(unsigned int remote_domain, unsigned int remote_evtchn, irq_handler_t handler, irq_handler_t thread_fn, unsigned long irqflags, const char *devname, void *dev_id)
{
	int irq, retval;

	DBG("%s: devname = %s / remote evtchn: %d remote domain: %d\n", __func__, devname, remote_evtchn, remote_domain);
	irq = bind_interdomain_evtchn_to_irq(remote_domain, remote_evtchn);
	if (irq < 0)
		return irq;

	if (handler != NULL) {
		retval = request_threaded_irq(irq, handler, thread_fn, irqflags, devname, dev_id);
		if (retval != 0)
			BUG();
	}

	return irq;
}
EXPORT_SYMBOL_GPL(bind_interdomain_evtchn_to_irqhandler);

void unbind_from_irqhandler(unsigned int irq, void *dev_id)
{
	free_irq(irq, dev_id);
	unbind_from_irq(irq);
}
EXPORT_SYMBOL_GPL(unbind_from_irqhandler);

/*
 * Interface to generic handling in irq.c
 */
static unsigned int startup_virtirq(struct irq_data *irq)
{
	int evtchn = __evtchn_from_irq(irq);

	unmask_evtchn(evtchn);
	return 0;
}

static void shutdown_virtirq(struct irq_data *irq)
{
	int evtchn = __evtchn_from_irq(irq);

	mask_evtchn(evtchn);
}

static void enable_virtirq(struct irq_data *irq)
{
	int evtchn = __evtchn_from_irq(irq);

	unmask_evtchn(evtchn);
}

static void disable_virtirq(struct irq_data *irq)
{
	int evtchn = __evtchn_from_irq(irq);

	mask_evtchn(evtchn);
}

static struct irq_chip virtirq_chip = {
		.name             = "avz_virt-irq",
		.irq_startup      = startup_virtirq,
		.irq_shutdown     = shutdown_virtirq,
		.irq_enable       = enable_virtirq,
		.irq_disable      = disable_virtirq,
		.irq_unmask       = enable_virtirq,
		.irq_mask         = disable_virtirq,
};


void notify_remote_via_irq(int irq)
{
	int evtchn = evtchn_from_irq(irq);

	notify_remote_via_evtchn(evtchn);
}
EXPORT_SYMBOL_GPL(notify_remote_via_irq);


void mask_evtchn(int evtchn)
{
	volatile shared_info_t *s = avz_shared_info;

	transaction_set_bit(evtchn, &s->evtchn_mask[0]);

}
EXPORT_SYMBOL_GPL(mask_evtchn);

void unmask_evtchn(int evtchn)
{
	volatile shared_info_t *s = avz_shared_info;

	transaction_clear_bit(evtchn, &s->evtchn_mask[0]);
}
EXPORT_SYMBOL_GPL(unmask_evtchn);

void virtshare_mask_irq(struct irq_data *irq_data) {
	int evtchn;
	volatile shared_info_t *s;

#if (LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0))
	if (cpumask_test_cpu(1, irq_data->common->affinity))
#else
	if (cpumask_test_cpu(1, irq_data->affinity))
#endif

	{
		s = HYPERVISOR_shared_info->subdomain_shared_info;
		evtchn = evtchn_from_irq_and_cpu(irq_data->irq, 1);
	} else {
		s = HYPERVISOR_shared_info;
		evtchn = evtchn_from_irq_and_cpu(irq_data->irq, 0);
	}

	transaction_set_bit(evtchn, &s->evtchn_mask[0]);

}
EXPORT_SYMBOL_GPL(virtshare_mask_irq);

void virtshare_unmask_irq(struct irq_data *irq_data) {
	int evtchn;
	volatile shared_info_t *s;

#if (LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0))
	if (cpumask_test_cpu(1, irq_data->common->affinity))
#else
	if (cpumask_test_cpu(1, irq_data->affinity))
#endif
	{
		s = HYPERVISOR_shared_info->subdomain_shared_info;
		evtchn = evtchn_from_irq_and_cpu(irq_data->irq, 1);
	} else {
		s = HYPERVISOR_shared_info;
		evtchn = evtchn_from_irq_and_cpu(irq_data->irq, 0);
	}

	transaction_clear_bit(evtchn, &s->evtchn_mask[0]);
}
EXPORT_SYMBOL_GPL(virtshare_unmask_irq);

int irq_masked[256];

void virtshare_init_irq(void)
{
	struct irq_desc *irqdesc;

	int i, cpu;
	cpumask_t cpumask = CPU_MASK_ALL;

	/*
	 * For each CPU, initialize event channels for all IRQs.
	 * An IRQ will processed by only one CPU, but it may be rebound to another CPU as well.
	 */

	for_each_cpu(cpu, &cpumask) {

		per_cpu(in_upcall_progress, cpu) = false;

		/* No event-channel -> IRQ mappings. */
		for (i = 0; i < NR_EVTCHN; i++) {
			per_cpu(evtchn_info, cpu).evtchn_to_irq[i] = -1;
			per_cpu(evtchn_info, cpu).valid[i] = false;
			mask_evtchn(i); /* No event channels are 'live' right now. */
		}

#ifdef CONFIG_SPARSE_IRQ
		irq_alloc_descs(VIRTIRQ_BASE, VIRTIRQ_BASE, NR_VIRTIRQS, numa_node_id());
#endif

		/* Dynamic IRQ space is currently unbound. Zero the refcnts. */
		for (i = 0; i < NR_VIRTIRQS; i++)
			irq_bindcount[virtirq_to_irq(i)] = 0;

		for (i = 0; i < NR_PIRQS; i++)
			irq_bindcount[pirq_to_irq(i)] = 1;
	}

	/* Configure the irqdesc associated to VIRQs */
	for (i = 0; i < NR_VIRTIRQS; i++) {
		irqdesc = irq_to_desc(virtirq_to_irq(i));

#if (LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0))
		irqdesc->irq_data.common->state_use_accessors |= IRQD_IRQ_DISABLED;
		irqdesc->status_use_accessors &= ~IRQ_NOREQUEST;
#else
		irqdesc->irq_data.state_use_accessors |= IRQD_IRQ_DISABLED;
#endif

		irqdesc->action = NULL;
		irqdesc->depth = 1;
		irqdesc->irq_data.chip = &virtirq_chip;
		irq_set_handler(virtirq_to_irq(i), handle_level_irq);

#if (LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0))
		set_irq_flags(virtirq_to_irq(i), IRQF_VALID);
#endif
	}

	for (i = 0; i < 256; i++)
		irq_masked[i] = 0;
}
