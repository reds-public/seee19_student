/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA/AG, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/serial.h>
#include <linux/major.h>
#include <linux/ptrace.h>
#include <linux/ioport.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/console.h>
#include <linux/bootmem.h>
#include <linux/sysrq.h>
#include <linux/serial_core.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/uaccess.h>
#include <virtshare/evtchn.h>
#include <virtshare/hypervisor.h>
#include <virtshare/avzcons.h>
#include <asm/irq_regs.h>

/* 1 is the code for CTRL-A */
#define SWITCH_CODE 1

#define N_SWITCH_FOCUS 2

/* Volatile to ensure the value is read from memory and no optimization occurs */
static int volatile avzcons_active = 0;
static int volatile vfb_active = 0;

/* Provided by vUART backend */
void me_cons_sendc(domid_t domid, uint8_t ch);

int avzcons_get_focus(void)
{
	return avzcons_active;
}

int avzcons_set_focus(int next_domain)
{
	avzcons_active = next_domain;
	return avzcons_active;
}

int vfb_get_focus(void)
{
	return vfb_active;
}
EXPORT_SYMBOL(vfb_get_focus);

int vfb_set_focus(int next_domain)
{
	vfb_active = next_domain;
	return vfb_active;
}
EXPORT_SYMBOL(vfb_set_focus);


/*
 * avz_switch_console() - Allow the user to give input focus to various input sources (agency, MEs, avz)
 *
 * Warning !! We are in interrupt context top half when the function is called and a lock is pending
 * on the UART. Use of printk() is forbidden and we need to use lprintk() to avoid deadlock.
 *
 */
int avz_switch_console(char ch)
{
	static int switch_code_count = 0;
#if 0
	static char *input_str[N_SWITCH_FOCUS] = { "Agency domain", "ME-1", "ME-2", "ME-3", "ME-4", "ME-5", "Agency AVZ Hypervisor" };
#endif
	static char *input_str[N_SWITCH_FOCUS] = { "Agency domain (dom 0)", "ME (dom 1)" };
	int active = 0;
	int next = 1;

/* Debugging purpose - enabled forces to forward to an ME */
#if 0
	me_cons_sendc(1, ch);
#endif

	if ((SWITCH_CODE != 0) && (ch == SWITCH_CODE)) {
		/* We eat CTRL-<switch_char> in groups of 2 to switch console input. */
		if (++switch_code_count == 1) {

			active = avzcons_set_focus((avzcons_get_focus() + 1) % N_SWITCH_FOCUS);
			next = (avzcons_get_focus() + 1) % N_SWITCH_FOCUS;
			switch_code_count = 0;

			lprintk("*** Serial input -> %s (type 'CTRL-%c' twice to switch input to %s).\n", input_str[active], 'a', input_str[next]);

			return 1;
		}

	}
	else {
		switch_code_count = 0;

		switch (avzcons_get_focus()) {
		default:
		case 0: /* Input to the agency */
			return 0;
		case 1: /* Input to ME #1 */
		case 2: /* Input to ME #2 */
		case 3: /* Input to ME #3 */
		case 4: /* Input to ME #4 */
		case 5: /* Input to ME #5 */
#ifdef CONFIG_VUART_BACKEND
			me_cons_sendc(avzcons_get_focus(), ch);
#endif /* CONFIG_VUART_BACKEND */
			return 1;
		case 6: /* Input to avz */
			hypercall_trampoline(__HYPERVISOR_console_io, CONSOLEIO_process_char, 1, (long) &ch, 0);
			return 1;
		}
	}

	return 0;

}

