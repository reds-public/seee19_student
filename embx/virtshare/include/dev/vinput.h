/******************************************************************************
 * inputif.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef VINPUT_H
#define VINPUT_H

#include <linux/input.h>

#include <virtshare/ring.h>
#include <virtshare/grant_table.h>

struct vinput_request {
  uint32_t cmd;  /* Not used right now */

};

struct vinput_response {
	unsigned int type;
	unsigned int code;
	int value;
};

/*
 * Generate vinput ring structures and types.
 */
DEFINE_RING_TYPES(vinput, struct vinput_request, struct vinput_response);

/* Bridging with the Linux input subsystem */

#ifdef CONFIG_SOO_AGENCY

extern struct hid_device *__hiddev;
int vinput_pass_event(struct input_dev *dev, unsigned int type, unsigned int code, int value);
void vinput_connect(void);
void vinput_disconnect(void);

#endif

#ifdef CONFIG_SOO_ME
extern void input_event(struct input_dev *dev, unsigned int type, unsigned int code, int value);
bool kbd_present(void);
#endif

#endif /* VINPUT_H */
