/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Daniel Rossier
 * - January 2019: Baptiste Delporte
 *
 */

#ifndef VUART_H
#define VUART_H

#include <virtshare/ring.h>
#include <virtshare/grant_table.h>

#if defined(CONFIG_SOO_AGENCY)
#include <linux/vt_kern.h>
#endif /* CONFIG_SOO_AGENCY */

#define VUART_NAME	"vuart"
#define VUART_PREFIX	"[" VUART_NAME "] "

#define INPUT_TRANSMIT_DESTINATION_VTTY (1 << 0)
#define INPUT_TRANSMIT_DESTINATION_TTY (1 << 1)

#if defined(CONFIG_SOO_AGENCY)
#define MAX_BUF_CHARS	16
#endif /* CONFIG_SOO_AGENCY */

typedef struct {
	uint8_t c;
	uint8_t	pad[1];
} vuart_request_t;

typedef struct {
	uint8_t c;
	uint8_t	pad[1];
} vuart_response_t;

DEFINE_RING_TYPES(vuart, vuart_request_t, vuart_response_t);

bool vuart_ready(void);

#if defined(CONFIG_SOO_ME)

extern struct vc_data *__vc;

void vuart_write(char *buffer, int count);
void send_to_console(char ch);

#endif

#endif /* VUART_H */
