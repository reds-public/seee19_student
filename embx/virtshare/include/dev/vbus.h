/*****************************************************************************
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015,2016 Sootech SA, Switzerland
 * 
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef _IO_VBUS_H
#define _IO_VBUS_H

/*
 * The following states are used to synchronize backend and frontend activities.
 * Detailed description is given in the SOO Framework technical reference.
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 *
 */
enum vbus_state
{
	VbusStateUnknown      = 0,
	VbusStateInitialising = 1,
	VbusStateInitWait     = 2,
	VbusStateInitialised  = 3,
	VbusStateConnected    = 4,
	VbusStateClosing      = 5,
	VbusStateClosed       = 6,
	VbusStateReconfiguring = 7,
	VbusStateReconfigured  = 8,
	VbusStateSuspending    = 9,
	VbusStateSuspended     = 10,
	VbusStateResuming      = 11

};

#endif /* _IO_VBUS_H */
