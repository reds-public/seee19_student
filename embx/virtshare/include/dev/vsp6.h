/******************************************************************************
 * dummyif.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef VSP6_H
#define VSP6_H

#include <virtshare/ring.h>
#include <virtshare/grant_table.h>

#define VSP6_PACKET_SIZE	4

#define VSP6_NAME		"vsp6"
#define VSP6_PREFIX		"[" VSP6_NAME "] "

typedef struct {

    /* Just to have a successful compilation. Can be removed later on... */
    char to_be_removed;
    
	/* to be completed */

} vsp6_request_t;

typedef struct  {

	/* to be completed */

} vsp6_response_t;

/*
 * Generate blkif ring structures and types.
 */
DEFINE_RING_TYPES(vsp6, vsp6_request_t, vsp6_response_t);

void vsp6_send_button(int button_nr);

#endif /* VSP6_H */
