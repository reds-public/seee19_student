/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - May 2018: David Truan
 * - November-December 2018: Baptiste Delporte
 *
 */

#ifndef VDOGA12V6NM_H
#define VDOGA12V6NM_H

#include <virtshare/ring.h>
#include <virtshare/grant_table.h>

#if defined(CONFIG_SOO_AGENCY)
#include <asm/ioctl.h>
#endif /* CONFIG_SOO_aAGENCY */

#if defined(CONFIG_SOO_ME)
#include <ioctl.h>
#endif /* CONFIG_SOO_ME */

#define VDOGA12V6NM_NAME	"vdoga12v6nm"
#define VDOGA12V6NM_PREFIX	"[" VDOGA12V6NM_NAME "] "

#define VDOGA12V6NM_DEV_MAJOR	120
#define VDOGA12V6NM_DEV_NAME	"/dev/vdoga12v6nm"

#define VDOGA12V6NM_PACKET_SIZE		1024

/* ioctl codes */
#define VDOGA12V6NM_ENABLE			_IOW(0x5000d08au, 1, uint32_t)
#define VDOGA12V6NM_DISABLE			_IOW(0x5000d08au, 2, uint32_t)
#define VDOGA12V6NM_SET_PERCENTAGE_SPEED	_IOW(0x5000d08au, 3, uint32_t)
#define VDOGA12V6NM_SET_ROTATION_DIRECTION	_IOW(0x5000d08au, 4, uint32_t)

#define MAX_BUF_CMDS	8

typedef struct {
	uint32_t	cmd;
	uint32_t	arg;
	bool		response_needed;
} vdoga12v6nm_cmd_request_t;

typedef struct  {
	uint32_t	cmd;
	uint32_t	arg;
	uint32_t	ret;
} vdoga12v6nm_cmd_response_t;

DEFINE_RING_TYPES(vdoga12v6nm_cmd, vdoga12v6nm_cmd_request_t, vdoga12v6nm_cmd_response_t);

#if defined(CONFIG_SOO_AGENCY)

/* PL3: GPIO number = (12 - 1) x 32 + 2 = 355 */
#define UP_GPIO		355

/* PD20: GPIO number = (3 - 1) x 32 + 3 = 116 */
#define DOWN_GPIO	116

/* GPIO polling period expressed in ms */
#define GPIO_POLL_PERIOD	100

#endif /* CONFIG_SOO_AGENCY */

#if defined(CONFIG_SOO_ME)

void vdoga12v6nm_enable(void);
void vdoga12v6nm_disable(void);
void vdoga12v6nm_set_percentage_speed(uint32_t speed);
void vdoga12v6nm_set_rotation_direction(uint8_t direction);

#endif /* CONFIG_SOO_ME */

#endif /* VDOGA12V6NM_H */
