/******************************************************************************
 * dummyif.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef VDUMMYRT_H
#define VDUMMYRT_H

#include <virtshare/ring.h>
#include <virtshare/grant_table.h>

#define VDUMMYRT_PACKET_SIZE	32

#define VDUMMYRT_NAME		"vdummyrt"
#define VDUMMYRT_PREFIX		"[" VDUMMYRT_NAME "] "

typedef struct {
	char buffer[VDUMMYRT_PACKET_SIZE];
} vdummyrt_request_t;

typedef struct  {
	char buffer[VDUMMYRT_PACKET_SIZE];
} vdummyrt_response_t;

/*
 * Generate blkif ring structures and types.
 */
DEFINE_RING_TYPES(vdummyrt, vdummyrt_request_t, vdummyrt_response_t);

#endif /* VDUMMYRT_H */
