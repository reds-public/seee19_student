/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - February-May 2018: Baptiste Delporte
 *
 */

#ifndef VLEDS_H
#define VLEDS_H

#include <virtshare/ring.h>

#include <asm/ioctl.h>

#define VLEDS_NAME	"vleds"
#define VLEDS_PREFIX	"[" VLEDS_NAME "] "

#define VLEDS_MAJOR			126
#define VLEDS_DEV_NAME			"soo/" VLEDS_NAME
#define VLEDS_DEV			"/dev/" VLEDS_DEV_NAME

#define VLEDS_N_LEDS			6

#define VLEDS_IOCTL_SET_BRIGHTNESS	_IOW(0x500001edu, 1, uint32_t)
#define VLEDS_IOCTL_SET_ON		_IOW(0x500001edu, 2, uint32_t)
#define VLEDS_IOCTL_SET_OFF		_IOW(0x500001edu, 3, uint32_t)
#define VLEDS_IOCTL_SET_BLINK		_IOW(0x500001edu, 4, uint32_t)

#define VLEDS_MAX_COMMANDS	1024

typedef struct {
	uint32_t cmd;
	uint32_t arg;
} vleds_cmd_request_t;

/* Not used */
typedef struct {
	uint32_t val;
} vleds_cmd_response_t;

DEFINE_RING_TYPES(vleds_cmd, vleds_cmd_request_t, vleds_cmd_response_t);

#if defined(CONFIG_SOO_ME)
void vleds_send_cmd(uint32_t cmd, uint32_t arg);
#endif /* CONFIG_SOO_ME */

#endif /* VLEDS_H */
