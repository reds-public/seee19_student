/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018,2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - March-May 2018: Baptiste Delporte
 * - November 2018: Baptiste Delporte
 * - February-March 2019: Baptiste Delporte
 *
 */

#ifndef VUIHANDLER_H
#define VUIHANDLER_H

#include <virtshare/soo.h>

#include <virtshare/ring.h>
#include <virtshare/grant_table.h>

#define VUIHANDLER_NAME		"vuihandler"
#define VUIHANDLER_PREFIX	"[" VUIHANDLER_NAME "] "

#define VUIHANDLER_DEV_MAJOR	121
#define VUIHANDLER_DEV_NAME	"/dev/vuihandler"

/* Periods and delays in ms */

#define VUIHANDLER_APP_WATCH_PERIOD	10000
#define VUIHANDLER_APP_RSP_TIMEOUT	2000
#define VUIHANDLER_APP_VBSTORE_DIR	"backend/" VUIHANDLER_NAME
#define VUIHANDLER_APP_VBSTORE_NODE	"connected-app-me-spid"

typedef struct {
	uint8_t		type;
	uint8_t		spid[SPID_SIZE];
	uint8_t		payload[0];
} vuihandler_pkt_t;

#define VUIHANDLER_MAX_PACKETS		8
/* Maximal size of a BT packet payload */
#define VUIHANDLER_MAX_PAYLOAD_SIZE	1024
/* Maximal size of a BT packet's data (the header is included) */
#define VUIHANDLER_MAX_PKT_SIZE		(sizeof(vuihandler_pkt_t) + VUIHANDLER_MAX_PAYLOAD_SIZE)
/* Shared buffer size */
#define VUIHANDLER_BUFFER_SIZE		(VUIHANDLER_MAX_PACKETS * VUIHANDLER_MAX_PKT_SIZE)

#define VUIHANDLER_BEACON	0
#define VUIHANDLER_DATA		1

#define VUIHANDLER_BT_PKT_HEADER_SIZE	sizeof(vuihandler_pkt_t)

typedef struct {
	uint32_t		id;
	size_t			size;
} vuihandler_tx_request_t;

/* Not used */
typedef struct {
	uint32_t		val;
} vuihandler_tx_response_t;

DEFINE_RING_TYPES(vuihandler_tx, vuihandler_tx_request_t, vuihandler_tx_response_t);

/* Not used */
typedef struct {
	uint32_t		val;
} vuihandler_rx_request_t;

typedef struct {
	uint32_t		id;
	size_t			size;
} vuihandler_rx_response_t;

DEFINE_RING_TYPES(vuihandler_rx, vuihandler_rx_request_t, vuihandler_rx_response_t);

typedef struct {
	uint8_t			spid[SPID_SIZE];
	struct list_head	list;
} vuihandler_connected_app_t;

bool vuihandler_ready(void);

#if defined(CONFIG_SOO_AGENCY)

void vuihandler_open_rfcomm(pid_t pid);

void vuihandler_sl_recv(vuihandler_pkt_t *vuihandler_pkt, size_t vuihandler_pkt_size);

#endif /* CONFIG_SOO_AGENCY */

#if defined(CONFIG_SOO_ME)

void vuihandler_send(void *data, size_t size);

#endif /* CONFIG_SOO_ME */

void vuihandler_get_app_spid(uint8_t spid[SPID_SIZE]);

#endif /* VUIHANDLER_H */
