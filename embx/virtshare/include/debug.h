/*
 *                                                                                                                                                                                                                            
 * -- Smart Object Oriented  --                                                                                                                                                                                               
 * Copyright (c) 2015,2016 Sootech SA, Switzerland                                                                                                                                                                            
 *                                                                                                                                                                                                                            
 * Authors: Raphaël Buache, Daniel Rossier
 * Emails: raphael.buache@heig-vd.ch, daniel.rossier@heig-vd.ch
 *
 */

#ifdef __KERNEL__
#define force_print lprintk
#else
#define force_print printf
#endif /* __KERNEL__ */

#ifdef DEBUG
#undef DBG

#ifdef __KERNEL__

#if !defined(CONFIG_SOO_AGENCY) && !defined(CONFIG_SOO_ME)
#define lprintk printk
#define lprintk_buffer printk_buffer
#else
#include <virtshare/console.h>
#endif

#else

#include <stdio.h>

#endif /* __KERNEL__ */

/*
 * force_print() is able to manage if the function is called from the kernel or the user space
*/
#define DBG(fmt, ...) \
	do { \
		force_print("%s:%i > "fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
	} while (0)

/* rtdm_printk() is broken for the time being. Use lprintk() instead.
 * One can safely switch back to rtdm_printk() once the issues are solved. */
#define RTDBG(fmt, ...) \
	do { \
		force_print("%s:%i > "fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
	} while (0)

#define DBG0(...) DBG("%s", ##__VA_ARGS__)

#define DBG_BUFFER(buffer, ...) \
	do { \
		lprintk_buffer(buffer, ##__VA_ARGS__); \
	} while (0)

#else

#define DBG(fmt, ...)
#define RTDBG(fmt, ...)
#define DBG0(...)
#define DBG_BUFFER(buffer, ...)
#define DBG_ON__
#define DBG_OFF__

#endif
