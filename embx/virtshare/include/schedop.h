/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef __SCHEDOP_H__
#define __SCHEDOP_H__


#define SCHEDOP_yield     	0
#define SCHEDOP_deadline	1
#define SCHEDOP_sleep		2
#define SCHEDOP_schedule	3


struct deadline_args {
	u64 delta_ns;
};
typedef struct deadline_args deadline_args_t;

#endif /* __SCHEDOP_H__ */

