#ifndef __AVZCONS_H__
#define __AVZCONS_H__

void avzcons_force_flush(void);
void avzcons_resume(void);

/* Interrupt work hooks. Receive data, or kick data out. */
void avzcons_rx(char *buf, unsigned len, struct pt_regs *regs);
void avzcons_tx(void);

int avzcons_ring_init(void);
int avzcons_ring_send(const char *data, unsigned len);

int avz_switch_console(char ch);

#endif /* __AVZCONS_H__ */
