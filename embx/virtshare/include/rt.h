/* 
 * rt.h
 */
 
#ifndef RT_H
#define RT_H

#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/jiffies.h>
#include <linux/time.h>
#include <linux/tick.h>

#include <virtshare/soo.h>
#include <virtshare/hypervisor.h>

#define SOO_RT_THREAD_DECL \
	int soo_rt_active_thread = 0;

#define SOO_RT_THREAD_DECL_FCT \
	static s64 soo_rt_thread_elapsed_delay, soo_rt_thread_start;

#define SOO_RT_THREAD_WAIT(next_id, delay) \
	soo_rt_thread_start = ktime_get().tv64; \
	while (1) { \
		soo_rt_thread_elapsed_delay = ktime_get().tv64 - soo_rt_thread_start; \
		if (soo_rt_thread_elapsed_delay >= (s64) (delay)) { \
			soo_rt_active_thread = next_id; \
			yield(); \
			if (kthread_should_park()) \
				kthread_parkme(); \
			break; \
		} \
		else { \
			avz_sched_sleep_ns((s64) (delay) - soo_rt_thread_elapsed_delay); \
			yield(); \
			if (kthread_should_park()) \
				kthread_parkme(); \
		} \
	}
#define SOO_RT_THREAD_BEGIN(id) \
	if (soo_rt_active_thread == id) {

#define SOO_RT_THREAD_END(next_id, delay) \
		SOO_RT_THREAD_WAIT(next_id, delay) \
	} \
	else { \
		yield(); \
		if (kthread_should_park()) \
			kthread_parkme(); \
	}

#if defined(CONFIG_SOO_AGENCY) && defined(CONFIG_SMP)
rtdm_task_t *get_dc_isr(void);
#endif

#endif /* RT_H */
