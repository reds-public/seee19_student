
/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan 2016: Daniel Rossier
 * - June 2016: Baptiste Delporte
 * - May 2018: Baptiste Delporte
 *
 */

#ifndef __VBSTORE_H
#define __VBSTORE_H

#include <virtshare/vbus.h>
#include <linux/list.h>
#include <linux/types.h>

enum vbus_msg_type
{
	VBS_DIRECTORY,
	VBS_DIRECTORY_EXISTS,
	VBS_READ,
	VBS_WATCH,
	VBS_UNWATCH,
	VBS_TRANSACTION_END,
	VBS_WRITE,
	VBS_MKDIR,
	VBS_RM,
	VBS_WATCH_EVENT,
};
typedef enum vbus_msg_type vbus_msg_type_t;

struct msgvec {
	void *base;
	uint32_t len;
};
typedef struct msgvec msgvec_t;

struct vbus_msg
{
	struct list_head list;   /* Used to store the msg into the standby list */

	/* The next field *must* be allocated dynamically since the size of a struct completion depends
	 * on SMP enabling.
	 */
	union {
		struct completion *reply_wait;
	} u;

	uint32_t type;  	/* vbus_msg type */
	uint32_t len;   	/* Length of data following this. */

	uint32_t id;		/* Unique msg ID (32-bit circular) */
	uint32_t transactionID;	/* A non-zero value means we are in a transaction */

	struct vbus_msg *reply; /* Refer to another vbus_msg message (case of the reply) */

	/* Message content */
	char *payload;

};
typedef struct vbus_msg vbus_msg_t;

/* Inter-domain shared memory communications. */
#define VBSTORE_RING_SIZE 1024

typedef uint32_t VBSTORE_RING_IDX;

struct vbstore_domain_interface {
	char req[VBSTORE_RING_SIZE]; /* Requests to vbstore daemon. */
	char rsp[VBSTORE_RING_SIZE]; /* Replies and async watch events. */
	volatile VBSTORE_RING_IDX req_cons, req_prod, req_pvt;
	volatile VBSTORE_RING_IDX rsp_cons, rsp_prod, rsp_pvt;

	unsigned int levtchn, revtchn;
};
typedef volatile struct vbstore_domain_interface vbstore_intf_t;

struct vbs_watcher {
	struct list_head list;
	volatile vbstore_intf_t *intf;
};

struct vbs_node {
	struct list_head children; /* subtree */
	struct list_head watchers;
	struct list_head sibling;  /* at the same level */
	struct vbs_node *parent;
	char *key;
	void *value;
};

void vbstorage_agency_init(void);

struct vbus_device;

struct vbs_node *vbs_store_lookup(const char *key);
void vbs_notify_watchers(vbus_msg_t vbus_msg, struct vbs_node *node);
int vbs_store_read(const char *key, char **value, size_t size);
int vbs_store_write(const char *key, const char *value);
int vbs_store_write_notify(const char *key, const char *value, struct vbs_node **notify_node);
int vbs_store_mkdir(const char *key);
int vbs_store_mkdir_notify(const char *key, struct vbs_node **notify_node);
int vbs_store_rm(const char *key);
int vbs_store_readdir(const char *key, char *children, const size_t size_children);
int vbs_store_dir_exists(const char *key, char *result);
int vbs_store_watch(const char *key, vbstore_intf_t *intf);
int vbs_store_unwatch(const char *key, vbstore_intf_t *intf);

void vbs_get_absolute_path(struct vbs_node *node, char *path);

void vbs_dump(void);
void vbs_dump_watches(void);

extern struct vbstore_domain_interface *vbstore_intf[MAX_DOMAINS];
extern void *__vbstore_vaddr[MAX_DOMAINS];

extern vbstore_intf_t *__intf;

extern struct list_head watches_rt;

extern int vbus_vbstore_init(void);

#ifdef CONFIG_SMP

extern vbstore_intf_t *__intf_rt;

struct vbus_transaction;
extern unsigned int transactionID;
extern void vbus_transaction_start_rt(struct vbus_transaction *t);
extern void transaction_end_rt(void);

extern void *vbs_talkv_rt(struct vbus_transaction t, vbus_msg_type_t type, const msgvec_t *vec, unsigned int num_vecs, unsigned int *len);

#endif /* CONFIG_SMP */

#endif /* __VBSTORE_H */
