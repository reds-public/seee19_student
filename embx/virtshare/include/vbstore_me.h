
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#include <virtshare/vbus.h>

int vbs_me_mkdir(struct vbus_transaction vbt, const char *dir, const char *node);
int vbs_me_write(struct vbus_transaction vbt, const char *dir, const char *node, const char *string);

int vbstore_init_dev_populate(void);
