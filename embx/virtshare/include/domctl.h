/******************************************************************************
 * domctl.h
 * 
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015,2016 Sootech SA, Switzerland
 * 
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef __DOMCTL_H__
#define __DOMCTL_H__

#include <virtshare/avz.h>

/*
 * There are two main scheduling policies: the one used for normal (standard) ME, and
 * a second one used for realtime ME.
 */
#define AVZ_SCHEDULER_FLIP			0
#define AVZ_SCHEDULER_RT	  		1

struct domctl_unpause_ME {
	uint32_t		store_mfn;
};

struct domctl {
    uint32_t cmd;

#define DOMCTL_pauseME       1
#define DOMCTL_unpauseME     2

    domid_t  domain;
    union {
       struct domctl_unpause_ME		unpause_ME;
    } u;
};
typedef struct domctl domctl_t;

#endif /* __DOMCTL_H__ */


