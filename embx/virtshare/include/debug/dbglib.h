/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2018: Baptiste Delporte
 *
 */
 
#ifndef DBGLIB_H
#define DBGLIB_H

#include <virtshare/console.h>
#include <virtshare/debug.h>

uint32_t dbglib_reserve_free_slot(void);

int dbglib_collect_sample(uint32_t slot, s64 sample);
s64 dbglib_collect_sample_and_get_mean(uint32_t slot, s64 sample);
void dbglib_collect_sample_and_show_mean(char *pre, uint32_t slot, s64 sample, char *post);

void dbglib_init(void);

#endif /* DBGLIB_H */
