/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 */
 
#ifndef __MEMINFO_H__
#define __MEMINFO_H__

#include <linux/mm.h>
#include <linux/swap.h>
#include <linux/vmalloc.h>

#include <uapi/linux/sysinfo.h>

#include <virtshare/console.h>
#include <virtshare/debug.h>

int debug_meminfo_proc_show(void);
void dump_pgtable(uint32_t *l1pgtable);

#endif /* __MEMINFO_H__ */
