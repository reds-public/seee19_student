/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - February 2018: Baptiste Delporte
 *
 */
 
#ifndef GPIO_H
#define GPIO_H

#include <virtshare/console.h>
#include <virtshare/debug.h>

void ll_gpio_set(int pin, int value);

void ll_gpio_init(void);

#endif /* GPIO_H */
