/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 */
 
#ifndef DEBUG_TIME_H
#define DEBUG_TIME_H

#include <virtshare/console.h>
#include <virtshare/debug.h>

s64 ll_time_get(void);
void ll_time_begin(uint32_t index);
void ll_time_end(uint32_t index);
s64 ll_time_collect_delay(uint32_t index);
void ll_time_collect_delay_show(char *pre, uint32_t index, char *post);
void ll_time_reset_delay_samples(uint32_t index);
void ll_time_collect_timestamp_show(char *pre, uint32_t index);
void ll_time_reset_timestamps(uint32_t index);
s64 ll_time_collect_period(uint32_t index);
void ll_time_collect_period_show(char *pre, uint32_t index, char *post);

#endif /* DEBUG_TIME_H */
