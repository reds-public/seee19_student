/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 * 
 * - August 2018: Baptiste Delporte
 *
 */
 
#ifndef DBGVAR_H
#define DBGVAR_H

#include <virtshare/console.h>
#include <virtshare/debug.h>

#ifdef CONFIG_SOO_AGENCY

typedef void (*dbgvar_fct_t)(void);

void dbgvar_register_fct(char key, dbgvar_fct_t fct);
void dbgvar_clear_fct(char key);

void dbgvar_init(void);

#endif /* CONFIG_SOO_AGENCY */

#define DBGEVENT_SIZE 	8192
#define DBGLIST_SIZE 	8192

typedef enum {
	DBGVAR_NR
} dbgvar_t;

extern int dbgvar[DBGVAR_NR];

/*
 * List mode:
 * - DBGLIST_DISABLED: the list is not used, thus inactive.
 * - DBGLIST_CLEAR: the list is cleared when it is full, that is, when there are DBG*_SIZE.
 * - DBGLIST_CONTINUOUS: the list is a circular buffer.
 */
typedef enum {
	DBGLIST_DISABLED,
	DBGLIST_CLEAR,
	DBGLIST_CONTINUOUS
} dbglist_mode_t;

/*
 * Clear the event list.
 */
void clear_dbgevent(void);

/*
 * Log an event.
 * Each event is associated to an arbitrary one-char key.
 */
void log_dbgevent(char c);

/*
 * Change the event logging mode.
 */
void set_dbgevent_mode(dbglist_mode_t mode);

/*
 * Show the event list.
 */
void show_dbgevent(void);

/*
 * Initialize the event list.
 */
void init_dbgevent(void);

/*
 * Clear the event list.
 */
void clear_dbglist(void);

/*
 * Log an event.
 * Each element is a 32 bit integer (signed or unsigned).
 */
void log_dbglist(int i);

/*
 * Change the integer logging mode.
 */
void set_dbglist_mode(dbglist_mode_t mode);

/*
 * Show the event list. The elements are printed in hexadecimal format.
 */
void show_dbglist(void);

/*
 * Clear the integer list.
 */
void init_dbglist(void);


#endif /* DBGVAR_H */
