/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2018: Daniel Rossier
 *
 */

#ifndef LOGBOOL_H
#define LOGBOOL_H

#include <virtshare/avz.h>

/* Number of (key, value) pair in the hashtable. The number is taken from the code example. */
#define LOGBOOL_HT_SIZE 	4096

/* Maximal size of the logbool string */
#define LOGBOOL_STR_SIZE	160

#if 0
#define ENABLE_LOGBOOL
#endif

typedef struct {
    bool section_state;	/* on / off */
    int cpu;
    int count;   /* Number of visits in this section */
} logbool_t;

struct entry_s {
	char *key;
	logbool_t value;
	struct entry_s *next;
};

typedef struct entry_s entry_t;

struct hashtable_s {
	int size;
	struct entry_s **table;
};

typedef struct hashtable_s logbool_hashtable_t;

#ifdef __AVZ__
extern	logbool_hashtable_t *avz_logbool_ht;
extern void logbool_init(void);
#define logbool_hashtable avz_logbool_ht
#else /* !__AVZ__ */
typedef void(*ht_set_t)(logbool_hashtable_t *, char *, logbool_t);
extern ht_set_t __ht_set;
#define logbool_hashtable avz_shared_info->logbool_ht
#endif

extern void ht_set(logbool_hashtable_t *hashtable, char *key, logbool_t value);
extern int sprintf(char * buf, const char *fmt, ...);

#ifdef ENABLE_LOGBOOL
#define __LOGBOOL_ON \
    { \
        char __logbool_str[LOGBOOL_STR_SIZE]; \
        static logbool_t logbool = { 0 }; \
        sprintf(__logbool_str, "%s:%s:%d", __FILE__, __func__, __LINE__); \
        logbool.section_state = true; \
        logbool.cpu = smp_processor_id(); \
        logbool.count++; \
        ht_set(logbool_hashtable, __logbool_str, logbool);

#define __LOGBOOL_OFF \
        logbool.section_state = false;          \
        ht_set(logbool_hashtable, __logbool_str, logbool); \
    }
#else
#define __LOGBOOL_ON
#define __LOGBOOL_OFF
#endif /* ENABLE_LOGBOOL */

void *ht_create(int size);
void ht_destroy(logbool_hashtable_t *hashtable);

void dump_all_logbool(unsigned char key);

#endif /* LOGBOOL_H */
