/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 */
 
#ifndef PACKGEN_H
#define PACKGEN_H

#ifdef __KERNEL__
#include <linux/types.h>
#endif /* __KERNEL__ */

#include <virtshare/debug/packcommon.h>

typedef struct {
	int (*send)(void *data, size_t size);
} packgen_callbacks_t;

void packgen_register(packgen_callbacks_t *callbacks, size_t size);
void packgen_enable_multi(unsigned int id);
void packgen_send_next_packet(void);

#endif /* PACKGEN_H */
