/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2018: Baptiste Delporte
 *
 */
 
#ifndef BANDWITDH_H
#define BANDWITDH_H

#if defined(CONFIG_SOO_AGENCY)

#include <virtshare/console.h>
#include <virtshare/debug.h>

#define N_BANDWIDTH_SLOTS	8
#define N_BANDWIDTH_DELAYS	4000

int ll_bandwidth_collect_delay(uint32_t index);
void ll_bandwidth_collect_delay_show(uint32_t index, size_t size);

int rtdm_ll_bandwidth_collect_delay(uint32_t index);
void rtdm_ll_bandwidth_collect_delay_show(uint32_t index, size_t size);

void ll_bandwidth_reset_delays(uint32_t index);

void ll_bandwidth_init(void);

/* NEON function */

void ll_bandwidth_compute(s64 *delays, size_t size, uint32_t *div, uint32_t *result);

#endif /* CONFIG_SOO_AGENCY */

#endif /* BANDWITDH_H */
