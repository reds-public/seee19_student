/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 */
 
#ifndef PACKCHK_H
#define PACKCHK_H

#ifdef __KERNEL__
#include <linux/types.h>
#endif /* __KERNEL__ */

#include <virtshare/debug/packcommon.h>

typedef struct {
	int (*recv_success)(void *buffer, size_t size);
	int (*recv_failure)(void *buffer, size_t size);
} packchk_callbacks_t;

void packchk_register(packchk_callbacks_t *callbacks, size_t size);
void packchk_enable_multi(void);
void packchk_send_next_packet(void);
void packchk_process_received_packet(void *data, size_t size);

#endif /* PACKCHK_H */
