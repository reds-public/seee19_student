/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 */
 
#ifndef PACKCOMMON_H
#define PACKCOMMON_H

#ifdef __KERNEL__
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/string.h>
#else
#include <core/types.h>
#include <stddef.h>
#endif /* __KERNEL__ */

#define PACKGENCHK_N_PREDEF_PACKETS		16
#define PACKGENCHK_MAX_SOOS			16

#ifdef __KERNEL__
#define packcommon_alloc(size) kzalloc(size, GFP_KERNEL)
#else
#define packcommon_alloc(size) malloc(size)
#endif /* __KERNEL__ */

void pack_prepare_predef_packets(unsigned char *predef_packets[PACKGENCHK_N_PREDEF_PACKETS], size_t size);
void pack_patch_predef_packets(unsigned char *predef_packets[PACKGENCHK_N_PREDEF_PACKETS], unsigned char *prefix, size_t prefix_size);

#endif /* PACKCOMMON_H */
