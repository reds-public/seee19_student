/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015,2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */
#ifndef CONSOLE_H
#define CONSOLE_H

#if defined(CONFIG_SOO_AGENCY) || defined(CONFIG_SOO_ME)
#include <linux/types.h>
#include <linux/string.h>
#endif /* CONFIG_SOO_AGENCY || CONFIG_SOO_ME */

#define CONSOLEIO_BUFFER_SIZE 256

struct pt_regs;

void avzcons_rx(char *buf, unsigned len, struct pt_regs *regs);
void avzcons_tx(void);

void init_console(void);

extern void (*__printch)(char c);

void lprintk(char *format, ...);
void lprintk_buffer(void *buffer, uint32_t n);
void lprintk_buffer_separator(void *buffer, uint32_t n, char separator);

void __lprintk(const char *format, va_list va);

void lprintk_int64_post(s64 number, char *post);
void lprintk_int64(s64 number);

/* Return the current active domain for the uart console */
int avzcons_get_focus(void);

/* Set the active domain to next_domain for the uart console and return the current active domain. */
int avzcons_set_focus(int next_domain);

#define avzcons_NEXT_FOCUS()	(avzcons_set_focus((avzcons_get_focus() + 1) % 4))

/* Return the current active domain for the graphic console */
int vfb_get_focus(void);

/* Set the active domain to next_domain for the graphic console and return the current active domain. */
int vfb_set_focus(int next_domain);

#define VFB_NEXT_FOCUS()	(vfb_set_focus((vfb_get_focus() + 1) % 3))

/* Temporary helper until the vfbback interaction have been cleared up */
#define VFB_TRIG_IRQ() \
	({\
		extern irqreturn_t vfb_interrupt(int irq, void *dev_id); \
	   	vfb_interrupt(0, NULL); \
	})

#endif /* CONSOLE_H */
