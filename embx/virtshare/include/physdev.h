/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#ifndef __PHYSDEV_H__
#define __PHYSDEV_H__

/*
 * Tell the hypervisor the hardware is ready to startup the system timer (HW)
 */
#define PHYSDEVOP_dump_page	1
#define PHYSDEVOP_dump_logbool	2
#define PHYSDEVOP_timer_init	3


#endif /* __PHYSDEV_H__ */

