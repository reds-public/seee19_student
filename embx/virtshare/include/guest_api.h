


/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2015 SOOtech Ltd, Switzerland
 *
 * Authors: Raphaël Buache, Daniel Rossier
 * Emails: raphael.buache@heig-vd.ch, daniel.rossier@heig-vd.ch
 *
 */

#ifndef GUEST_API_INCLUDE
#define GUEST_API_INCLUDE

#include <linux/list.h>
#include <linux/kobject.h>
#include <linux/device.h>

#include <virtshare/imec.h>

#ifdef DEBUG

#include <virtshare/avz.h>
#include <virtshare/console.h>

#undef DBG
#define DBG(fmt, ...) \
    do { \
        lprintk("(Dom#%d): %s:%i > "fmt, ME_domID(), __FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } while(0)

#define DBG0(...) DBG("%s", ##__VA_ARGS__)

#else
#define DBG(fmt, ...)
#define DBG0(...)
#endif


/*
 * Entry for a list of uevent which are propagated to the user space
 */
typedef struct {
	struct list_head list;
	unsigned int uevent_type;
	unsigned int slotID;
} soo_uevent_t;

int soo_uevent(struct device *dev, struct kobj_uevent_env *env);
int agency_ctl(agency_ctl_args_t *agency_ctl_args);

#ifdef CONFIG_SOO_AGENCY

int get_ME_state(unsigned int ME_slotID);
int set_ME_state(unsigned int ME_slotID, ME_state_t state);

int get_agency_desc(agency_desc_t *SOO_desc);

int get_ME_desc(unsigned int slotID, ME_desc_t *ME_desc);
int get_ME_spid(unsigned int slotID, unsigned char *spid);

#endif /* CONFIG_SOO_AGENCY */

#ifdef CONFIG_SOO_ME

int get_ME_state(void);
void set_ME_state(int state);

ME_desc_t *get_ME_desc(void);

#endif /* CONFIG_SOO_ME */

void vunmap_page_range(unsigned long addr, unsigned long end);

#endif /* GUEST_API_INCLUDE */

