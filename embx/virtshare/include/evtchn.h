/******************************************************************************
 * evtchn.h
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 *
 *
 */

#ifndef __ASM_EVTCHN_H__
#define __ASM_EVTCHN_H__

#include <linux/interrupt.h>
#include <asm/ptrace.h>
#include <linux/smp.h>
#include <linux/irq.h>

#include <virtshare/hypervisor.h>
#include <virtshare/event_channel.h>
#include <virtshare/soo.h>

#include <virtshare/console.h>
#include <virtshare/vbstore.h>

#define hard_irqs_disabled  irqs_disabled

typedef unsigned int (*guest_set_irq_type_t)(u32, u32);
typedef unsigned int (*guest_set_wake_t)(u32, u32);

extern unsigned int type_from_irq(int irq);
extern int set_pirq_type(unsigned int irq, unsigned int type);

extern unsigned int evtchn_from_irq(int irq);
extern unsigned int evtchn_from_irq_data(struct irq_data *irq_data);

/*
 * Bind a fresh evtchn to remote <@remote_domain, @remote_evtchn>.
 * Return allocated evtchn.
 */
#define IOCTL_EVTCHN_BIND_INTERDOMAIN			\
	_IOC(_IOC_NONE, 'E', 1, sizeof(struct ioctl_evtchn_bind_interdomain))
struct ioctl_evtchn_bind_interdomain {
	unsigned int remote_domain, remote_evtchn;
	unsigned int use;
};

/* Binding types. */
enum {
	IRQT_UNBOUND, IRQT_PIRQ, IRQT_VIRQ, IRQT_IPI, IRQT_EVTCHN
};

static inline void transaction_clear_bit(int evtchn, volatile unsigned long *p) {
	unsigned long tmp;
	unsigned long b, old_b;
	unsigned long mask = ~(1UL << (evtchn & 31));
	unsigned long x, ret, mask2;
	unsigned long flags;

	p += evtchn >> 5;

	local_irq_save(flags);

	__asm__ __volatile__("@transaction_clear_bit\n"
			"	ldr	%5, [%7] \n"
			"	and	%1, %5, %6 \n"
			"50:	mov %4, %1\n"
			"51:ldrex   %0, [%7]\n"
			"   strex   %3, %4, [%7]\n"
			"   teq     %3, #0\n"
			"   bne     51b\n"
			"	cmp	%0, %5\n"
			"	eorne	%2,%0, %5\n"
			"	mvnne	%3, %2\n"
			"	andne	%1,%1,%3\n"
			"	andne	%3, %0, %2\n"
			"	orrne	%1,%1, %3\n"
			"	movne %5, %4\n"
			"	bne		50b\n"
			: "=&r" (x), "=&r" (ret), "=&r" (mask2),"=&r" (tmp) , "=&r" (b),"=&r" (old_b)
			  :"r" (mask), "r" (p)
			   : "cc", "memory");

	local_irq_restore(flags);

}

static inline void transaction_set_bit(int evtchn, volatile unsigned long *p) {
	unsigned long tmp;
	unsigned long b, old_b;
	unsigned long mask = 1UL << (evtchn & 31);
	unsigned long x, ret, mask2;
	unsigned long flags;

	p += evtchn >> 5; /* p = p/32 since we have 32 bits per int */

	local_irq_save(flags);

	__asm__ __volatile__("@transaction_set_bit\n"
			"	ldr	%5, [%7] \n"
			"	orr	%1, %5, %6 \n"
			"50:	mov	%4, %1\n"
			"51:ldrex   %0, [%7]\n"
			"   strex   %3, %4, [%7]\n"
			"   teq     %3, #0\n"
			"   bne     51b\n"
			"	cmp	%0, %5\n"
			"	eorne	%2,%0, %5\n"
			"	mvnne	%3, %2\n"
			"	andne	%1,%1,%3\n"
			"	andne	%3, %0, %2\n"
			"	orrne	%1,%1, %3\n"
			"	movne %5, %4\n"
			"	bne		50b\n"
			: "=&r" (x), "=&r" (ret), "=&r" (mask2),"=&r" (tmp) , "=&r" (b),"=&r" (old_b)
			  :"r" (mask), "r" (p)
			   : "cc", "memory");

	local_irq_restore(flags);
}

/* Inline Functions */
/* Constructor for packed IRQ information. */
static inline u32 mk_irq_info(u32 type, u32 index, u32 evtchn) {
	return ((type << 24) | (index << 16) | evtchn);
}

static inline void clear_evtchn(u32 evtchn) {
  volatile shared_info_t *s = avz_shared_info;

  transaction_clear_bit(evtchn, &s->evtchn_pending[0]);

  dmb();

}
static inline int notify_remote_via_evtchn(uint32_t evtchn)
{
    evtchn_send_t op;
    op.evtchn = evtchn;

    return hypercall_trampoline(__HYPERVISOR_event_channel_op, EVTCHNOP_send, (long) &op, 0, 0);
}

/* Entry point for notifications into Linux subsystems. */
asmlinkage void evtchn_do_upcall(struct pt_regs *regs);

/*
 * LOW-LEVEL DEFINITIONS
 */

/*
 * Dynamically bind an event source to an IRQ-like callback handler.
 * On some platforms this may not be implemented via the Linux IRQ subsystem.
 * The IRQ argument passed to the callback handler is the same as returned
 * from the bind call. It may not correspond to a Linux IRQ number.
 * Returns IRQ or negative errno.
 * UNBIND: Takes IRQ to unbind from; automatically closes the event channel.
 */
extern int bind_evtchn_to_irq_handler(unsigned int evtchn, irq_handler_t handler, irq_handler_t thread_fn, unsigned long irqflags, const char *devname, void *dev_id);
extern int bind_interdomain_evtchn_to_irqhandler(unsigned int remote_domain, unsigned int remote_evtchn, irq_handler_t handler, irq_handler_t thread_fn, unsigned long irqflags, const char *devname, void *dev_id);
extern int bind_existing_interdomain_evtchn(unsigned int local_channel, unsigned int remote_domain, unsigned int remote_evtchn);
extern int bind_virq_to_irqhandler(unsigned int virq, irq_handler_t handler, unsigned long irqflags, const char *devname, void *dev_id);

extern unsigned int virq_to_irq(unsigned int virq);
extern int bind_virq_to_irq(unsigned int virq);

extern void virtshare_mask_irq(struct irq_data *irq_data);
extern void virtshare_unmask_irq(struct irq_data *irq_data);

/*
 * Common unbind function for all event sources. Takes IRQ to unbind from.
 * Automatically closes the underlying event channel (even for bindings
 * made with bind_evtchn_to_irqhandler()).
 */
extern void unbind_from_irqhandler(unsigned int irq, void *dev_id);
extern int unbind_domain_evtchn(unsigned int domID, unsigned int evtchn);

extern void mask_evtchn(int );
extern void unmask_evtchn(int );

extern unsigned int type_from_irq(int irq);
/*
 * Unlike notify_remote_via_evtchn(), this is safe to use across
 * save/restore. Notifications on a broken connection are silently dropped.
 */
extern void notify_remote_via_irq(int irq);

void virtshare_init_irq(void);

#endif
