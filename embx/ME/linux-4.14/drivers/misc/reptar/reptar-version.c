/*
 *  reptar-version.c - Test communication with FPGA and read version registers
 *
 *  Copyright (C) HEIG-VD / REDS 2016
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


/*
 * TODO:
 * - Provide a sysfs interface to forward version strings to userspace
 */


#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/io.h>
#include <linux/slab.h>

#include <linux/of_platform.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>

#define DRV_NAME "reptar_version"

#define REPTAR_VERSION1_REG		0x00
#define REPTAR_VERSION2_REG		0x02
#define REPTAR_CONSTANT_REG		0x04
/* 0x06 reserved */
#define REPTAR_PHERIPH_DESC_H_REG	0x08	/* not used */
#define REPTAR_PHERIPH_DESC_L_REG	0x0A	/* not used */
#define REPTAR_SCRATCH1_REG		0x0C
#define REPTAR_SCRATCH2_REG		0x0E

/* REPTAR_VERSION1_REG */
#define REPTAR_DESIGN_UUID_OFFSET	0
#define REPTAR_DESIGN_UUID_MASK		GENMASK(5, REPTAR_DESIGN_UUID_OFFSET)
#define REPTAR_DESIGN_CAT_OFFSET	6
#define REPTAR_DESIGN_CAT_MASK		GENMASK(8, REPTAR_DESIGN_CAT_OFFSET)
#define REPTAR_FPGA_ID_OFFSET		9
#define REPTAR_FPGA_ID_MASK		GENMASK(11, REPTAR_FPGA_ID_OFFSET)
#define REPTAR_HW_OFFSET		12
#define REPTAR_HW_MASK			GENMASK(15, REPTAR_HW_OFFSET)

/* REPTAR_VERSION2_REG */
#define REPTAR_SUBDESIGN_ID_OFFSET	8
#define REPTAR_SUBDESIGN_ID_MASK	GENMASK(15, REPTAR_SUBDESIGN_ID_OFFSET)
#define REPTAR_VERSION_NUMBER_OFFSET	0
#define REPTAR_VERSION_NUMBER_MASK	GENMASK(7, REPTAR_VERSION_NUMBER_OFFSET)

/* REPTAR_CONSTANT_REG */
#define REPTAR_CONSTANT_VALUE		0x1234

#define READ_BACK_VALUE1		0xdead
#define READ_BACK_VALUE2		0xbeef

#define REG_VALUE(val, offset, mask)	((val & mask) >> offset)

MODULE_AUTHOR("Florian Vaussard <florian.vaussard@heig-vd.ch>");
MODULE_DESCRIPTION("Read bitstream version from Reptar FPGA");
MODULE_LICENSE("GPL v2");

struct reptar_device {
	void __iomem *base;

	uint16_t version1;
	uint16_t version2;
};

static uint16_t reptar_read(const struct reptar_device *reptar, unsigned reg)
{
	return ioread16(reptar->base + reg);
}

static void reptar_write(const struct reptar_device *reptar, unsigned reg, uint16_t value)
{
	iowrite16(value, reptar->base + reg);
}

static void reptar_decode_version(const struct device *dev, const struct reptar_device *reptar)
{
	uint16_t value;
	char *comment;

	/* -> hw version */
	value = REG_VALUE(reptar->version1, REPTAR_HW_OFFSET, REPTAR_HW_MASK);
	switch (value) {
		case 0x0:
			comment = "v1.0";
			break;
		case 0x1:
			comment = "v1.1";
			break;
		default:
			comment = "unknown version";
	}
	dev_info(dev, "Reptar hardware version: 0x%x (%s)\n", value, comment);

	/* -> fpga id */
	value = REG_VALUE(reptar->version1, REPTAR_FPGA_ID_OFFSET, REPTAR_FPGA_ID_MASK);
	switch (value) {
		case 0x0:
			comment = "Spartan 3; XC3S200AN-5FTG256C";
			break;
		case 0x1:
			comment = "Spartan 6; XC6SLX150TFGG900-3";
			break;
		default:
			comment = "unknown FPGA";
	}
	dev_info(dev, "Reptar FPGA ID: 0x%x (%s)\n", value, comment);

	/* -> design id category */
	value = REG_VALUE(reptar->version1, REPTAR_DESIGN_CAT_OFFSET, REPTAR_DESIGN_CAT_MASK);
	switch (value) {
		case 0b000:
			comment = "basic design";
			break;
		case 0b001:
			comment = "internal project";
			break;
		case 0b010:
			comment = "demonstrator";
			break;
		case 0b011:
			comment = "teaching";
			break;
		case 0b100:
			comment = "external project";
			break;
		case 0b101:
		case 0b110:
		case 0b111:
			comment = "reserved";
			break;
		default:
			comment = "unkown";
	}
	dev_info(dev, "Reptar design category: 0x%x (%s)\n", value, comment);

	/* -> design id uuid */
	value = REG_VALUE(reptar->version1, REPTAR_DESIGN_UUID_OFFSET, REPTAR_DESIGN_UUID_MASK);
	dev_info(dev, "Reptar design UUID: 0x%02x\n", value);

	/* -> subdesign id */
	value = REG_VALUE(reptar->version2, REPTAR_SUBDESIGN_ID_OFFSET, REPTAR_SUBDESIGN_ID_MASK);
	dev_info(dev, "Reptar sub-design ID: 0x%02x\n", value);
	/* -> subdesign version */
	value = REG_VALUE(reptar->version2, REPTAR_VERSION_NUMBER_OFFSET, REPTAR_VERSION_NUMBER_MASK);
	dev_info(dev, "Reptar design version: 0x%02x\n", value);
}

static int reptar_version_remove(struct platform_device *pdev)
{
	struct reptar_device *reptar = platform_get_drvdata(pdev);

	iounmap(reptar->base);
	kfree(reptar);

	return 0;
}

static int reptar_version_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct reptar_device *reptar;
	void __iomem *base;
	int err = 0;
	uint16_t value;

	reptar = kzalloc(sizeof(*reptar), GFP_KERNEL);
	if (reptar ==  NULL) {
		dev_err(&pdev->dev, "Unable to allocate device\n");
		return -ENOMEM;
	}

	/* Remap the registers */

	base = of_iomap(np, 0);
	if (IS_ERR(base)) {
		dev_err(&pdev->dev, "Failed to request memory region\n");
		err = PTR_ERR(base);
		goto err_free;
	}

	reptar->base = base;

	/* Sanity check on the constant value */
	value = reptar_read(reptar, REPTAR_CONSTANT_REG);
	if (value != REPTAR_CONSTANT_VALUE) {
		dev_err(&pdev->dev, "Failed to check the magic number! Read 0x%x, expecting 0x%x\n", value, REPTAR_CONSTANT_VALUE);
		err = -EIO;
		goto err_unmap;
	}
	dev_info(&pdev->dev, "Found FPGA!\n");

	/* Perform read-back tests */
	reptar_write(reptar, REPTAR_SCRATCH1_REG, READ_BACK_VALUE1);
	reptar_write(reptar, REPTAR_SCRATCH2_REG, READ_BACK_VALUE2);
	value = reptar_read(reptar, REPTAR_SCRATCH1_REG);
	if (value != READ_BACK_VALUE1) {
		dev_err(&pdev->dev, "Failed to read back scratch1_reg! Read 0x%x, expecting 0x%x\n", value, READ_BACK_VALUE1);
		err = -EIO;
		goto err_unmap;
	}
	value = reptar_read(reptar, REPTAR_SCRATCH2_REG);
	if (value != READ_BACK_VALUE2) {
		dev_err(&pdev->dev, "Failed to read back scratch2_reg! Read 0x%x, expecting 0x%x\n", value, READ_BACK_VALUE2);
		err = -EIO;
		goto err_unmap;
	}
	dev_info(&pdev->dev, "Read-back test succeeded\n");

	/* Read version registers */
	reptar->version1 = reptar_read(reptar, REPTAR_VERSION1_REG);
	reptar->version2 = reptar_read(reptar, REPTAR_VERSION2_REG);
	reptar_decode_version(&pdev->dev, reptar);

	platform_set_drvdata(pdev, reptar);

	return 0;

err_unmap:
	iounmap(base);
err_free:
	kfree(reptar);

	return err;
}

static struct of_device_id reptar_version_table[] = {
	{.compatible = "reds,reptar-version"},
	{},
};
MODULE_DEVICE_TABLE(of, reptar_version_table);

static struct platform_driver reptar_version_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DRV_NAME,
		.of_match_table = reptar_version_table,
	},
	.probe = reptar_version_probe,
	.remove = reptar_version_remove,
};

module_platform_driver(reptar_version_driver);
