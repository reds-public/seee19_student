/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2018,2019: Daniel Rossier, Baptiste Delporte
 *
 */

#include <linux/irqreturn.h>
#include <linux/serial_core.h>
#include <asm/atomic.h>

#include <virtshare/evtchn.h>
#include <virtshare/avz.h>

#include <virtshare/dev/vuart.h>

typedef struct {
	struct uart_port port; /* Must be as the *first* field */

	struct vbus_device  *dev;

	vuart_front_ring_t ring;
	grant_ref_t ring_ref;
	grant_handle_t handle;
	uint32_t evtchn;
	uint32_t irq;


} vuart_t;

/* ISR associated to the ring */
extern vuart_t vuart;

irqreturn_t vuart_interrupt(int irq, void *data);

/* State management */
void vuart_probe(void);
void vuart_closed(void);
void vuart_connected(void);
void vuart_shutdown(void);

void vuart_vbus_init(void);

/* Processing and connected state management */
void vuart_start(void);
void vuart_end(void);
bool vuart_is_connected(void);

