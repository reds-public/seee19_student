/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2018,2019: Daniel Rossier, Baptiste Delporte
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/mutex.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/console.h>
#include <linux/tty_flip.h>

#include <asm/atomic.h>

#include <virtshare/evtchn.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>
#include <virtshare/debug.h>

#include "common.h"

/* We've been assigned a range on the "Low-density serial ports" major */
#define VUART_SERIAL_MAJOR	207
#define VUART_SERIAL_MINORS	 16

#define VUART_SERIAL_NAME "vtty"

/* IOCTL */
#define VUART_SELECT_INPUT_TRANSMIT_DESTINATION _IOW(0x50000a21, 1, char)

vuart_t vuart;

static char vuart_accept_input = INPUT_TRANSMIT_DESTINATION_TTY;
static char uart_enabled = 0;
static struct uart_ops vuart_ops;
static struct mutex uart_mutex_stop;

bool vuart_ready(void) {
	return vuart_is_connected();
}

irqreturn_t vuart_interrupt(int irq, void *dev_id) {
	RING_IDX i, rp;
	vuart_response_t *ring_rsp;
	struct tty_port *port = &vuart.port.state->port;

	if (!vuart_is_connected())
		return IRQ_HANDLED;

	rp = vuart.ring.sring->rsp_prod;
	dmb();

	for (i = vuart.ring.sring->rsp_cons; i != rp; i++) {
		ring_rsp = RING_GET_RESPONSE(&vuart.ring, i);
		vuart.port.icount.rx++;
		tty_insert_flip_char(port, ring_rsp->c, TTY_NORMAL);
	}
	vuart.ring.sring->rsp_cons = i;

	tty_flip_buffer_push(port);

	return IRQ_HANDLED;
}

vuart_t vuart = {
		.port = {
		  .ops  = &vuart_ops,
		  .type	= PORT_OMAP,
		  .line	= 0,
		  .flags = UPF_BOOT_AUTOCONF,
    }
};

static void vuart_stop_tx(struct uart_port *port)
{
	vuart_t *sport = (vuart_t *) port;
	bool irqs_off_before = false;

	if ((sport->dev == NULL) || !vuart_is_connected())
		return;

	/* Linux serial core disables IRQs when sending to UART.
	 * We re-enable IRQs temporary.
	 */
	if (irqs_disabled()) {
		irqs_off_before = true;
		local_irq_enable();
	}

	mutex_lock(&uart_mutex_stop);

	RING_PUSH_REQUESTS(&sport->ring);

	notify_remote_via_irq(sport->irq);

	mutex_unlock(&uart_mutex_stop);

	if (irqs_off_before)
		local_irq_disable();

}

static void vuart_stop_rx(struct uart_port *port)
{
	uart_enabled = 0;
}

static void vuart_enable_ms(struct uart_port *port)
{
	uart_enabled = 1;
}

void vuart_start_tx(struct uart_port *port)
{
	vuart_request_t *ring_req;
	vuart_t *sport = (vuart_t *) port;
	struct circ_buf *xmit = &sport->port.state->xmit;

	if ((sport->dev == NULL) || !vuart_is_connected())
		return ;

	vuart_start();

	while (!uart_circ_empty(xmit)) {

		while (RING_FREE_REQUESTS(&sport->ring)) {
			/* send xmit->buf[xmit->tail]
			 * out the port here */

			/* Fill out a communications ring structure. */
			ring_req = RING_GET_REQUEST(&vuart.ring, vuart.ring.req_prod_pvt);
			ring_req->c = xmit->buf[xmit->tail];
			sport->ring.req_prod_pvt++;
			xmit->tail = (xmit->tail + 1) & (UART_XMIT_SIZE - 1);
			sport->port.icount.tx++;
			if (uart_circ_empty(xmit))
				break;

		}

		vuart_stop_tx(&sport->port);
	}

	if (uart_circ_chars_pending(xmit) < WAKEUP_CHARS)
		uart_write_wakeup(&sport->port);

	vuart_end();
}

static unsigned int vuart_tx_empty(struct uart_port *port)
{
	vuart_t *sport = (vuart_t *) port;

	if ((sport->dev == NULL) || !vuart_is_connected())
		return 0;

	/* We are always ready to send! */
	return TIOCSER_TEMT;
}

static unsigned int vuart_get_mctrl(struct uart_port *port)
{
	unsigned int tmp = TIOCM_DSR | TIOCM_CAR | TIOCM_CTS | TIOCM_RTS;

	return tmp;
}

static void vuart_set_mctrl(struct uart_port *port, unsigned int mctrl)
{

}

static void vuart_break_ctl(struct uart_port *port, int break_state)
{

}

static int vuart_startup(struct uart_port *port)
{

	uart_enabled = 1;

	return 0;
}

/* We shutdown our vuart */
static void __vuart_shutdown(struct uart_port *port)
{

}

static const char *vuart_type(struct uart_port *port)
{
	return (port->type == PORT_OMAP) ? VUART_SERIAL_NAME : NULL;
}

static void vuart_set_termios(struct uart_port *port, struct ktermios *new,
		struct ktermios *old)
{

}


static void vuart_release_port(struct uart_port *port)
{

}

static int vuart_request_port(struct uart_port *port)
{
	return 0;
}

static void vuart_config_port(struct uart_port *port, int flags)
{

}

static int vuart_verify_port(struct uart_port *port, struct serial_struct *ser)
{
	return 0;
}

static int vuart_ioctl(struct uart_port *port, unsigned int cmd, unsigned long arg)
{
	switch (cmd) {
	case VUART_SELECT_INPUT_TRANSMIT_DESTINATION:
		vuart_accept_input = arg;
		break;

	default:
		break;
	}

	/* The ioctl must return -ENOIOCTLCMD */
	return -ENOIOCTLCMD;
}

static struct uart_ops vuart_ops = {
		.tx_empty	= vuart_tx_empty,
		.set_mctrl	= vuart_set_mctrl,
		.get_mctrl	= vuart_get_mctrl,
		.stop_tx	= vuart_stop_tx,
		.start_tx	= vuart_start_tx,
		.stop_rx	= vuart_stop_rx,
		.enable_ms	= vuart_enable_ms,
		.break_ctl	= vuart_break_ctl,
		.startup	= vuart_startup,
		.shutdown	= __vuart_shutdown,
		.type		= vuart_type,
		.set_termios	= vuart_set_termios,
		.release_port	= vuart_release_port,
		.request_port	= vuart_request_port,
		.config_port	= vuart_config_port,
		.verify_port	= vuart_verify_port,
		.ioctl		= vuart_ioctl,
};


void vuart_probe(void) {
	DBG0(VUART_PREFIX "Frontend probe\n");
}

void vuart_shutdown(void) {

	DBG0(VUART_PREFIX "Frontend shutdown\n");
}

void vuart_closed(void) {

	DBG0(VUART_PREFIX "Frontend close\n");
}

void vuart_connected(void) {

	DBG0(VUART_PREFIX "Frontend connected\n");

	/* Force the processing of pending requests, if any */
	notify_remote_via_irq(vuart.irq);
}

static void vuart_console_putchar(struct uart_port *port, int ch)
{
	vuart_request_t *ring_req;
	vuart_t *sport = (vuart_t *) port;

	if ((sport->dev == NULL) || !vuart_is_connected())
		return;

	if (sport->ring.sring == NULL)
		return;

	/* Fill out a communications ring structure. */
	ring_req = RING_GET_REQUEST(&sport->ring, sport->ring.req_prod_pvt);
	ring_req->c = ch;

	sport->ring.req_prod_pvt++;
	vuart_stop_tx(&sport->port);
}

/*
 * Interrupts are disabled on entering
 */
static void vuart_console_write(struct console *co, const char *s, unsigned int count)
{
	vuart_t *sport = &vuart;

	if ((sport->dev == NULL) || !vuart_is_connected())
		return;

	/*
	 *	First, save UCR1/2 and then disable interrupts
	 */

	uart_console_write(&sport->port, s, count, vuart_console_putchar);
}

static int __init vuart_console_setup(struct console *co, char *options)
{
	vuart_t *sport = &vuart;
	int baud = 115200;
	int bits = 8;
	int parity = 'n';
	int flow = 'n';

	return uart_set_options(&sport->port, co, baud, parity, bits, flow);
}

static struct uart_driver vuart_drv;

static struct console vuart_console = {
		.name		= VUART_SERIAL_NAME,
		.write		= vuart_console_write,
		.device		= uart_console_device,
		.setup		= vuart_console_setup,
		.flags		= CON_PRINTBUFFER,
		.index		= -1,
		.data		= &vuart_drv,
};

static int __init vuart_rs_console_init(void)
{
	register_console(&vuart_console);

	return 0;
}

console_initcall(vuart_rs_console_init);

static struct uart_driver vuart_drv = {
		.owner		= THIS_MODULE,
		.driver_name	= VUART_SERIAL_NAME,
		.dev_name	= VUART_SERIAL_NAME,
		.major	 	= VUART_SERIAL_MAJOR,
		.minor	 	= VUART_SERIAL_MINORS,
		.nr		= 1,
		.cons		= &vuart_console,
};

static int vuart_init(void) {
	int res;

	mutex_init(&uart_mutex_stop);

	vuart_vbus_init();

	res = uart_register_driver(&vuart_drv);
	if (res)
		BUG();

	res = uart_add_one_port(&vuart_drv, &vuart.port);
	if (res)
		BUG();

	return 0;
}

device_initcall(vuart_init);
