/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2018: Daniel Rossier, Baptiste Delporte
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/mutex.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/of.h>

#include <virtshare/evtchn.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>
#include <virtshare/debug.h>

#include "common.h"

vdummy_t vdummy;

static bool thread_created = false;

irqreturn_t vdummy_interrupt(int irq, void *dev_id) {
	RING_IDX i, rp;
	vdummy_response_t *ring_rsp;

	if (!vdummy_is_connected())
		return IRQ_HANDLED;

	DBG("%s, %d\n", __func__, ME_domID());

	rp = vdummy.ring.sring->rsp_prod;
	dmb(); /* Ensure we see queued responses up to 'rp'. Just to make sure ;-) not necessary in all cases... */

	for (i = vdummy.ring.sring->rsp_cons; i != rp; i++) {
		DBG("%s, cons=%d\n", __func__, i);
		ring_rsp = RING_GET_RESPONSE(&vdummy.ring, i);

		/* Do something with the response */
	}

	/* At the end rsp_cons = rsp_prod and refers to a free response index */
	vdummy.ring.sring->rsp_cons = i;

	return IRQ_HANDLED;
}

/*
 * The following function is given as an example.
 *
 */
void vdummy_generate_request(char *buffer) {
	vdummy_request_t *ring_req;

	vdummy_start();

	/*
	 * Try to generate a new request to the backend
	 */
	if (!RING_FULL(&vdummy.ring)) {
		ring_req = RING_GET_REQUEST(&vdummy.ring, vdummy.ring.req_prod_pvt);

		memcpy(ring_req->buffer, buffer, VDUMMY_PACKET_SIZE);

		/* Fill in the ring_req structure */

		/* Make sure the other end "sees" the request when updating the index */
		dmb();

		vdummy.ring.req_prod_pvt++;

		RING_PUSH_REQUESTS(&vdummy.ring);

		notify_remote_via_irq(vdummy.irq);
	}

	vdummy_end();
}

void vdummy_probe(void) {

	DBG0("[" VDUMMY_NAME "] Frontend probe\n");
}

void vdummy_shutdown(void) {

	DBG0("[" VDUMMY_NAME "] Frontend shutdown\n");
}

void vdummy_closed(void) {

	DBG0("[" VDUMMY_NAME "] Frontend close\n");
}

static int notify_fn(void *args) {

	msleep(50);

	vdummy_start();

	/* Make sure the backend is connected and ready for interactions. */

	notify_remote_via_irq(vdummy.irq);

	vdummy_end();



	return 0;
}

void vdummy_connected(void) {
	DBG0("[" VDUMMY_NAME "] Frontend connected\n");

	/* Force the processing of pending requests, if any */
	notify_remote_via_irq(vdummy.irq);

	if (!thread_created) {
		thread_created = true;
#if 1
		kthread_run(notify_fn, NULL, "notify_th");
#endif
	}
}

static int vdummy_init(void) {

	struct device_node *np;

	np = of_find_compatible_node(NULL, NULL, "vdummy,frontend");

	/* Check if DTS has vuihandler enabled */
	if (!of_device_is_available(np))
		return 0;

	vdummy_vbus_init();

	return 0;
}

device_initcall(vdummy_init);
