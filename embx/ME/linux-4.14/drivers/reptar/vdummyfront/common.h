
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015-2019 SOOtech SA, Switzerland
 *
 * Authors: Daniel Rossier
 * Emails: daniel.rossier@sootech.ch
 *
 */

#include <linux/irqreturn.h>

#include <virtshare/evtchn.h>
#include <virtshare/avz.h>

#include <virtshare/dev/vdummy.h>

typedef struct {

	struct vbus_device  *dev;

	vdummy_front_ring_t ring;
	grant_ref_t ring_ref;
	grant_handle_t handle;
	uint32_t evtchn;
	uint32_t irq;

} vdummy_t;

extern vdummy_t vdummy;

/* ISR associated to the ring */
irqreturn_t vdummy_interrupt(int irq, void *data);

/* State management */
void vdummy_probe(void);
void vdummy_closed(void);
void vdummy_connected(void);
void vdummy_shutdown(void);

void vdummy_vbus_init(void);

/* Processing and connected state management */
void vdummy_start(void);
void vdummy_end(void);
bool vdummy_is_connected(void);
