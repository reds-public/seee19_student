
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015-2019 SOOtech SA, Switzerland
 *
 * Authors: Daniel Rossier
 * Emails: daniel.rossier@sootech.ch
 *
 */

#include <linux/irqreturn.h>

#include <virtshare/evtchn.h>
#include <virtshare/avz.h>

#include <virtshare/dev/vsp6.h>

typedef struct {

	struct vbus_device  *dev;

	vsp6_front_ring_t ring;
	grant_ref_t ring_ref;
	grant_handle_t handle;
	uint32_t evtchn;
	uint32_t irq;

} vsp6_t;

extern vsp6_t vsp6;

/* ISR associated to the ring */
irqreturn_t vsp6_interrupt(int irq, void *data);

/* State management */
void vsp6_probe(void);
void vsp6_closed(void);
void vsp6_connected(void);
void vsp6_shutdown(void);

void vsp6_vbus_init(void);

/* Processing and connected state management */
void vsp6_start(void);
void vsp6_end(void);
bool vsp6_is_connected(void);
