/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018-2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2018,2019: Daniel Rossier, Baptiste Delporte
 *
 */

#if 1
#define DEBUG
#endif

#include <linux/mutex.h>

#include <asm/mmu.h>

#include <virtshare/gnttab.h>
#include <virtshare/evtchn.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>
#include <virtshare/debug.h>

#include "common.h"

/* Protection against shutdown (or other) */
static struct mutex processing_lock;
static uint32_t processing_count;
static struct mutex processing_count_lock;

/*
 * Boolean that tells if the interface is in the Connected state.
 * In this case, and only in this case, the interface can be used.
 */
static volatile bool __connected;
static struct completion connected_sync;

/*
 * The functions processing_start() and processing_end() are used to
 * prevent pre-migration suspending actions.
 * The functions can be called in multiple execution context (threads).
 *
 * Assumptions:
 * - If the frontend is suspended during processing_start, it is for a short time, until the FE gets connected.
 * - If the frontend is suspended and a shutdown operation is in progress, the ME will disappear! Therefore,
 *   we do not take care about ongoing activities. All will disappear...
 *
 */
static void processing_start(void) {

	mutex_lock(&processing_count_lock);

	if (processing_count == 0)
		mutex_lock(&processing_lock);

	processing_count++;

	mutex_unlock(&processing_count_lock);

	/* At this point, the frontend has not been closed and may be in a transient state
	 * before getting connected. We can wait until it becomes connected.
	 *
	 * If a first call to processing_start() has succeeded, subsequent calls to this function will never lead
	 * to a wait_for_completion as below since the frontend will not be able to disconnect itself (through
	 * suspend for example). The function can therefore be safely called many times (by multiple threads).
	 */

	if (!__connected)
		wait_for_completion(&connected_sync);

}

static void processing_end(void) {

	mutex_lock(&processing_count_lock);

	processing_count--;

	if (processing_count == 0)
		mutex_unlock(&processing_lock);

	mutex_unlock(&processing_count_lock);
}

void vsp6_start(void) {
	processing_start();
}

void vsp6_end(void) {
	processing_end();
}

bool vsp6_is_connected(void) {
	return __connected;
}

/*
 * When the ME begins its execution, it needs to set up an event channel associated to this device.
 * But after migration, the ME can keep its previous allocated event channel and just
 * push it again onto vbstore.
 *
 * The free page allocated to the ring can also be kept the same.
 */
static int setup_sring(struct vbus_device *dev, bool initall) {
	int res;
	unsigned int evtchn;
	vsp6_sring_t *sring;
	struct vbus_transaction vbt;

	if (dev->state == VbusStateConnected)
		return 0;

	DBG("Frontend: Setup ring\n");

	/* Prepare to set up the ring. */

	vsp6.ring_ref = GRANT_INVALID_REF;

	if (initall) {
		/* Allocate an event channel associated to the ring */
		res = vbus_alloc_evtchn(dev, &evtchn);
		BUG_ON(res);

		res = bind_evtchn_to_irq_handler(evtchn, vsp6_interrupt, NULL, 0, "vsp6-fe", &vsp6);
		if (res <= 0) {
			lprintk("%s - line %d: Binding event channel failed for device %s\n", __func__, __LINE__, dev->nodename);
			BUG();
		}

		vsp6.evtchn = evtchn;
		vsp6.irq = res;

		/* Allocate a shared page for the ring */
		sring = (vsp6_sring_t *)__get_free_page(GFP_NOIO | __GFP_HIGH);
		if (!sring) {
			lprintk("%s - line %d: Allocating shared ring failed for device %s\n", __func__, __LINE__, dev->nodename);
			BUG();
		}

		SHARED_RING_INIT(sring);
		FRONT_RING_INIT(&vsp6.ring, sring, PAGE_SIZE);
	} else {
		SHARED_RING_INIT(vsp6.ring.sring);
		FRONT_RING_INIT(&vsp6.ring, vsp6.ring.sring, PAGE_SIZE);
	}

	/* Prepare the shared to page to be visible on the other end */

	res = vbus_grant_ring(dev, virt_to_pfn(vsp6.ring.sring));
	if (res < 0)
		BUG();

	vsp6.ring_ref = res;

	vbus_transaction_start(&vbt);

	vbus_printf(vbt, dev->nodename, "ring-ref", "%u", vsp6.ring_ref);
	vbus_printf(vbt, dev->nodename, "ring-evtchn", "%u", vsp6.evtchn);

	vbus_transaction_end(vbt);

	return 0;
}

/**
 * Free the ring and deallocate the proper data.
 */
static void free_sring(void) {

	/* Free resources associated with old device channel. */
	if (vsp6.ring_ref != GRANT_INVALID_REF) {
		gnttab_end_foreign_access(vsp6.ring_ref);
		free_page((uint32_t) vsp6.ring.sring);

		vsp6.ring_ref = GRANT_INVALID_REF;
		vsp6.ring.sring = NULL;
	}

	if (vsp6.irq)
		unbind_from_irqhandler(vsp6.irq, &vsp6);

	vsp6.irq = 0;

}

/**
 * Entry point to this code when a new device is created.  Allocate the basic
 * structures and the ring buffer for communication with the backend, and
 * inform the backend of the appropriate details for those.  Switch to
 * Initialised state.
 *
 */
static int __vsp6_probe(struct vbus_device *dev, const struct vbus_device_id *id) {

	vsp6.dev = dev;

	DBG0("SOO Virtual dummy frontend driver\n");

	DBG("%s: allocate a shared page and set up the ring...\n", __func__);

	setup_sring(dev, true);

	vsp6_probe();

	__connected = false;

	return 0;
}

/**
 * State machine by the frontend's side.
 */
static void backend_changed(struct vbus_device *dev, enum vbus_state backend_state) {
	DBG("SOO vsp6 frontend, backend %s changed its state to %d.\n", dev->nodename, backend_state);

	switch (backend_state) {

	case VbusStateClosed:
		BUG_ON(__connected);

		vsp6_closed();
		free_sring();

		/* The processing_lock is kept forever, since it has to keep all processing activities suspended.
		 * Until the ME disappears...
		 */

		break;

	case VbusStateConnected:
		vsp6_connected();

		/* Now, the FE is considered as connected */
		__connected = true;

		complete(&connected_sync);
		break;

	case VbusStateUnknown:
	default:
		lprintk("%s - line %d: Unknown state %d (backend) for device %s\n", __func__, __LINE__, backend_state, dev->nodename);
		BUG();
	}
}

int __vsp6_shutdown(struct vbus_device *dev) {

	/*
	 * Ensure all frontend processing is in a stable state.
	 * The lock will be never released once acquired.
	 * The frontend will be never be in a shutdown procedure before the end of resuming operation.
	 * It's mainly the case of a force_terminate callback which may intervene only after the frontend
	 * gets connected (not before).
	 */

	mutex_lock(&processing_lock);

	__connected = false;
	reinit_completion(&connected_sync);

	vsp6_shutdown();

	return 0;
}

static const struct vbus_device_id vsp6_ids[] = {
	{ VSP6_NAME },
	{ "" }
};

static struct vbus_driver vsp6_drv = {
	.name = VSP6_NAME,
	.ids = vsp6_ids,
	.probe = __vsp6_probe,
	.shutdown = __vsp6_shutdown,
	.otherend_changed = backend_changed,
};

void vsp6_vbus_init(void) {
	vbus_register_frontend(&vsp6_drv);

	mutex_init(&processing_lock);
	mutex_init(&processing_count_lock);

	processing_count = 0;

	init_completion(&connected_sync);

}
