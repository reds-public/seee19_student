/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2018: Daniel Rossier, Baptiste Delporte
 *
 */

#if 1
#define DEBUG
#endif

#include <linux/mutex.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/of.h>
#include <linux/input.h>

#include <virtshare/evtchn.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>
#include <virtshare/debug.h>

#include "common.h"

vsp6_t vsp6;

irqreturn_t vsp6_interrupt(int irq, void *dev_id) {

	return IRQ_HANDLED;
}

/*
 * Frontend probe callback called during its initialization.
 */
void vsp6_probe(void) {

	DBG0("[" VSP6_NAME "] Frontend probe\n");

}

void vsp6_shutdown(void) {

	DBG0("[" VSP6_NAME "] Frontend shutdown\n");
}

void vsp6_closed(void) {

	DBG0("[" VSP6_NAME "] Frontend close\n");
}

void vsp6_connected(void) {
	DBG0("[" VSP6_NAME "] Frontend connected\n");

	/* Force the processing of pending requests, if any */
	notify_remote_via_irq(vsp6.irq);
}

static int vsp6_init(void) {
	struct device_node *np;

	np = of_find_compatible_node(NULL, NULL, "vsp6,frontend");

	/* Check if DTS has vuihandler enabled */
	if (!of_device_is_available(np))
		return 0;

	vsp6_vbus_init();

	return 0;
}

device_initcall(vsp6_init);
