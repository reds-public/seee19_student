/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#include <linux/irqreturn.h>
#include <virtshare/evtchn.h>
#include <virtshare/dev/vinput.h>
#include <virtshare/avz.h>
#include <virtshare/vbus.h>

/* Olimex keyboards values */
#define OLIMEX_KBD_VENDOR_ID			0x1220
#define OLIMEX_KBD_PRODUCT_ID			0x0008

extern int vinput_vbus_init(void);

typedef struct {
	struct vbus_device *dev;

	vinput_back_ring_t ring;
	uint32_t evtchn;
	unsigned int irq;
} vinput_ring_t;

/*
 * General structure for this virtual device (backend side)
 */
typedef struct {
	atomic_t refcnt;
	wait_queue_head_t waiting_to_free;

	vinput_ring_t rings[MAX_ME_DOMAINS];

	int domfocus;

} vinput_t;

extern vinput_t vinput;
irqreturn_t vinput_interrupt(int irq, void *dev_id);

int vinput_subsys_init(struct vbus_device *dev);
int vinput_subsys_enable(struct vbus_device *dev);
void vinput_subsys_remove(struct vbus_device *dev);
void vinput_subsys_disable(struct vbus_device *dev);

#define DRV_PFX "vinput:"
#define DPRINTK(fmt, args...)	pr_debug(DRV_PFX "(%s:%d) " fmt ".\n",	__func__, __LINE__, ##args)
