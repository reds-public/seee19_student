

/*
 *
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 */

#include <linux/irqreturn.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>

#include <virtshare/evtchn.h>
#include <virtshare/dev/netif.h>
#include <virtshare/avz.h>
#include <virtshare/vbus.h>


#define NETIF_TX_RING_SIZE __CONST_RING_SIZE(netif_tx, PAGE_SIZE)
#define NETIF_RX_RING_SIZE __CONST_RING_SIZE(netif_rx, PAGE_SIZE)

#define MAX_PENDING_REQS 256


/*
 * Two rings (rx, tx) managed by one event channel.
 */
typedef struct {
	struct vbus_device  *dev;

	/* The shared rings and indexes. */
	struct netif_tx_back_ring ring_tx;
	struct netif_rx_back_ring ring_rx;

	uint32_t     			  evtchn;
	unsigned int			  irq;

} vnet_ring_t;


/*
 * vnet_desc_t is associated to one ME (frontend)
 */
typedef struct {

	/* tx and rx rings */
	vnet_ring_t rings;

	atomic_t refcnt;
	wait_queue_head_t waiting_to_free;

	u8               fe_dev_addr[6];

	/* Physical parameters of the comms window. */
	grant_handle_t   tx_shmem_handle;
	grant_ref_t      tx_shmem_ref;

	grant_handle_t   rx_shmem_handle;
	grant_ref_t      rx_shmem_ref;

	/* Frontend feature information. */
	u8 can_sg:1;
	u8 gso:1;
	u8 gso_prefix:1;
	u8 csum:1;

	/* Internal feature information. */
	u8 can_queue:1;	    /* can queue packets for receiver? */

	/*
	 * Allow vnet_start_xmit() to peek ahead in the rx request
	 * ring.  This is a prediction of what rx_req_cons will be
	 * once all queued skbs are put on the ring.
	 */
	RING_IDX rx_req_cons_peek;

	/* Transmit shaping: allow 'credit_bytes' every 'credit_usec'. */
	unsigned long   credit_bytes;
	unsigned long   credit_usec;
	unsigned long   remaining_credit;
	struct timer_list credit_timeout;

	/* Statistics */
	unsigned long rx_gso_checksum_fixup;

	/* Miscellaneous private stuff. */
	struct list_head schedule_list;

	struct net_device *dev;

	/* List of frontends to notify after a batch of frames sent. */
	struct list_head notify_list;

} vnet_desc_t;

struct pending_tx_info {
	struct netif_tx_request req;
	vnet_desc_t *vnet_desc;
};

struct netbk_rx_meta {
	int id;
	int size;
	int gso_size;
};


/*
 * General structure for this virtual device (backend side)
 */
typedef struct {
	atomic_t refcnt;
	wait_queue_head_t waiting_to_free;

  vnet_desc_t descs[MAX_DOMAINS];

  /* The following fields are unique to the backend and do not
   * depend on the number of frontends.
   */

	wait_queue_head_t wq;
	struct task_struct *task;

	struct sk_buff_head rx_queue;
	struct sk_buff_head tx_queue;

	struct timer_list net_timer;

	struct page *mmap_pages[MAX_PENDING_REQS];

	unsigned int pending_prod;
	unsigned int pending_cons;

	struct list_head net_schedule_list;

	/* Protect the net_schedule_list in netif. */
	spinlock_t net_schedule_list_lock;

	atomic_t netfront_count;

	struct pending_tx_info pending_tx_info[MAX_PENDING_REQS];
	struct gnttab_copy tx_copy_ops[MAX_PENDING_REQS];

	u16 pending_ring[MAX_PENDING_REQS];

	/*
	 * Given MAX_BUFFER_OFFSET of 4096 the worst case is that each
	 * head/fragment page uses 2 copy operations because it
	 * straddles two buffers in the frontend.
	 */
	struct gnttab_copy grant_copy_op[2 * NETIF_RX_RING_SIZE];
	struct netbk_rx_meta meta[2 * NETIF_RX_RING_SIZE];


} vnet_t;


extern vnet_t vnet;

static inline unsigned int pending_index(unsigned i)
{
	return i & (MAX_PENDING_REQS - 1);
}

static inline unsigned int nr_pending_reqs(void)
{
	return MAX_PENDING_REQS - vnet.pending_prod + vnet.pending_cons;
}

irqreturn_t netif_interrupt(int irq, void *dev_id);

int vnet_subsys_init(struct vbus_device *dev);
void vnet_subsys_remove(struct vbus_device *dev);
int vnet_subsys_init(struct vbus_device *dev);
int vnet_subsys_enable(struct vbus_device *dev);

void vnet_subsys_remove(struct vbus_device *dev);
void vnet_subsys_disable(struct vbus_device *dev);

void vnet_connect(struct vbus_device *dev);

int netback_uevent(struct vbus_device *dev, struct kobj_uevent_env *env);

extern int netback_vbus_init(void);

#define DRV_PFX "avz-netback:"
#define DPRINTK(fmt, args...)	pr_debug(DRV_PFX "(%s:%d) " fmt ".\n",	__func__, __LINE__, ##args)
