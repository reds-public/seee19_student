/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015,2016 Sootech SA, Switzerland
 *
 * Authors: Raphaël Buache, Daniel Rossier
 * Emails: raphael.buache@heig-vd.ch, daniel.rossier@heig-vd.ch
 *
*/

#if 0
#define DEBUG
#endif

#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>

#include <linux/if_vlan.h>
#include <linux/udp.h>
#include <net/tcp.h>

#include "common.h"

#include <virtshare/gnttab.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>

#include <stdarg.h>
#include <linux/kthread.h>

#define VNET_QUEUE_LENGTH 		32
#define TX_QUEUES_NR					1

#define MAX_BUFFER_OFFSET PAGE_SIZE

/* Discriminate from any valid pending_idx value. */
#define INVALID_PENDING_IDX 0xFFFF

/*
 * This is the amount of packet we copy rather than map, so that the
 * guest can't fiddle with the contents of the headers while we do
 * packet processing on them (netfilter, routing, etc).
 */
#define PKT_PROT_LEN  (ETH_HLEN + VLAN_HLEN +  sizeof(struct iphdr) + MAX_IPOPTLEN + sizeof(struct tcphdr) + MAX_TCP_OPTION_SPACE)

/* extra field used in struct page */
union page_ext {
  unsigned int idx;
	void *mapping;
};

struct skb_cb_overlay {
	int meta_slots_used;
};

vnet_t vnet;

struct netrx_pending_operations {
	unsigned copy_prod, copy_cons;
	unsigned meta_prod, meta_cons;
	struct gnttab_copy *copy;
	struct netbk_rx_meta *meta;
	int copy_off;
	grant_ref_t copy_gref;
};

static const struct xenvif_stat {
	char name[ETH_GSTRING_LEN];
	u16 offset;
} vnet_stats[] = {
	{
		"rx_gso_checksum_fixup",
		offsetof(vnet_desc_t, rx_gso_checksum_fixup)
	},
};

vnet_desc_t *netdev_to_desc(struct net_device *dev) {

	domid_t *p_domid;

	p_domid = netdev_priv(dev);

	return &vnet.descs[*p_domid];
}


void vnet_desc_get(vnet_desc_t *vnet_desc)
{
	atomic_inc(&vnet_desc->refcnt);
}

void vnet_desc_put(vnet_desc_t *vnet_desc)
{
	if (atomic_dec_and_test(&vnet_desc->refcnt))
		wake_up(&vnet_desc->waiting_to_free);
}


static int max_required_rx_slots(vnet_desc_t *vnet_desc)
{
	int max = DIV_ROUND_UP(vnet_desc->dev->mtu, PAGE_SIZE);

	if (vnet_desc->can_sg || vnet_desc->gso || vnet_desc->gso_prefix)
		max += MAX_SKB_FRAGS + 1; /* extra_info + frags */

	return max;
}

int netbk_rx_ring_full(vnet_desc_t *vnet_desc)
{
	RING_IDX peek   = vnet_desc->rx_req_cons_peek;
	RING_IDX needed = max_required_rx_slots(vnet_desc);

	return ((vnet_desc->rings.ring_rx.sring->req_prod - peek) < needed) ||
	       ((vnet_desc->rings.ring_rx.rsp_prod_pvt + NETIF_RX_RING_SIZE - peek) < needed);
}


int vnet_schedulable(vnet_desc_t *vnet_desc)
{
	return netif_running(vnet_desc->dev) && netif_carrier_ok(vnet_desc->dev);
}

static int vnet_rx_schedulable(vnet_desc_t *vnet_desc)
{
	return vnet_schedulable(vnet_desc) && !netbk_rx_ring_full(vnet_desc);
}

static int __on_net_schedule_list(vnet_desc_t *vnet_desc)
{
	return !list_empty(&vnet_desc->schedule_list);
}

void netbk_schedule_vnet(vnet_desc_t *vnet_desc)
{
	unsigned long flags;

	if (__on_net_schedule_list(vnet_desc))
		goto kick;

	spin_lock_irqsave(&vnet.net_schedule_list_lock, flags);

	if (!__on_net_schedule_list(vnet_desc) && likely(vnet_schedulable(vnet_desc))) {
		list_add_tail(&vnet_desc->schedule_list, &vnet.net_schedule_list);
		vnet_desc_get(vnet_desc);
	}
	spin_unlock_irqrestore(&vnet.net_schedule_list_lock, flags);

kick:
	dmb();

	if ((nr_pending_reqs() < (MAX_PENDING_REQS / 2 )) && !list_empty(&vnet.net_schedule_list))
		wake_up(&vnet.wq); /* Kick the thread */
}

irqreturn_t netif_interrupt(int irq, void *dev_id)
{
	struct vbus_device *dev;
	vnet_desc_t *vnet_desc;

	dev = (struct vbus_device *) dev_id;
	vnet_desc = &vnet.descs[dev->otherend_id];

  DBG("Interrupt from domain: %d\n", dev->otherend_id);

  netbk_schedule_vnet(vnet_desc);

	if (vnet_rx_schedulable(vnet_desc))
		netif_wake_queue(vnet_desc->dev);


	return IRQ_HANDLED;
}

static inline int rx_work_todo(void)
{
	return !skb_queue_empty(&vnet.rx_queue);
}

static inline int tx_work_todo(void)
{
	if (((nr_pending_reqs() + MAX_SKB_FRAGS) < MAX_PENDING_REQS) && !list_empty(&vnet.net_schedule_list))
		return 1;

	return 0;
}

/* Must be called with net_schedule_list_lock held */
static void remove_from_net_schedule_list(vnet_desc_t *vnet_desc)
{
	if (likely(__on_net_schedule_list(vnet_desc))) {
		list_del_init(&vnet_desc->schedule_list);
		vnet_desc_put(vnet_desc);
	}
}

static vnet_desc_t *poll_net_schedule_list(void)
{
	vnet_desc_t *vnet_desc = NULL;

	spin_lock_irq(&vnet.net_schedule_list_lock);
	if (list_empty(&vnet.net_schedule_list))
		goto out;

	vnet_desc = list_first_entry(&vnet.net_schedule_list, vnet_desc_t, schedule_list);
	if (!vnet_desc)
		goto out;

	vnet_desc_get(vnet_desc);

	remove_from_net_schedule_list(vnet_desc);

out:
	spin_unlock_irq(&vnet.net_schedule_list_lock);

	return vnet_desc;
}

static void tx_add_credit(vnet_desc_t *vnet_desc)
{
	unsigned long max_burst, max_credit;

	/*
	 * Allow a burst big enough to transmit a jumbo packet of up to 128kB.
	 * Otherwise the interface can seize up due to insufficient credit.
	 */
	max_burst = RING_GET_REQUEST(&vnet_desc->rings.ring_tx, vnet_desc->rings.ring_tx.sring->req_cons)->size;
	max_burst = min(max_burst, 131072UL);
	max_burst = max(max_burst, vnet_desc->credit_bytes);

	/* Take care that adding a new chunk of credit doesn't wrap to zero. */
	max_credit = vnet_desc->remaining_credit + vnet_desc->credit_bytes;
	if (max_credit < vnet_desc->remaining_credit)
		max_credit = ULONG_MAX; /* wrapped: clamp to ULONG_MAX */

	vnet_desc->remaining_credit = min(max_credit, max_burst);
}

void netbk_check_rx_vnet(vnet_desc_t *vnet_desc)
{
	int more_to_do;

	RING_FINAL_CHECK_FOR_REQUESTS(&vnet_desc->rings.ring_tx, more_to_do);

	if (more_to_do)
		netbk_schedule_vnet(vnet_desc);
}

static void tx_credit_callback(unsigned long data)
{
	vnet_desc_t *vnet_desc = (vnet_desc_t *) data;

	tx_add_credit(vnet_desc);
	netbk_check_rx_vnet(vnet_desc);
}

static bool tx_credit_exceeded(vnet_desc_t *vnet_desc, unsigned size)
{
	unsigned long now = jiffies;
	unsigned long next_credit =	vnet_desc->credit_timeout.expires +	msecs_to_jiffies(vnet_desc->credit_usec / 1000);

	/* Timer could already be pending in rare cases. */
	if (timer_pending(&vnet_desc->credit_timeout))
		return true;

	/* Passed the point where we can replenish credit? */
	if (time_after_eq(now, next_credit)) {
		vnet_desc->credit_timeout.expires = now;
		tx_add_credit(vnet_desc);
	}

	/* Still too big to send right now? Set a callback. */
	if (size > vnet_desc->remaining_credit) {
		vnet_desc->credit_timeout.data  = (unsigned long) vnet_desc;
		vnet_desc->credit_timeout.function = tx_credit_callback;
		mod_timer(&vnet_desc->credit_timeout, next_credit);

		return true;
	}

	return false;
}

static void make_tx_response(vnet_desc_t *vnet_desc, struct netif_tx_request *txp, s8 st)
{
	RING_IDX i = vnet_desc->rings.ring_tx.rsp_prod_pvt;
	struct netif_tx_response *resp;
	int notify;

	resp = RING_GET_RESPONSE(&vnet_desc->rings.ring_tx, i);
	resp->id     = txp->id;
	resp->status = st;

	if (txp->flags & VNETTXF_extra_info)
		RING_GET_RESPONSE(&vnet_desc->rings.ring_tx, ++i)->status = NETIF_RSP_NULL;

	vnet_desc->rings.ring_tx.rsp_prod_pvt = ++i;

	RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&vnet_desc->rings.ring_tx, notify);
	if (notify)
		notify_remote_via_irq(vnet_desc->rings.irq);
}

static void netbk_tx_err(vnet_desc_t *vnet_desc, struct netif_tx_request *txp, RING_IDX end)
{
	RING_IDX cons = vnet_desc->rings.ring_tx.sring->req_cons;

	do {
		make_tx_response(vnet_desc, txp, NETIF_RSP_ERROR);
		if (cons >= end)
			break;

		txp = RING_GET_REQUEST(&vnet_desc->rings.ring_tx, cons++);
	} while (1);

	vnet_desc->rings.ring_tx.sring->req_cons = cons;

	netbk_check_rx_vnet(vnet_desc);
	vnet_desc_put(vnet_desc);
}


static int netbk_get_extras(vnet_desc_t *vnet_desc,	struct netif_extra_info *extras, int work_to_do)
{
	struct netif_extra_info extra;
	RING_IDX cons = vnet_desc->rings.ring_tx.sring->req_cons;

	do {
		if (unlikely(work_to_do-- <= 0)) {
			netdev_dbg(vnet_desc->dev, "Missing extra info\n");
			return -EBADR;
		}

		memcpy(&extra, RING_GET_REQUEST(&vnet_desc->rings.ring_tx, cons), sizeof(extra));

		if (unlikely(!extra.type || extra.type >= NETIF_EXTRA_TYPE_MAX)) {
			vnet_desc->rings.ring_tx.sring->req_cons = ++cons;
			netdev_dbg(vnet_desc->dev, "Invalid extra type: %d\n", extra.type);
			return -EINVAL;
		}

		memcpy(&extras[extra.type - 1], &extra, sizeof(extra));
		vnet_desc->rings.ring_tx.sring->req_cons = ++cons;

	} while (extra.flags & NETIF_EXTRA_FLAG_MORE);

	return work_to_do;
}

static int netbk_count_requests(vnet_desc_t *vnet_desc,	struct netif_tx_request *first,	struct netif_tx_request *txp, int work_to_do)
{
	RING_IDX cons = vnet_desc->rings.ring_tx.sring->req_cons;
	int frags = 0;

	if (!(first->flags & VNETTXF_more_data))
		return 0;

	do {
		if (frags >= work_to_do) {
			netdev_dbg(vnet_desc->dev, "Need more frags\n");
			return -frags;
		}

		if (unlikely(frags >= MAX_SKB_FRAGS)) {
			netdev_dbg(vnet_desc->dev, "Too many frags\n");
			return -frags;
		}

		memcpy(txp, RING_GET_REQUEST(&vnet_desc->rings.ring_tx, cons + frags), sizeof(*txp));
		if (txp->size > first->size) {
			netdev_dbg(vnet_desc->dev, "Frags galore\n");
			return -frags;
		}

		first->size -= txp->size;
		frags++;

		if (unlikely((txp->offset + txp->size) > PAGE_SIZE)) {
			netdev_dbg(vnet_desc->dev, "txp->offset: %x, size: %u\n", txp->offset, txp->size);
			return -frags;
		}
	} while ((txp++)->flags & VNETTXF_more_data);

	return frags;
}

static int netbk_set_skb_gso(vnet_desc_t *vnet_desc, struct sk_buff *skb, struct netif_extra_info *gso)
{
	if (!gso->u.gso.size) {
		netdev_dbg(vnet_desc->dev, "GSO size must not be zero.\n");
		return -EINVAL;
	}

	/* Currently only TCPv4 S.O. is supported. */
	if (gso->u.gso.type != NETIF_GSO_TYPE_TCPV4) {
		netdev_dbg(vnet_desc->dev, "Bad GSO type %d.\n", gso->u.gso.type);
		return -EINVAL;
	}

	skb_shinfo(skb)->gso_size = gso->u.gso.size;
	skb_shinfo(skb)->gso_type = SKB_GSO_TCPV4;

	/* Header must be checked, and gso_segs computed. */
	skb_shinfo(skb)->gso_type |= SKB_GSO_DODGY;
	skb_shinfo(skb)->gso_segs = 0;

	return 0;
}

/* extra field used in struct page */
static inline void set_page_ext(struct page *pg, unsigned int idx)
{
	union page_ext ext = { .idx = idx };

	BUILD_BUG_ON(sizeof(ext) > sizeof(ext.mapping));
	pg->mapping = ext.mapping;
}

static int get_page_ext(struct page *pg, unsigned int *pidx)
{
	union page_ext ext = { .mapping = pg->mapping };
	unsigned int idx;

	idx = ext.idx;

	if ((idx < 0) || (idx >= MAX_PENDING_REQS))
		return 0;

	if (vnet.mmap_pages[idx] != pg)
		return 0;

	*pidx = idx;

	return 1;
}

static struct page *netbk_alloc_page(struct sk_buff *skb, u16 pending_idx)
{
	struct page *page;

	page = alloc_page(GFP_KERNEL|__GFP_COLD);
	if (!page)
		return NULL;

	set_page_ext(page, pending_idx);
	vnet.mmap_pages[pending_idx] = page;

	return page;
}

static void frag_set_pending_idx(skb_frag_t *frag, u16 pending_idx)
{
	frag->page_offset = pending_idx;
}

static u16 frag_get_pending_idx(skb_frag_t *frag)
{
	return (u16)frag->page_offset;
}

static struct gnttab_copy *netbk_get_requests(vnet_desc_t *vnet_desc, struct sk_buff *skb, struct netif_tx_request *txp, struct gnttab_copy *gop)
{
	struct skb_shared_info *shinfo = skb_shinfo(skb);
	skb_frag_t *frags = shinfo->frags;
	u16 pending_idx = *((u16 *)skb->data);
	int i, start;

	/* Skip first skb fragment if it is on same page as header fragment. */
	start = (frag_get_pending_idx(&shinfo->frags[0]) == pending_idx);

	for (i = start; i < shinfo->nr_frags; i++, txp++) {
		struct page *page;
		unsigned int index;
		struct pending_tx_info *pending_tx_info = vnet.pending_tx_info;

		index = pending_index(vnet.pending_cons++);
		pending_idx = vnet.pending_ring[index];
		page = netbk_alloc_page(skb, pending_idx);
		if (!page)
			return NULL;

		gop->source.u.ref = txp->gref;
		gop->source.domid = vnet_desc->rings.dev->otherend_id;
		gop->source.offset = txp->offset;

		gop->dest.u.gmfn = virt_to_mfn(page_address(page));
		gop->dest.domid = DOMID_SELF;
		gop->dest.offset = txp->offset;

		gop->len = txp->size;
		gop->flags = GNTCOPY_source_gref;

		gop++;

		memcpy(&pending_tx_info[pending_idx].req, txp, sizeof(*txp));
		vnet_desc_get(vnet_desc);
		pending_tx_info[pending_idx].vnet_desc = vnet_desc;

		frag_set_pending_idx(&frags[i], pending_idx);
	}

	return gop;
}

static unsigned netbk_tx_build_gops(void)
{
	struct gnttab_copy *gop = vnet.tx_copy_ops, *request_gop;
	struct sk_buff *skb;
	int ret;

	while (((nr_pending_reqs() + MAX_SKB_FRAGS) < MAX_PENDING_REQS) && !list_empty(&vnet.net_schedule_list)) {

		vnet_desc_t *vnet_desc;
		struct netif_tx_request txreq;
		struct netif_tx_request txfrags[MAX_SKB_FRAGS];
		struct page *page;
		struct netif_extra_info extras[NETIF_EXTRA_TYPE_MAX - 1];
		u16 pending_idx;
		RING_IDX idx;
		int work_to_do;
		unsigned int data_len;
		unsigned int index;

		/* Get a netif from the list with work to do. */
		vnet_desc = poll_net_schedule_list();
		if (!vnet_desc)
			continue;

		RING_FINAL_CHECK_FOR_REQUESTS(&vnet_desc->rings.ring_tx, work_to_do);
		if (!work_to_do) {
			vnet_desc_put(vnet_desc);
			continue;
		}

		idx = vnet_desc->rings.ring_tx.sring->req_cons;
		dmb(); /* Ensure that we see the request before we copy it. */
		memcpy(&txreq, RING_GET_REQUEST(&vnet_desc->rings.ring_tx, idx), sizeof(txreq));

		/* Credit-based scheduling. */
		if (txreq.size > vnet_desc->remaining_credit && tx_credit_exceeded(vnet_desc, txreq.size)) {
			vnet_desc_put(vnet_desc);
			continue;
		}

		vnet_desc->remaining_credit -= txreq.size;

		work_to_do--;
		vnet_desc->rings.ring_tx.sring->req_cons = ++idx;

		memset(extras, 0, sizeof(extras));
		if (txreq.flags & VNETTXF_extra_info) {
			work_to_do = netbk_get_extras(vnet_desc, extras, work_to_do);
			idx = vnet_desc->rings.ring_tx.sring->req_cons;
			if (unlikely(work_to_do < 0)) {
				netbk_tx_err(vnet_desc, &txreq, idx);
				continue;
			}
		}

		ret = netbk_count_requests(vnet_desc, &txreq, txfrags, work_to_do);
		if (unlikely(ret < 0)) {
			netbk_tx_err(vnet_desc, &txreq, idx - ret);
			continue;
		}
		idx += ret;

		if (unlikely(txreq.size < ETH_HLEN)) {
			netdev_dbg(vnet_desc->dev, "Bad packet size: %d\n", txreq.size);
			netbk_tx_err(vnet_desc, &txreq, idx);
			continue;
		}

		/* No crossing a page as the payload mustn't fragment. */
		if (unlikely((txreq.offset + txreq.size) > PAGE_SIZE)) {
			netdev_dbg(vnet_desc->dev, "txreq.offset: %x, size: %u, end: %d\n", txreq.offset, txreq.size, (txreq.offset & ~PAGE_MASK) + txreq.size);
			netbk_tx_err(vnet_desc, &txreq, idx);
			continue;
		}

		index = pending_index(vnet.pending_cons);
		pending_idx = vnet.pending_ring[index];

		data_len = (txreq.size > PKT_PROT_LEN && ret < MAX_SKB_FRAGS) ? PKT_PROT_LEN : txreq.size;

		skb = alloc_skb(data_len + NET_SKB_PAD + NET_IP_ALIGN, GFP_ATOMIC | __GFP_NOWARN);
		if (unlikely(skb == NULL)) {
			netdev_dbg(vnet_desc->dev, "Can't allocate a skb in start_xmit.\n");
			netbk_tx_err(vnet_desc, &txreq, idx);
			break;
		}

		/* Packets passed to netif_rx() must have some headroom. */
		skb_reserve(skb, NET_SKB_PAD + NET_IP_ALIGN);

		if (extras[NETIF_EXTRA_TYPE_GSO - 1].type) {
			struct netif_extra_info *gso;
			gso = &extras[NETIF_EXTRA_TYPE_GSO - 1];

			if (netbk_set_skb_gso(vnet_desc, skb, gso)) {
				kfree_skb(skb);
				netbk_tx_err(vnet_desc, &txreq, idx);
				continue;
			}
		}

		/* XXX could copy straight to head */
		page = netbk_alloc_page(skb, pending_idx);
		if (!page) {
			kfree_skb(skb);
			netbk_tx_err(vnet_desc, &txreq, idx);
			continue;
		}

		gop->source.u.ref = txreq.gref;
		gop->source.domid = vnet_desc->rings.dev->otherend_id;
		gop->source.offset = txreq.offset;

		gop->dest.u.gmfn = virt_to_mfn(page_address(page));
		gop->dest.domid = DOMID_SELF;
		gop->dest.offset = txreq.offset;

		gop->len = txreq.size;
		gop->flags = GNTCOPY_source_gref;

		gop++;

		memcpy(&vnet.pending_tx_info[pending_idx].req, &txreq, sizeof(txreq));

		vnet.pending_tx_info[pending_idx].vnet_desc = vnet_desc;
		*((u16 *)skb->data) = pending_idx;

		__skb_put(skb, data_len);

		skb_shinfo(skb)->nr_frags = ret;

		if (data_len < txreq.size) {
			skb_shinfo(skb)->nr_frags++;
			frag_set_pending_idx(&skb_shinfo(skb)->frags[0], pending_idx);
		} else
			frag_set_pending_idx(&skb_shinfo(skb)->frags[0], INVALID_PENDING_IDX);


		vnet.pending_cons++;

		request_gop = netbk_get_requests(vnet_desc, skb, txfrags, gop);
		if (request_gop == NULL) {
			kfree_skb(skb);
			netbk_tx_err(vnet_desc, &txreq, idx);
			continue;
		}
		gop = request_gop;

		__skb_queue_tail(&vnet.tx_queue, skb);

		vnet_desc->rings.ring_tx.sring->req_cons = idx;
		netbk_check_rx_vnet(vnet_desc);

		if ((gop - vnet.tx_copy_ops) >= ARRAY_SIZE(vnet.tx_copy_ops))
			break;
	}

	return gop - vnet.tx_copy_ops;
}


static void netbk_idx_release(u16 pending_idx)
{
	vnet_desc_t *vnet_desc;
	struct pending_tx_info *pending_tx_info;
	unsigned int index;

	/* Already complete? */
	if (vnet.mmap_pages[pending_idx] == NULL)
		return;

	pending_tx_info = &vnet.pending_tx_info[pending_idx];

	vnet_desc = pending_tx_info->vnet_desc;

	make_tx_response(vnet_desc, &pending_tx_info->req, NETIF_RSP_OKAY);

	index = pending_index(vnet.pending_prod++);
	vnet.pending_ring[index] = pending_idx;

	vnet_desc_put(vnet_desc);

	vnet.mmap_pages[pending_idx]->mapping = 0;
	put_page(vnet.mmap_pages[pending_idx]);

	vnet.mmap_pages[pending_idx] = NULL;
}

static int netbk_tx_check_gop(struct sk_buff *skb, struct gnttab_copy **gopp)
{
	struct gnttab_copy *gop = *gopp;
	u16 pending_idx = *((u16 *)skb->data);
	struct pending_tx_info *pending_tx_info = vnet.pending_tx_info;
	vnet_desc_t *vnet_desc = pending_tx_info[pending_idx].vnet_desc;
	struct netif_tx_request *txp;
	struct skb_shared_info *shinfo = skb_shinfo(skb);
	int nr_frags = shinfo->nr_frags;
	int i, err, start;

	/* Check status of header. */
	err = gop->status;
	if (unlikely(err)) {
		unsigned int index;
		index = pending_index(vnet.pending_prod++);
		txp = &pending_tx_info[pending_idx].req;
		make_tx_response(vnet_desc, txp, NETIF_RSP_ERROR);
		vnet.pending_ring[index] = pending_idx;
		vnet_desc_put(vnet_desc);
	}

	/* Skip first skb fragment if it is on same page as header fragment. */
	start = (frag_get_pending_idx(&shinfo->frags[0]) == pending_idx);

	for (i = start; i < nr_frags; i++) {
		int j, newerr;
		unsigned int index;

		pending_idx = frag_get_pending_idx(&shinfo->frags[i]);

		/* Check error status: if okay then remember grant handle. */
		newerr = (++gop)->status;
		if (likely(!newerr)) {
			/* Had a previous error? Invalidate this fragment. */
			if (unlikely(err))
				netbk_idx_release(pending_idx);
			continue;
		}

		/* Error on this fragment: respond to client with an error. */
		txp = &vnet.pending_tx_info[pending_idx].req;
		make_tx_response(vnet_desc, txp, NETIF_RSP_ERROR);
		index = pending_index(vnet.pending_prod++);
		vnet.pending_ring[index] = pending_idx;

		vnet_desc_put(vnet_desc);

		/* Not the first error? Preceding frags already invalidated. */
		if (err)
			continue;

		/* First error: invalidate header and preceding fragments. */
		pending_idx = *((u16 *)skb->data);
		netbk_idx_release(pending_idx);

		for (j = start; j < i; j++) {
			pending_idx = frag_get_pending_idx(&shinfo->frags[j]);
			netbk_idx_release(pending_idx);
		}

		/* Remember the error: invalidate all subsequent fragments. */
		err = newerr;
	}

	*gopp = gop + 1;
	return err;
}

static inline unsigned long idx_to_pfn(u16 idx)
{
	return page_to_pfn(vnet.mmap_pages[idx]);
}


static inline unsigned long idx_to_kaddr(u16 idx)
{
	return (unsigned long)pfn_to_kaddr(idx_to_pfn(idx));
}

static void netbk_fill_frags(struct sk_buff *skb)
{
	struct skb_shared_info *shinfo = skb_shinfo(skb);
	int nr_frags = shinfo->nr_frags;
	int i;

	for (i = 0; i < nr_frags; i++) {
		skb_frag_t *frag = shinfo->frags + i;
		struct netif_tx_request *txp;
		struct page *page;
		u16 pending_idx;

		pending_idx = frag_get_pending_idx(frag);

		txp = &vnet.pending_tx_info[pending_idx].req;

		page = virt_to_page(idx_to_kaddr(pending_idx));
		__skb_fill_page_desc(skb, i, page, txp->offset, txp->size);
		skb->len += txp->size;
		skb->data_len += txp->size;
		skb->truesize += txp->size;

		/* Take an extra reference to offset xen_netbk_idx_release */
		get_page(vnet.mmap_pages[pending_idx]);
		netbk_idx_release(pending_idx);
	}
}

static int checksum_setup(vnet_desc_t *vnet_desc, struct sk_buff *skb)
{
	struct iphdr *iph;
	unsigned char *th;
	int err = -EPROTO;
	int recalculate_partial_csum = 0;

	/*
	 * A GSO SKB must be CHECKSUM_PARTIAL. However some buggy
	 * peers can fail to set NETRXF_csum_blank when sending a GSO
	 * frame. In this case force the SKB to CHECKSUM_PARTIAL and
	 * recalculate the partial checksum.
	 */
	if (skb->ip_summed != CHECKSUM_PARTIAL && skb_is_gso(skb)) {
		vnet_desc->rx_gso_checksum_fixup++;
		skb->ip_summed = CHECKSUM_PARTIAL;
		recalculate_partial_csum = 1;
	}

	/* A non-CHECKSUM_PARTIAL SKB does not require setup. */
	if (skb->ip_summed != CHECKSUM_PARTIAL)
		return 0;

	if (skb->protocol != htons(ETH_P_IP))
		goto out;

	iph = (void *)skb->data;
	th = skb->data + 4 * iph->ihl;
	if (th >= skb_tail_pointer(skb))
		goto out;

	skb->csum_start = th - skb->head;

	switch (iph->protocol) {
	case IPPROTO_TCP:
		skb->csum_offset = offsetof(struct tcphdr, check);

		if (recalculate_partial_csum) {
			struct tcphdr *tcph = (struct tcphdr *)th;
			tcph->check = ~csum_tcpudp_magic(iph->saddr, iph->daddr, skb->len - iph->ihl*4, IPPROTO_TCP, 0);
		}
		break;

	case IPPROTO_UDP:
		skb->csum_offset = offsetof(struct udphdr, check);

		if (recalculate_partial_csum) {
			struct udphdr *udph = (struct udphdr *)th;
			udph->check = ~csum_tcpudp_magic(iph->saddr, iph->daddr, skb->len - iph->ihl*4, IPPROTO_UDP, 0);
		}
		break;

	default:
		if (net_ratelimit())
			netdev_err(vnet_desc->dev, "Attempting to checksum a non-TCP/UDP packet, dropping a protocol %d packet\n", iph->protocol);
		goto out;
	}

	if ((th + skb->csum_offset + 2) > skb_tail_pointer(skb))
		goto out;

	err = 0;

out:
	return err;
}

void vif_receive_skb(struct sk_buff *skb)
{
	netif_rx_ni(skb);
}

static void netbk_tx_submit(void)
{
	struct gnttab_copy *gop = vnet.tx_copy_ops;
	struct sk_buff *skb;

	while ((skb = __skb_dequeue(&vnet.tx_queue)) != NULL) {
		struct netif_tx_request *txp;
		vnet_desc_t *vnet_desc;
		u16 pending_idx;
		unsigned data_len;

		pending_idx = *((u16 *)skb->data);

		vnet_desc = vnet.pending_tx_info[pending_idx].vnet_desc;
		txp = &vnet.pending_tx_info[pending_idx].req;

		/* Check the remap error code. */
		if (unlikely(netbk_tx_check_gop(skb, &gop))) {
			netdev_dbg(vnet_desc->dev, "netback grant failed.\n");
			skb_shinfo(skb)->nr_frags = 0;
			kfree_skb(skb);
			continue;
		}

		data_len = skb->len;
		memcpy(skb->data, (void *)(idx_to_kaddr(pending_idx)|txp->offset), data_len);
		if (data_len < txp->size) {
			/* Append the packet payload as a fragment. */
			txp->offset += data_len;
			txp->size -= data_len;
		} else
			/* Schedule a response immediately. */
			netbk_idx_release(pending_idx);


		if (txp->flags & VNETTXF_csum_blank)
			skb->ip_summed = CHECKSUM_PARTIAL;
		else if (txp->flags & VNETTXF_data_validated)
			skb->ip_summed = CHECKSUM_UNNECESSARY;

		netbk_fill_frags(skb);

		/*
		 * If the initial fragment was < PKT_PROT_LEN then
		 * pull through some bytes from the other fragments to
		 * increase the linear region to PKT_PROT_LEN bytes.
		 */
		if (skb_headlen(skb) < PKT_PROT_LEN && skb_is_nonlinear(skb)) {
			int target = min_t(int, skb->len, PKT_PROT_LEN);
			__pskb_pull_tail(skb, target - skb_headlen(skb));
		}

		skb->dev      = vnet_desc->dev;
		skb->protocol = eth_type_trans(skb, skb->dev);

		if (checksum_setup(vnet_desc, skb)) {
			netdev_dbg(vnet_desc->dev, "Can't setup checksum in net_tx_action\n");
			kfree_skb(skb);
			continue;
		}

		vnet_desc->dev->stats.rx_bytes += skb->len;
		vnet_desc->dev->stats.rx_packets++;

		vif_receive_skb(skb);
	}
}

/*
 * Called after netfront has transmitted something; this has to be propagated in
 * the SOO networking subsystem.
 */
static void netbk_tx_action(void)
{
	unsigned nr_gops;
	int ret;

	nr_gops = netbk_tx_build_gops();

	if (nr_gops == 0)
		return;

	ret = grant_table_op(GNTTABOP_copy, vnet.tx_copy_ops, nr_gops);
	BUG_ON(ret);

	netbk_tx_submit();

}

/*
 * Returns true if we should start a new receive buffer instead of
 * adding 'size' bytes to a buffer which currently contains 'offset'
 * bytes.
 */
static bool start_new_rx_buffer(int offset, unsigned long size, int head)
{
	/* simple case: we have completely filled the current buffer. */
	if (offset == MAX_BUFFER_OFFSET)
		return true;

	/*
	 * complex case: start a fresh buffer if the current frag
	 * would overflow the current buffer but only if:
	 *     (i)   this frag would fit completely in the next buffer
	 * and (ii)  there is already some data in the current buffer
	 * and (iii) this is not the head buffer.
	 *
	 * Where:
	 * - (i) stops us splitting a frag into two copies
	 *   unless the frag is too large for a single buffer.
	 * - (ii) stops us from leaving a buffer pointlessly empty.
	 * - (iii) stops us leaving the first buffer
	 *   empty. Strictly speaking this is already covered
	 *   by (ii) but is explicitly checked because
	 *   netfront relies on the first buffer being
	 *   non-empty and can crash otherwise.
	 *
	 * This means we will effectively linearise small
	 * frags but do not needlessly split large buffers
	 * into multiple copies tend to give large frags their
	 * own buffers as before.
	 */
	if ((offset + size > MAX_BUFFER_OFFSET) &&
	    (size <= MAX_BUFFER_OFFSET) && offset && !head)
		return true;

	return false;
}

static struct netbk_rx_meta *get_next_rx_buffer(vnet_desc_t *vnet_desc, struct netrx_pending_operations *npo)
{
	struct netbk_rx_meta *meta;
	struct netif_rx_request *req;

	req = RING_GET_REQUEST(&vnet_desc->rings.ring_rx, vnet_desc->rings.ring_rx.sring->req_cons++);

	meta = npo->meta + npo->meta_prod++;
	meta->gso_size = 0;
	meta->size = 0;
	meta->id = req->id;

	npo->copy_off = 0;
	npo->copy_gref = req->gref;

	return meta;
}

/*
 * Set up the grant operations for this fragment. If it's a flipping
 * interface, we also set up the unmap request from here.
 */
static void netbk_gop_frag_copy(vnet_desc_t *vnet_desc, struct sk_buff *skb,
		struct netrx_pending_operations *npo,
		struct page *page, unsigned long size,
		unsigned long offset, int *head)
{
	struct gnttab_copy *copy_gop;
	struct netbk_rx_meta *meta;
	/*
	 * These variables are used iff get_page_ext returns true,
	 * in which case they are guaranteed to be initialized.
	 */
	unsigned int uninitialized_var(group), uninitialized_var(idx);
	int foreign = get_page_ext(page, &idx);
	unsigned long bytes;

	/* Data must not cross a page boundary. */
	BUG_ON(size + offset > PAGE_SIZE);

	meta = npo->meta + npo->meta_prod - 1;

	while (size > 0) {
		BUG_ON(npo->copy_off > MAX_BUFFER_OFFSET);

		if (start_new_rx_buffer(npo->copy_off, size, *head)) {
			/*
			 * Netfront requires there to be some data in the head
			 * buffer.
			 */
			BUG_ON(*head);

			meta = get_next_rx_buffer(vnet_desc, npo);
		}

		bytes = size;
		if (npo->copy_off + bytes > MAX_BUFFER_OFFSET)
			bytes = MAX_BUFFER_OFFSET - npo->copy_off;

		copy_gop = npo->copy + npo->copy_prod++;
		copy_gop->flags = GNTCOPY_dest_gref;
		if (foreign) {
			struct pending_tx_info *src_pend;

			src_pend = &vnet.pending_tx_info[idx];

			copy_gop->source.domid = src_pend->vnet_desc->rings.dev->otherend_id;
			copy_gop->source.u.ref = src_pend->req.gref;
			copy_gop->flags |= GNTCOPY_source_gref;
		} else {
			void *vaddr = page_address(page);

			copy_gop->source.domid = DOMID_SELF;
			copy_gop->source.u.gmfn = virt_to_mfn(vaddr);
		}
		copy_gop->source.offset = offset;
		copy_gop->dest.domid = vnet_desc->rings.dev->otherend_id;

		copy_gop->dest.offset = npo->copy_off;
		copy_gop->dest.u.ref = npo->copy_gref;
		copy_gop->len = bytes;

		npo->copy_off += bytes;
		meta->size += bytes;

		offset += bytes;
		size -= bytes;

		/* Leave a gap for the GSO descriptor. */
		if (*head && skb_shinfo(skb)->gso_size && !vnet_desc->gso_prefix)
			vnet_desc->rings.ring_rx.sring->req_cons++;

		*head = 0; /* There must be something in this buffer now. */

	}
}

/*
 * Prepare an SKB to be transmitted to the frontend.
 *
 * This function is responsible for allocating grant operations, meta
 * structures, etc.
 *
 * It returns the number of meta structures consumed. The number of
 * ring slots used is always equal to the number of meta slots used
 * plus the number of GSO descriptors used. Currently, we use either
 * zero GSO descriptors (for non-GSO packets) or one descriptor (for
 * frontend-side LRO).
 */
static int netbk_gop_skb(struct sk_buff *skb, struct netrx_pending_operations *npo)
{
	vnet_desc_t *vnet_desc = netdev_to_desc(skb->dev);
	int nr_frags = skb_shinfo(skb)->nr_frags;
	int i;
	struct netif_rx_request *req;
	struct netbk_rx_meta *meta;
	unsigned char *data;
	int head = 1;
	int old_meta_prod;

	old_meta_prod = npo->meta_prod;

	/* Set up a GSO prefix descriptor, if necessary */
	if (skb_shinfo(skb)->gso_size && vnet_desc->gso_prefix) {
		req = RING_GET_REQUEST(&vnet_desc->rings.ring_rx, vnet_desc->rings.ring_rx.sring->req_cons++);
		meta = npo->meta + npo->meta_prod++;
		meta->gso_size = skb_shinfo(skb)->gso_size;
		meta->size = 0;
		meta->id = req->id;
	}

	req = RING_GET_REQUEST(&vnet_desc->rings.ring_rx, vnet_desc->rings.ring_rx.sring->req_cons++);
	meta = npo->meta + npo->meta_prod++;

	if (!vnet_desc->gso_prefix)
		meta->gso_size = skb_shinfo(skb)->gso_size;
	else
		meta->gso_size = 0;

	meta->size = 0;
	meta->id = req->id;
	npo->copy_off = 0;
	npo->copy_gref = req->gref;

	data = skb->data;
	while (data < skb_tail_pointer(skb)) {
		unsigned int offset = offset_in_page(data);
		unsigned int len = PAGE_SIZE - offset;

		if (data + len > skb_tail_pointer(skb))
			len = skb_tail_pointer(skb) - data;

		netbk_gop_frag_copy(vnet_desc, skb, npo, virt_to_page(data), len, offset, &head);
		data += len;
	}

	for (i = 0; i < nr_frags; i++) {
		netbk_gop_frag_copy(vnet_desc, skb, npo,
				    skb_frag_page(&skb_shinfo(skb)->frags[i]),
				    skb_frag_size(&skb_shinfo(skb)->frags[i]),
				    skb_shinfo(skb)->frags[i].page_offset,
				    &head);
	}

	return npo->meta_prod - old_meta_prod;
}

/*
 * This is a twin to netbk_gop_skb.  Assume that netbk_gop_skb was
 * used to set up the operations on the top of
 * netrx_pending_operations, which have since been done.  Check that
 * they didn't give any errors and advance over them.
 */
static int netbk_check_gop(vnet_desc_t *vnet_desc, int nr_meta_slots, struct netrx_pending_operations *npo)
{
	struct gnttab_copy     *copy_op;
	int status = NETIF_RSP_OKAY;
	int i;

	for (i = 0; i < nr_meta_slots; i++) {
		copy_op = npo->copy + npo->copy_cons++;
		if (copy_op->status != GNTST_okay) {
			netdev_dbg(vnet_desc->dev, "Bad status %d from copy to DOM%d.\n", copy_op->status, vnet_desc->rings.dev->otherend_id);
			status = NETIF_RSP_ERROR;
		}
	}

	return status;
}

static struct netif_rx_response *make_rx_response(vnet_desc_t *vnet_desc, u16 id, s8 st, u16 offset, u16 size, u16 flags)
{
	RING_IDX i = vnet_desc->rings.ring_rx.rsp_prod_pvt;
	struct netif_rx_response *resp;

	resp = RING_GET_RESPONSE(&vnet_desc->rings.ring_rx, i);
	resp->offset     = offset;
	resp->flags      = flags;
	resp->id         = id;
	resp->status     = (s16)size;
	if (st < 0)
		resp->status = (s16)st;

	vnet_desc->rings.ring_rx.rsp_prod_pvt = ++i;

	return resp;
}


static void netbk_add_frag_responses(vnet_desc_t *vnet_desc, int status, struct netbk_rx_meta *meta, int nr_meta_slots)
{
	int i;
	unsigned long offset;

	/* No fragments used */
	if (nr_meta_slots <= 1)
		return;

	nr_meta_slots--;

	for (i = 0; i < nr_meta_slots; i++) {
		int flags;
		if (i == nr_meta_slots - 1)
			flags = 0;
		else
			flags = VNETRXF_more_data;

		offset = 0;
		make_rx_response(vnet_desc, meta[i].id, status, offset, meta[i].size, flags);
	}
}

void notify_tx_completion(vnet_desc_t *vnet_desc)
{
	if (netif_queue_stopped(vnet_desc->dev) && vnet_rx_schedulable(vnet_desc))
		netif_wake_queue(vnet_desc->dev);
}

static void netbk_rx_action(void)
{
	vnet_desc_t *vnet_desc = NULL, *tmp;
	s8 status;
	u16 irq, flags;
	struct netif_rx_response *resp;
	struct sk_buff_head rxq;
	struct sk_buff *skb;
	LIST_HEAD(notify);
	int ret;
	int nr_frags;
	int count;
	unsigned long offset;
	struct skb_cb_overlay *sco;

	struct netrx_pending_operations npo = {
		.copy  = vnet.grant_copy_op,
		.meta  = vnet.meta,
	};

	skb_queue_head_init(&rxq);

	count = 0;

	while ((skb = skb_dequeue(&vnet.rx_queue)) != NULL) {
		vnet_desc = netdev_to_desc(skb->dev);
		nr_frags = skb_shinfo(skb)->nr_frags;

		sco = (struct skb_cb_overlay *)skb->cb;
		sco->meta_slots_used = netbk_gop_skb(skb, &npo);

		count += nr_frags + 1;

		__skb_queue_tail(&rxq, skb);

		/* Filled the batch queue? */
		if (count + MAX_SKB_FRAGS >= NETIF_RX_RING_SIZE)
			break;
	}

	BUG_ON(npo.meta_prod > ARRAY_SIZE(vnet.meta));

	if (!npo.copy_prod)
		return;

	BUG_ON(npo.copy_prod > ARRAY_SIZE(vnet.grant_copy_op));
	ret = grant_table_op(GNTTABOP_copy, &vnet.grant_copy_op, npo.copy_prod);
	BUG_ON(ret != 0);

	while ((skb = __skb_dequeue(&rxq)) != NULL) {
		sco = (struct skb_cb_overlay *)skb->cb;

		vnet_desc = netdev_to_desc(skb->dev);

		if (vnet.meta[npo.meta_cons].gso_size && vnet_desc->gso_prefix) {
			resp = RING_GET_RESPONSE(&vnet_desc->rings.ring_rx, vnet_desc->rings.ring_rx.rsp_prod_pvt++);

			resp->flags = VNETRXF_gso_prefix | VNETRXF_more_data;

			resp->offset = vnet.meta[npo.meta_cons].gso_size;
			resp->id = vnet.meta[npo.meta_cons].id;
			resp->status = sco->meta_slots_used;

			npo.meta_cons++;
			sco->meta_slots_used--;
		}


		vnet_desc->dev->stats.tx_bytes += skb->len;
		vnet_desc->dev->stats.tx_packets++;

		status = netbk_check_gop(vnet_desc, sco->meta_slots_used, &npo);

		if (sco->meta_slots_used == 1)
			flags = 0;
		else
			flags = VNETRXF_more_data;

		if (skb->ip_summed == CHECKSUM_PARTIAL) /* local packet? */
			flags |= VNETRXF_csum_blank | VNETRXF_data_validated;
		else if (skb->ip_summed == CHECKSUM_UNNECESSARY)
			/* remote but checksummed. */
			flags |= VNETRXF_data_validated;

		offset = 0;
		resp = make_rx_response(vnet_desc, vnet.meta[npo.meta_cons].id, status, offset, vnet.meta[npo.meta_cons].size, flags);

		if (vnet.meta[npo.meta_cons].gso_size && !vnet_desc->gso_prefix) {
			struct netif_extra_info *gso = (struct netif_extra_info *) RING_GET_RESPONSE(&vnet_desc->rings.ring_rx, vnet_desc->rings.ring_rx.rsp_prod_pvt++);

			resp->flags |= VNETRXF_extra_info;

			gso->u.gso.size = vnet.meta[npo.meta_cons].gso_size;
			gso->u.gso.type = NETIF_GSO_TYPE_TCPV4;
			gso->u.gso.pad = 0;
			gso->u.gso.features = 0;

			gso->type = NETIF_EXTRA_TYPE_GSO;
			gso->flags = 0;
		}

		netbk_add_frag_responses(vnet_desc, status, vnet.meta + npo.meta_cons + 1, sco->meta_slots_used);

		RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&vnet_desc->rings.ring_rx, ret);
		irq = vnet_desc->rings.irq;
		if (ret && list_empty(&vnet_desc->notify_list))
			list_add_tail(&vnet_desc->notify_list, &notify);

		notify_tx_completion(vnet_desc);

		vnet_desc_put(vnet_desc);
		npo.meta_cons += sco->meta_slots_used;
		dev_kfree_skb(skb);
	}

	list_for_each_entry_safe(vnet_desc, tmp, &notify, notify_list) {
		notify_remote_via_irq(vnet_desc->rings.irq);
		list_del_init(&vnet_desc->notify_list);
	}

	/* More work to do? */
	if (!skb_queue_empty(&vnet.rx_queue) && !timer_pending(&vnet.net_timer))
		wake_up(&vnet.wq); /* Kick the thread */
}


/*
 * Main backend thread to preocess tx and rx work
 */
static int netbk_kthread(void *data)
{

	while (!kthread_should_stop()) {

		wait_event_interruptible(vnet.wq, rx_work_todo() || tx_work_todo() ||	kthread_should_stop());
		cond_resched();

		if (kthread_should_stop())
			break;

		if (rx_work_todo())
			netbk_rx_action();

		if (tx_work_todo())
			netbk_tx_action();
	}

	return 0;
}

/*
 * Interface to Linux subsystem
 */

/*
 * Handle the creation of the hotplug script environment.  We add the script
 * and vif variables to the environment, for the benefit of the vif-* hotplug
 * scripts.
 */
int netback_uevent(struct vbus_device *dev, struct kobj_uevent_env *env)
{
  vnet_desc_t *vnet_desc = &vnet.descs[dev->otherend_id];
	char *val;

	val = vbus_read(VBT_NIL, dev->nodename, "script", NULL);
	if (IS_ERR(val)) {
		lprintk("%s - line %d: Reading script failed for device name %s\n", __func__, __LINE__, dev->nodename);
		BUG();
	} else {

		if (add_uevent_var(env, "script=%s", val)) {
			kfree(val);
			return -ENOMEM;
		}

		kfree(val);
	}

	return add_uevent_var(env, "vnet=%s", vnet_desc->dev->name);
}


static int vnet_get_sset_count(struct net_device *dev, int string_set)
{
	switch (string_set) {

	case ETH_SS_STATS:
		return ARRAY_SIZE(vnet_stats);

	default:
		return -EINVAL;
	}
}

static void vnet_get_ethtool_stats(struct net_device *dev, struct ethtool_stats *stats, u64 *data)
{
	void *vnet_desc = netdev_to_desc(dev);
	int i;

	for (i = 0; i < ARRAY_SIZE(vnet_stats); i++)
		data[i] = *(unsigned long *)(vnet_desc + vnet_stats[i].offset);
}

static void vnet_get_strings(struct net_device *dev, u32 stringset, u8 *data)
{
	int i;

	switch (stringset) {

	case ETH_SS_STATS:
		for (i = 0; i < ARRAY_SIZE(vnet_stats); i++)
			memcpy(data + i * ETH_GSTRING_LEN, vnet_stats[i].name, ETH_GSTRING_LEN);
		break;
	}
}

/*
 * Figure out how many ring slots we're going to need to send @skb to
 * the guest. This function is essentially a dry run of
 * netbk_gop_frag_copy.
 */
unsigned int netbk_count_skb_slots(vnet_desc_t *vnet_desc, struct sk_buff *skb)
{
	unsigned int count;
	int i, copy_off;

	count = DIV_ROUND_UP(skb_headlen(skb), PAGE_SIZE);

	copy_off = skb_headlen(skb) % PAGE_SIZE;

	if (skb_shinfo(skb)->gso_size)
		count++;

	for (i = 0; i < skb_shinfo(skb)->nr_frags; i++) {
		unsigned long size = skb_frag_size(&skb_shinfo(skb)->frags[i]);
		unsigned long bytes;
		while (size > 0) {
			BUG_ON(copy_off > MAX_BUFFER_OFFSET);

			if (start_new_rx_buffer(copy_off, size, 0)) {
				count++;
				copy_off = 0;
			}

			bytes = size;
			if (copy_off + bytes > MAX_BUFFER_OFFSET)
				bytes = MAX_BUFFER_OFFSET - copy_off;

			copy_off += bytes;
			size -= bytes;
		}
	}
	return count;
}


void netbk_queue_tx_skb(vnet_desc_t *vnet_desc, struct sk_buff *skb)
{
	skb_queue_tail(&vnet.rx_queue, skb);

	wake_up(&vnet.wq); /* Kick the thread */
}

int netbk_must_stop_queue(vnet_desc_t *vnet_desc)
{
	if (!netbk_rx_ring_full(vnet_desc))
		return 0;

	vnet_desc->rings.ring_rx.sring->req_event = vnet_desc->rx_req_cons_peek + max_required_rx_slots(vnet_desc);
	dmb(); /* request notification /then/ check the queue */

	return netbk_rx_ring_full(vnet_desc);
}

/*
 * Start to transmit to the frontend.
 */
static int vnet_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	vnet_desc_t *vnet_desc = netdev_to_desc(dev);

	BUG_ON(skb->dev != dev);

	/* Drop the packet if the target domain has no receive buffers. */
	if (!vnet_rx_schedulable(vnet_desc))
		goto drop;

	/* Reserve ring slots for the worst-case number of fragments. */
	vnet_desc->rx_req_cons_peek += netbk_count_skb_slots(vnet_desc, skb);
	vnet_desc_get(vnet_desc);

	if (vnet_desc->can_queue && netbk_must_stop_queue(vnet_desc))
		netif_stop_queue(dev);

	netbk_queue_tx_skb(vnet_desc, skb);

	return NETDEV_TX_OK;

 drop:
	vnet_desc->dev->stats.tx_dropped++;

	dev_kfree_skb(skb);

	return NETDEV_TX_OK;
}

static struct net_device_stats *vnet_get_stats(struct net_device *dev)
{
	vnet_desc_t *vnet_desc = netdev_to_desc(dev);

	return &vnet_desc->dev->stats;
}

void netbk_deschedule_vnet(vnet_desc_t *vnet_desc)
{
	spin_lock_irq(&vnet.net_schedule_list_lock);

	remove_from_net_schedule_list(vnet_desc);

	spin_unlock_irq(&vnet.net_schedule_list_lock);
}

static void vnet_up(vnet_desc_t *vnet_desc) {
	atomic_inc(&vnet.netfront_count);

	enable_irq(vnet_desc->rings.irq);

	netbk_check_rx_vnet(vnet_desc);
}

static void vnet_down(vnet_desc_t *vnet_desc) {
	disable_irq(vnet_desc->rings.irq);

	netbk_deschedule_vnet(vnet_desc);

	atomic_dec(&vnet.netfront_count);
}

static int vnet_open(struct net_device *dev)
{
	vnet_desc_t *vnet_desc  = netdev_to_desc(dev);

	if (netif_carrier_ok(dev))
		vnet_up(vnet_desc);

	netif_start_queue(dev);

	return 0;
}

static int vnet_close(struct net_device *dev)
{
	vnet_desc_t *vnet_desc = netdev_to_desc(dev);

	if (netif_carrier_ok(dev))
		vnet_down(vnet_desc);

	netif_stop_queue(dev);
	return 0;
}

static int vnet_change_mtu(struct net_device *dev, int mtu)
{
	vnet_desc_t *vnet_desc = netdev_to_desc(dev);
	int max = vnet_desc->can_sg ? 65535 - VLAN_ETH_HLEN : ETH_DATA_LEN;

	if (mtu > max)
		return -EINVAL;

	dev->mtu = mtu;
	return 0;
}


static netdev_features_t vnet_fix_features(struct net_device *dev, netdev_features_t features)
{
	vnet_desc_t *vnet_desc = netdev_to_desc(dev);

	if (!vnet_desc->can_sg)
		features &= ~NETIF_F_SG;

	if (!vnet_desc->gso && !vnet_desc->gso_prefix)
		features &= ~NETIF_F_TSO;

	if (!vnet_desc->csum)
		features &= ~NETIF_F_IP_CSUM;

	return features;
}

static const struct ethtool_ops vnet_ethtool_ops = {
	.get_link	= ethtool_op_get_link,

	.get_sset_count = vnet_get_sset_count,
	.get_ethtool_stats = vnet_get_ethtool_stats,
	.get_strings = vnet_get_strings,
};

static const struct net_device_ops vnet_netdev_ops = {
	.ndo_start_xmit	= vnet_start_xmit,
	.ndo_get_stats	= vnet_get_stats,
	.ndo_open	= vnet_open,
	.ndo_stop	= vnet_close,
	.ndo_change_mtu	= vnet_change_mtu,
	.ndo_fix_features = vnet_fix_features,
};


vnet_desc_t *vnet_desc_alloc(struct device *parent, domid_t domid)
{
	int err;
	struct net_device *dev;
	vnet_desc_t *vnet_desc;
	domid_t *p_domid;
	char name[IFNAMSIZ] = {};

	snprintf(name, IFNAMSIZ - 1, "vnet%u", domid);

	dev = alloc_netdev_mq(sizeof(domid_t), name, NET_NAME_UNKNOWN, ether_setup, TX_QUEUES_NR);

	if (dev == NULL) {
		pr_warn("Could not allocate netdev\n");
		return ERR_PTR(-ENOMEM);
	}

	SET_NETDEV_DEV(dev, parent);

	/*
	 * We set the domid value as private data of the netdev structure.
	 * For such structure, private data is appended to the canonical structure.
	 */
	p_domid = netdev_priv(dev);
	*p_domid = domid;

	vnet_desc = &vnet.descs[domid];

	vnet_desc->can_sg = 1;
	vnet_desc->csum = 1;

	atomic_set(&vnet_desc->refcnt, 1);
	init_waitqueue_head(&vnet_desc->waiting_to_free);

	vnet_desc->dev = dev;

	INIT_LIST_HEAD(&vnet_desc->schedule_list);
	INIT_LIST_HEAD(&vnet_desc->notify_list);

	vnet_desc->credit_bytes = vnet_desc->remaining_credit = ~0UL;
	vnet_desc->credit_usec  = 0UL;

	init_timer(&vnet_desc->credit_timeout);

	/* Initialize 'expires' now: it's used to track the credit window. */
	vnet_desc->credit_timeout.expires = jiffies;

	dev->netdev_ops	= &vnet_netdev_ops;
	dev->hw_features = NETIF_F_SG | NETIF_F_IP_CSUM | NETIF_F_TSO;
	dev->features = dev->hw_features;
	dev->ethtool_ops = &vnet_ethtool_ops;

	dev->tx_queue_len = VNET_QUEUE_LENGTH;

	/*
	 * Initialise a dummy MAC address. We choose the numerically
	 * largest non-broadcast address to prevent the address getting
	 * stolen by an Ethernet bridge for STP purposes.
	 * (FE:FF:FF:FF:FF:FF)
	 */
	memset(dev->dev_addr, 0xFF, ETH_ALEN);
	dev->dev_addr[0] &= ~0x01;

	netif_carrier_off(dev);

	err = register_netdev(dev);
	if (err) {
		netdev_warn(dev, "Could not register device: err=%d\n", err);
		free_netdev(dev);
		return ERR_PTR(err);
	}

	netdev_dbg(dev, "Successfully created vnet\n");

	return vnet_desc;
}

int vnet_subsys_init(struct vbus_device *dev) {
	struct vbus_transaction vbt;
	const char *message = NULL;
	int res;

	/* Keep a reference to the corresponding vbus dev */
	vnet.descs[dev->otherend_id].rings.dev = dev;

	vbus_transaction_start(&vbt);

	res = vbus_printf(vbt, dev->nodename, "mac", "%s", "00:16:3e:01:01:00");

	if (res) {
		message = "writing mac";
		goto abort_transaction;
	}

	res = vbus_printf(vbt, dev->nodename, "feature-sg", "%d", 1);
	if (res) {
		message = "writing feature-sg";
		goto abort_transaction;
	}

	res = vbus_printf(vbt, dev->nodename, "feature-gso-tcpv4", "%d", 1);
	if (res) {
		message = "writing feature-gso-tcpv4";
		goto abort_transaction;
	}

	/* We support rx-copy path. */
	res = vbus_printf(vbt, dev->nodename, "feature-rx-copy", "%d", 1);
	if (res) {
		message = "writing feature-rx-copy";
		goto abort_transaction;
	}

	/*
	 * We don't support rx-flip path.
	 */
	res = vbus_printf(vbt, dev->nodename, "feature-rx-flip", "%d", 0);
	if (res) {
		message = "writing feature-rx-flip";
		goto abort_transaction;
	}

	vbus_transaction_end(vbt);

	return 0;

abort_transaction:
	lprintk("%s: failed: %s\n", __func__, message);
	BUG();

}

void vnet_subsys_remove(struct vbus_device *dev) {
	struct vbus_transaction vbt;
	const char *message = NULL;
	int res;

	/* Removing related Vbstore entries */

	vbus_transaction_start(&vbt);

	res = vbus_rm(vbt, dev->nodename, "mac");

	if (res) {
		message = "removing mac";
		goto abort_transaction;
	}

	res = vbus_rm(vbt, dev->nodename, "feature-sg");
	if (res) {
		message = "removing feature-sg";
		goto abort_transaction;
	}

	res = vbus_rm(vbt, dev->nodename, "feature-gso-tcpv4");
	if (res) {
		message = "removing feature-gso-tcpv4";
		goto abort_transaction;
	}

	res = vbus_rm(vbt, dev->nodename, "feature-rx-copy");
	if (res) {
		message = "removing feature-rx-copy";
		goto abort_transaction;
	}

	res = vbus_rm(vbt, dev->nodename, "feature-rx-flip");
	if (res) {
		message = "removing feature-rx-flip";
		goto abort_transaction;
	}

	vbus_transaction_end(vbt);

	goto out;

abort_transaction:

	vbus_transaction_end(vbt);
	if (message) {
		lprintk("%s - line %d: Transaction failed for device name %s\n", __func__, __LINE__, dev->nodename);
		BUG();
	}

out:

	vnet.descs[dev->otherend_id].rings.dev = NULL;

}

static int vnet_read_mac(struct vbus_device *dev, u8 mac[])
{
	char *s, *e, *macstr;
	int i;

	macstr = s = vbus_read(VBT_NIL, dev->nodename, "mac", NULL);
	if (IS_ERR(macstr))
		return PTR_ERR(macstr);

	for (i = 0; i < ETH_ALEN; i++) {
		mac[i] = simple_strtoul(s, &e, 16);
		if ((s == e) || (*e != ((i == ETH_ALEN-1) ? '\0' : ':'))) {
			kfree(macstr);
			return -ENOENT;
		}
		s = e+1;
	}

	kfree(macstr);
	return 0;
}

static void vnet_read_rate(struct vbus_device *dev, unsigned long *bytes, unsigned long *usec)
{
	char *s, *e;
	unsigned long b, u;
	char *ratestr;

	/* Default to unlimited bandwidth. */
	*bytes = ~0UL;
	*usec = 0;

	ratestr = vbus_read(VBT_NIL, dev->nodename, "rate", NULL);
	if (IS_ERR(ratestr))
		return;

	s = ratestr;
	b = simple_strtoul(s, &e, 10);
	if ((s == e) || (*e != ','))
		goto fail;

	s = e + 1;
	u = simple_strtoul(s, &e, 10);
	if ((s == e) || (*e != '\0'))
		goto fail;

	*bytes = b;
	*usec = u;

	kfree(ratestr);
	return;

 fail:
	pr_warn("Failed to parse network rate limit. Traffic unlimited.\n");
	kfree(ratestr);
}

/*
 * Frontend-dependent subsystem initialization
 */
int vnet_subsys_enable(struct vbus_device *dev) {

	int ret = 0;
	int val;
	vnet_desc_t *vnet_desc;
	unsigned int rx_copy;

	vnet_desc = vnet_desc_alloc(&dev->dev, dev->otherend_id);

	if (IS_ERR(vnet_desc)) {
		ret = PTR_ERR(vnet_desc);
		lprintk("%s - line %d: Creating interface for device name %s\n", __func__, __LINE__, dev->nodename);
		BUG();
	}

	ret = vbus_scanf(VBT_NIL, dev->otherend, "request-rx-copy", "%u", &rx_copy);
	if (ret == -ENOENT) {
		ret = 0;
		rx_copy = 0;
	}
	if (ret < 0) {
		lprintk("%s - line %d: Reading %s/request-rx-copy failed for device name %s\n", __func__, __LINE__, dev->otherend, dev->nodename);
		BUG();
	}

	if (!rx_copy)
		return -EOPNOTSUPP;

	if (vnet.descs[dev->otherend_id].dev->tx_queue_len != 0) {
		if (vbus_scanf(VBT_NIL, dev->otherend, "feature-rx-notify", "%d", &val) < 0)
			val = 0;
		if (val)
			vnet.descs[dev->otherend_id].can_queue = 1;
		else
			/* Must be non-zero for pfifo_fast to work. */
			vnet.descs[dev->otherend_id].dev->tx_queue_len = 1;
	}

	if (vbus_scanf(VBT_NIL, dev->otherend, "feature-sg", "%d", &val) < 0)
		val = 0;

	vnet.descs[dev->otherend_id].can_sg = val;

	if (vbus_scanf(VBT_NIL, dev->otherend, "feature-gso-tcpv4", "%d", &val) < 0)
		val = 0;
	vnet.descs[dev->otherend_id].gso = val;

	if (vbus_scanf(VBT_NIL, dev->otherend, "feature-gso-tcpv4-prefix", "%d", &val) < 0)
		val = 0;
	vnet.descs[dev->otherend_id].gso_prefix = val;

	if (vbus_scanf(VBT_NIL, dev->otherend, "feature-no-csum-offload", "%d", &val) < 0)
		val = 0;

	vnet.descs[dev->otherend_id].csum = val;

	kobject_uevent(&dev->dev.kobj, KOBJ_ONLINE);

	disable_irq(vnet_desc->rings.irq);
	vnet_desc_get(vnet_desc);

	rtnl_lock();

	if (!vnet_desc->can_sg && vnet_desc->dev->mtu > ETH_DATA_LEN)
		dev_set_mtu(vnet_desc->dev, ETH_DATA_LEN);

	netdev_update_features(vnet_desc->dev);
	netif_carrier_on(vnet_desc->dev);

	if (netif_running(vnet_desc->dev))
		vnet_up(vnet_desc);

	rtnl_unlock();

	ret = vnet_read_mac(dev, vnet.descs[dev->otherend_id].fe_dev_addr);
	if (ret) {
		lprintk("%s - line %d: Parsing %s/mac failed.\n", __func__, __LINE__, dev->otherend);
		BUG();
	}

	vnet_read_rate(dev, &vnet.descs[dev->otherend_id].credit_bytes, &vnet.descs[dev->otherend_id].credit_usec);
	vnet.descs[dev->otherend_id].remaining_credit = vnet.descs[dev->otherend_id].credit_bytes;

	netif_wake_queue(vnet.descs[dev->otherend_id].dev);

	return ret;

}

void vnet_subsys_disable(struct vbus_device *dev) {

	vnet_desc_t *vnet_desc;
	struct net_device *netdev;

	vnet_desc = &vnet.descs[dev->otherend_id];

	netdev = vnet_desc->dev;

	if (netif_carrier_ok(netdev)) {
		rtnl_lock();
		netif_carrier_off(netdev); /* discard queued packets */

		if (netif_running(netdev))
			vnet_down(vnet_desc);

		rtnl_unlock();
		vnet_desc_put(vnet_desc);
	}

	atomic_dec(&vnet_desc->refcnt);
	wait_event(vnet_desc->waiting_to_free, atomic_read(&vnet_desc->refcnt) == 0);

	del_timer_sync(&vnet_desc->credit_timeout);

	unregister_netdev(vnet_desc->dev);

	free_netdev(vnet_desc->dev);

}

static void netbk_alarm(unsigned long data)
{
	wake_up(&vnet.wq); /* Kick the thread */
}

/*
 * Frontend-independent backend initialization
 */
int netback_init(void)
{
	int i, rc = 0;

	skb_queue_head_init(&vnet.rx_queue);
	skb_queue_head_init(&vnet.tx_queue);

	init_timer(&vnet.net_timer);

	vnet.net_timer.function = netbk_alarm;

	vnet.pending_cons = 0;
	vnet.pending_prod = MAX_PENDING_REQS;

	for (i = 0; i < MAX_PENDING_REQS; i++)
		vnet.pending_ring[i] = i;

	init_waitqueue_head(&vnet.wq);

	vnet.task = kthread_create(netbk_kthread, NULL, "netback_thread");

	if (IS_ERR(vnet.task)) {
		printk(KERN_ALERT "kthread_create() fails at netback\n");
		del_timer(&vnet.net_timer);
		rc = PTR_ERR(vnet.task);

		goto failed_init;
	}

	INIT_LIST_HEAD(&vnet.net_schedule_list);

	spin_lock_init(&vnet.net_schedule_list_lock);

	atomic_set(&vnet.netfront_count, 0);

	wake_up_process(vnet.task);

	return netback_vbus_init();

failed_init:

for (i = 0; i < MAX_PENDING_REQS; i++) {
		if (vnet.mmap_pages[i])
			__free_page(vnet.mmap_pages[i]);
	}
	del_timer(&vnet.net_timer);
	kthread_stop(vnet.task);

	return rc;
}


module_init(netback_init);

