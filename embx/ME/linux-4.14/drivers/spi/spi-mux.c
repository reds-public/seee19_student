/*
 * SPI multiplexer core driver
 *
 * Dries Van Puymbroeck <Dries.VanPuymbroeck@barco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not,  see <http://www.gnu.org/licenses>
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi-mux.h>

#define SPI_MUX_NO_CS	((unsigned)-1)

/**
 * DOC: Driver description
 *
 * This driver supports a MUX on an SPI bus. This can be useful when you need
 * more chip selects than the hardware peripherals support, or than are
 * available in a particular board setup.
 *
 * The driver will create an additional master bus. Devices added under the mux
 * will be handled as 'chip selects' on the mux bus.
 *
 * This is just the core mux driver, you will need an aditional mux-specific
 * driver which needs to implement the spi_mux_select callback to set the mux.
 */

/**
 * struct spi_mux_priv - the basic spi_mux structure
 * @spi_device:		pointer to the device struct attached to the parent
 *			spi master
 * @current_cs:		The current chip select set in the mux
 * @child_mesg_complete: The mux replaces the complete callback in the child's
 *			message to its own callback; this field is used by the
 *			driver to store the child's callback during a transfer
 * @child_mesg_context: Used to store the child's context to the callback
 * @child_mesg_dev:	Used to store the spi_device pointer to the child
 * @spi_mux_select:	Callback to the specific mux implementation to set the
 *			mux to a chip select
 * @mux_dev:		Data passed to spi_mux_select callback and returned
 *			with spi_del_mux
 */
struct spi_mux_priv {
	struct spi_device	*spi;
	unsigned		current_cs;

	void			(*child_mesg_complete)(void *context);
	void			*child_mesg_context;
	struct spi_device	*child_mesg_dev;

	int	(*spi_mux_select)(void *mux_dev, u8 chip_select);
	void	*mux_dev;
};

/* should not get called when the parent master is doing a transfer */
static int spi_mux_setup_mux(struct spi_device *spi)
{
	struct spi_mux_priv *priv = spi_master_get_devdata(spi->master);
	int ret = 0;

	if (priv->current_cs != spi->chip_select) {
		dev_dbg(&priv->spi->dev,
			"setting up the mux for cs %d\n",
			spi->chip_select);

		/* copy the child device's settings except for the cs */
		priv->spi->max_speed_hz = spi->max_speed_hz;
		priv->spi->mode = spi->mode;
		priv->spi->bits_per_word = spi->bits_per_word;

		ret = priv->spi_mux_select(priv->mux_dev, spi->chip_select);
		if (ret)
			return ret;

		priv->current_cs = spi->chip_select;
	}

	return ret;
}

static int spi_mux_setup(struct spi_device *spi)
{
	struct spi_mux_priv *priv = spi_master_get_devdata(spi->master);

	/*
	 * can be called multiple times, won't do a valid setup now but we will
	 * change the settings when we do a transfer (necessary because we
	 * can't predict from which device it will be anyway)
	 */
	return spi_setup(priv->spi);
}

static void spi_mux_complete_cb(void *context)
{
	struct spi_mux_priv *priv = (struct spi_mux_priv *)context;
	struct spi_master *master = spi_get_drvdata(priv->spi);
	struct spi_message *m = master->cur_msg;

	m->complete = priv->child_mesg_complete;
	m->context = priv->child_mesg_context;
	m->spi = priv->child_mesg_dev;
	spi_finalize_current_message(master);
}

static int spi_mux_transfer_one_message(struct spi_master *master,
						struct spi_message *m)
{
	struct spi_mux_priv *priv = spi_master_get_devdata(master);
	struct spi_device *spi = m->spi;
	int ret = 0;

	ret = spi_mux_setup_mux(spi);
	if (ret)
		return ret;

	/*
	 * Replace the complete callback, context and spi_device with our own
	 * pointers. Save originals
	 */
	priv->child_mesg_complete = m->complete;
	priv->child_mesg_context = m->context;
	priv->child_mesg_dev = m->spi;

	m->complete = spi_mux_complete_cb;
	m->context = priv;
	m->spi = priv->spi;

	/* do the transfer */
	ret = spi_async(priv->spi, m);
	return ret;
}

int spi_add_mux(struct spi_device *spi, void *mux_dev, u16 num_chipselect,
		int (*spi_mux_select)(void *mux_dev, u8 chip_select))
{
	struct spi_master *master;
	struct spi_mux_priv *priv;
	int ret = 0;

	master = spi_alloc_master(&spi->dev, sizeof(*priv));
	if (master == NULL) {
		dev_dbg(&spi->dev, "master allocation failed\n");
		return -ENOMEM;
	}

	dev_set_drvdata(&spi->dev, master);
	priv = spi_master_get_devdata(master);
	priv->spi = spi;
	priv->spi_mux_select = spi_mux_select;
	priv->mux_dev = mux_dev;

	priv->current_cs = SPI_MUX_NO_CS;
	ret = priv->spi_mux_select(priv->mux_dev, priv->current_cs);
	if (ret)
		goto err_mux_select;

	/* supported modes are the same as our parent's */
	master->mode_bits = spi->master->mode_bits;

	master->setup = spi_mux_setup;
	master->transfer_one_message = spi_mux_transfer_one_message;
	master->num_chipselect = num_chipselect;
	master->dev.of_node = spi->dev.of_node;

	/*
	 * when we register our mux as an spi master, it will parse the
	 * the children of this node and add them as devices.
	 * So we don't need to parse the child nodes here.
	 */
	ret = spi_register_master(master);
	if (ret < 0)
		goto err_register_master;

	return ret;

err_mux_select:
err_register_master:
	spi_master_put(master);

	return ret;
}
EXPORT_SYMBOL_GPL(spi_add_mux);

void spi_del_mux(struct spi_device *spi)
{
	struct spi_master *master = spi_get_drvdata(spi);

	spi_unregister_master(master);
	spi_master_put(master);
}
EXPORT_SYMBOL_GPL(spi_del_mux);

void *spi_get_mux_dev(struct spi_device *spi)
{
	struct spi_master *master = spi_get_drvdata(spi);
	struct spi_mux_priv *priv = spi_master_get_devdata(master);
	return priv->mux_dev;
}
EXPORT_SYMBOL_GPL(spi_get_mux_dev);

MODULE_DESCRIPTION("SPI multiplexer core functions");
MODULE_AUTHOR("Dries Van Puymbroeck <Dries.VanPuymbroeck@barco.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("spi:spi-mux");
