/*
 * SPI multiplexer driver using GPIO API for Barco Orka board
 *
 * Dries Van Puymbroeck <Dries.VanPuymbroeck@barco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not,  see <http://www.gnu.org/licenses>
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi-mux.h>

/**
 * DOC: Driver description
 *
 * This driver supports the mux on the Barco Orka board. The MUX can set up 2^n
 * channels, where n is the number of GPIO's connected to set the MUX. The chip
 * select of the child devices will be interpreted as a bitmask for the GPIO's,
 * with the least significant bit defining the state of first gpio, the next
 * bit the state of the second gpio and so forth.
 */

/**
 * struct barco_orka_spi_mux_gpio - the basic barco_orka_spi_mux_gpio structure
 * @gpios:		Array of GPIO numbers used to control MUX
 * @n_gpios:		Number of GPIOs used to control MUX
 */
struct barco_orka_spi_mux_gpio {
	unsigned int	*gpios;
	int		n_gpios;
};

static int barco_orka_spi_mux_gpio_select(void *mux_dev, u8 chip_select)
{
	struct barco_orka_spi_mux_gpio *mux =
		(struct barco_orka_spi_mux_gpio *)mux_dev;
	int i;

	for (i = 0; i < mux->n_gpios; i++) {
		gpio_set_value(mux->gpios[i],
			       chip_select & (1 << i));
	}

	return 0;
}

static int barco_orka_spi_mux_gpio_probe(struct spi_device *spi)
{
	struct barco_orka_spi_mux_gpio *mux;
	struct device_node *np;
	int ret = 0, i;
	u16 num_chipselect;

	mux = devm_kzalloc(&spi->dev, sizeof(*mux), GFP_KERNEL);
	if (mux == NULL) {
		dev_err(&spi->dev, "Failed to allocate driver struct\n");
		return -ENOMEM;
	}

	np = spi->dev.of_node;
	if (!np)
		return -ENODEV;

	mux->n_gpios = of_gpio_named_count(np, "gpios");
	if (mux->n_gpios < 0) {
		dev_err(&spi->dev, "Missing gpios property in the DT.\n");
		return -EINVAL;
	}

	mux->gpios = devm_kzalloc(&spi->dev,
			     sizeof(*mux->gpios) * mux->n_gpios,
			     GFP_KERNEL);
	if (!mux->gpios) {
		dev_err(&spi->dev, "Cannot allocate gpios array");
		return -ENOMEM;
	}

	for (i = 0; i < mux->n_gpios; i++)
		mux->gpios[i] = of_get_named_gpio(np, "gpios", i);

	for (i = 0; i < mux->n_gpios; i++) {
		devm_gpio_request(&spi->dev, mux->gpios[i],
				  "barco-orka-spi-mux-gpio");
		gpio_direction_output(mux->gpios[i], 0);
	}

	/* the mux can have 2 ^ <nr_gpio_used_for_muxing> chip selects */
	num_chipselect = 1 << mux->n_gpios;

	ret = spi_add_mux(spi, mux, num_chipselect,
			  barco_orka_spi_mux_gpio_select);
	return ret;
}

static int barco_orka_spi_mux_gpio_remove(struct spi_device *spi)
{
	spi_del_mux(spi);
	return 0;
}

static const struct of_device_id barco_orka_spi_mux_gpio_of_match[] = {
	{ .compatible = "barco,orka-spi-mux-gpio", },
	{},
};
MODULE_DEVICE_TABLE(of, barco_orka_spi_mux_gpio_of_match);

static struct spi_driver barco_orka_spi_mux_gpio_driver = {
	.probe	= barco_orka_spi_mux_gpio_probe,
	.remove	= barco_orka_spi_mux_gpio_remove,
	.driver	= {
		.owner	= THIS_MODULE,
		.name	= "barco,orka-spi-mux-gpio",
		.of_match_table = barco_orka_spi_mux_gpio_of_match,
	},
};

module_spi_driver(barco_orka_spi_mux_gpio_driver);

MODULE_DESCRIPTION("GPIO-based SPI multiplexer driver for Barco Orka");
MODULE_AUTHOR("Dries Van Puymbroeck <Dries.VanPuymbroeck@barco.com>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("spi:barco,orka-spi-mux-gpio");
