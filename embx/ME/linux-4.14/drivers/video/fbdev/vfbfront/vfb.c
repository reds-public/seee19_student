/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#if 1
#define DEBUG
#endif

#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/console.h>
#include <linux/tty_flip.h>
#include <linux/mutex.h>
#include <linux/jiffies.h>
#include <linux/slab.h>
#include <linux/time.h>
#include <linux/wait.h>
#include <linux/fb.h>

#include <asm/mach/map.h>

#include "common.h"
#include <virtshare/debug.h>

#include <virtshare/dev/vfb.h>
#include <virtshare/evtchn.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>

#define PSEUDO_PALETTE_SIZE 16

vfb_t vfb;

irqreturn_t vfb_interrupt(int irq, void *dev_id) {

	/* Not used... */

	return IRQ_HANDLED;
}

static int vfb_setcolreg(u_int regno, u_int red, u_int green, u_int blue, u_int transp, struct fb_info *info)
{
	u32 *pal = info->pseudo_palette;
	u32 cr = red >> (16 - info->var.red.length);
	u32 cg = green >> (16 - info->var.green.length);
	u32 cb = blue >> (16 - info->var.blue.length);
	u32 value;

	if (regno >= 16)
		return -EINVAL;

	value = (cr << info->var.red.offset) |
			(cg << info->var.green.offset) |
			(cb << info->var.blue.offset);
	if (info->var.transp.length > 0) {
		u32 mask = (1 << info->var.transp.length) - 1;
		mask <<= info->var.transp.offset;
		value |= mask;
	}
	pal[regno] = value;

	return 0;
}

static void vfb_destroy(struct fb_info *info)
{
	__free_pages((struct page *) info->screen_base, get_order(info->fix.smem_len));
}

static int vfb_cursor(struct fb_info *fb, struct fb_cursor *cursor)
{
	return 0;
}

static int vfb_blank(int blank, struct fb_info *info)
{
	return 0;
}

static struct fb_ops vfb_fb_ops = {
	.owner		= THIS_MODULE,
	.fb_destroy	= vfb_destroy,
	.fb_setcolreg	= vfb_setcolreg,
	.fb_fillrect	= cfb_fillrect,
	.fb_copyarea	= cfb_copyarea,
	.fb_imageblit	= cfb_imageblit,
	.fb_cursor	= vfb_cursor,
	.fb_blank	= vfb_blank,
};

static struct fb_fix_screeninfo vfb_fix = {
	.id			= "vfb",
	.type		= FB_TYPE_PACKED_PIXELS,
	.visual		= FB_VISUAL_TRUECOLOR,
	.accel		= FB_ACCEL_NONE,
};

static struct fb_var_screeninfo vfb_var = {
	.height		= -1,
	.width		= -1,
	.activate	= FB_ACTIVATE_NOW,
	.vmode		= FB_VMODE_NONINTERLACED,
};

struct vfb_par {
	u32 palette[PSEUDO_PALETTE_SIZE];
};

int vfb_subsys_enable(struct vbus_device *dev) {
	return 0;
}


void vfb_probe(void) {
	struct vfb_par *par;
	struct fb_info *fb_info;
	int ret;
	int order;
	vfb_hw_params_t hw;

	DBG0("[" VFB_NAME "] Frontend probe\n");

	/* Retrieve all framebuffer features from vstore */
	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "width", "%d", &hw.width) != 1) {
		lprintk("Error reading width\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "height", "%d", &hw.height) != 1) {
		lprintk("Error reading height\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "depth", "%u", &hw.depth) != 1) {
		lprintk("Error reading depth\n");
		BUG();
	}

	/* Red pixel information */
	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "red_length", "%d", &hw.red.length) != 1) {
		lprintk("Error reading red_length\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "red_offset", "%d", &hw.red.offset) != 1) {
		lprintk("Error reading red_offset\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "red_msb_right", "%d", &hw.red.msb_right) != 1) {
		lprintk("Error reading red_msb_right\n");
		BUG();
	}

	/* Green pixel information */
	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "green_length", "%d", &hw.green.length) != 1) {
		lprintk("Error reading green_length\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "green_offset", "%d", &hw.green.offset) != 1) {
		lprintk("Error reading green_offset\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "green_msb_right", "%d", &hw.green.msb_right) != 1) {
		lprintk("Error reading green_msb_right\n");
		BUG();
	}

	/* Blue pixel information */
	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "blue_length", "%d", &hw.blue.length) != 1) {
		lprintk("Error reading blue_length\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "blue_offset", "%d", &hw.blue.offset) != 1) {
		lprintk("Error reading blue_offset\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "blue_msb_right", "%d", &hw.blue.msb_right) != 1) {
		lprintk("Error reading blue_msb_right\n");
		BUG();
	}

	/* Transp pixel information */
	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "transp_length", "%d", &hw.transp.length) != 1) {
		lprintk("Error reading transp_length\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "transp_offset", "%d", &hw.transp.offset) != 1) {
		lprintk("Error reading transp_offset\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "transp_msb_right", "%d", &hw.transp.msb_right) != 1) {
		lprintk("Error reading transp_msb_right\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "line_length", "%d", &hw.line_length) != 1) {
		lprintk("Error reading line_length\n");
		BUG();
	}

	if (vbus_scanf(VBT_NIL, vfb.dev->otherend, "fb_mem_len", "%d", &hw.fb_mem_len) != 1) {
		lprintk("Error reading fb_mem_len\n");
		BUG();
	}

	dev_set_drvdata(&vfb.dev->dev, &vfb);

	order = get_order(hw.fb_mem_len);

	lprintk("## Trying to allocate order %d\n", order);
	vfb.fb = (unsigned char *) __get_free_pages(GFP_KERNEL, order);
	memset(vfb.fb, 0, hw.fb_mem_len);

	if (vfb.fb == NULL) {
		lprintk("%s - failed to alloc FB memory order = %d (size = %d bytes)\n", __FUNCTION__, order, hw.fb_mem_len);
		BUG();
	}

	vfb.fb_pfn = virt_to_pfn(vfb.fb);

	vbus_printf(VBT_NIL, vfb.dev->nodename, "vfb_fb_pfn", "%u", vfb.fb_pfn);

	fb_info = framebuffer_alloc(sizeof(struct vfb_par), NULL);
	if (fb_info == NULL)
		BUG();

	par = fb_info->par;

	fb_info->fix = vfb_fix;
	fb_info->fix.line_length = hw.line_length;
	fb_info->fix.smem_start = virt_to_phys(vfb.fb);
	fb_info->fix.smem_len = hw.fb_mem_len;

	fb_info->var = vfb_var;
	fb_info->var.xres = hw.width;
	fb_info->var.yres = hw.height;
	fb_info->var.xres_virtual = hw.width;
	fb_info->var.yres_virtual = hw.height;
	fb_info->var.bits_per_pixel = hw.depth;

	fb_info->var.red = hw.red;
	fb_info->var.green = hw.green;
	fb_info->var.blue = hw.blue;
	fb_info->var.transp = hw.transp;

	fb_info->apertures = alloc_apertures(1);
	if (!fb_info->apertures) {
		framebuffer_release(fb_info);
		BUG();
	}

	fb_info->apertures->ranges[0].base = fb_info->fix.smem_start;
	fb_info->apertures->ranges[0].size = fb_info->fix.smem_len;


	fb_info->fbops = &vfb_fb_ops;
	fb_info->flags = FBINFO_DEFAULT | FBINFO_MISC_FIRMWARE;

	fb_info->screen_base = vfb.fb;

	fb_info->pseudo_palette = par->palette;

	DBG("%s: framebuffer at 0x%lx, 0x%x bytes, mapped to 0x%p\n", __func__,
			fb_info->fix.smem_start, fb_info->fix.smem_len,
			fb_info->screen_base);

	DBG("%s: mode=%dx%dx%d, linelength=%d\n", __func__,
			fb_info->var.xres, fb_info->var.yres,
			fb_info->var.bits_per_pixel, fb_info->fix.line_length);

	ret = register_framebuffer(fb_info);

	if (ret) {
		fb_dealloc_cmap(&fb_info->cmap);
		framebuffer_release(fb_info);

		lprintk("%s - line %d: Registering framebuffer failed for device %s\n", __func__, __LINE__, vfb.dev->nodename);
		BUG();
	}

	vfb.fb_info = fb_info;
}

void vfb_shutdown(void) {

	DBG0("[" VFB_NAME "] Frontend shutdown\n");

	unregister_framebuffer(vfb.fb_info);
	fb_dealloc_cmap(&vfb.fb_info->cmap);
	framebuffer_release(vfb.fb_info);

	dev_set_drvdata(&vfb.dev->dev, NULL);
}

void vfb_closed(void) {

	DBG0("[" VFB_NAME "] Frontend close\n");
}

void vfb_connected(void) {
	DBG0("[" VFB_NAME "] Frontend connected\n");

	/* Force the processing of pending requests, if any */
	notify_remote_via_irq(vfb.irq);
}

static int vfb_init(void) {

	struct device_node *np;

	np = of_find_compatible_node(NULL, NULL, "vfb,frontend");

	/* Check if DTS has vuihandler enabled */
	if (!of_device_is_available(np))
		return 0;

	spin_lock_init(&vfb.resize_lock);

	vfb_vbus_init();

	return 0;
}

device_initcall(vfb_init);



