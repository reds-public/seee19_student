/*
 * Copyright (C) 2014-2019 Daniel Rossier <daniel.rossier@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <linux/irqreturn.h>

#include <virtshare/evtchn.h>
#include <virtshare/dev/vfb.h>
#include <virtshare/avz.h>

struct vfb_resize {
	uint8_t type;		/* VFB_TYPE_RESIZE */
	int32_t width;		/* width in pixels */
	int32_t height;		/* height in pixels */
	int32_t stride;		/* stride in bytes */
	int32_t depth;		/* depth in bits */
	int32_t offset;		/* start offset within framebuffer */
};

typedef struct {

	struct vbus_device  *dev;
	
	uint32_t evtchn;
	uint32_t irq;

	/* Framebuffer memory area */
	unsigned char *fb;
	unsigned int fb_pfn;

	spinlock_t resize_lock;
	struct vfb_resize resize;
	int resize_dpy;

	/* Linux fb subsys */
	struct fb_info *fb_info;
} vfb_t;

extern vfb_t vfb;

/* ISR associated to the ring */
irqreturn_t vfb_interrupt(int irq, void *data);

/* State management */
void vfb_probe(void);
void vfb_closed(void);
void vfb_connected(void);
void vfb_shutdown(void);

void vfb_vbus_init(void);

/* Processing and connected state management */
void vfb_start(void);
void vfb_end(void);
bool vfb_is_connected(void);

