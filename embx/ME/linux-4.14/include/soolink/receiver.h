/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2016: Raphaël Buache, Xavier Ruppen
 * - January-March 2017: Raphaël Buache, Xavier Ruppen
 * - July 2017: Daniel Rossier
 */

#ifndef RECEIVER_H
#define RECEIVER_H

#include <linux/types.h>

#include <soolink/transceiver.h>
#include <soolink/plugin.h>

void receiver_request_rx(sl_desc_t *sl_desc, plugin_desc_t *plugin_desc, void *packet, size_t size);
void receiver_rx(sl_desc_t *sl_desc, plugin_desc_t *plugin_desc, void *packet, size_t size);

void receiver_init(void);

#endif /* RECEIVER_H */

