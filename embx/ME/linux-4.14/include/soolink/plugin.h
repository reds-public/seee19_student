/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2016: Raphaël Buache, Xavier Ruppen
 * - January-March 2017: Raphaël Buache, Xavier Ruppen
 * - July 2017: Daniel Rossier
 * - October 2017: Baptiste Delporte
 *
 * Soolink transceiver v1.10
 */

#ifndef PLUGIN_H
#define PLUGIN_H

#include <linux/list.h>
#include <linux/if_ether.h>

#include <virtshare/console.h>
#include <virtshare/debug.h>

#include <soolink/soolink.h>
#include <soolink/transceiver.h>
#include <soolink/plugin/common.h>

typedef struct {

	/* To help for a list of available plugin */
	struct list_head list;

	/* Associated interface type for this plugin */
	if_type_t if_type;

	/* Function to be called when sending data out */
	void (*tx_callback)(sl_desc_t *sl_desc, void *data, size_t size, unsigned long flags);


} plugin_desc_t;

void transceiver_plugin_init(void);

void transceiver_plugin_register(plugin_desc_t *plugin_desc);

void plugin_tx(sl_desc_t *sl_desc, void *data, size_t size, unsigned long flags);
void plugin_rx(plugin_desc_t *plugin_desc, agencyUID_t *agencyUID_from, req_type_t req_type, void *data, size_t size);

extern unsigned char broadcast_addr[ETH_ALEN];

req_type_t get_sl_req_type_from_protocol(uint16_t protocol);
uint16_t get_protocol_from_sl_req_type(req_type_t req_type);

#endif /* PLUGIN_H */
