/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 *
 * Soolink Send Controller header
 *
 * Contributors:
 * - 2017, 2018: Daniel Rossier, Baptiste Delporte
 * - August 2018: Baptiste Delporte
 *
 */

#ifndef SENDER_H
#define SENDER_H

#include <soolink/transceiver.h>
#include <soolink/soolink.h>

void sender_request_xmit(sl_desc_t *sl_desc);
int sender_xmit(sl_desc_t *sl_desc, void *data, size_t size, bool completed);
void sender_tx(sl_desc_t *sl_desc, void *packet, size_t size, unsigned long flags);

int sender_stream_xmit(sl_desc_t *sl_desc, void *data);

void sender_init(void);

#endif /* SENDER_H */
