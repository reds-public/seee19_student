/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017,2019 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier
 * - February 2019: Baptiste Delporte
 */

#ifndef PLUGIN_BLUETOOTH_H
#define PLUGIN_BLUETOOTH_H

#include <linux/skbuff.h>

#include <soolink/soolink.h>

void sl_plugin_bluetooth_rx(struct sk_buff *skb);
void propagate_plugin_bluetooth_send(void);
void rtdm_propagate_sl_plugin_bluetooth_rx(void);
void plugin_bluetooth_delete_remote(agencyUID_t *agencyUID);

#endif /* PLUGIN_BLUETOOTH_H */
