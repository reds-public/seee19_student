
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier, Baptiste Delporte
 * - 2018: Daniel Rossier, Baptiste Delporte
 */

#ifndef PLUGIN_WLAN_H
#define PLUGIN_WLAN_H

#include <linux/skbuff.h>

#include <soolink/soolink.h>

#ifdef CONFIG_WLAN_VENDOR_MARVELL
#define WLAN_NET_DEV_NAME 	"mlan0"
#endif

#ifdef CONFIG_BRCMFMAC_SDIO
#define WLAN_NET_DEV_NAME 	"wlan0"
#endif

void sl_plugin_wlan_rx_skb(struct sk_buff *skb, struct net_device *net_dev, uint8_t *mac_src);

void propagate_plugin_wlan_send(void);
void rtdm_propagate_sl_plugin_wlan_rx(void);

void plugin_wlan_delete_remote(agencyUID_t *agencyUID);
bool is_rtdm_wifi_enabled(void);
void rtdm_reconfigure_wifi(void);
void reconfigure_wifi(void);

#endif /* PLUGIN_WLAN_H */
