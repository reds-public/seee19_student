/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 * Soolink Ethernet plugin v2.1
 *
 */

#include <linux/skbuff.h>

#include <uapi/linux/if_ether.h>

#include <soolink/soolink.h>

/*
 * Delay in ms to give a chance to Linux netdev to get initialized.
 */
#define NET_DEV_DETECT_DELAY	(3 * 1000)

typedef struct {
	sl_desc_t		*sl_desc;
	void			*data;
	size_t			size;
	struct list_head	list;
} plugin_send_args_t;

typedef struct {
	req_type_t	req_type;
	void		*data;
	size_t		size;
	uint8_t		mac[ETH_ALEN];
} plugin_recv_args_t;

typedef struct {
	agencyUID_t		agencyUID;
	uint8_t			mac[ETH_ALEN];
	struct list_head	list;   /* Take part of the list of neighbours */
} plugin_remote_soo_desc_t;

