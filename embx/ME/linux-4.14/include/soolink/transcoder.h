/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte
 */

#ifndef TRANSCODER_H
#define TRANSCODER_H

#include <linux/types.h>

#include <soolink/soolink.h>

#define SL_CODER_PACKET_MAX_SIZE 	1024

#define CODER_CONSISTENCY_SIMPLE	0x00
#define CODER_CONSISTENCY_EXT		0x01

/*
 * Simple packet format is used when a data block does not exceed SL_CODER_PACKET_MAX_SIZE
 */
typedef struct {
	uint8_t	consistency_type;  	/* Consistency algorithm ID */
} simple_packet_format_t;

/*
 * Extended packet format used when a block must be splitted into multiple packets.
 */
typedef struct {
	 uint8_t	consistency_type; 	/* Consistency ID refering to a specific consistency algorithm */
	 uint32_t	packetID;		/* Sequential packet ID */
	 uint32_t	nr_packets;		/* Number of packets */
	 uint16_t	payload_length;		/* Length of the payload */
} ext_packet_format_t;

typedef union {
	simple_packet_format_t	simple;
	ext_packet_format_t	ext;
} transcoder_packet_format_t;

/*
 * General transcoder packet format; it has to be noted that the payload is refered by itsbyte
 */
typedef struct {

	transcoder_packet_format_t u;

	uint8_t payload[0];

} transcoder_packet_t;

void transcoder_init(void);
void transcoder_stream_init(sl_desc_t *sl_desc);
void transcoder_stream_terminate(sl_desc_t *sl_desc);

#endif /* TRANSCODER_H */
