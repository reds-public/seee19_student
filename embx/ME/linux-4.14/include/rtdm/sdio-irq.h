/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Baptiste Delporte
 *
 */

#ifndef SDIO_IRQ_H
#define SDIO_IRQ_H

struct sdio_func;

void rtdm_sdio_start_thread(void *_host);
void rtdm_sdio_init_funcs(void *_host);
void rtdm_sdio_irq(void *_host);

void rtdm_wlan_sdio_interrupt(struct sdio_func *func);
void rtdm_btmrvl_sdio_interrupt(struct sdio_func *func);
void rtdm_sdio_irq_init_threads(struct mmc_host *host);

bool in_rtdm_sdio_irq_context(struct sdio_func *func);

#endif /* SDIO_IRQ_H */


