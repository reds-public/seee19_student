/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Baptiste Delporte
 *
 */

#ifndef BTMRVL_SDIO_H
#define BTMRVL_SDIO_H

struct btmrvl_private;

int rtdm_btmrvl_sdio_process_int_status(struct btmrvl_private *priv);
int rtdm_btmrvl_sdio_host_to_card(struct btmrvl_private *priv, u8 *payload, u16 nb);

void propagate_btmrvl_interrupt_from_rt(void);

#endif /* BTMRVL_SDIO_H */

