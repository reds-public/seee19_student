
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Daniel Rossier
 *
 */

#ifndef CORE_MMC_H
#define CORE_MMC_H

struct mmc_host;
struct mmc_request;
struct mmc_command;

struct mmc_host *get_current_host(void);

void rtdm_mmc_request_done(struct mmc_host *host, struct mmc_request *mrq);
void rtdm_mmc_wait_for_req(struct mmc_host *host, struct mmc_request *mrq);
int rtdm_mmc_wait_for_cmd(struct mmc_host *host, struct mmc_command *cmd, int retries);

#endif /* CORE_MMC_H */
