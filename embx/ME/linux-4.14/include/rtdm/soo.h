/**
 * Definition of RTDM task priorities and periods
 */

#ifndef RTDM_SOO_H
#define RTDM_SOO_H

#define NSECS           	1000000000ull
#define SECONDS(_s)     	((u64)((_s)  * 1000000000ull))
#define MILLISECS(_ms)  	((u64)((_ms) * 1000000ull))
#define MICROSECS(_us)  	((u64)((_us) * 1000ull))

/* Periods are expressed in ns as it is used by the function APIs */

#define SL_TX_REQUEST_TASK_PRIO		50
#define SL_SEND_TASK_PRIO		50
#define SL_RECV_TASK_PRIO		50

#define SL_PLUGIN_WLAN_TASK_PRIO	50
#define SL_PLUGIN_ETHERNET_TASK_PRIO	50
#define SL_PLUGIN_TCP_TASK_PRIO		50
#define SL_PLUGIN_BLUETOOTH_TASK_PRIO	50
#define SL_PLUGIN_LOOPBACK_TASK_PRIO	50

#define VBUS_TASK_PRIO			50

/*
 * The priority of the Directcomm thread must be higher than the priority of the SDIO
 * thread to make the Directcomm thread process a DC event and release it before any new
 * request by the SDIO's side thus avoiding a deadlock.
 */
#define DC_ISR_TASK_PRIO		55

#define SDIO_IRQ_TASK_PRIO		50
#define SDHCI_FINISH_TASK_PRIO		50

/* Soolink definition */

/* Discovery */

#define DISCOVERY_TASK_PRIO		50
#define DISCOVERY_TASK_PERIOD		MILLISECS(1000)

/* Soolink Coder */

#define CODER_TASK_PRIO			50

/* Soolink Decoder */

#define DECODER_WATCHDOG_TASK_PRIO	50
#define DECODER_WATCHDOG_TASK_PERIOD	MILLISECS(1000)

/* Soolink Winenet Datalink */

#define WINENET_TASK_PRIO               50

/* vNetstream virtual interface */

#define VNETSTREAM_TASK_PRIO            50

/* Tests related */
#define PLUGIN_TEST_TASK_PRIO		50
#define TRANSCODER_TEST_TASK_PRIO	50
#define DISCOVERY_TEST_TASK_PRIO	50

/* WLan driver (Marvell) */
#define MAIN_WORK_TASK_PRIO		50
#define RX_WORK_TASK_PRIO		50
#define HANG_WORK_TASK_PRIO		50
#define CSA_WORK_TASK_PRIO		50

/* BCM2835 driver (Raspberry Pi 3) */
#define DMA_COMPLETE_WORK_TASK_PRIO	50
#define FWEH_EVENT_WORK_TASK_PRIO 	50
#define SDIO_EVENT_WORK_TASK_PRIO 	50

#define AGENCY_CPU	        	0
#define AGENCY_RT_CPU	 		1

#ifndef __ASSEMBLY__

extern volatile bool __xenomai_ready_to_go;
extern volatile bool __cobalt_ready;

#endif /* __ASSEMBLY__ */

#endif /* RTDM_SOO_H */
