/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - February 2018: Baptiste Delporte
 *
 */

#ifndef DEVICE_ACCESS_H
#define DEVICE_ACCESS_H

#include <linux/device.h>

#include <virtshare/soo.h>

extern uint8_t devcaps_class[DEVCAPS_CLASS_NR];

/* ME SPIDs */
extern uint8_t SOO_heat_spid[SPID_SIZE];
extern uint8_t SOO_ambient_spid[SPID_SIZE];
extern uint8_t SOO_blind_spid[SPID_SIZE];
extern uint8_t SOO_outdoor_spid[SPID_SIZE];

agencyUID_t *get_null_agencyUID(void);
agencyUID_t *get_my_agencyUID(void);

void set_agencyUID(uint8_t val);

static inline bool agencyUID_is_valid(agencyUID_t *agencyUID) {
	return (memcmp(agencyUID, get_null_agencyUID(), SOO_AGENCY_UID_SIZE) != 0);
}

void devaccess_dump_agencyUID(void);

bool devaccess_is_devcaps_class_supported(uint32_t class);
bool devaccess_is_devcaps_supported(uint32_t class, uint8_t devcaps);

void devaccess_set_devcaps(uint32_t class, uint8_t devcaps, bool available);
void devaccess_dump_devcaps(void);

void devaccess_set_soo_name(char *name);
void devaccess_get_soo_name(char *ptr);
void devaccess_dump_soo_name(void);

void devaccess_init(void);

/* sysfs handlers */
ssize_t agencyUID_show(struct device *dev, struct device_attribute *attr, char *buf);
ssize_t agencyUID_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size);
ssize_t soo_name_show(struct device *dev, struct device_attribute *attr, char *buf);
ssize_t soo_name_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size);

#endif /* DEVICE_ACCESS_H */
