/*
 * Copyright (C) 2014 Jan Kiszka <jan.kiszka@siemens.com>.
 * Copyright (C) 2014 Philippe Gerum <rpm@xenomai.org>.
 *
 * Xenomai is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * Xenomai is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xenomai; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#undef TRACE_SYSTEM
#define TRACE_SYSTEM cobalt_posix
#undef TRACE_INCLUDE_FILE
#define TRACE_INCLUDE_FILE cobalt-posix

#if !defined(_TRACE_COBALT_POSIX_H) || defined(TRACE_HEADER_MULTI_READ)
#define _TRACE_COBALT_POSIX_H

#include <linux/tracepoint.h>

DECLARE_EVENT_CLASS(cobalt_void,
	TP_PROTO(int dummy),
	TP_ARGS(dummy),
	TP_STRUCT__entry(
		__array(char, dummy, 0)
	),
	TP_fast_assign(
		(void)dummy;
	),
	TP_printk("%s", "")
);

#define cobalt_print_thread_mode(__mode)			\
	__print_flags(__mode, "|",				\
		      {PTHREAD_WARNSW, "warnsw"},		\
		      {PTHREAD_LOCK_SCHED, "lock"},		\
		      {PTHREAD_DISABLE_LOCKBREAK, "nolockbreak"})

DECLARE_EVENT_CLASS(cobalt_posix_pid,
	TP_PROTO(pid_t pid),
	TP_ARGS(pid),
	TP_STRUCT__entry(
		__field(pid_t, pid)
	),
	TP_fast_assign(
		__entry->pid = pid;
	),
	TP_printk("pid=%d", __entry->pid)
);


DECLARE_EVENT_CLASS(cobalt_clock_timespec,
	TP_PROTO(clockid_t clk_id, const struct timespec *val),
	TP_ARGS(clk_id, val),

	TP_STRUCT__entry(
		__field(clockid_t, clk_id)
		__timespec_fields(val)
	),

	TP_fast_assign(
		__entry->clk_id = clk_id;
		__assign_timespec(val, val);
	),

	TP_printk("clock_id=%d timeval=(%ld.%09ld)",
		  __entry->clk_id,
		  __timespec_args(val)
	)
);

DEFINE_EVENT(cobalt_clock_timespec, cobalt_clock_getres,
	TP_PROTO(clockid_t clk_id, const struct timespec *res),
	TP_ARGS(clk_id, res)
);

DEFINE_EVENT(cobalt_clock_timespec, cobalt_clock_gettime,
	TP_PROTO(clockid_t clk_id, const struct timespec *time),
	TP_ARGS(clk_id, time)
);

DEFINE_EVENT(cobalt_clock_timespec, cobalt_clock_settime,
	TP_PROTO(clockid_t clk_id, const struct timespec *time),
	TP_ARGS(clk_id, time)
);

#define cobalt_print_timer_flags(__flags)			\
	__print_flags(__flags, "|",				\
		      {TIMER_ABSTIME, "TIMER_ABSTIME"})

TRACE_EVENT(cobalt_clock_nanosleep,
	TP_PROTO(clockid_t clk_id, int flags, const struct timespec *time),
	TP_ARGS(clk_id, flags, time),

	TP_STRUCT__entry(
		__field(clockid_t, clk_id)
		__field(int, flags)
		__timespec_fields(time)
	),

	TP_fast_assign(
		__entry->clk_id = clk_id;
		__entry->flags = flags;
		__assign_timespec(time, time);
	),

	TP_printk("clock_id=%d flags=%#x(%s) rqt=(%ld.%09ld)",
		  __entry->clk_id,
		  __entry->flags, cobalt_print_timer_flags(__entry->flags),
		  __timespec_args(time)
	)
);

DECLARE_EVENT_CLASS(cobalt_clock_ident,
	TP_PROTO(const char *name, clockid_t clk_id),
	TP_ARGS(name, clk_id),
	TP_STRUCT__entry(
		__string(name, name)
		__field(clockid_t, clk_id)
	),
	TP_fast_assign(
		__assign_str(name, name);
		__entry->clk_id = clk_id;
	),
	TP_printk("name=%s, id=%#x", __get_str(name), __entry->clk_id)
);

DEFINE_EVENT(cobalt_clock_ident, cobalt_clock_register,
	TP_PROTO(const char *name, clockid_t clk_id),
	TP_ARGS(name, clk_id)
);

DEFINE_EVENT(cobalt_clock_ident, cobalt_clock_deregister,
	TP_PROTO(const char *name, clockid_t clk_id),
	TP_ARGS(name, clk_id)
);

#define cobalt_print_clock(__clk_id)					\
	__print_symbolic(__clk_id,					\
			 {CLOCK_MONOTONIC, "CLOCK_MONOTONIC"},		\
			 {CLOCK_MONOTONIC_RAW, "CLOCK_MONOTONIC_RAW"},	\
			 {CLOCK_REALTIME, "CLOCK_REALTIME"})


#define cobalt_print_evflags(__flags)			\
	__print_flags(__flags,  "|",			\
		      {COBALT_EVENT_SHARED, "shared"},	\
		      {COBALT_EVENT_PRIO, "prio"})

#define cobalt_print_evmode(__mode)			\
	__print_symbolic(__mode,			\
			 {COBALT_EVENT_ANY, "any"},	\
			 {COBALT_EVENT_ALL, "all"})



#endif /* _TRACE_COBALT_POSIX_H */

/* This part must be outside protection */
#include <trace/define_trace.h>
