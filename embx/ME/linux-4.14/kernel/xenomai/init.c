/*
 * Copyright (C) 2001-2013 Philippe Gerum <rpm@xenomai.org>.
 *
 * Xenomai is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * Xenomai is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Xenomai; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/ipipe_tickdev.h>
#include <xenomai/version.h>
#include <cobalt/kernel/sched.h>
#include <cobalt/kernel/clock.h>
#include <cobalt/kernel/timer.h>
#include <cobalt/kernel/heap.h>
#include <cobalt/kernel/intr.h>
#include <cobalt/kernel/apc.h>
#include <cobalt/kernel/ppd.h>
#include <cobalt/kernel/pipe.h>
#include <cobalt/kernel/select.h>
#include <cobalt/kernel/vdso.h>
#include <rtdm/fd.h>
#include "rtdm/internal.h"

#include <asm/smp.h>

#include <rtdm/soo.h>

#include <virtshare/hypervisor.h>
#include <virtshare/console.h>

/**
 * @defgroup cobalt Cobalt
 *
 * Cobalt supplements the native Linux kernel in dual kernel
 * configurations. It deals with all time-critical activities, such as
 * handling interrupts, and scheduling real-time threads. The Cobalt
 * kernel has higher priority over all the native kernel activities.
 *
 * Cobalt provides an implementation of the POSIX and RTDM interfaces
 * based on a set of generic RTOS building blocks.
 */

#ifdef CONFIG_SMP
static unsigned long supported_cpus_arg = -1;
module_param_named(supported_cpus, supported_cpus_arg, ulong, 0444);
#endif /* CONFIG_SMP */

static unsigned long sysheap_size_arg;
module_param_named(sysheap_size, sysheap_size_arg, ulong, 0444);

static char init_state_arg[16] = "enabled";
module_param_string(state, init_state_arg, sizeof(init_state_arg), 0444);

static BLOCKING_NOTIFIER_HEAD(state_notifier_list);

volatile bool __cobalt_ready = false;

struct cobalt_pipeline cobalt_pipeline;
EXPORT_SYMBOL_GPL(cobalt_pipeline);

struct cobalt_machine_cpudata cobalt_machine_cpudata;
EXPORT_PER_CPU_SYMBOL_GPL(cobalt_machine_cpudata);

atomic_t cobalt_runstate = ATOMIC_INIT(COBALT_STATE_WARMUP);
EXPORT_SYMBOL_GPL(cobalt_runstate);

struct cobalt_ppd cobalt_kernel_ppd = {
	.exe_path = "vmlinux",
};
EXPORT_SYMBOL_GPL(cobalt_kernel_ppd);

#ifdef CONFIG_XENO_OPT_DEBUG
#define boot_debug_notice "[DEBUG]"
#else
#define boot_debug_notice ""
#endif

#ifdef CONFIG_IPIPE_TRACE
#define boot_lat_trace_notice "[LTRACE]"
#else
#define boot_lat_trace_notice ""
#endif

#ifdef CONFIG_ENABLE_DEFAULT_TRACERS
#define boot_evt_trace_notice "[ETRACE]"
#else
#define boot_evt_trace_notice ""
#endif

#define boot_state_notice						\
	({								\
		realtime_core_state() == COBALT_STATE_STOPPED ?		\
			"[STOPPED]" : "";				\
	})

void cobalt_add_state_chain(struct notifier_block *nb)
{
	blocking_notifier_chain_register(&state_notifier_list, nb);
}
EXPORT_SYMBOL_GPL(cobalt_add_state_chain);

void cobalt_remove_state_chain(struct notifier_block *nb)
{
	blocking_notifier_chain_unregister(&state_notifier_list, nb);
}
EXPORT_SYMBOL_GPL(cobalt_remove_state_chain);

void cobalt_call_state_chain(enum cobalt_run_states newstate)
{
	blocking_notifier_call_chain(&state_notifier_list, newstate, NULL);
}
EXPORT_SYMBOL_GPL(cobalt_call_state_chain);

static int __init mach_setup(void)
{
	int ret;

	ret = ipipe_select_timers(&xnsched_realtime_cpus);
	if (ret < 0)
		return ret;

	if (cobalt_machine.init) {
		ret = cobalt_machine.init();
		if (ret)
			BUG();
	}

	/* Now, we are ready to initialize the IPIs for CPU #1 (head) */

	ret = xnclock_init();
	if (ret)
		BUG();

	return 0;


}

static inline int __init mach_late_setup(void)
{
	if (cobalt_machine.late_init)
		return cobalt_machine.late_init();

	return 0;
}

static struct {
	const char *label;
	enum cobalt_run_states state;
} init_states[] __initdata = {
	{ "disabled", COBALT_STATE_DISABLED },
	{ "stopped", COBALT_STATE_STOPPED },
	{ "enabled", COBALT_STATE_WARMUP },
};
	
static void __init setup_init_state(void)
{
	static char warn_bad_state[] __initdata =
		XENO_WARNING "invalid init state '%s'\n";
	int n;

	for (n = 0; n < ARRAY_SIZE(init_states); n++)
		if (strcmp(init_states[n].label, init_state_arg) == 0) {
			set_realtime_core_state(init_states[n].state);
			return;
		}

	lprintk(warn_bad_state, init_state_arg);
}

static __init int sys_init(void)
{
	struct xnsched *sched;
	void *heapaddr;
	int ret;

	if (sysheap_size_arg == 0)
		sysheap_size_arg = CONFIG_XENO_OPT_SYS_HEAPSZ;

	heapaddr = xnheap_vmalloc(sysheap_size_arg * 1024);
	if (heapaddr == NULL ||
	    xnheap_init(&cobalt_heap, heapaddr, sysheap_size_arg * 1024)) {
		return -ENOMEM;
	}
	xnheap_set_name(&cobalt_heap, "system heap");

	sched = &per_cpu(nksched, AGENCY_RT_CPU);
	xnsched_init(sched, AGENCY_RT_CPU);

	xnregistry_init();

	/*
	 * If starting in stopped mode, do all initializations, but do
	 * not enable the core timer.
	 */
	if (realtime_core_state() == COBALT_STATE_WARMUP) {
		ret = xntimer_grab_hardware();
		if (ret)
			BUG();

		set_realtime_core_state(COBALT_STATE_RUNNING);
	}

	return 0;
}

void xenomai_init(void)
{
	int ret, __maybe_unused cpu;

	setup_init_state();

	cpumask_clear(&xnsched_realtime_cpus);

	lprintk("Agency RT CPU is CPU #1\n");

	cpumask_set_cpu(AGENCY_RT_CPU, &xnsched_realtime_cpus);

	if (cpumask_empty(&xnsched_realtime_cpus)) {
		lprintk("disabled via empty real-time CPU mask\n");
		set_realtime_core_state(COBALT_STATE_DISABLED);
		return ;
	}
	cobalt_cpu_affinity = xnsched_realtime_cpus;

	xnsched_register_classes();

	ret = mach_setup();
	if (ret)
		BUG();

	ret = xnpipe_mount();
	if (ret)
		BUG();
 
	ret = sys_init();
	if (ret)
		BUG();

	__cobalt_ready = true;

	lprintk("Cobalt v%s (%s) %s%s%s%s\n",
	       XENO_VERSION_STRING,
	       XENO_VERSION_NAME,
	       boot_debug_notice,
	       boot_lat_trace_notice,
	       boot_evt_trace_notice,
	       boot_state_notice);

	 return ;
}

static int __init xenomai_pre_init(void) {

	lprintk("%s: waiting on CPU %d for Xenomai/Cobalt fully initialized...\n", __func__, smp_processor_id());

	/* Now, start the init sequence in Xenomai */
	__xenomai_ready_to_go = true;

	/* Send a wakeup PI to CPU #1 */
	smp_kick_rt_agency_for_wakeup();

	while (!__cobalt_ready) ;

	return 0;
}

arch_initcall(xenomai_pre_init);
