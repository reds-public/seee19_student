/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __ASM_ARM_IRQ_H
#define __ASM_ARM_IRQ_H

#include <linux/cpumask.h>

#define NR_IRQS_LEGACY	16

#ifndef CONFIG_SPARSE_IRQ
#include <mach/irqs.h>
#else
#define NR_IRQS NR_IRQS_LEGACY
#endif
 
/* paravirt */

#define	NR_PIRQS 		256
#define PIRQ_BASE               0

#define VIRTIRQ_BASE             (PIRQ_BASE + NR_PIRQS)
#define NR_VIRTIRQS              256

#define pirq_to_irq(_x)         ((_x) + PIRQ_BASE)
#define irq_to_pirq(_x)         ((_x) - PIRQ_BASE)

#define virtirq_to_irq(_x)       ((_x) + VIRTIRQ_BASE)
#define irq_to_virtirq(_x)       ((_x) - VIRTIRQ_BASE)


#ifndef irq_canonicalize
#define irq_canonicalize(i)	(i)
#endif

/* Max number of possible IPIs */
#define NR_IPIS			16

/* paravirt */
/* Linux IRQ domain WIFI irq */
#define SOO_IRQ_SDIO_WIFI	24

/* Hardware WIFI irq */
#define SOO_IRQ_SDIO_RT_WIFI	93

/* Hardware sun50i I2S IRQ */
#define SOO_IRQ_SUN50I_RT_I2S	46

/*
 * Use this value to indicate lack of interrupt
 * capability
 */
#ifndef NO_IRQ
#define NO_IRQ	((unsigned int)(-1))
#endif

#ifndef __ASSEMBLY__
struct irqaction;
struct pt_regs;
extern void migrate_irqs(void);

extern void asm_do_IRQ(unsigned int, struct pt_regs *);
void handle_IRQ(unsigned int, struct pt_regs *);
void init_IRQ(void);

#ifdef CONFIG_MULTI_IRQ_HANDLER
extern void (*handle_arch_irq)(struct pt_regs *);
extern void set_handle_irq(void (*handle_irq)(struct pt_regs *));
#endif

#ifdef CONFIG_SMP
extern void arch_trigger_cpumask_backtrace(const cpumask_t *mask,
					   bool exclude_self);
#define arch_trigger_cpumask_backtrace arch_trigger_cpumask_backtrace
#endif

static inline int nr_legacy_irqs(void)
{
	return NR_IRQS_LEGACY;
}

#endif

#endif

