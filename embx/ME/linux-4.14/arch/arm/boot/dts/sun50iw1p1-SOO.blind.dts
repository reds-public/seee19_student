
/dts-v1/;
#include "sun50iw1p1.dtsi"

/ {
	model = "Merida A64-1.0";
	compatible = "allwinner,sun50i-a64,sun50iw1p1";

	chosen {
		stdout-path = "serial0:115200n8";
		bootargs = "earlyprintk=sunxi-uart,0x01c28000 loglevel=8 console=ttyS0,115200n8 init=/init";
		linux,initrd-start = <0x0 0x0>;
		linux,initrd-end = <0x0 0x0>;
	};

	reg_vcc3v3: vcc3v3 {
		compatible = "regulator-fixed";
		regulator-name = "vcc3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
	};

	reg_vcc1v8: vcc1v8 {
		compatible = "regulator-fixed";
		regulator-name = "vcc1v8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-always-on;
	};

	soo {
		compatible = "arm,soo";
		/* Backends */
		
		vuihandler {
			compatible = "soo,vuiHandler";
			status = "ok";
		};
		
	};
	
	soo {
		compatible = "arm,soo";
		
		/* Backends */
		
		vuart {
			compatible = "soo,vuart";
			status = "ok";
		};
		
		vuihandler {
			compatible = "soo,vuihandler";
			status = "ok";
		};
		
		vdoga12v6nm {
			compatible = "soo,vdoga12v6nm";
			status = "ok";
		};
		
	};
};

&pio {

	drv8703q1_pins: drv8703q1@0 {
		//allwinner,pins = "PC0"; -- PC0 for the v1
		allwinner,pins = "PD23", "PD24"; /* PD23 nSLEEP, PD24 MODE */
		allwinner,function = "gpio_out";
		allwinner,drive = <SUN4I_PINCTRL_10_MA>;
		allwinner,pull = <SUN4I_PINCTRL_NO_PULL>;
	};

	avdd_pins: avdd@0 {
		allwinner,pins = "PH11";
		allwinner,function = "gpio_out";
		allwinner,drive = <SUN4I_PINCTRL_10_MA>;
		allwinner,pull = <SUN4I_PINCTRL_NO_PULL>;
	};
	
	down_pins_a: down_pins@0 {
		allwinner,pins = "PD20";
		allwinner,function = "gpio_in";
		allwinner,drive = <0>;
		allwinner,pull = <1>;
	};
	
};

&r_pio {
	up_pins_a: up_pins@0 {
		allwinner,pins = "PL3";
		allwinner,function = "gpio_in";
		allwinner,drive = <0>;
		allwinner,pull = <1>;
	};
};

&soc {
	blind_gpios: blind_gpios {
		pinctrl-0 = /*<&up_pins_a>,*/ <&down_pins_a>;
		status = "okay";
	};
};

&mmc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc1_pins>;
	vmmc-supply = <&reg_vcc3v3>;
	vqmmc-supply = <&reg_vcc1v8>;
	bus-width = <4>;
	non-removable;
	status = "okay";
	sd-uhs-sdr104;
	
	realtime = <1>;
	
	mwifiex {
		compatible = "marvell,sd8897";
		reg = <1>;
	};
	
	btmrvl: btmrvl@2 {
		compatible = "marvell,sd8897-bt";
		reg = <2>;
	};
};

&mmc2 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc2_pins>;
	vmmc-supply = <&reg_vcc3v3>;
	vqmmc-supply = <&reg_vcc1v8>;
	bus-width = <8>;
	non-removable;
	cap-mmc-hw-reset;
	status = "okay";
	mmc-hs200-1_8v;
};

&spi1 {
	status = "okay";

	drv8703q1@0 {
		pinctrl-names = "default";
		pinctrl-0 = <&spi1_pins>, <&drv8703q1_pins>;

		compatible = "ti,drv8703q1";
		reg = <0>;
		spi-max-frequency = <100000>;
		//sleep-gpio = <&pio 2 0 GPIO_ACTIVE_HIGH>; /* PC0 for v1*/
		sleep-gpio = <&pio 3 23 GPIO_ACTIVE_HIGH>; /* PD23 for v2*/
		mode-gpio = <&pio 3 24 GPIO_ACTIVE_HIGH>; /* PD23 for v2*/
	};
};

&i2c0 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&i2c0_pins>;

    ds1050@28 {
        compatible = "maxim,ds1050";
        reg = <0x28>; /* This is due to the control byte (101) + A2, A1, A0 (000) combination (101000 = 0x28) */
    };
};

&sunxi_thermal_sensor {
    reg = <0x01c25000 0x400>;
    clocks = <&ccu CLK_THS>, <&ccu CLK_BUS_THS>;
    clock-names = "ths", "bus-ths";
    resets = <&ccu RST_BUS_THS>;
};
