/*
 * Allwinner Technology CO., Ltd. sun50iw1p1 platform
 * 
 * (c) 2016 Sootech SA
 * 
 * This device tree is still under elaboration for the MERIDA board.
 *
 * The different sources are :
 * - sun50i-a64 mainline dts
 * - sun50iw1p1 BSP_A64_2.0 from AllWinner (apparently used by the Pine64 board)  
 */
 

 
/* kernel used */
/memreserve/ 0x45000000 0x00200000; /* sun50iw1p1.dtb range       : [0x45000000~0x45200000], size = 2M */
/memreserve/ 0x41010000 0x00010000; /* sys_config.fex range       : [0x41010000~0x41020000], size = 64K */
/memreserve/ 0x41020000 0x00000800; /* super standby range        : [0x41020000~0x41020800], size = 2K  */
/* tf used */
/memreserve/ 0x40100000 0x00004000; /* arisc dram code space range: [0x40100000~0x40104000], size = 16K */
/memreserve/ 0x40104000 0x00001000; /* arisc para cfg range       : [0x40104000~0x40105000], size = 4K  */
/memreserve/ 0x40105000 0x00001000; /* arisc message pool range   : [0x40105000~0x40106000], size = 4K  */

#include <dt-bindings/interrupt-controller/arm-gic.h>
#include <dt-bindings/gpio/gpio.h>
#include "sun50iw1p1-clk.dtsi"
#include "sun50iw1p1-pinctrl.dtsi"

#include <dt-bindings/clock/sun50i-a64-ccu.h>
#include <dt-bindings/reset/sun50i-a64-ccu.h>

/ {
	model = "sun50iw1p1";
	compatible = "arm,sun50iw1p1";
	interrupt-parent = <&gic>;
	
	#address-cells = <1>;
	#size-cells = <1>;
	 
	aliases {
		serial0 = &uart0;
	};
	
	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "arm,cortex-a53","arm,armv8";
			/*reg = <0x0 0x0>;*/
			reg = <0x0>;
			//enable-method = "psci";
			cpufreq_tbl = < 480000
					600000
					720000
					816000
					1008000
					1104000
					1152000
					1200000
					1344000>;
			clock-latency = <2000000>;
			clock-frequency = <1008000000>;
			cpu-idle-states = <&CPU_SLEEP_0 &CLUSTER_SLEEP_0 &SYS_SLEEP_0>;
		};
		cpu@1 {
			device_type = "cpu";
			compatible = "arm,cortex-a53","arm,armv8";
			/*reg = <0x0 0x1>;*/
			reg = <0x1>;
			enable-method = "psci";
			clock-frequency = <1008000000>;
			cpu-idle-states = <&CPU_SLEEP_0 &CLUSTER_SLEEP_0 &SYS_SLEEP_0>;
		};
        
#if 0 /* paravirt */
		cpu@2 {
			device_type = "cpu";
			compatible = "arm,cortex-a53","arm,armv8";
			/*reg = <0x0 0x2>;*/
			reg = <0x2>;
			enable-method = "psci";
			clock-frequency = <1008000000>;
			cpu-idle-states = <&CPU_SLEEP_0 &CLUSTER_SLEEP_0 &SYS_SLEEP_0>;
		};
		cpu@3 {
			device_type = "cpu";
			compatible = "arm,cortex-a53","arm,armv8";
			/*reg = <0x0 0x3>;*/
			reg = <0x3>;
			enable-method = "psci";
			clock-frequency = <1008000000>;
			cpu-idle-states = <&CPU_SLEEP_0 &CLUSTER_SLEEP_0 &SYS_SLEEP_0>;
		};
#endif /* 0 */
			 
		idle-states {
			entry-method = "arm,psci";

			CPU_SLEEP_0: cpu-sleep-0 {
				compatible = "arm,idle-state";
				arm,psci-suspend-param = <0x0010000>;
				entry-latency-us = <40>;
				exit-latency-us = <100>;
				min-residency-us = <150>;
			};

			CLUSTER_SLEEP_0: cluster-sleep-0 {
				compatible = "arm,idle-state";
				arm,psci-suspend-param = <0x1010000>;
				entry-latency-us = <500>;
				exit-latency-us = <1000>;
				min-residency-us = <2500>;
			};

			SYS_SLEEP_0: sys-sleep-0 {
				compatible = "arm,idle-state";
				arm,psci-suspend-param = <0x2010000>;
				entry-latency-us = <1000>;
				exit-latency-us = <2000>;
				min-residency-us = <4500>;
			};
		};
 

		psci {
	   	 	compatible		=  "arm,psci-0.2";
	    	method		=  "smc";
	   		psci_version	=  <0x84000000>;
	   		cpu_suspend		=  <0xc4000001>;
	    	cpu_off		=  <0x84000002>;
	    	cpu_on		=  <0xc4000003>;
	    	affinity_info	=  <0xc4000004>;
	    	migrate		=  <0xc4000005>;
	    	migrate_info_type	=  <0x84000006>;
	    	migrate_info_up_cpu =  <0xc4000007>;
	    	system_off		=  <0x84000008>;
	    	system_reset	=  <0x84000009>;

		};
	};
	
	memory@40000000 {
		device_type = "memory";
		reg = <0x00000000 0x40000000 0x00000000 0x40000000>;
	};

	soc: soc@01c00000 {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <1>;
		 
		ranges;
		
		device_type = "soc";
		
#if 0	/* issued from sun50i-a64 dts */
		gic: interrupt-controller@1c81000 {
			compatible = "arm,gic-400";
			reg = <0x01c81000 0x1000>,
			      <0x01c82000 0x2000>,
			      <0x01c84000 0x2000>,
			      <0x01c86000 0x2000>;
			interrupts = <GIC_PPI 9 (GIC_CPU_MASK_SIMPLE(4) | IRQ_TYPE_LEVEL_HIGH)>;
			interrupt-controller;
			#interrupt-cells = <3>;
		};
#endif 
		
		/* preferred gic */
		gic: interrupt-controller@1c81000 {
			compatible = "arm,cortex-a15-gic", "arm,cortex-a9-gic";
			#interrupt-cells = <3>;
			#address-cells = <0>;
			device_type = "gic";
			interrupt-controller;
			reg = <0x01c81000 0x1000>, /* GIC Dist */
			      <0x01c82000 0x2000>, /* GIC CPU */
			      <0x01c84000 0x2000>, /* GIC VCPU Control */
			      <0x01c86000 0x2000>; /* GIC VCPU */
			interrupts = <GIC_PPI 9 0xf04>; /* GIC Maintenence IRQ */
		};

		mmc0: mmc@1c0f000 {
			compatible = "allwinner,sun50i-a64-mmc";
			reg = <0x01c0f000 0x1000>;
			clocks = <&ccu CLK_BUS_MMC0>, <&ccu CLK_MMC0>;
			clock-names = "ahb", "mmc";
			resets = <&ccu RST_BUS_MMC0>;
			reset-names = "ahb";
			interrupts = <GIC_SPI 60 IRQ_TYPE_LEVEL_HIGH>;
			max-frequency = <150000000>;
			status = "disabled";
			#address-cells = <1>;
			#size-cells = <0>;
		};

		mmc1: mmc@1c10000 {
			compatible = "allwinner,sun50i-a64-mmc";
			reg = <0x01c10000 0x1000>;
			clocks = <&ccu CLK_BUS_MMC1>, <&ccu CLK_MMC1>;
			clock-names = "ahb", "mmc";
			resets = <&ccu RST_BUS_MMC1>;
			reset-names = "ahb";
			interrupts = <GIC_SPI 61 IRQ_TYPE_LEVEL_HIGH>;
			max-frequency = <150000000>;
			status = "disabled";
			#address-cells = <1>;
			#size-cells = <0>;
		};

		mmc2: mmc@1c11000 {
			compatible = "allwinner,sun50i-a64-emmc";
			reg = <0x01c11000 0x1000>;
			clocks = <&ccu CLK_BUS_MMC2>, <&ccu CLK_MMC2>;
			clock-names = "ahb", "mmc";
			resets = <&ccu RST_BUS_MMC2>;
			reset-names = "ahb";
			interrupts = <GIC_SPI 62 IRQ_TYPE_LEVEL_HIGH>;
			max-frequency = <200000000>;
			status = "disabled";
			#address-cells = <1>;
			#size-cells = <0>;
		};

		timer {
			compatible = "arm,armv8-timer";
			interrupts = <GIC_PPI 13 0xff01>, /* Secure Phys IRQ */
				     <GIC_PPI 14 0xff01>, /* Non-secure Phys IRQ */
				     <GIC_PPI 11 0xff01>, /* Virt IRQ */
				     <GIC_PPI 10 0xff01>; /* Hyp IRQ */
			clock-frequency = <24000000>;
			always-on;
		};
	
		pmu {
			compatible = "arm,armv8-pmuv3";
			interrupts = <GIC_SPI 120 4>,
				     <GIC_SPI 121 4>,
				     <GIC_SPI 122 4>,
				     <GIC_SPI 123 4>;
		};
		rtc: rtc@1f00000 {
			compatible = "allwinner,sun6i-a31-rtc";
			reg = <0x01f00000 0x54>;
			interrupts = <GIC_SPI 40 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 41 IRQ_TYPE_LEVEL_HIGH>;
		};
 
 		/* uart-0 mainly used for the console */
 
		uart0: uart@01c28000 {
			compatible = "snps,dw-apb-uart";
			reg = <0x01c28000 0x400>;
			device_type = "uart0";
			interrupts = <GIC_SPI 0 IRQ_TYPE_LEVEL_HIGH>;
 
 			clocks = <&ccu CLK_BUS_UART0>;
			resets = <&ccu RST_BUS_UART0>;
			
			pinctrl-names = "default", "sleep";
			pinctrl-0 = <&uart0_pins_a>;
			 
			uart0_port = <0>;
			uart0_type = <4>;
			reg-shift = <2>;
			reg-io-width = <4>;
			status = "okay";
		};
        
		uart4: uart@01c29000 {
			compatible = "snps,dw-apb-uart";
			reg = <0x01c29000 0x400>;
			device_type = "uart4";
			interrupts = <GIC_SPI 4 IRQ_TYPE_LEVEL_HIGH>;
 
 			clocks = <&ccu CLK_BUS_UART4>;
			resets = <&ccu RST_BUS_UART4>;
			
			pinctrl-names = "default", "sleep";
			pinctrl-0 = <&uart4_pins_a>;
			 
			uart4_port = <4>;
			uart4_type = <4>;
			reg-shift = <2>;
			reg-io-width = <4>;
            
            status = "disabled";
		};

		spi0: spi0@1c68000 {
			#address-cells = <1>;
			#size-cells = <0>;
			compatible = "allwinner,sun50i-spi";
			reg = <0x01c68000 0x1000>;
			interrupts = <GIC_SPI 65 IRQ_TYPE_LEVEL_HIGH>;
            clocks = <&ccu CLK_BUS_SPI0>, <&ccu CLK_SPI0>;
			clock-names = "ahb", "mod";
			#pinctrl-names = "default";
			resets = <&ccu RST_BUS_SPI0>;
			status = "disabled";
			#device_type = "spi0";
#if 0 /* paravirt */
			clocks = <&clk_pll_periph0>, <&clk_spi0>;
			clock-frequency = <100000000>;
			pinctrl-names = "default", "sleep";
            pinctrl-0 = <&spi0_pins>;
			pinctrl-0 = <&spi0_pins_a &spi0_pins_b>;
			pinctrl-1 = <&spi0_pins_c>;
			spi0_cs_number = <1>;
			spi0_cs_bitmap = <1>;
#endif /* 0 */
		};

		spi1: spi1@1c69000 {
			#address-cells = <1>;
			#size-cells = <0>;
			compatible = "allwinner,sun50i-spi";
			reg = <0x01c69000 0x1000>;
			interrupts = <GIC_SPI 66 IRQ_TYPE_LEVEL_HIGH>;
            clocks = <&ccu CLK_BUS_SPI1>, <&ccu CLK_SPI1>;
			clock-names = "ahb", "mod";
			#pinctrl-names = "default";
			resets = <&ccu RST_BUS_SPI1>;
			status = "disabled";
			#device_type = "spi1";
#if 0 /* paravirt */
			clocks = <&clk_pll_periph0>, <&clk_spi1>;
			clock-frequency = <100000000>;
			pinctrl-names = "default", "sleep";
            pinctrl-0 = <&spi1_pins>;
			pinctrl-0 = <&spi1_pins_a &spi1_pins_b>;
			pinctrl-1 = <&spi1_pins_c>;
			spi1_cs_number = <1>;
			spi1_cs_bitmap = <1>;
#endif /* 0 */
		};

		sunxi_thermal_sensor:thermal_sensor{
			compatible = "allwinner,thermal_sensor";
			reg = <0x0 0x01c25000 0x0 0x400>;
			interrupts = <GIC_SPI 31 IRQ_TYPE_NONE>;
			clocks = <&clk_hosc>,<&clk_ths>;
			sensor_num = <3>;
			shut_temp= <120>;
			status = "okay";

			combine0:combine0{
				#thermal-sensor-cells = <1>;
				combine_cnt = <3>;
				combine_type = "max";
				combine_chn = <0 1 2>;
			};
		};

		i2c0: i2c@1c2ac00 {
			compatible = "allwinner,sun6i-a31-i2c";
			reg = <0x01c2ac00 0x400>;
			interrupts = <GIC_SPI 6 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&ccu CLK_BUS_I2C0>;
			resets = <&ccu RST_BUS_I2C0>;
			status = "disabled";
			#address-cells = <1>;
			#size-cells = <0>;
		};

		i2c1: i2c@1c2b000 {
			compatible = "allwinner,sun6i-a31-i2c";
			reg = <0x01c2b000 0x400>;
			interrupts = <GIC_SPI 7 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&ccu CLK_BUS_I2C1>;
			resets = <&ccu RST_BUS_I2C1>;
			status = "disabled";
			#address-cells = <1>;
			#size-cells = <0>;
		};

		syscon: syscon@01c00000 {
			compatible = "syscon";
			reg = <0x01c00000 0x1000>;
		};

		emac: ethernet@1c30000 {
			compatible = "allwinner,sun50i-a64-emac";
			syscon = <&syscon>;
			reg = <0x01c30000 0x104>;
			interrupts = <GIC_SPI 82 IRQ_TYPE_LEVEL_HIGH>;
			interrupt-names = "macirq";
			resets = <&ccu RST_BUS_EMAC>;
			reset-names = "stmmaceth";
			clocks = <&ccu CLK_BUS_EMAC>;
			clock-names = "stmmaceth";
			#address-cells = <1>;
			#size-cells = <0>;
			status = "disabled";

			phy-mode = "rgmii";
			phy-handle = <&phy0>;
			allwinner,leds-active-low;
			pinctrl-names = "default";
			pinctrl-0 = <&rgmii_pins>;

			mdio: mdio {
				compatible = "snps,dwmac-mdio";
				#address-cells = <1>;
				#size-cells = <0>;

				phy0: ethernet-phy@0 {
					reg = <0>;
					interrupts = <0 2 IRQ_TYPE_LEVEL_LOW>;
				};
			};
		};
		
	}; /* soc */
};
