/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 * 
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * -  January 2018: Baptiste Delporte
 */

#include <soolink/sender.h>
#include <soolink/receiver.h>
#include <soolink/datalink.h>
#include <soolink/discovery.h>
#include <soolink/plugin.h>

/**
 * Initialize the Transceiver functional block of Soolink.
 */
void transceiver_init(void) {

	lprintk("SOOlink: transceiver functional block initializing now ...\n");

	/* Internal blocks initialization */
	sender_init();
	receiver_init();

	/* Initialize the Discovery */
	discovery_init();

	/* Initialize the protocols within the Datalink */
	datalink_init();

	/* Initialize the SOOlink plugins (registered as SOOlink initcall). */
	transceiver_plugin_init();
}
