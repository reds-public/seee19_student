/*
 * SOO Agency driver
 *
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016 Sootech SA, Switzerland
 *
 * This code is under the property of Sootech SA and must not be shared.
 *
 * This code has been originally developed by Laurent Colloud, REDS Institute
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 * - January 2018: Baptiste Delporte
 *
 */

#if 1
#define DEBUG
#endif

#if 0
#define ENABLE_LOGBOOL
#endif

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/ioctl.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/mm.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/random.h>
#include <linux/completion.h>
#include <linux/delay.h>
#include <linux/mmc/host.h>
#include <linux/sched/signal.h>
#include <linux/uaccess.h>

#include <asm/io.h>
#include <asm/uaccess.h>

#ifdef CONFIG_ARM
#include <asm/mach/map.h>
#endif

#include <asm/cacheflush.h>

#include <virtshare/gnttab.h>
#include <virtshare/vbstore_me.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/soo.h>
#include <virtshare/guest_api.h>
#include <virtshare/console.h>
#include <virtshare/avz.h>
#include <virtshare/evtchn.h>
#include <virtshare/vbstore.h>

#include <soo/core/core.h>

int ME_init(void) {

	int rc;

	DBG("SOO Migration subsystem registering...\n");

	soo_guest_activity_init();

	vbstore_init_dev_populate();

	return 0;
}


late_initcall(ME_init);

