#ifndef SUN50I_REBOOT_H
#define SUN50I_REBOOT_H

#define SUN50I_TIMER_BASE	(0x01c20c00)
#define SUN50I_WDT_CFG_RESET	(0x1)
#define SUN50I_WDT_MODE_EN	(0x1)

#define SUN50I_WDT_CTRL_RESTART	(0x1 << 0)
#define SUN50I_WDT_CTRL_KEY	(0x0a57 << 1)

struct sunxi_wdog {
	uint32_t irq_en;	/* 0x00 */
	uint32_t irq_sta;	/* 0x04 */
	uint32_t res1[2];
	uint32_t ctl;		/* 0x10 */
	uint32_t cfg;		/* 0x14 */
	uint32_t mode;		/* 0x18 */
	uint32_t res2;
};

/* General purpose timer */
struct sunxi_timer {
	volatile uint32_t ctl;
	volatile uint32_t inter;
	volatile uint32_t val;
	volatile uint8_t res[4];
};

/* Audio video sync*/
struct sunxi_avs {
	volatile uint32_t ctl;		/* 0x80 */
	volatile uint32_t cnt0;		/* 0x84 */
	volatile uint32_t cnt1;		/* 0x88 */
	volatile uint32_t div;		/* 0x8c */
};

struct sunxi_timer_reg {
	volatile uint32_t tirqen;	/* 0x00 */
	volatile uint32_t tirqsta;	/* 0x04 */
	uint8_t res1[8];
	struct sunxi_timer timer[6];	/* We have 6 timers */
	uint8_t res2[16];
	struct sunxi_avs avs;
	uint8_t res3[16];
	struct sunxi_wdog wdog[5];	/* We have 5 watchdogs */
};

#endif /* SUN50I_REBOOT_H */
