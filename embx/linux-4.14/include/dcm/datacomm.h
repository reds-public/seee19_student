
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 * - November 2017: Baptiste Delporte
 * - April 2018: Baptiste Delporte
 */

#ifndef DATACOMM_H
#define DATACOMM_H

#include <linux/types.h>

long datacomm_init(void);

bool datacomm_ready_to_send(void);

void datacomm_send(void *ME_buffer, size_t size, uint32_t prio);

#endif /* DATACOMM_H */
