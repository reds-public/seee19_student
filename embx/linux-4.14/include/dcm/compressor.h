
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - November 2017: Baptiste Delporte
 */

#ifndef COMPRESSOR_H
#define COMPRESSOR_H

#include <linux/types.h>

#define COMPRESSOR_NO_COMPRESSION	0
#define COMPRESSOR_LZ4			1
#define COMPRESSOR_N_METHODS		2

typedef struct {

	uint8_t	compression_method;

	size_t	decompressed_size;

	/*
	 * First byte of the payload. Accessing to its address gives a direct access to the
	 * payload buffer.
	 */
	uint8_t	payload[0];

} compressor_data_t;

typedef struct {
	/* Function to be called when compressing data */
	int (*compress_callback)(void **data_compressed, void *source_data, size_t source_size);

	/* Function to be called when decompressing data */
	int (*decompress_callback)(void **data_decompressed, compressor_data_t *data_compressed, size_t compressed_size);

} compressor_method_t;

int compress_data(uint8_t method, void **data_compressed, void *source_data, size_t source_size);
int decompress_data(void **data_decompressed, void *data_compressed, size_t compressed_size);

void compressor_method_register(uint8_t method, compressor_method_t *method_desc);
void compressor_init(void);

#endif /* COMPRESSOR_H */
