/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - December 2018: Daniel Rossier
 *
 */

unsigned int xcrc32 (const unsigned char *buf, int len, unsigned int init);
