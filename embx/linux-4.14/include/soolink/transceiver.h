/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte
 * - February 2018: Baptiste Delporte
 */

#ifndef TRANSCEIVER_H
#define TRANSCEIVER_H

#include <linux/types.h>

#define TRANSCEIVER_PKT_DATA	 	1
#define TRANSCEIVER_PKT_DATALINK 	2

typedef struct {

	uint8_t packet_type;

	/* The size is the size of the payload */
	size_t size;

	uint32_t transID;

	/*
	 * First byte of the payload. Accessing to its address gives a direct access to the
	 * payload buffer.
	 */
	uint8_t	payload[0];

} transceiver_packet_t;

typedef struct {

	uint8_t packet_type; /* Data or beacon */

	uint32_t peerID; /* SOO concerned by the packet within the burst */

	/*
	 * First byte of the payload. Accessing to its address gives a direct access to the
	 * payload buffer.
	 */
	uint8_t	payload[0];

} netstream_transceiver_packet_t;

void transceiver_init(void);

#endif /* TRANSCEIVER_H */
