
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2016: Raphaël Buache, Xavier Ruppen
 * - January-March 2017: Raphaël Buache, Xavier Ruppen
 * - July 2017: Daniel Rossier
 * - January 2018: Baptiste Delporte
 * - August 2018: Baptiste Delporte
 * - December 2018: Baptiste Delporte
 */

#ifndef DECODER_H
#define DECODER_H

#include <linux/types.h>
#include <linux/spinlock.h>
#include <linux/ktime.h>

#include <xenomai/rtdm/driver.h>

#include <soolink/soolink.h>

/* Timeout after which a block is deleted, in ms */
#define SOOLINK_DECODE_BLOCK_TIMEOUT	4000

/* A decoder block maintains all attributes of a given block which may be simple or extended */
typedef struct {
	sl_desc_t 	*sl_desc;

	/* The block frame itself */
	void		*incoming_block;

	size_t		size;
	size_t		total_size;
	size_t		real_size;

	uint32_t	n_total_packets;
	uint32_t	n_recv_packets;

	uint32_t	cur_packetID;
	uint8_t		*cur_pos;
	bool		block_ext_in_progress;
	bool		discarded_block;
	bool		complete;

	s64		last_timestamp;

	/* List of block */
	struct list_head list;
} decoder_block_t;

int decoder_recv(sl_desc_t *sl_desc, void **data);
int decoder_rx(sl_desc_t *sl_desc, void *data, size_t size);

int decoder_stream_recv(sl_desc_t *sl_desc, void **data);
int decoder_stream_rx(sl_desc_t *sl_desc, void *data);

void decoder_init(void);

#endif /* DECODER_H */
