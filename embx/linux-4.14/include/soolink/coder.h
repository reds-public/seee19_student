
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2016: Raphaël Buache, Xavier Ruppen
 * - January-March 2017: Raphaël Buache, Xavier Ruppen
 * - July 2017: Daniel Rossier
 * - August 2018: Baptiste Delporte
 *
 */

#ifndef CODER_H
#define CODER_H

#include <linux/types.h>

#include <rtdm/driver.h>

#include <soolink/soolink.h>

void coder_send(sl_desc_t *sl_desc, void *data, size_t size);
void coder_init(void);

void coder_stream_send(sl_desc_t *sl_desc, void *data);

#endif /* CODER_H */

