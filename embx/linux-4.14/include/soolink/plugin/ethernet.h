/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2017: Baptiste Delporte
 *
 * Soolink Ethernet plugin v2.1
 *
 */

#ifndef PLUGIN_ETHERNET_H
#define PLUGIN_ETHERNET_H

#include <linux/skbuff.h>

#include <soolink/soolink.h>

#define ETHERNET_NET_DEV_NAME 	"eth0"

void sl_plugin_ethernet_rx(struct sk_buff *skb, struct net_device *net_dev, uint8_t *mac_src);
void propagate_plugin_ethernet_send(void);
void rtdm_propagate_sl_plugin_ethernet_rx(void);

void sl_plugin_tcp_rx(void *data, size_t size);
void propagate_plugin_tcp_send(void);
void rtdm_propagate_sl_plugin_tcp_rx(void);

void plugin_ethernet_delete_remote(agencyUID_t *agencyUID);

#endif /* PLUGIN_ETHERNET_H */
