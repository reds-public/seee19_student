/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2016: Raphaël Buache, Xavier Ruppen
 * - January-March 2017: Raphaël Buache, Xavier Ruppen
 * - July 2017: Daniel Rossier
 */

#ifndef PLUGIN_LOOPBACK_H
#define PLUGIN_LOOPBACK_H

#include <linux/skbuff.h>

#include <soolink/soolink.h>

#define LOOPBACK_NET_DEV_NAME 	"lo"

void sl_plugin_loopback_rx(struct sk_buff *skb);
void propagate_plugin_loopback_send(void);
void rtdm_propagate_sl_plugin_loopback_rx(void);

#endif /* PLUGIN_LOOPBACK_H */
