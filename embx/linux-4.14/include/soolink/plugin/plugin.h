

/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - 2016: Raphaël Buache, Xavier Ruppen
 * - January-March 2017: Raphaël Buache, Xavier Ruppen
 * - July, August 2017: Daniel Rossier
 */

#ifndef PLUGIN_H
#define PLUGIN_H

#include <linux/skbuff.h>

extern uint8_t broadcast_dest[ETH_ALEN];

#endif /* PLUGIN_H */
