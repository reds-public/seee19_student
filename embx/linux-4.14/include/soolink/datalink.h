/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte, Jean-Pierre Miceli (REDS)
 * - April 2018: Baptiste Delporte
 */

#ifndef DATALINK_H
#define DATALINK_H

#include <virtshare/console.h>
#include <virtshare/debug.h>

#include <soolink/soolink.h>
#include <soolink/transceiver.h>
#include <soolink/plugin.h>

/**
 * This is the descriptor of a protocol that can be used by Datalink.
 */
typedef struct {

	/* Function to inform if the sl_desc channel is ready for sending. */
	bool (*ready_to_send)(sl_desc_t *sl_desc);

	/* Function to be called when sending data */
	int (*xmit_callback)(sl_desc_t *sl_desc, void *packet, size_t size, bool completed);

	/* Function to be called when receiving data */
	void (*rx_callback)(sl_desc_t *sl_desc, plugin_desc_t *plugin_desc, void *packet, size_t size);

	/* Function to be called when requesting to send data */
	int (*request_xmit_callback)(sl_desc_t *sl_desc);

} datalink_proto_desc_t;

void datalink_register_protocol(datalink_proto_t proto, datalink_proto_desc_t *proto_desc);
int datalink_request_xmit(sl_desc_t *sl_desc);
int datalink_xmit(sl_desc_t *sl_desc, void *packet, size_t size, bool completed);
void datalink_rx(sl_desc_t *sl_desc, plugin_desc_t *plugin_desc, void *packet, size_t size);

void datalink_init(void);

#endif /* DATALINK_H */
