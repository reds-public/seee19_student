


void rtdm_mwifiex_wmm_add_buf_txqueue(struct mwifiex_private *priv, struct sk_buff *skb);
void rtdm_mwifiex_wmm_process_tx(struct mwifiex_adapter *adapter);

struct mwifiex_ra_list_tbl *mwifiex_wmm_get_highest_priolist_ptr(struct mwifiex_adapter *adapter, struct mwifiex_private **priv, int *tid);
int mwifiex_is_11n_aggragation_possible(struct mwifiex_private *priv, struct mwifiex_ra_list_tbl *ptr, int max_buf_size);

int mwifiex_bypass_txlist_empty(struct mwifiex_adapter *adapter);
