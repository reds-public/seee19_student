/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Baptiste Delporte
 *
 */


#ifndef UTIL_H
#define UTIL_H

int rtdm_mwifiex_recv_packet(struct mwifiex_private *priv, struct sk_buff *skb);

extern void soolink_wlan_skb_receive(struct sk_buff *skb, struct net_device *dev, const uint8_t *mac_src);

#endif /* UTIL_H */

