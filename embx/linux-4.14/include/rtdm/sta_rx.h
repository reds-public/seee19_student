/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Baptiste Delporte
 *
 */


#ifndef STA_RX_H
#define STA_RX_H

int mwifiex_process_sta_rx_packet(struct mwifiex_private *priv, struct sk_buff *skb);

#endif /* STA_RX_H */

