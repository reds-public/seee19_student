/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2018: Daniel Rossier
 *
 */

#ifndef BCM2835_H
#define BCM2835_H

#include <linux/netdevice.h>

struct mmc_request;
struct mmc_host;

netdev_tx_t brcmf_netdev_start_xmit(struct sk_buff *skb, struct net_device *ndev);

void rtdm_sdhci_mmc_init_threads(void);
void rtdm_sdhci_mmc_init(void);
void rtdm_sdhci_request_irq(void);

void rtdm_sdhci_request_irq(void);

void sdhci_request(struct mmc_host *mmc, struct mmc_request *mrq);

void rtdm_sdhci_sdio_init_funcs(void);
#endif /* BCM2835_H */


