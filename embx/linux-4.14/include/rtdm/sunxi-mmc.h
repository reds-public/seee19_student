/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Daniel Rossier
 *
 */

#ifndef SUNXI_MMC_H
#define SUNXI_MMC_H

#include <linux/mmc/host.h>

struct sunxi_mmc_host;
struct mmc_data;
struct mmc_request;

void rtdm_sunxi_sdio_start_thread(void);
void rtdm_sunxi_sdio_init_funcs(void);
void rtdm_sunxi_mmc_request(struct mmc_host *mmc, struct mmc_request *mrq);
void rtdm_sunxi_request_irq(void);


int sunxi_mmc_map_dma(struct sunxi_mmc_host *host, struct mmc_data *data);

enum dma_data_direction sunxi_mmc_get_dma_dir(struct mmc_data *data);
void sunxi_mmc_start_dma(struct sunxi_mmc_host *host, struct mmc_data *data);

void rtdm_sunxi_mmc_init(void);
void rtdm_sunxi_mmc_init_threads(void);
void sunxi_mmc_host_init(struct sunxi_mmc_host *mmc_host);

void sunxi_mmc_dump_errinfo(struct sunxi_mmc_host *host);

#endif /* SUNXI_MMC_H */


