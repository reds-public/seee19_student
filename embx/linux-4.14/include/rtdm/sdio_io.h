

/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Daniel Rossier
 *
 */

#ifndef SDIO_IO_H
#define SDIO_IO_H

u8 rtdm_sdio_readb(struct sdio_func *func, unsigned int addr, int *err_ret);
void rtdm_sdio_writeb(struct sdio_func *func, u8 b, unsigned int addr, int *err_ret);
int rtdm_sdio_readsb(struct sdio_func *func, void *dst, unsigned int addr, int count);
int rtdm_sdio_writesb(struct sdio_func *func, unsigned int addr, void *src, int count);

#endif /* SDIO_IO_H */

