/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Daniel Rossier
 *
 */


#ifndef TXRX_H
#define TXRX_H

struct mwifiex_adapter;
struct mwifiex_tx_param;
struct mwifiex_private;

int rtdm_mwifiex_handle_rx_packet(struct mwifiex_adapter *adapter, struct sk_buff *skb);

int rtdm_mwifiex_host_to_card(struct mwifiex_adapter *adapter, struct sk_buff *skb, struct mwifiex_tx_param *tx_param);
int rtdm_mwifiex_process_tx(struct mwifiex_private *priv, struct sk_buff *skb, struct mwifiex_tx_param *tx_param);

int rtdm_mwifiex_write_data_complete(struct mwifiex_adapter *adapter, struct sk_buff *skb, int aggr, int status);
void rtdm_mwifiex_event_data_sent(void);

void rtdm_txrx_init_data_sent(void);

#endif /* TXRX_H */

