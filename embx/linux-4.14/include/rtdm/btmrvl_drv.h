/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - April 2017: Baptiste Delporte
 *
 */

#ifndef BTMRVL_DRV_H
#define BTMRVL_DRV_H

void rtdm_btmrvl_interrupt(struct btmrvl_private *priv);

#endif /* BTMRVL_DRV_H */

