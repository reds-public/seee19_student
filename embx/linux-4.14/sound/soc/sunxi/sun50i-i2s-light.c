/*
 * Copyright (C) 2015 Andrea Venturi
 * Andrea Venturi <be17068@iperbole.bo.it>
 *
 * Copyright (C) 2016 Maxime Ripard
 * Maxime Ripard <maxime.ripard@free-electrons.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 *
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 *- 2017: Florian Vaussard (ReDS)
 * - March 2018: Baptiste Delporte
 * - September 2018: Baptiste Delporte
 *
 *
 * This is the sun50 I2S driver. It interacts with the audio codec using an I2S interface.
 * It can work in two modes:
 * - Non RT mode, typically for the SOO.ambient product. Audio snapshot recording and playback can be
 *   requested.
 * - RT mode (streaming), typically for the SOO.dio product. Capture and playback are being performed in
 *   a parallel way and there are RT constraints.
 * By default, we are working in non-RT mode. Once the RT mode has been enabled, we cannot go back to
 * non-RT mode.
 *
 */

#if 1
#define DEBUG
#endif

#include <linux/clk.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/regmap.h>
#include <linux/reset.h>

#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dai.h>
#include <linux/io.h>

#include <virtshare/avz.h>
#include <virtshare/debug.h>
#include <virtshare/console.h>

#include <xenomai/rtdm/driver.h>

#include <rtdm/soo.h>

#include <virtshare/debug/time.h>

#if defined(CONFIG_VAUDIO_BACKEND)
#include <virtshare/dev/vaudio.h>
#endif /* CONFIG_VAUDIO_BACKEND */

#if defined(CONFIG_VAUDIO_BACKEND)

#define SUN50I_NRT_SAMPLING_FREQUENCY	VAUDIO_REC_SAMPLING_FREQUENCY
#define SUN50I_NRT_WIDTH		VAUDIO_REC_WIDTH
#define SUN50I_NRT_PHYSICAL_WIDTH	VAUDIO_REC_PHYSICAL_WIDTH
#define SUN50I_NRT_N_CHANNELS		VAUDIO_REC_N_CHANNELS
#define SUN50I_RECORD_DURATION		VAUDIO_REC_DURATION
#define SUN50I_N_SAMPLES		VAUDIO_REC_N_SAMPLES
#define SUN50I_BUFFER_SIZE		VAUDIO_REC_BUFFER_SIZE

#define SUN50I_RT_SAMPLING_FREQUENCY	VAUDIO_STR_SAMPLING_FREQUENCY
#define SUN50I_RT_WIDTH			VAUDIO_STR_WIDTH
#define SUN50I_RT_PHYSICAL_WIDTH	VAUDIO_STR_PHYSICAL_WIDTH
#define SUN50I_RT_N_CHANNELS		VAUDIO_STR_N_CHANNELS

#define SUN50I_RXFIFO_TH		(VAUDIO_STR_N_SAMPLES_PER_PERIOD - 1)

#else

#define SUN50I_NRT_SAMPLING_FREQUENCY	32000
#define SUN50I_NRT_WIDTH		16
#define SUN50I_NRT_PHYSICAL_WIDTH	32
#define SUN50I_NRT_N_CHANNELS		1

#define SUN50I_RECORD_DURATION		3
#define SUN50I_N_SAMPLES		(SUN50I_NRT_N_CHANNELS * SUN50I_RECORD_DURATION * SUN50I_NRT_SAMPLING_FREQUENCY)
#define SUN50I_BUFFER_SIZE		((SUN50I_NRT_WIDTH / 8) * SUN50I_N_SAMPLES)

#define SUN50I_RT_SAMPLING_FREQUENCY	48000
#define SUN50I_RT_WIDTH			24
#define SUN50I_RT_PHYSICAL_WIDTH	32
#define SUN50I_RT_N_CHANNELS		2

#define SUN50I_RXFIFO_TH		0x2f

#endif /* CONFIG_VAUDIO_BACKEND */

#ifndef _SUN50I_I2S_H
#define _SUN50I_I2S_H

#define SUN50I_I2S_CTRL_REG			0x00
#define SUN50I_I2S_CTRL_BCLK_OUT		BIT(18)
#define SUN50I_I2S_CTRL_LRCK_OUT		BIT(17)
#define SUN50I_I2S_CTRL_SDO_EN_MASK		BIT(8)
#define SUN50I_I2S_CTRL_SDO_EN			BIT(8)
#define SUN50I_I2S_CTRL_MODE_SEL_MASK		GENMASK(5, 4)
#define SUN50I_I2S_CTRL_MODE_PCM		(0 << 4)
#define SUN50I_I2S_CTRL_MODE_LEFT_JUSTIFIED	(1 << 4)
#define SUN50I_I2S_CTRL_MODE_RIGHT_JUSTIFIED	(2 << 4)
#define SUN50I_I2S_CTRL_TX_EN			BIT(2)
#define SUN50I_I2S_CTRL_RX_EN			BIT(1)
#define SUN50I_I2S_CTRL_GL_EN			BIT(0)

#define SUN50I_I2S_FMT0_REG			0x04
#define SUN50I_I2S_FMT0_LRCLK_POLARITY_MASK	BIT(19)
#define SUN50I_I2S_FMT0_LRCLK_POLARITY_INVERTED	(1 << 19)
#define SUN50I_I2S_FMT0_LRCLK_POLARITY_NORMAL	(0 << 19)
#define SUN50I_I2S_FMT0_LRCK_PERIOD_MASK	GENMASK(17, 8)
#define SUN50I_I2S_FMT0_LRCK_PERIOD(p)		((p-1) << 8)
#define SUN50I_I2S_FMT0_BCLK_POLARITY_MASK	BIT(7)
#define SUN50I_I2S_FMT0_BCLK_POLARITY_INVERTED	(1 << 7)
#define SUN50I_I2S_FMT0_BCLK_POLARITY_NORMAL	(0 << 7)
#define SUN50I_I2S_FMT0_SR_MASK			GENMASK(6, 4)
#define SUN50I_I2S_FMT0_SR(sr)			((sr) << 4)
#define SUN50I_I2S_FMT0_EDGE_TRANSFER_MASK	BIT(3)
#define SUN50I_I2S_FMT0_SAME_EDGE		(1 << 3)
#define SUN50I_I2S_FMT0_DIFFERENT_EDGE		(0 << 3)
#define SUN50I_I2S_FMT0_SW_MASK			GENMASK(2, 0)
#define SUN50I_I2S_FMT0_SW(sw)			((sw) << 0)

#define SUN50I_I2S_FMT1_REG			0x08
#define SUN50I_I2S_FIFO_RX_REG			0x10

#define SUN50I_I2S_INT_STA_REG			0x0c
#define SUN50I_I2S_INT_STA_TXU_INT		BIT(6)
#define SUN50I_I2S_INT_STA_TXO_INT		BIT(5)
#define SUN50I_I2S_INT_STA_TXE_INT		BIT(4)
#define SUN50I_I2S_INT_STA_RXU_INT		BIT(2)
#define SUN50I_I2S_INT_STA_RXO_INT		BIT(1)
#define SUN50I_I2S_INT_STA_RXA_INT		BIT(0)

#define SUN50I_I2S_FIFO_CTRL_REG		0x14
#define SUN50I_I2S_FIFO_CTRL_FLUSH_TX		BIT(25)
#define SUN50I_I2S_FIFO_CTRL_FLUSH_RX		BIT(24)
#define SUN50I_I2S_FIFO_CTRL_TXTL_MASK		GENMASK(18, 12)
#define SUN50I_I2S_FIFO_CTRL_TXTL(level)	((level) << 12)
#define SUN50I_I2S_FIFO_CTRL_RXTL_MASK		GENMASK(9, 4)
#define SUN50I_I2S_FIFO_CTRL_RXTL(level)	((level) << 4)
#define SUN50I_I2S_FIFO_CTRL_TX_MODE_MASK	BIT(2)
#define SUN50I_I2S_FIFO_CTRL_TX_MODE(mode)	((mode) << 2)
#define SUN50I_I2S_FIFO_CTRL_RX_MODE_MASK	GENMASK(1, 0)
#define SUN50I_I2S_FIFO_CTRL_RX_MODE(mode)	(mode)

#define SUN50I_I2S_FIFO_STA_REG			0x18
#define SUN50I_I2S_FIFO_STA_TXE_CNT_OFFSET	16
#define SUN50I_I2S_FIFO_STA_TXE_CNT_MASK	GENMASK(23, 16)
#define SUN50I_I2S_FIFO_STA_RXA_CNT_OFFSET	0
#define SUN50I_I2S_FIFO_STA_RXA_CNT_MASK	GENMASK(6, 0)

#define SUN50I_I2S_DMA_INT_CTRL_REG		0x1c
#define SUN50I_I2S_DMA_INT_CTRL_TX_DRQ_EN	BIT(7)
#define SUN50I_I2S_DMA_INT_CTRL_TXUI_EN		BIT(6)
#define SUN50I_I2S_DMA_INT_CTRL_TXOI_EN		BIT(5)
#define SUN50I_I2S_DMA_INT_CTRL_TXEI_EN		BIT(4)
#define SUN50I_I2S_DMA_INT_CTRL_RX_DRQ_EN	BIT(3)
#define SUN50I_I2S_DMA_INT_CTRL_RXUI_EN		BIT(2)
#define SUN50I_I2S_DMA_INT_CTRL_RXOI_EN		BIT(1)
#define SUN50I_I2S_DMA_INT_CTRL_RXAI_EN		BIT(0)

#define SUN50I_I2S_FIFO_TX_REG			0x20

#define SUN50I_I2S_CLK_DIV_REG			0x24
#define SUN50I_I2S_CLK_DIV_MCLK_EN		BIT(8)
#define SUN50I_I2S_CLK_DIV_BCLK_MASK		GENMASK(7, 4)
#define SUN50I_I2S_CLK_DIV_BCLK(bclk)		((bclk) << 4)
#define SUN50I_I2S_CLK_DIV_MCLK_MASK		GENMASK(3, 0)
#define SUN50I_I2S_CLK_DIV_MCLK(mclk)		((mclk) << 0)

#define SUN50I_I2S_TX_CNT_REG			0x28
#define SUN50I_I2S_RX_CNT_REG			0x2c

#define SUN50I_I2S_CHCFG_REG			0x30
#define SUN50I_I2S_CHCFG_RX_SLOT_NUM_MASK	GENMASK(6, 4)
#define SUN50I_I2S_CHCFG_RX_SLOT_NUM(n)		((n - 1) << 4)
#define SUN50I_I2S_CHCFG_TX_SLOT_NUM_MASK	GENMASK(2, 0)
#define SUN50I_I2S_CHCFG_TX_SLOT_NUM(n)		((n - 1) << 0)

#define SUN50I_I2S_TX_CHAN_SEL_REG(n)		(0x34 + n * 4)
#define SUN50I_I2S_TX_CHAN_OFFSET_MASK		BIT(12)
#define SUN50I_I2S_TX_CHAN_OFFSET(val)		(val << 12)
#define SUN50I_I2S_TX_CHAN_EN_MASK		GENMASK(11, 4)
#define SUN50I_I2S_TX_CHAN_EN(num_chan)		BIT(num_chan + 4)
#define SUN50I_I2S_TX_CHAN_SEL_MASK		GENMASK(2, 0)
#define SUN50I_I2S_TX_CHAN_SEL(num_chan)	(((num_chan) - 1) << 0)

#define SUN50I_I2S_TX_CHAN_MAP_REG(n)		(0x44 + n * 4)
#define SUN50I_I2S_TX_CHAN_MAP(chan, sample)	((sample) << (chan << 2))

#define SUN50I_I2S_RX_CHAN_SEL_REG		0x54
#define SUN50I_I2S_RX_CHAN_OFFSET_MASK		BIT(12)
#define SUN50I_I2S_RX_CHAN_OFFSET(val)		(val << 12)
#define SUN50I_I2S_RX_CHAN_SEL_MASK		GENMASK(2, 0)
#define SUN50I_I2S_RX_CHAN_SEL(num_chan)	(((num_chan) - 1) << 0)

#define SUN50I_I2S_RX_CHAN_MAP_REG		0x58
#define SUN50I_I2S_RX_CHAN_MAP(chan, sample)	((sample) << (chan << 2))

#define SUN50I_MAX_RXFIFO_TH			64

#endif /* _SUN50I_I2S_H */

static struct sun50i_i2s *sun50i_i2s;

/* By default, we are in non-RT mode */
static bool rt_enabled = false;
static bool rt_stream_enabled = false;

/* nRT mode */

static bool loopback_in_progress = false;

static bool recording_in_progress = false;
static void *recording_buffer = NULL;
static uint32_t recorded_sample_counter = 0;
static uint16_t last_recorded_sample = 0;

static bool playback_in_progress = false;
static void *playback_buffer = NULL;
static uint32_t played_sample_counter = 0;
static uint16_t last_played_sample = 0;

/* RT mode */

static uint32_t *captured_samples;
static uint32_t *playback_samples;

struct sun50i_i2s {
	struct device	*dev;
	unsigned int	irq; /* Non-RT */
	rtdm_irq_t	rtdm_irq_handle; /* RT */
	struct clk	*bus_clk;
	struct clk	*mod_clk;
	struct reset_control *rstc;
	struct regmap	*regmap;
	void __iomem	*base;
};

struct sun50i_i2s_clk_div {
	u8	div;
	u8	val;
};

static const struct sun50i_i2s_clk_div sun50i_i2s_bclk_div[] = {
	{ .div = 1, .val = 1 },
	{ .div = 2, .val = 2 },
	{ .div = 4, .val = 3 },
	{ .div = 6, .val = 4 },
	{ .div = 8, .val = 5 },
	{ .div = 12, .val = 6 },
	{ .div = 16, .val = 7 },
	{ .div = 24, .val = 8 },
	{ .div = 32, .val = 9 },
	{ .div = 48, .val = 10 },
	{ .div = 64, .val = 11 },
	{ .div = 96, .val = 12 },
	{ .div = 128, .val = 13 },
	{ .div = 176, .val = 14 },
	{ .div = 192, .val = 15 },
};

static const struct sun50i_i2s_clk_div sun50i_i2s_mclk_div[] = {
	{ .div = 1, .val = 1 },
	{ .div = 2, .val = 2 },
	{ .div = 4, .val = 3 },
	{ .div = 6, .val = 4 },
	{ .div = 8, .val = 5 },
	{ .div = 12, .val = 6 },
	{ .div = 16, .val = 7 },
	{ .div = 24, .val = 8 },
	{ .div = 32, .val = 9 },
	{ .div = 48, .val = 10 },
	{ .div = 64, .val = 11 },
	{ .div = 96, .val = 12 },
	{ .div = 128, .val = 13 },
	{ .div = 176, .val = 14 },
	{ .div = 192, .val = 15 },
};

static uint32_t i2s_read(const struct sun50i_i2s *i2s, unsigned reg) {
	return ioread32(i2s->base + reg);
}

static void i2s_write(const struct sun50i_i2s *i2s, unsigned reg, uint32_t value) {
	iowrite32(value, i2s->base + reg);
}

static int sun50i_i2s_get_bclk_div(struct sun50i_i2s *i2s,
				  unsigned int module_rate,
				  unsigned int sampling_rate,
				  unsigned int word_size) {
	int div = module_rate / sampling_rate / word_size / 2;
	int i;

	for (i = 0; i < ARRAY_SIZE(sun50i_i2s_bclk_div); i++) {
		const struct sun50i_i2s_clk_div *bdiv = &sun50i_i2s_bclk_div[i];

		if (bdiv->div == div)
			return bdiv->val;
	}

	return -EINVAL;
}

static int sun50i_i2s_get_mclk_div(struct sun50i_i2s *i2s,
				  unsigned int oversample_rate,
				  unsigned int module_rate,
				  unsigned int sampling_rate) {
	int div = module_rate / sampling_rate / oversample_rate;
	int i;

	for (i = 0; i < ARRAY_SIZE(sun50i_i2s_mclk_div); i++) {
		const struct sun50i_i2s_clk_div *mdiv = &sun50i_i2s_mclk_div[i];

		if (mdiv->div == div)
			return mdiv->val;
	}

	return -EINVAL;
}

static int sun50i_i2s_oversample_rates[] = { 128, 192, 256, 384, 512, 768 };

static int sun50i_i2s_set_clk_rate(struct sun50i_i2s *i2s,
				  unsigned int rate,
				  unsigned int word_size) {
	unsigned int clk_rate;
	int bclk_div, mclk_div;
	int ret, i;

	switch (rate) {
	case 176400:
	case 88200:
	case 44100:
	case 22050:
	case 11025:
		clk_rate = 22579200;
		break;

	case 192000:
	case 128000:
	case 96000:
	case 64000:
	case 48000:
	case 32000:
	case 24000:
	case 16000:
	case 12000:
	case 8000:
		clk_rate = 24576000;
		break;

	default:
		return -EINVAL;
	}

	ret = clk_set_rate(i2s->mod_clk, clk_rate);
	if (ret)
		return ret;

	/* Always favor the highest oversampling rate */
	for (i = (ARRAY_SIZE(sun50i_i2s_oversample_rates) - 1); i >= 0; i--) {
		unsigned int oversample_rate = sun50i_i2s_oversample_rates[i];

		bclk_div = sun50i_i2s_get_bclk_div(i2s, clk_rate, rate,
						  word_size);
		mclk_div = sun50i_i2s_get_mclk_div(i2s, oversample_rate,
						  clk_rate,
						  rate);

		if ((bclk_div >= 0) && (mclk_div >= 0))
			break;
	}

	if ((bclk_div < 0) || (mclk_div < 0))
		return -EINVAL;

	regmap_write(i2s->regmap, SUN50I_I2S_CLK_DIV_REG,
		     SUN50I_I2S_CLK_DIV_BCLK(bclk_div) |
		     SUN50I_I2S_CLK_DIV_MCLK(mclk_div) |
		     SUN50I_I2S_CLK_DIV_MCLK_EN);

	return 0;
}

static int sun50i_i2s_hw_params(struct sun50i_i2s *i2s,
				int width, int physical_width, unsigned int rate) {
	int lrck, sr, sw;

	switch (physical_width) {
	case 16:
		sw = 3;
		lrck = 16;
		break;
	case 24:
	case 32:
		sw = 7;
		lrck = 32;
		break;
	default:
		return -EINVAL;
	}

	regmap_update_bits(i2s->regmap, SUN50I_I2S_FMT0_REG,
					SUN50I_I2S_FMT0_SW_MASK,
					SUN50I_I2S_FMT0_SW(sw));

	regmap_update_bits(i2s->regmap, SUN50I_I2S_FMT0_REG,
					SUN50I_I2S_FMT0_LRCK_PERIOD_MASK,
					SUN50I_I2S_FMT0_LRCK_PERIOD(lrck));

	switch (width) {
	case 16:
		sr = 3;
		break;
	case 24:
		sr = 5;
		break;
	case 32:
		sr = 7;
		break;
	default:
		return -EINVAL;
	}

	regmap_update_bits(i2s->regmap, SUN50I_I2S_FMT0_REG,
					SUN50I_I2S_FMT0_SR_MASK,
					SUN50I_I2S_FMT0_SR(sr));

	regmap_update_bits(i2s->regmap, SUN50I_I2S_FMT0_REG,
					SUN50I_I2S_FMT0_EDGE_TRANSFER_MASK,
					SUN50I_I2S_FMT0_SAME_EDGE);

	return sun50i_i2s_set_clk_rate(i2s, rate, physical_width);
}

static int sun50i_i2s_set_fmt(struct sun50i_i2s *i2s, unsigned int fmt) {
	u32 val, offset;
	bool format_i2s = false;

	/* DAI Mode */
	switch (fmt & SND_SOC_DAIFMT_FORMAT_MASK) {
	case SND_SOC_DAIFMT_I2S:
		val = SUN50I_I2S_CTRL_MODE_LEFT_JUSTIFIED;
		format_i2s = true;
		offset = 1;
		break;
	case SND_SOC_DAIFMT_LEFT_J:
		val = SUN50I_I2S_CTRL_MODE_LEFT_JUSTIFIED;
		offset = 0;
		break;
	case SND_SOC_DAIFMT_RIGHT_J:
		val = SUN50I_I2S_CTRL_MODE_RIGHT_JUSTIFIED;
		offset = 0;
		break;
	default:
		return -EINVAL;
	}

	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_MODE_SEL_MASK,
			   val);
	regmap_update_bits(i2s->regmap, SUN50I_I2S_TX_CHAN_SEL_REG(0),
			   SUN50I_I2S_TX_CHAN_OFFSET_MASK,
			   SUN50I_I2S_TX_CHAN_OFFSET(offset));
	regmap_update_bits(i2s->regmap, SUN50I_I2S_RX_CHAN_SEL_REG,
			   SUN50I_I2S_RX_CHAN_OFFSET_MASK,
			   SUN50I_I2S_RX_CHAN_OFFSET(offset));

	/* BLCK clock polarity */
	switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
	case SND_SOC_DAIFMT_NB_IF:
		/* Normal bit clock */
		val = SUN50I_I2S_FMT0_BCLK_POLARITY_NORMAL;
		break;
	case SND_SOC_DAIFMT_IB_NF:
	case SND_SOC_DAIFMT_IB_IF:
		/* Invert bit clock */
		val = SUN50I_I2S_FMT0_BCLK_POLARITY_INVERTED;
		break;
	default:
		return -EINVAL;
	}

	regmap_update_bits(i2s->regmap, SUN50I_I2S_FMT0_REG,
			   SUN50I_I2S_FMT0_BCLK_POLARITY_MASK,
			   val);

	/* LRCLK polarity */
	switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
	case SND_SOC_DAIFMT_IB_NF:
		/* Invert bit clock */
		val = (format_i2s ?
				SUN50I_I2S_FMT0_LRCLK_POLARITY_INVERTED :
				SUN50I_I2S_FMT0_LRCLK_POLARITY_NORMAL);
		break;
	case SND_SOC_DAIFMT_NB_IF:
	case SND_SOC_DAIFMT_IB_IF:
		/* Invert both clocks */
		val = (format_i2s ?
				SUN50I_I2S_FMT0_LRCLK_POLARITY_NORMAL :
				SUN50I_I2S_FMT0_LRCLK_POLARITY_INVERTED);
		break;
	default:
		return -EINVAL;
	}

	regmap_update_bits(i2s->regmap, SUN50I_I2S_FMT0_REG,
			   SUN50I_I2S_FMT0_LRCLK_POLARITY_MASK,
			   val);

	/* DAI clock master masks */
	switch (fmt & SND_SOC_DAIFMT_MASTER_MASK) {
	case SND_SOC_DAIFMT_CBS_CFS:
		/* BCLK and LRCLK master */
		regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
				   SUN50I_I2S_CTRL_BCLK_OUT,
				   SUN50I_I2S_CTRL_BCLK_OUT);
		regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
				   SUN50I_I2S_CTRL_LRCK_OUT,
				   SUN50I_I2S_CTRL_LRCK_OUT);
		break;
	case SND_SOC_DAIFMT_CBM_CFM:
		/* BCLK and LRCLK slave */
		regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
				   SUN50I_I2S_CTRL_BCLK_OUT, 0);
		regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
				   SUN50I_I2S_CTRL_LRCK_OUT, 0);
		break;
	default:
		return -EINVAL;
	}

	/* Set significant bits in our FIFOs */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_FIFO_CTRL_REG,
			   SUN50I_I2S_FIFO_CTRL_TX_MODE_MASK |
			   SUN50I_I2S_FIFO_CTRL_RX_MODE_MASK,
			   SUN50I_I2S_FIFO_CTRL_TX_MODE(1) |
			   SUN50I_I2S_FIFO_CTRL_RX_MODE(1));

	return 0;
}

static void sun50i_i2s_start_playback(struct sun50i_i2s *i2s) {
	/* Flush TX FIFO */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_FIFO_CTRL_REG,
			   SUN50I_I2S_FIFO_CTRL_FLUSH_TX,
			   SUN50I_I2S_FIFO_CTRL_FLUSH_TX);

	/* Set TX FIFO trigger level */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_FIFO_CTRL_REG,
			   SUN50I_I2S_FIFO_CTRL_TXTL_MASK,
			   SUN50I_I2S_FIFO_CTRL_TXTL(0x20));

	/* Clear TX counter */
	regmap_write(i2s->regmap, SUN50I_I2S_TX_CNT_REG, 0);

	/* Enable TX Block */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_TX_EN,
			   SUN50I_I2S_CTRL_TX_EN);
}


static void sun50i_i2s_stop_playback(struct sun50i_i2s *i2s) {
	/* Disable TX Block */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_TX_EN,
			   0);
}

static void sun50i_i2s_start_capture(struct sun50i_i2s *i2s) {
	/* Flush RX FIFO */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_FIFO_CTRL_REG,
			   SUN50I_I2S_FIFO_CTRL_FLUSH_RX,
			   SUN50I_I2S_FIFO_CTRL_FLUSH_RX);

	/* Set RX FIFO trigger level */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_FIFO_CTRL_REG,
			   SUN50I_I2S_FIFO_CTRL_RXTL_MASK,
			   SUN50I_I2S_FIFO_CTRL_RXTL(SUN50I_RXFIFO_TH));

	/* Clear RX counter */
	regmap_write(i2s->regmap, SUN50I_I2S_RX_CNT_REG, 0);

	/* Enable RX IRQ */
	regmap_write(i2s->regmap, SUN50I_I2S_INT_STA_REG, 0x07);
	regmap_update_bits(i2s->regmap, SUN50I_I2S_DMA_INT_CTRL_REG,
			   SUN50I_I2S_DMA_INT_CTRL_RXAI_EN,
			   SUN50I_I2S_DMA_INT_CTRL_RXAI_EN);

	/* Enable RX Block */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_RX_EN,
			   SUN50I_I2S_CTRL_RX_EN);
}

static void sun50i_i2s_stop_capture(struct sun50i_i2s *i2s) {
	/* Disable RX Block */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_RX_EN,
			   0);

	regmap_update_bits(i2s->regmap, SUN50I_I2S_DMA_INT_CTRL_REG,
			   SUN50I_I2S_DMA_INT_CTRL_RXAI_EN,
			   0);
}

static int sun50i_i2s_trigger(struct sun50i_i2s *i2s, int stream, int cmd) {
	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
	case SNDRV_PCM_TRIGGER_RESUME:
		if (stream == SNDRV_PCM_STREAM_PLAYBACK)
			sun50i_i2s_start_playback(i2s);
		else if (stream == SNDRV_PCM_STREAM_CAPTURE)
			sun50i_i2s_start_capture(i2s);
		else
			return -EINVAL;
		break;

	case SNDRV_PCM_TRIGGER_STOP:
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
	case SNDRV_PCM_TRIGGER_SUSPEND:
		if (stream == SNDRV_PCM_STREAM_PLAYBACK)
			sun50i_i2s_stop_playback(i2s);
		else if (stream == SNDRV_PCM_STREAM_CAPTURE)
			sun50i_i2s_stop_capture(i2s);
		else
			return -EINVAL;
		break;

	default:
		return -EINVAL;
	}

	return 0;
}

static int sun50i_i2s_startup(struct sun50i_i2s *i2s) {
	/* Enable the whole hardware block */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
					SUN50I_I2S_CTRL_GL_EN,
					SUN50I_I2S_CTRL_GL_EN);

	/* Enable the first output line */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_SDO_EN_MASK,
			   SUN50I_I2S_CTRL_SDO_EN);

	/* Enable the first two channels (Tx) */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CHCFG_REG,
					SUN50I_I2S_CHCFG_TX_SLOT_NUM_MASK,
					SUN50I_I2S_CHCFG_TX_SLOT_NUM(2));
	regmap_update_bits(i2s->regmap, SUN50I_I2S_TX_CHAN_SEL_REG(0),
			   SUN50I_I2S_TX_CHAN_SEL_MASK,
			   SUN50I_I2S_TX_CHAN_SEL(2));
	regmap_update_bits(i2s->regmap, SUN50I_I2S_TX_CHAN_SEL_REG(0),
			   SUN50I_I2S_TX_CHAN_EN_MASK,
			   SUN50I_I2S_TX_CHAN_EN(0) | SUN50I_I2S_TX_CHAN_EN(1));

	/* Map them to the two first samples coming in (Tx) */
	regmap_write(i2s->regmap, SUN50I_I2S_TX_CHAN_MAP_REG(0),
		     SUN50I_I2S_TX_CHAN_MAP(0, 0) | SUN50I_I2S_TX_CHAN_MAP(1, 1));

	/* Enable the first two channels (Rx) */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CHCFG_REG,
					SUN50I_I2S_CHCFG_RX_SLOT_NUM_MASK,
					SUN50I_I2S_CHCFG_RX_SLOT_NUM(2));
	regmap_update_bits(i2s->regmap, SUN50I_I2S_RX_CHAN_SEL_REG,
			   SUN50I_I2S_RX_CHAN_SEL_MASK,
			   SUN50I_I2S_RX_CHAN_SEL(2));

	/* Map them to the two first samples coming in (Rx) */
	regmap_write(i2s->regmap, SUN50I_I2S_RX_CHAN_MAP_REG,
		     SUN50I_I2S_RX_CHAN_MAP(0, 0) | SUN50I_I2S_RX_CHAN_MAP(1, 1));

	return clk_prepare_enable(i2s->mod_clk);
}

static void sun50i_i2s_shutdown(struct sun50i_i2s *i2s)
{
	clk_disable_unprepare(i2s->mod_clk);

	/* Disable our output lines */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
			   SUN50I_I2S_CTRL_SDO_EN_MASK, 0);

	/* Disable the whole hardware block */
	regmap_update_bits(i2s->regmap, SUN50I_I2S_CTRL_REG,
					SUN50I_I2S_CTRL_GL_EN,
					0);
}

/**
 * ISR function in non-RT mode.
 * There are three modes:
 * - Loopback mode
 *   The captured samples are simply copied into the output.
 * - Recording mode
 *   The captured samples are used to emulate a stereo signal from a mono signal where the left channel is copied
 *   into the right one. This divides the size of the recorded audio data buffer by a factor of 2.
 * - Playback mode
 *   Each sample extracted from the recorded audio data buffer is used twice to emulate a stereo signal.
 * "Stereo" does not mean "stereophony" but the fact that the sound is reproduced on both channels.
 */
static int sun50i_i2s_process(struct sun50i_i2s *i2s) {
	unsigned int sta_val, recorded_val, play_val, available, empty;
	int i;

	sta_val = i2s_read(i2s, SUN50I_I2S_FIFO_STA_REG);

	available = (sta_val & SUN50I_I2S_FIFO_STA_RXA_CNT_MASK) >> SUN50I_I2S_FIFO_STA_RXA_CNT_OFFSET;
	empty = (sta_val & SUN50I_I2S_FIFO_STA_TXE_CNT_MASK) >> SUN50I_I2S_FIFO_STA_TXE_CNT_OFFSET;

	if (available > empty)
		dev_warn(i2s->dev, "synchro problem\n");

	for (i = 0; i < available; i++) {
		recorded_val = i2s_read(i2s, SUN50I_I2S_FIFO_RX_REG);

		if (unlikely(loopback_in_progress)) {
			i2s_write(i2s, SUN50I_I2S_FIFO_TX_REG, recorded_val);
			continue;
		}

		if (playback_in_progress) {
			/* "* 2" means that each sample is played on both left and right channels, that is, each sample is used twice */
			if (played_sample_counter < SUN50I_N_SAMPLES * 2) {
				/* Each element in the audio buffer is played twice: on the left channel, then on the right channel */

				if (played_sample_counter % 2 == 0)
					last_played_sample = ((uint16_t *) playback_buffer)[played_sample_counter / 2];

				play_val = (unsigned int) last_played_sample;

				i2s_write(i2s, SUN50I_I2S_FIFO_TX_REG, play_val);

				played_sample_counter++;
			}
			else {
				DBG0("Playback OK\n");

				playback_in_progress = false;
				played_sample_counter = 0;

#if defined(CONFIG_VAUDIO_BACKEND)
				vaudio_set_playback_status(VAUDIO_FINISHED);
#endif /* CONFIG_VAUDIO_BACKEND */
			}
		}

		if (recording_in_progress) {
			/* "* 2" means that each sample is played on both left and right channels, that is, each sample is used twice */
			if (recorded_sample_counter < SUN50I_N_SAMPLES * 2) {
				/* Only samples with an even index are inserted into the audio buffer */

				if (recorded_sample_counter % 2 == 0) {
					last_recorded_sample = (uint16_t) recorded_val;
					memcpy(&((uint16_t *) recording_buffer)[recorded_sample_counter / 2], &last_recorded_sample, sizeof(uint16_t));
				}

				recorded_sample_counter++;
			}
			else {
				DBG0("Recording OK\n");

				recording_in_progress = false;
				recorded_sample_counter = 0;

#if defined(CONFIG_VAUDIO_BACKEND)
				vaudio_set_recording_status(VAUDIO_FINISHED);
#endif /* CONFIG_VAUDIO_BACKEND */
			}
		}
	}

	return 0;
}

/**
 * ISR function in RT mode.
 * An interrupt is triggered every SUN50I_RXFIFO_TH samples. As the acquisition is performed in
 * stereophonic mode, we need to keep only one sample over two to obtain a monophonic stream.
 * Two interrupts are necessary to collect SUN50I_RXFIFO_TH samples in monophonic mode.
 * By the capture path's side:
 * - During the first interrupt, an accumulator counter is initialized to 0. On sample over two is
 *   copied into the capture buffer. The accumulator counter is updated with the number of collected
 *   samples.
 * - During the second interrupt, the samples are written into the second part of the capture buffer.
 * By the playback path's side, the operations are similar. The playback buffer is also processed in
 * two steps: first part during the first interrupt, second part during the second interrupt.
 * In stereophonic mode, an audio interrupt occurs every ~10.4us and the audio period is ~520us.
 * In monophonic mode, we forward the interrupt to the vAudio interface every ~1040ms.
 */
static int rtdm_sun50i_i2s_process(struct sun50i_i2s *i2s) {
	unsigned int sta_val, recorded_val, available, empty;
	static unsigned int last_available = 0;
	int i;
	/* Sample counter accumulation among interrupts */
	static uint32_t available_buf_samples = 0;

	sta_val = i2s_read(i2s, SUN50I_I2S_FIFO_STA_REG);

	available = (sta_val & SUN50I_I2S_FIFO_STA_RXA_CNT_MASK) >> SUN50I_I2S_FIFO_STA_RXA_CNT_OFFSET;
	empty = (sta_val & SUN50I_I2S_FIFO_STA_TXE_CNT_MASK) >> SUN50I_I2S_FIFO_STA_TXE_CNT_OFFSET;

	if (available > empty)
		dev_warn(i2s->dev, "Sync problem\n");

	/* Retrieve the playback data from vAudio */
	if (available_buf_samples) {
		/* Second part */
		vaudio_play(&playback_samples[available_buf_samples], available / 2, available_buf_samples);
	} else {
		/* First part */
		vaudio_play(playback_samples, available / 2, 0);
	}

#if 0
	/*
	 * As the number of available samples can vary from one interrupt to another, a playback FIFO underrun
	 * can occur, that is, the playback FIFO requests a greater number of samples than the number of captured
	 * ones at the previous interrupt. This can occur when a first interrupt processes n1 samples, then a second
	 * interrupt processes n2 samples, with n1 < n2. In this case, the last playback samples are simply cloned to
	 * avoid glitches.
	 *
	 * In the case of an overrun, that is, the playback FIFO requests a smaller number of samples than the
	 * number of captured ones at the previous interrupt, the last samples are ignored.
	 */
	if (unlikely(last_available == 0))
		last_available = available;
	if (available > last_available) {
		/* Playback FIFO underrun */
		for (i = last_available; i < available; i++)
			playback_samples[i] = playback_samples[i - 2];
	}
	last_available = available;
#endif

	for (i = 0; i < available; i++) {
		recorded_val = i2s_read(i2s, SUN50I_I2S_FIFO_RX_REG);
		/* Keep only one sample over two */
		if (i % 2 == 0)
			captured_samples[available_buf_samples + i / 2] = recorded_val;

		i2s_write(i2s, SUN50I_I2S_FIFO_TX_REG, playback_samples[available_buf_samples + i]);
	}

	/* Forward the captured data to vAudio */
	if (available_buf_samples) {
		/* Second part */
		vaudio_capture(&captured_samples[available_buf_samples], available / 2, available_buf_samples);

		/* Forward the audio interrupt to the vAudio interface */
		vaudio_hw_interrupt();

		/* Reset the accumulation counter */
		available_buf_samples = 0;
	} else {
		/* First part */
		vaudio_capture(captured_samples, available / 2, 0);

		/* Save the number of processed samples */
		available_buf_samples = available / 2;
	}

	return 0;
}

/**
 * ISR in non-RT mode.
 */
static irqreturn_t sun50i_i2s_isr(int irq, void *dev_id) {
	struct sun50i_i2s *i2s = dev_id;
	unsigned int status;

	BUG_ON(rt_enabled);

	status = i2s_read(i2s, SUN50I_I2S_INT_STA_REG);

	if (status & SUN50I_I2S_INT_STA_RXA_INT) {
		sun50i_i2s_process(i2s);
		i2s_write(i2s, SUN50I_I2S_INT_STA_REG, SUN50I_I2S_INT_STA_RXA_INT);
		return IRQ_HANDLED;
	}

	return IRQ_NONE;
}

/**
 * ISR in RT mode.
 */
static int rtdm_sun50i_i2s_isr(rtdm_irq_t *irq_context) {
	struct sun50i_i2s *i2s = rtdm_irq_get_arg(irq_context, struct sun50i_i2s);
	unsigned int status;

	BUG_ON(!rt_enabled);

	if (unlikely(!rt_stream_enabled))
		return RTDM_IRQ_NONE;

	status = i2s_read(i2s, SUN50I_I2S_INT_STA_REG);

	if (status & SUN50I_I2S_INT_STA_RXA_INT) {
		rtdm_sun50i_i2s_process(i2s);
		i2s_write(i2s, SUN50I_I2S_INT_STA_REG, SUN50I_I2S_INT_STA_RXA_INT);
		return RTDM_IRQ_HANDLED;
	}

	return RTDM_IRQ_NONE;
}

static int start_soond(struct sun50i_i2s *i2s) {
	int ret;

	/* Config dev */
	if ((ret = sun50i_i2s_set_fmt(i2s, SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS)))
		return ret;

	if ((ret = sun50i_i2s_startup(i2s)))
		return ret; /* TODO: fix error path */

	if (rt_enabled)
		ret = sun50i_i2s_hw_params(i2s, SUN50I_RT_WIDTH, SUN50I_RT_PHYSICAL_WIDTH, SUN50I_RT_SAMPLING_FREQUENCY);
	else
		ret = sun50i_i2s_hw_params(i2s, SUN50I_NRT_WIDTH, SUN50I_NRT_PHYSICAL_WIDTH, SUN50I_NRT_SAMPLING_FREQUENCY);
	if (ret)
		return ret; /* TODO: fix error path */

	/* Start dev */
	if ((ret = sun50i_i2s_trigger(i2s, SNDRV_PCM_STREAM_CAPTURE, SNDRV_PCM_TRIGGER_START)))
		return ret; /* TODO: fix error path */

	if ((ret = sun50i_i2s_trigger(i2s, SNDRV_PCM_STREAM_PLAYBACK, SNDRV_PCM_TRIGGER_START)))
		return ret; /* TODO: fix error path */

	return 0;
}

static void stop_soond(struct sun50i_i2s *i2s){
	sun50i_i2s_trigger(i2s, SNDRV_PCM_STREAM_PLAYBACK, SNDRV_PCM_TRIGGER_STOP);
	sun50i_i2s_trigger(i2s, SNDRV_PCM_STREAM_CAPTURE, SNDRV_PCM_TRIGGER_STOP);

	sun50i_i2s_shutdown(i2s);
}

/* Interface with the vAudio backend */

void vaudio_start_recording(void *dest) {
	DBG0("Start recording\n");

#if defined(CONFIG_VAUDIO_BACKEND)
	vaudio_set_recording_status(VAUDIO_IN_PROGRESS);
#endif /* CONFIG_VAUDIO_BACKEND */

	recording_buffer = dest;
	recorded_sample_counter = 0;
	recording_in_progress = true;
}

void vaudio_start_playback(void *dest) {
	DBG0("Start playback\n");

#if defined(CONFIG_VAUDIO_BACKEND)
	vaudio_set_playback_status(VAUDIO_IN_PROGRESS);
#endif /* CONFIG_VAUDIO_BACKEND */

	playback_buffer = dest;
	played_sample_counter = 0;
	playback_in_progress = true;
}

/* Regmap stuff */

static bool sun50i_i2s_rd_reg(struct device *dev, unsigned int reg) {
	switch (reg) {
	case SUN50I_I2S_FIFO_TX_REG:
		return false;

	default:
		return true;
	}
}

static bool sun50i_i2s_wr_reg(struct device *dev, unsigned int reg) {
	switch (reg) {
	case SUN50I_I2S_FIFO_RX_REG:
	case SUN50I_I2S_FIFO_STA_REG:
		return false;

	default:
		return true;
	}
}

static bool sun50i_i2s_volatile_reg(struct device *dev, unsigned int reg) {
	switch (reg) {
	case SUN50I_I2S_FIFO_STA_REG:
	case SUN50I_I2S_FIFO_RX_REG:
	case SUN50I_I2S_INT_STA_REG:
	case SUN50I_I2S_RX_CNT_REG:
	case SUN50I_I2S_TX_CNT_REG:
		return true;

	default:
		return false;
	}
}

static const struct reg_default sun50i_i2s_reg_defaults[] = {
	{ SUN50I_I2S_CTRL_REG, 0x00060000 },
	{ SUN50I_I2S_FMT0_REG, 0x00000033 },
	{ SUN50I_I2S_FMT1_REG, 0x00000030 },
	{ SUN50I_I2S_FIFO_CTRL_REG, 0x000400f0 },
	{ SUN50I_I2S_DMA_INT_CTRL_REG, 0x00000000 },
	{ SUN50I_I2S_CLK_DIV_REG, 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_SEL_REG(0), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_SEL_REG(1), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_SEL_REG(2), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_SEL_REG(3), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_MAP_REG(0), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_MAP_REG(1), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_MAP_REG(2), 0x00000000 },
	{ SUN50I_I2S_TX_CHAN_MAP_REG(3), 0x00000000 },
	{ SUN50I_I2S_RX_CHAN_SEL_REG, 0x00000000 },
	{ SUN50I_I2S_RX_CHAN_MAP_REG, 0x00000000 },
};

static const struct regmap_config sun50i_i2s_regmap_config = {
	.reg_bits	= 32,
	.reg_stride	= 4,
	.val_bits	= 32,
	.max_register	= SUN50I_I2S_RX_CHAN_MAP_REG,

	.cache_type	= REGCACHE_FLAT,
	.reg_defaults	= sun50i_i2s_reg_defaults,
	.num_reg_defaults = ARRAY_SIZE(sun50i_i2s_reg_defaults),
	.writeable_reg	= sun50i_i2s_wr_reg,
	.readable_reg	= sun50i_i2s_rd_reg,
	.volatile_reg	= sun50i_i2s_volatile_reg,
};

static void rtdm_sun50i_i2s_rebind_irq(struct device *dev) {
	struct sun50i_i2s *i2s = dev_get_drvdata(dev);
	int ret;

	DBG("Registering ISR for IRQ %d\n", SOO_IRQ_SUN50I_RT_I2S);

	/* We must use the physical IRQ number here */
	ret = rtdm_irq_request(&i2s->rtdm_irq_handle, SOO_IRQ_SUN50I_RT_I2S, rtdm_sun50i_i2s_isr, IRQF_NO_SUSPEND, "rtdm_sun50i_i2s", i2s);
	if (ret != 0)
		BUG();
}

void rtdm_reconfigure_audio_hw(void) {
	BUG_ON(smp_processor_id() != AGENCY_RT_CPU);

	stop_soond(sun50i_i2s);

	rtdm_sun50i_i2s_rebind_irq(sun50i_i2s->dev);

	rt_enabled = true;
	start_soond(sun50i_i2s);

	DBG0("RT audio enabled\n");
}

void vaudio_start_stream(void) {
	DBG0("sun50i-i2s: Start RT stream\n");

	rt_stream_enabled = true;
}

void vaudio_stop_stream(void) {
	DBG0("sun50i-i2s: Stop RT stream\n");

	rt_stream_enabled = false;
}

static int sun50i_i2s_runtime_resume(struct device *dev) {
	struct sun50i_i2s *i2s = dev_get_drvdata(dev);
	int ret;

	ret = clk_prepare_enable(i2s->bus_clk);
	if (ret) {
		dev_err(dev, "Failed to enable bus clock\n");
		return ret;
	}

	ret = reset_control_deassert(i2s->rstc);
	if (ret) {
		dev_err(dev, "Couldn't deassert the device from reset\n");
		goto err_disable_clk;
	}

	regcache_cache_only(i2s->regmap, false);
	regcache_mark_dirty(i2s->regmap);

	ret = regcache_sync(i2s->regmap);
	if (ret) {
		dev_err(dev, "Failed to sync regmap cache\n");
		goto err_disable_clk;
	}

	return 0;

err_disable_clk:
	clk_disable_unprepare(i2s->bus_clk);
	return ret;
}

static int sun50i_i2s_runtime_suspend(struct device *dev) {
	struct sun50i_i2s *i2s = dev_get_drvdata(dev);

	regcache_cache_only(i2s->regmap, true);

	clk_disable_unprepare(i2s->bus_clk);

	return 0;
}

static int sun50i_i2s_probe(struct platform_device *pdev) {
	struct device_node *np = pdev->dev.of_node;
	struct resource *res;
	void __iomem *regs;
	uint32_t mclk_rate;
	int irq, ret;

	sun50i_i2s = devm_kzalloc(&pdev->dev, sizeof(struct sun50i_i2s), GFP_KERNEL);
	if (!sun50i_i2s)
		return -ENOMEM;
	platform_set_drvdata(pdev, sun50i_i2s);
	sun50i_i2s->dev = &pdev->dev;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	regs = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(regs))
		return PTR_ERR(regs);

	sun50i_i2s->base = regs;

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		dev_err(&pdev->dev, "Can't retrieve our interrupt\n");
		return irq;
	}
	sun50i_i2s->irq = irq;
	/*
	 * The IRQ obtained here is a virq returned by irq_create_fwspec_mapping. This IRQ cannot
	 * be used when rebinding on CPU #1.
	 */

	sun50i_i2s->bus_clk = devm_clk_get(&pdev->dev, "apb");
	if (IS_ERR(sun50i_i2s->bus_clk)) {
		dev_err(&pdev->dev, "Can't get our bus clock\n");
		return PTR_ERR(sun50i_i2s->bus_clk);
	}

	sun50i_i2s->regmap = devm_regmap_init_mmio(&pdev->dev, regs,
					    &sun50i_i2s_regmap_config);
	if (IS_ERR(sun50i_i2s->regmap)) {
		dev_err(&pdev->dev, "Regmap initialisation failed\n");
		return PTR_ERR(sun50i_i2s->regmap);
	}

	sun50i_i2s->mod_clk = devm_clk_get(&pdev->dev, "mod");
	if (IS_ERR(sun50i_i2s->mod_clk)) {
		dev_err(&pdev->dev, "Can't get our mod clock\n");
		return PTR_ERR(sun50i_i2s->mod_clk);
	}

	sun50i_i2s->rstc = devm_reset_control_get(&pdev->dev, NULL);
	if (IS_ERR(sun50i_i2s->rstc)) {
		dev_err(&pdev->dev, "Couldn't get reset controller\n");
		return PTR_ERR(sun50i_i2s->rstc);
	}
	reset_control_deassert(sun50i_i2s->rstc);

	ret = sun50i_i2s_runtime_resume(&pdev->dev);
	if (ret) {
		dev_err(&pdev->dev, "Couldn't resume the device\n");
		goto err_pm_disable;
	}

	/* HACK to force i2s0 to output MCLK if we are using i2s1 */
	ret = of_property_read_u32(np, "hack,force-mclk", &mclk_rate);
	if (ret == 0) {
		/* Enable the whole hardware block */
		regmap_update_bits(sun50i_i2s->regmap, SUN50I_I2S_CTRL_REG,
						SUN50I_I2S_CTRL_GL_EN,
						SUN50I_I2S_CTRL_GL_EN);
		/* Force MCLK to the defined rate */
		clk_prepare_enable(sun50i_i2s->mod_clk);
		clk_set_rate(sun50i_i2s->mod_clk, 24576000);

		ret = sun50i_i2s_get_mclk_div(sun50i_i2s, 1, 24576000, mclk_rate);
		pr_info("sun50i_i2s: HACK: setting mclk divider to %d\n", ret);
		if (ret > 0)
			regmap_write(sun50i_i2s->regmap, SUN50I_I2S_CLK_DIV_REG,
						  SUN50I_I2S_CLK_DIV_MCLK(ret) | SUN50I_I2S_CLK_DIV_MCLK_EN);

		/* do not initialize further */
		return 0;
	}

	/*
	 * By default, we are in non-RT mode. We request an IRQ without RT constraints.
	 * Enabling the RT mode will rebind the IRQ in a RT compliant way.
	 */
	ret = devm_request_irq(&pdev->dev, sun50i_i2s->irq, sun50i_i2s_isr, IRQF_NO_SUSPEND, "sun50i-i2s", sun50i_i2s);
	if (ret) {
		dev_err(sun50i_i2s->dev, "Unable to request IRQ\n");
		return ret;
	}

	captured_samples = (uint32_t *) kmalloc(SUN50I_MAX_RXFIFO_TH * sizeof(uint32_t), GFP_KERNEL);
	playback_samples = (uint32_t *) kmalloc(SUN50I_MAX_RXFIFO_TH * sizeof(uint32_t), GFP_KERNEL);

	if ((ret = start_soond(sun50i_i2s)))
		goto err_pm_disable;

	dev_info(&pdev->dev, "probed\n");

	return 0;

err_pm_disable:

	return ret;
}

static int sun50i_i2s_remove(struct platform_device *pdev) {
	struct sun50i_i2s *i2s = dev_get_drvdata(&pdev->dev);

	stop_soond(i2s);

	sun50i_i2s_runtime_suspend(&pdev->dev);

	if (rt_enabled)
		rtdm_irq_free(&i2s->rtdm_irq_handle);
	else
		devm_free_irq(&pdev->dev, i2s->irq, i2s);

	return 0;
}

static const struct of_device_id sun50i_i2s_match[] = {
	{ .compatible = "allwinner,sun50i-a64-i2s-light" },
	{  }
};
MODULE_DEVICE_TABLE(of, sun50i_i2s_match);

static struct platform_driver sun50i_i2s_driver = {
	.probe	= sun50i_i2s_probe,
	.remove	= sun50i_i2s_remove,
	.driver	= {
		.name		= "sun50i-i2s-light",
		.of_match_table	= sun50i_i2s_match,
	},
};
module_platform_driver(sun50i_i2s_driver);

MODULE_AUTHOR("Florian Vaussard <florian.vaussard@heig-vd.ch>");
MODULE_DESCRIPTION("Allwinner A64 I2S driver");
MODULE_LICENSE("GPL");
