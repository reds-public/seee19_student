/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 * 
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - January 2018: Baptiste Delporte
 */

#include <soolink/coder.h>
#include <soolink/decoder.h>
#include <soolink/sender.h>
#include <soolink/discovery.h>

/**
 * Initialize the Transcoder functional block of Soolink.
 */
void transcoder_init(void) {
	coder_init();
	decoder_init();
}

/*
 * Initiate a netstream with neighbours.
 */
void transcoder_stream_init(sl_desc_t *sl_desc) {
	/* Ask the transcoder to deactivate the Discovery */
	discovery_disable();

	/* Initiate the netstream. Now or soon, we will become speaker. In the meanwhile, we will
	 * be designated as listener.
	 */
	sender_request_xmit(sl_desc);
}

void transcoder_stream_terminate(sl_desc_t *sl_desc) {
	sender_request_xmit(sl_desc);
}
