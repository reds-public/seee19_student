/*
 *
 * -- Smart Object Oriented  --
 *
 * Copyright (c) 2016-2018 SOOtech SA, Switzerland
 * 
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - July 2017: Daniel Rossier
 * - January 2018: Baptiste Delporte
 * - April 2018: Baptiste Delporte
 *
 */

#if 0
#define DEBUG
#endif

#include <soolink/receiver.h>
#include <soolink/datalink.h>
#include <soolink/decoder.h>
#include <soolink/discovery.h>
#include <soolink/plugin.h>

#include <virtshare/console.h>
#include <virtshare/debug.h>

static rtdm_mutex_t receiver_lock;

/*
 * This function is called when a plugin has data available. Datalink will
 * forward the packet to the selected protocol. Datalink decides when
 * the packet has to be given back to the Receiver.
 * The size parameter refers to the whole transceiver packet.
 */
void receiver_request_rx(sl_desc_t *sl_desc, plugin_desc_t *plugin_desc, void *packet, size_t size) {
	transceiver_packet_t *transceiver_packet;

	if (sl_desc->req_type != SL_REQ_NETSTREAM) {
		transceiver_packet = (transceiver_packet_t *) packet;
		transceiver_packet->size = size;
	}

	datalink_rx(sl_desc, plugin_desc, packet, size);
}

/**
 * This function is called by Datalink when the packet is ready to be
 * forwarded to the consumer(s).
 * The size parameter refers to the whole transceiver packet. It is set to 0 in netstream mode.
 */
void receiver_rx(sl_desc_t *sl_desc, plugin_desc_t *plugin_desc, void *packet, size_t size) {
	transceiver_packet_t *transceiver_packet;
	size_t payload_size;

	rtdm_mutex_lock(&receiver_lock);

	switch (sl_desc->req_type) {
	case SL_REQ_NETSTREAM:
		decoder_stream_rx(sl_desc, packet);
		break;

	case SL_REQ_DISCOVERY:
		transceiver_packet = (transceiver_packet_t *) packet;
		/* Substract the transceiver's packet header size from the total size */
		payload_size = size - sizeof(transceiver_packet_t);
		discovery_rx(plugin_desc, transceiver_packet->payload, payload_size);
		break;

	default:
		transceiver_packet = (transceiver_packet_t *) packet;
		/* Substract the transceiver's packet header size from the total size */
		payload_size = size - sizeof(transceiver_packet_t);
		decoder_rx(sl_desc, transceiver_packet->payload, payload_size);
	}

	rtdm_mutex_unlock(&receiver_lock);
}

void receiver_init(void) {
	rtdm_mutex_init(&receiver_lock);
}
