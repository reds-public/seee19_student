/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 * - Jun. 2016: Baptiste Delporte
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/of.h>

#include <virtshare/gnttab.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>

#include <stdarg.h>
#include <linux/kthread.h>

#include "common.h"

vsp6_t vsp6;

irqreturn_t vsp6_interrupt(int irq, void *dev_id)
{

	/* Nothing to do, but need to exist right after probe. */

	return IRQ_HANDLED;
}

void vsp6_probe(struct vbus_device *dev) {

	DBG(VSP6_PREFIX "Backend probe: %d\n", dev->otherend_id);
}

void vsp6_close(struct vbus_device *dev) {

	DBG(VSP6_PREFIX "Backend close: %d\n", dev->otherend_id);
}

void vsp6_reconfigured(struct vbus_device *dev) {

	DBG(VSP6_PREFIX "Backend reconfigured: %d\n", dev->otherend_id);
}

void vsp6_connected(struct vbus_device *dev) {

	DBG(VSP6_PREFIX "Backend connected: %d\n", dev->otherend_id);
}

int vsp6_init(void) {
	struct device_node *np;

	np = of_find_compatible_node(NULL, NULL, "vsp6,backend");

	/* Check if DTS has vuihandler enabled */
	if (!of_device_is_available(np))
		return 0;


	vsp6_vbus_init();

	return 0;
}

device_initcall(vsp6_init);
