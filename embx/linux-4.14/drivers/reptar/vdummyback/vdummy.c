/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Jan. 2016: Daniel Rossier
 * - Jun. 2016: Baptiste Delporte
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/of.h>

#include <virtshare/gnttab.h>
#include <virtshare/hypervisor.h>
#include <virtshare/vbus.h>
#include <virtshare/console.h>

#include <stdarg.h>
#include <linux/kthread.h>

#include "common.h"

vdummy_t vdummy;

/*
 * The terms of "request" and "response" are used only to determine the origin/destination of the message.
 * By convention, the request is the message sent by the frontend while the response is sent by the backend.
 * There is no predefined protocol related to req/resp behind the scene.
 */

/*
 * Interrupt routine called when a notification from the frontend is received.
 * All processing within this function is performed with IRQs disabled (top half processing).
 */
irqreturn_t vdummy_interrupt(int irq, void *dev_id)
{
	struct vbus_device *dev = (struct vbus_device *) dev_id;
	RING_IDX i, rp;
	vdummy_request_t *ring_req;
	vdummy_response_t *ring_rsp;

	if (!vdummy_is_connected(dev->otherend_id))
		return IRQ_HANDLED;

	/* dev->otherend_id is equivalent to the domain id (hence 1 for domain 1 here). */

	DBG("%d\n", dev->otherend_id);

	rp = vdummy.rings[dev->otherend_id].ring.sring->req_prod;
	dmb();

	/* Providing the frontend sends several requests, we process all available requests
	 * in the shared ring.
	 */
	for (i = vdummy.rings[dev->otherend_id].ring.sring->req_cons; i != rp; i++) {

		ring_req = RING_GET_REQUEST(&vdummy.rings[dev->otherend_id].ring, i);

		/* Prepare to send data to the frontend in reply to the request */
		ring_rsp = RING_GET_RESPONSE(&vdummy.rings[dev->otherend_id].ring, vdummy.rings[dev->otherend_id].ring.rsp_prod_pvt);

		DBG("%s, cons=%d, prod=%d\n", __func__, i, vdummy.rings[dev->otherend_id].ring.rsp_prod_pvt);

		/* As an example, we simply copy the request contents to build a response contents (the same actually) */
		memcpy(ring_rsp->buffer, ring_req->buffer, VDUMMY_PACKET_SIZE);

		/* The following statements are to be used by all transmission to the frontend. */
		dmb();
		vdummy.rings[dev->otherend_id].ring.rsp_prod_pvt++;

		RING_PUSH_RESPONSES(&vdummy.rings[dev->otherend_id].ring);

		/* Finally, notify the frontend that the response is available. */
		notify_remote_via_irq(vdummy.rings[dev->otherend_id].irq);
	}

	/* Do not forget to update the reqest consumer index at the end */
	vdummy.rings[dev->otherend_id].ring.sring->req_cons = i;

	return IRQ_HANDLED;
}

void vdummy_probe(struct vbus_device *dev) {

	DBG(VDUMMY_PREFIX "Backend probe: %d\n", dev->otherend_id);
}

void vdummy_close(struct vbus_device *dev) {

	DBG(VDUMMY_PREFIX "Backend close: %d\n", dev->otherend_id);
}

void vdummy_suspend(struct vbus_device *dev) {

	DBG(VDUMMY_PREFIX "Backend suspend: %d\n", dev->otherend_id);
}

void vdummy_resume(struct vbus_device *dev) {

	DBG(VDUMMY_PREFIX "Backend resume: %d\n", dev->otherend_id);
}

void vdummy_reconfigured(struct vbus_device *dev) {

	DBG(VDUMMY_PREFIX "Backend reconfigured: %d\n", dev->otherend_id);
}

void vdummy_connected(struct vbus_device *dev) {

	DBG(VDUMMY_PREFIX "Backend connected: %d\n", dev->otherend_id);
}

int vdummy_init(void) {
	struct device_node *np;

	np = of_find_compatible_node(NULL, NULL, "vdummy,backend");

	/* Check if DTS has vuihandler enabled */
	if (!of_device_is_available(np))
		return 0;

	vdummy_vbus_init();

	return 0;
}

device_initcall(vdummy_init);
