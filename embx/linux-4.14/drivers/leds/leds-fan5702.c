/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - June 2017: Baptiste Delporte
 *
 * FAN5702 LED controller driver
 *
 */

#if 0
#define DEBUG
#endif

#include <linux/bitops.h>
#include <linux/i2c.h>
#include <linux/kernel.h>
#include <linux/leds.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/kthread.h>
#include <linux/delay.h>

#include <virtshare/debug.h>

#define MAX_BRIGHTNESS_VALUE	255
#define FAN5702_MAX_LEDS	6
#define FAN5702_N_STEPS		64
#define FAN5702_STEP_VALUE	((MAX_BRIGHTNESS_VALUE + 1) / FAN5702_N_STEPS)
#define FAN5702_BLINK_DELAY	1000

struct fan5702_led {
	bool				active;
	unsigned int			led_no;
	struct led_classdev		ldev;
	struct work_struct		work;
	struct fan5702_priv		*priv;
};

struct fan5702_priv {
	struct fan5702_led		leds[FAN5702_MAX_LEDS];
#if 0
	u32 led_iref;
	u32 led_max_current;
#endif
	struct i2c_client		*client;
};

static struct fan5702_priv *led_priv = NULL;

static bool blink_bool[FAN5702_MAX_LEDS];

enum reg_idx {
	FAN5702_REG_CH1A = 0,
	FAN5702_REG_CH2A,
	FAN5702_REG_CH3,
	FAN5702_REG_CH4,
	FAN5702_REG_CH5,
	FAN5702_REG_CH6,
	FAN5702_REG_GENERAL,
	FAN5702_REG_CONFIG,
	FAN5702_N_REG
} ;

static uint8_t reg_addr[FAN5702_N_REG] = {
	[FAN5702_REG_CH1A]		= 0xa0,
	[FAN5702_REG_CH2A]		= 0xa0,
	[FAN5702_REG_CH3]		= 0x30,
	[FAN5702_REG_CH4]		= 0x40,
	[FAN5702_REG_CH5]		= 0x50,
	[FAN5702_REG_CH6]		= 0x60,
	[FAN5702_REG_GENERAL]		= 0x10,
	[FAN5702_REG_CONFIG]		= 0x20,
};

static struct fan5702_led *ldev_to_led(struct led_classdev *ldev) {
	return container_of(ldev, struct fan5702_led, ldev);
}

static struct fan5702_led *work_to_led(struct work_struct *work) {
	return container_of(work, struct fan5702_led, work);
}

static int fan5702_send_cmd(struct fan5702_priv *priv, uint8_t cmd, uint8_t data) {
	int err;
	uint8_t command[] = { cmd, data };

	err = i2c_master_send(priv->client, command, ARRAY_SIZE(command));

	return (err < 0 ? err : 0);
}

static int fan5702_set_pwm_ledno(struct fan5702_priv *priv, unsigned int led_no, uint8_t scaled_brightness) {
	return fan5702_send_cmd(priv, reg_addr[led_no], scaled_brightness & 0x3f);
}

static int fan5702_set_pwm(struct fan5702_led *led, uint8_t scaled_brightness) {
	struct fan5702_priv *priv = led->priv;

	if (unlikely(led->led_no >= FAN5702_MAX_LEDS))
		return -EINVAL;

	return fan5702_set_pwm_ledno(priv, led->led_no, scaled_brightness);
}

static void fan5702_led_work(struct work_struct *work) {
	struct fan5702_led *led = work_to_led(work);
	enum led_brightness brightness = led->ldev.brightness;
	uint8_t scaled_brightness = (brightness == 0) ? 0 : (brightness + FAN5702_STEP_VALUE) / FAN5702_STEP_VALUE - 1;
	int err;

	DBG("Set brightness on led #%d: %d -> %02x\n", led->led_no, brightness, scaled_brightness);
	err = fan5702_set_pwm(led, scaled_brightness);

	if (err < 0)
		dev_err(led->ldev.dev, "failed setting brightness\n");
}

static void fan5702_brightness_set(struct led_classdev *led_cdev, enum led_brightness brightness) {
	struct fan5702_led *led = ldev_to_led(led_cdev);
	led->ldev.brightness = brightness;

	schedule_work(&led->work);
}

void fan5702_set_brightness(unsigned int led_no, uint8_t brightness) {
#if defined(CONFIG_ARCH_VEXPRESS)
	lprintk("%s(...)\n", __func__);

	/* LEDs non avilable on vExpress */
	return ;
#endif /* CONFIG_ARCH_VEXPRESS */

	fan5702_brightness_set(&led_priv->leds[led_no].ldev, brightness);
}

/* Interface with the vleds backend */

void led_ctrl_set_brightness(uint8_t id, uint8_t value) {
	fan5702_set_brightness((uint32_t) id, value);
}

void led_ctrl_blink(uint8_t id, uint8_t value) {
	if (unlikely(id >= FAN5702_MAX_LEDS))
		return;

	blink_bool[id] = (value == 1);
}

static int fan5702_blink_fn(void *arg) {
	uint32_t i;
	static uint8_t value = 0;

	while (1) {
		msleep(FAN5702_BLINK_DELAY);

		for (i = 0 ; i < FAN5702_MAX_LEDS ; i++) {
			if (!blink_bool[i])
				continue;

			led_ctrl_set_brightness(i, value);
		}

		value = (value == 0) ? 255 : 0;
	}

	return 0;
}

static void fan5702_destroy_devices(struct fan5702_priv *priv) {
	struct fan5702_led *led;
	unsigned int i;

	for (i = 0 ; i < FAN5702_MAX_LEDS ; i++) {
		led = &priv->leds[i];
		if (led->active) {
			led_classdev_unregister(&led->ldev);
			cancel_work_sync(&led->work);
		}
	}
}

static int fan5702_configure(struct device *dev, struct fan5702_priv *priv) {
	unsigned int i;
	struct fan5702_led *led;
	int err;

	/* Setup each individual LED */
	for (i = 0; i < FAN5702_MAX_LEDS; i++) {
		led = &priv->leds[i];

		if (!led->active)
			continue;

		led->priv = priv;
		led->led_no = i;
		led->ldev.brightness_set = fan5702_brightness_set;
		led->ldev.max_brightness = MAX_BRIGHTNESS_VALUE;
		INIT_WORK(&led->work, fan5702_led_work);
		err = led_classdev_register(dev, &led->ldev);
		if (err < 0) {
			dev_err(dev, "couldn't register LED %s\n", led->ldev.name);
			goto exit;
		}
	}

	/* Enable group A LEDs and all the LED channels */
	err = fan5702_send_cmd(priv, reg_addr[FAN5702_REG_GENERAL], 0x9f);
	if (err < 0) {
		dev_err(dev, "cannot enable the channels\n");
		return err;
	}

	/* Do not tie any channels together, and set current ramp rate for group A channels */
	err = fan5702_send_cmd(priv, reg_addr[FAN5702_REG_CONFIG], 0);
	if (err < 0) {
		dev_err(dev, "cannot configure the channels\n");
		return err;
	}

	/* Turn all the LEDs off */
	for (i = 0; i < FAN5702_MAX_LEDS; i++)
		fan5702_set_brightness(i, 0);

	kthread_run(fan5702_blink_fn, NULL, "FAN5702-blink");

	return 0;

exit:
	fan5702_destroy_devices(priv);

	return err;
}

static const struct of_device_id fan5702_of_match[] = {
	{ .compatible = "onnn,fan5702" },
	{  },
};
MODULE_DEVICE_TABLE(of, fan5702_of_match);

static int fan5702_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	struct device *dev = &client->dev;
	struct device_node *np = dev->of_node, *child;
	struct fan5702_priv *priv;
	struct fan5702_led *led;
	u32 reg;
	int err, count;
	uint32_t i;

	count = of_get_child_count(np);
	if (!count || count > FAN5702_MAX_LEDS)
		return -EINVAL;

	priv = devm_kzalloc(dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	led_priv = priv;

	priv->client = client;

	i2c_set_clientdata(client, priv);

	for_each_child_of_node(np, child) {
		err = of_property_read_u32(child, "reg", &reg);
		if (err)
			return err;
		if (reg < 0 || reg >= FAN5702_MAX_LEDS)
			return -EINVAL;
		led = &priv->leds[reg];
		if (led->active)
			return -EINVAL;
		led->active = true;
		led->ldev.name = of_get_property(child, "label", NULL) ? : child->name;
		led->ldev.default_trigger = of_get_property(child, "linux,default-trigger", NULL);
	}

	for (i = 0 ; i < FAN5702_MAX_LEDS ; i++)
		blink_bool[i] = false;

	return fan5702_configure(dev, priv);
}

static int fan5702_remove(struct i2c_client *client) {
	struct fan5702_priv *priv = i2c_get_clientdata(client);

	fan5702_destroy_devices(priv);

	return 0;
}

static const struct i2c_device_id fan5702_id[] = {
	{ "fan5702" },
	{},
};
MODULE_DEVICE_TABLE(i2c, fan5702_id);

static struct i2c_driver fan5702_driver = {
	.driver = {
		.name = "fan5702",
		.of_match_table = of_match_ptr(fan5702_of_match),
	},
	.probe = fan5702_probe,
	.remove = fan5702_remove,
	.id_table = fan5702_id,
};

module_i2c_driver(fan5702_driver);

