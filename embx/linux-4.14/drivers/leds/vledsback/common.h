/*
 * -- Smart Object Oriented  --
 * Copyright (c) 2018 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - March-May 2018: Baptiste Delporte
 *
 */

#include <linux/irqreturn.h>
#include <virtshare/evtchn.h>
#include <virtshare/avz.h>
#include <virtshare/vbus.h>

#include <virtshare/dev/vleds.h>

typedef struct {

	vleds_cmd_back_ring_t ring;
	unsigned int	irq;

} vleds_cmd_ring_t;

typedef struct {

	vleds_cmd_ring_t	cmd_rings[MAX_DOMAINS];
	struct vbus_device	*vdev[MAX_DOMAINS];

} vleds_t;

extern vleds_t vleds;

irqreturn_t vleds_cmd_interrupt(int irq, void *dev_id);

/* State management */
void vleds_probe(struct vbus_device *dev);
void vleds_close(struct vbus_device *dev);
void vleds_suspend(struct vbus_device *dev);
void vleds_resume(struct vbus_device *dev);
void vleds_connected(struct vbus_device *dev);
void vleds_reconfigured(struct vbus_device *dev);
void vleds_shutdown(struct vbus_device *dev);

extern void vleds_vbus_init(void);

bool vleds_start(domid_t domid);
void vleds_end(domid_t domid);
bool vleds_is_connected(domid_t domid);
