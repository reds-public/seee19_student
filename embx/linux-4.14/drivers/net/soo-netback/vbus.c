
/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2015 SOOtech Ltd, Switzerland
 *
 * Authors: Raphaël Buache, Daniel Rossier
 * Emails: raphael.buache@heig-vd.ch, daniel.rossier@heig-vd.ch
 *
*/

#if 0
#define DEBUG
#endif

#include "common.h"

#include <virtshare/evtchn.h>
#include <virtshare/hypercall-arm.h>
#include <linux/wait.h>
#include <linux/slab.h>

#include <virtshare/gnttab.h>
#include <virtshare/hypervisor.h>
#include <virtshare/debug.h>
#include <virtshare/console.h>

/* Give the possibility to maintain the number of frontend currently connected to this backend,
 * and to manage an associated waitqueue.
 */
void vnet_get(void)
{
	atomic_inc(&vnet.refcnt);
}

void vnet_put(void)
{
	if (atomic_dec_and_test(&vnet.refcnt))
		wake_up(&vnet.waiting_to_free);
}

/*
 * Set up a ring (shared page & event channel) between the agency and the ME.
 */
static int setup_sring(struct vbus_device *dev)
{
	int res;
	unsigned long ring_tx_ref;
	unsigned long ring_rx_ref;
	unsigned int evtchn;

	netif_tx_sring_t *sring_tx;
	netif_rx_sring_t *sring_rx;

	vnet_ring_t *p_vnet_ring;

	p_vnet_ring = &vnet.descs[dev->otherend_id].rings;

	res = vbus_gather(VBT_NIL, dev->otherend, "ring-tx-ref", "%lu", &ring_tx_ref, "ring-rx-ref", "%lu", &ring_rx_ref, "event-channel", "%u", &evtchn, NULL);
	if (res) {
		lprintk("%s - line %d: Gathering %s/ring and event channel failed for device name %s\n", __func__, __LINE__, dev->otherend, dev->nodename);
		BUG();
	}

	/* Map the tx ring */
	res = vbus_map_ring_valloc(dev, ring_tx_ref, (void **) &sring_tx);
	if (res < 0)
	  return res;

	SHARED_RING_INIT(sring_tx);
	BACK_RING_INIT(&p_vnet_ring->ring_tx, sring_tx, PAGE_SIZE);

	/* Map the rx ring */
	res = vbus_map_ring_valloc(dev, ring_rx_ref, (void **) &sring_rx);
	if (res < 0)
	  return res;

	SHARED_RING_INIT(sring_rx);
	BACK_RING_INIT(&p_vnet_ring->ring_rx, sring_rx, PAGE_SIZE);


	res = bind_interdomain_evtchn_to_irqhandler(dev->otherend_id, evtchn, netif_interrupt, NULL, 0, "netif-backend", dev);
  if (res < 0) {
	  vbus_unmap_ring_vfree(dev, sring_tx);
	  vbus_unmap_ring_vfree(dev, sring_rx);

	  (&p_vnet_ring->ring_tx)->sring = NULL;
	  (&p_vnet_ring->ring_rx)->sring = NULL;

		return res;
	}

  p_vnet_ring->irq = res;

  /* Increment refcount for this vnet */
  vnet_get();

	return 0;
}


/*
 * Entry point to this code when a new device is created.  Allocate the basic
 * structures, and watch the store waiting for the hotplug scripts to tell us
 * the device's physical major and minor numbers.  Switch to InitWait.
 */
static int netback_probe(struct vbus_device *dev, const struct vbus_device_id *id)
{
	int ret;

	DBG("%s: SOO net driver for testing\n", __func__);

  ret = vnet_subsys_init(dev);

	return ret;

}


static int frontend_resumed(struct vbus_device *dev)
{
	/* Resume Step 3 */
	DBG("%s: now is %s ...\n", __func__, dev->nodename);

	return 0;

}

/*
 * Callback received when the frontend's state changes.
 */
static void frontend_changed(struct vbus_device *dev, enum vbus_state frontend_state)
{
	int res = 0;
	vnet_ring_t *p_vnet_ring;

	p_vnet_ring = &vnet.descs[dev->otherend_id].rings;

	DBG("%s", vbus_strstate(frontend_state));

	switch (frontend_state) {

	case VbusStateInitialised:
	case VbusStateReconfigured:

		res = setup_sring(dev);
		if (res) {
			lprintk("%s - line %d: Retrieval of ring info failed for device name %s\n", __func__, __LINE__, dev->nodename);
			BUG();
		}

		res = vnet_subsys_enable(dev);
		if (res) {
			lprintk("%s - line %d: Vnet subsys enable failed for device name %s\n", __func__, __LINE__, dev->nodename);
			BUG();
		}

		DBG0("SOO vnet: now connected...\n");

		break;

	case VbusStateConnected:

		DBG0("->Virtual network frontend connected, all right.\n");

		break;

	case VbusStateClosing:
		DBG0("Got that the virtual network frontend now closing...\n");

		vnet_subsys_disable(dev);
	  vnet_subsys_remove(dev);

		vnet_put();

		/* Prepare to empty all buffers */
		BACK_RING_INIT(&p_vnet_ring->ring_tx, (&p_vnet_ring->ring_tx)->sring, PAGE_SIZE);
		BACK_RING_INIT(&p_vnet_ring->ring_rx, (&p_vnet_ring->ring_rx)->sring, PAGE_SIZE);

		unbind_from_irqhandler(p_vnet_ring->irq, dev);

		vbus_unmap_ring_vfree(dev, p_vnet_ring->ring_tx.sring);
		vbus_unmap_ring_vfree(dev, p_vnet_ring->ring_rx.sring);

		p_vnet_ring->ring_tx.sring = NULL;
		p_vnet_ring->ring_rx.sring = NULL;

		break;

	case VbusStateSuspended:

		/* Suspend Step 3 */
		DBG("frontend_suspended: %s ...\n", dev->nodename);

		break;

	case VbusStateUnknown:
	default:
		lprintk("%s - line %d: Unknown state %d (frontend) for device name %s\n", __func__, __LINE__, frontend_state, dev->nodename);
		BUG();
		break;
	}
}

/*
 * When the backend is being shutdown, we have to ensure that there is no still-using frontend.
 * 		wait_event(vnet.waiting_to_free, atomic_read(&vnet.refcnt) == 0);
 */

/* ** Driver Registration ** */

static const struct vbus_device_id netback_ids[] = {
	{ "net" },
	{ "" }
};

static int netback_suspend(struct vbus_device *dev)
{
	/* Suspend Step 1 */

	DBG0("Backend waiting for frontend now...\n");

	return 0;
}

static int netback_resume(struct vbus_device *dev)
{
	/* Resume Step 1 */

	DBG("Backend resuming: %s ...\n", dev->nodename);

	return 0;
}

static struct vbus_driver netback = {
	.name = "net",
	.owner = THIS_MODULE,
	.ids = netback_ids,
	.probe = netback_probe,
	.otherend_changed = frontend_changed,
	.suspend = netback_suspend,
	.resume = netback_resume,
	.uevent = netback_uevent,
	.resumed = frontend_resumed,
};

int netback_vbus_init(void) {
	init_waitqueue_head(&vnet.waiting_to_free);

  return vbus_register_backend(&netback);
}



