/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - Daniel Rossier
 * - January 2019: Baptiste Delporte
 *
 */

#include <linux/irqreturn.h>
#include <virtshare/evtchn.h>
#include <virtshare/avz.h>
#include <virtshare/vbus.h>

#include <virtshare/dev/vuart.h>

typedef struct {

	vuart_back_ring_t ring;
	unsigned int irq;

} vuart_ring_t;

/*
 * General structure for this virtual device (backend side)
 */
typedef struct {

	vuart_ring_t rings[MAX_DOMAINS];	
	struct vbus_device  *vdev[MAX_DOMAINS];

	/* Array and counters handling bufferized chars when the backend is not Connected yet */
	char buf_chars[MAX_DOMAINS][MAX_BUF_CHARS];
	uint32_t buf_chars_prod[MAX_DOMAINS];
	uint32_t buf_chars_cons[MAX_DOMAINS];

} vuart_t;

extern vuart_t vuart;

irqreturn_t vuart_interrupt(int irq, void *dev_id);

void vuart_probe(struct vbus_device *dev);
void vuart_close(struct vbus_device *dev);
void vuart_suspend(struct vbus_device *dev);
void vuart_resume(struct vbus_device *dev);
void vuart_connected(struct vbus_device *dev);
void vuart_reconfigured(struct vbus_device *dev);
void vuart_shutdown(struct vbus_device *dev);

void vuart_vbus_init(void);

bool vuart_start(domid_t domid);
void vuart_end(domid_t domid);
bool vuart_is_connected(domid_t domid);

