/*
 *
 * -- Smart Object Oriented  --
 * Copyright (c) 2018,2019 SOOtech SA, Switzerland
 *
 * The contents of this file is strictly under the property of SOOtech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - October 2018: Baptiste Delporte
 * - February 2019: Baptiste Delporte
 *
 */

#include <linux/irqreturn.h>
#include <virtshare/evtchn.h>
#include <virtshare/avz.h>
#include <virtshare/vbus.h>

#include <virtshare/dev/vweather.h>

typedef struct {
	char		*data;
	unsigned int	pfn;
} vweather_shared_buffer_t;

typedef struct {
	unsigned int	irq;
} vweather_notification_t;

typedef struct {
	vweather_shared_buffer_t	weather_buffers[MAX_DOMAINS];
	vweather_notification_t		update_notifications[MAX_DOMAINS];
	struct vbus_device 		*vdev[MAX_DOMAINS];

} vweather_t;

extern vweather_t vweather;

extern size_t vweather_packet_size;

/* State management */
void vweather_probe(struct vbus_device *dev);
void vweather_close(struct vbus_device *dev);
void vweather_suspend(struct vbus_device *dev);
void vweather_resume(struct vbus_device *dev);
void vweather_connected(struct vbus_device *dev);
void vweather_reconfigured(struct vbus_device *dev);
void vweather_shutdown(struct vbus_device *dev);

/* Shared buffer setup */
int vweather_setup_shared_buffer(struct vbus_device *dev);

void vweather_vbus_init(void);

bool vweather_start(domid_t domid);
void vweather_end(domid_t domid);
bool vweather_is_connected(domid_t domid);

irqreturn_t vweather_interrupt(int irq, void *dev_id);
