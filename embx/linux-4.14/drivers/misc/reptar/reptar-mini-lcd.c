/*
 *  reptar-mini-lcd.c
 *
 *  Copyright (C) HEIG-VD / REDS 2016
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


/*
 * TODO:
 * -
 */


#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/slab.h>

#include <asm/uaccess.h>
#include <linux/uaccess.h>

#include <linux/of_platform.h>
#include <linux/of_address.h>

#define DRV_NAME "reptar_mini_lcd"
#define GENMASK_LEN(len, low)		GENMASK(low+len-1, low)

#define REPTAR_LCD_IOCTL_MAGIC		42
#define REPTAR_LCD_IOCTL_SWITCH_ON	_IO(REPTAR_LCD_IOCTL_MAGIC, 0)
#define REPTAR_LCD_IOCTL_SWITCH_OFF	_IO(REPTAR_LCD_IOCTL_MAGIC, 1)
#define REPTAR_LCD_IOCTL_CLEAR		_IO(REPTAR_LCD_IOCTL_MAGIC, 2)
#define REPTAR_LCD_IOCTL_HOME		_IO(REPTAR_LCD_IOCTL_MAGIC, 3)

/* Registers */
#define REPTAR_LCD_CTL_REG		0x00
#define REPTAR_LCD_STATUS_REG		0x02

/* REPTAR_LCD_CTL_REG */
#define REPTAR_CTL_DATA_OFFSET		0
#define REPTAR_CTL_DATA_MASK		GENMASK_LEN(8, REPTAR_CTL_DATA_OFFSET)
#define REPTAR_CTL_READ_BIT_OFFSET	8
#define REPTAR_CTL_READ			(1 << REPTAR_CTL_READ_BIT_OFFSET)
#define REPTAR_CTL_RAM_WRITE_OFFSET	9
#define REPTAR_CTL_RAM_WRITE		(1 << REPTAR_CTL_RAM_WRITE_OFFSET)
#define REPTAR_CTL_START_OFFSET		10
#define REPTAR_CTL_START		(1 << REPTAR_CTL_START_OFFSET)

/* REPTAR_LCD_STATUS_REG */
#define REPTAR_STATUS_DATA_OFFSET	0
#define REPTAR_STATUS_DATA_MASK		GENMASK_LEN(8, REPTAR_STATUS_DATA_OFFSET)
#define REPTAR_STATUS_READY_OFFSET	8
#define REPTAR_STATUS_READY		(1 << REPTAR_STATUS_READY_OFFSET)

#define LCD_CMD_CLEAR			(1 << 0)
#define LCD_CMD_HOME			(1 << 1)
#define LCD_CMD_ENTRY_MODE		(1 << 2)
#define LCD_CMD_ENTRY_MODE_LEFT		(LCD_CMD_ENTRY_MODE)
#define LCD_CMD_ENTRY_MODE_RIGHT	(LCD_CMD_ENTRY_MODE | 1 << 1)
#define LCD_CMD_ON_CTL			(1 << 3)
#define LCD_CMD_DISPLAY_OFF		(LCD_CMD_ON_CTL)
#define LCD_CMD_BLINK_ON		(LCD_CMD_ON_CTL | 1 << 0)
#define LCD_CMD_CURSOR_ON		(LCD_CMD_ON_CTL | 1 << 1)
#define LCD_CMD_DISPLAY_ON		(LCD_CMD_ON_CTL | 1 << 2)
#define LCD_CMD_SHIFT_CTL		(1 << 4)
#define LCD_CMD_FUNC_CTL		(1 << 5)
#define LCD_CMD_FUNC_5X11		(LCD_CMD_FUNC_CTL | 1 << 2)
#define LCD_CMD_FUNC_2_LINES		(LCD_CMD_FUNC_CTL | 1 << 3)
#define LCD_CMD_FUNC_8BITS		(LCD_CMD_FUNC_CTL | 1 << 4)
#define LCD_CMD_SET_CGRAM		(1 << 6)	/* not used */
#define LCD_CMD_SET_DDRAM		(1 << 7)
#define LCD_CMD_SET_DDRAM_MASK		GENMASK(6, 0)

#define LCD_STATUS_BUSY_FLAG		(1 << 7)

#define LCD_BUFFER_SIZE			100	/* 4 lines of 20 characters */

MODULE_AUTHOR("Florian Vaussard <florian.vaussard@heig-vd.ch>");
MODULE_DESCRIPTION("Control the mini-LCD connected to the Reptar FPGA");
MODULE_LICENSE("GPL v2");

struct reptar_device {
	struct miscdevice miscdev;
	void __iomem *base;

	bool opened;
	char buffer[LCD_BUFFER_SIZE+1];	/* +1 for null byte */
	int buffer_count;
};

static inline struct device *reptar_to_dev(const struct reptar_device *reptar)
{
	return reptar->miscdev.this_device;
}

static uint16_t reptar_read(const struct reptar_device *reptar, unsigned reg)
{
	return ioread16(reptar->base + reg);
}

static void reptar_write(const struct reptar_device *reptar, unsigned reg, uint16_t value)
{
	iowrite16(value, reptar->base + reg);
}

static int reptar_lcd_write_cmd(const struct reptar_device *reptar, uint16_t cmd)
{
	int retries = 50;
	uint16_t reg = reptar_read(reptar, REPTAR_LCD_STATUS_REG);

	/* Wait for the FPGA to be ready */
	while (!(reg & REPTAR_STATUS_READY) && retries--) {
		udelay(200);
		reg = reptar_read(reptar, REPTAR_LCD_STATUS_REG);
	}

	if (retries == 0) {
		dev_warn(reptar_to_dev(reptar), "Timeout waiting for fpga response\n");
		return -EBUSY;
	}

	reptar_write(reptar, REPTAR_LCD_CTL_REG, REPTAR_CTL_START | cmd);

	return 0;
}

static int reptar_lcd_busy_wait(const struct reptar_device *reptar)
{
	int retries = 10;
	uint16_t reg;

	do {
		reptar_lcd_write_cmd(reptar, REPTAR_CTL_READ);
		reg = reptar_read(reptar, REPTAR_LCD_STATUS_REG);
	} while ((reg & LCD_STATUS_BUSY_FLAG) && --retries);

	if (retries == 0) {
		dev_err(reptar_to_dev(reptar), "Timeout waiting for busyflag\n");
		return -EBUSY;
	}

	return 0;
}

static int reptar_lcd_write_cmd_busy(const struct reptar_device *reptar, uint16_t cmd)
{
	int err;

	err = reptar_lcd_write_cmd(reptar, cmd);
	if (err) {
		;
	}

	err = reptar_lcd_busy_wait(reptar);
	if (err) {
		;
	}

	return 0;
}

static inline int reptar_lcd_line_to_offset(int line)
{
	int line_offset = 0;

	/* TODO: use LUT? */
	switch (line) {
		case 1:
			line_offset = 0x40;
			break;
		case 2:
			line_offset = 0x14;
			break;
		case 3:
			line_offset = 0x54;
			break;
		default:
			;
	}

	return line_offset;
}

static int reptar_lcd_print_string(const struct reptar_device *reptar, int line, uint8_t offset, const char *str, int N)
{
	int n = 0;
	unsigned int total_offset = 0;
	bool update_offset = false;
	char c;

	/* wrap-up lines */
	while (offset >= 20) {
		offset -= 20;
		line++;
	}

	while (line >= 4)
		line -= 4;

	/* set offset */
	total_offset = offset + reptar_lcd_line_to_offset(line);
	total_offset &= LCD_CMD_SET_DDRAM_MASK;
	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_SET_DDRAM | total_offset);

	/* send characters */
	for (; (n < N-1) && (n < 80); n++) {
		c = str[n];

		/* substitue "\n" with '\n' */
		if (c == '\\' && str[n+1] == 'n') {
			c = '\n';
			n++;	/* skip 1 character */
		}

		/* skip non-printable characters */
		if ((c >= 0x20) && (c <= 0x7e)) {
			reptar_lcd_write_cmd_busy(reptar, REPTAR_CTL_RAM_WRITE | c);
			/* move forward by one character */
			offset++;
			if (offset >= 20) {
				line++;
				offset -= 20;
				update_offset = true;
			}
		} else {
			/* manage new lines */
			if (c != '\n')
				continue;
			line++;
			offset = 0;
			update_offset = true;
		}

		/* adjust line offset if necessary */
		if (!update_offset)
			/* no need */
			continue;

		if (line >= 4)
			line -= 4;
		total_offset = offset + reptar_lcd_line_to_offset(line);
		total_offset &= LCD_CMD_SET_DDRAM_MASK;
		reptar_lcd_write_cmd_busy(reptar, LCD_CMD_SET_DDRAM | total_offset);
		update_offset = false;
	}

	return n;
}

static int reptar_lcd_open(struct inode *inode, struct file *file)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);

	/* TODO: locking... */
	if (reptar->opened)
		return -EBUSY;

	reptar->opened = true;

	return 0;
}

static int reptar_lcd_release(struct inode *inode, struct file *file)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);

	reptar->opened = false;

	return 0;
}

static long reptar_lcd_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);
	(void)arg;

	switch(cmd)
	{
		case REPTAR_LCD_IOCTL_SWITCH_ON:
			reptar_lcd_write_cmd_busy(reptar, LCD_CMD_DISPLAY_ON);
			break;

		case REPTAR_LCD_IOCTL_SWITCH_OFF:
			reptar_lcd_write_cmd_busy(reptar, LCD_CMD_DISPLAY_OFF);
			break;

		case REPTAR_LCD_IOCTL_CLEAR:
			reptar_lcd_write_cmd_busy(reptar, LCD_CMD_CLEAR);
			break;

		case REPTAR_LCD_IOCTL_HOME:
			reptar_lcd_write_cmd_busy(reptar, LCD_CMD_HOME);
			break;

		/* TODO
		case FPGA_LCD_PRINT:
			break;
		*/

		default:
			dev_warn(reptar_to_dev(reptar), "Unknown ioctl (%08x)\n", cmd);
			return -EINVAL;
	}

	return 0;
}

static ssize_t reptar_lcd_write(struct file *file, const char *buffer, size_t count, loff_t * offset)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);
	int n;

	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_DISPLAY_ON);
	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_CLEAR);

	reptar->buffer_count = min_t(size_t, count, LCD_BUFFER_SIZE);
	if(copy_from_user(reptar->buffer, buffer, reptar->buffer_count))
		return -EFAULT;
	reptar->buffer[reptar->buffer_count] = 0;	/* ensure null-terminated string */

	n = reptar_lcd_print_string(reptar, 0, 0, reptar->buffer, reptar->buffer_count);

	return count;
}


struct file_operations reptar_lcd_fops = {
	.owner =   THIS_MODULE,
	.write =   reptar_lcd_write,
	.unlocked_ioctl =  reptar_lcd_ioctl,
	.open =    reptar_lcd_open,
	.release = reptar_lcd_release,
};

static void reptar_lcd_init(const struct reptar_device *reptar)
{
	/* LCD 8-bit mode initialization, we cannot busy wait according to datasheet */
	reptar_lcd_write_cmd(reptar, LCD_CMD_FUNC_8BITS);
	udelay(2000);	/* yes, udelay is bad... */
	udelay(2000);	/* I know */
	udelay(1000);	/* will do better later */
	reptar_lcd_write_cmd(reptar, LCD_CMD_FUNC_8BITS);
	udelay(160);
	reptar_lcd_write_cmd(reptar, LCD_CMD_FUNC_8BITS);
	udelay(160);

	/* 8-bit, 2 lines, 5x8 dots mode */
	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_FUNC_8BITS | LCD_CMD_FUNC_2_LINES);

	/* Clear */
	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_DISPLAY_OFF);
	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_CLEAR);

	/* Sets the entry mode (left to right) */
	reptar_lcd_write_cmd_busy(reptar, LCD_CMD_ENTRY_MODE_RIGHT);
}

static int reptar_lcd_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct reptar_device *reptar;
	void __iomem *base;
	int err = 0;

	reptar = kzalloc(sizeof(*reptar), GFP_KERNEL);
	if (reptar ==  NULL) {
		dev_err(&pdev->dev, "Unable to allocate device\n");
		return -ENOMEM;
	}

	/* Remap the registers */
	base = of_iomap(np, 0);
	if (IS_ERR(base)) {
		dev_err(&pdev->dev, "Failed to request memory region\n");
		err = PTR_ERR(base);
		goto err_free;
	}

	reptar->base = base;
	reptar->miscdev.minor = MISC_DYNAMIC_MINOR,
	reptar->miscdev.name = DRV_NAME,
	reptar->miscdev.fops = &reptar_lcd_fops,
	reptar->opened = false;
	reptar->buffer_count = 0;

	/* Init hardware */
	reptar_lcd_init(reptar);

	err = misc_register(&reptar->miscdev);
	if (err) {
		pr_err("%s: misc_register failed (%d)\n", __func__, err);
		goto err_unmap;
	}

	dev_info(reptar_to_dev(reptar), "Successfully registered\n");

	platform_set_drvdata(pdev, reptar);

	return 0;

err_unmap:
	iounmap(base);
err_free:
	kfree(reptar);

	return err;
}

static int reptar_lcd_remove(struct platform_device *pdev)
{
	struct reptar_device *reptar = platform_get_drvdata(pdev);

	misc_deregister(&reptar->miscdev);
	iounmap(reptar->base);
	kfree(reptar);

	return 0;
}

static struct of_device_id reptar_lcd_table[] = {
	{.compatible = "reds,reptar-mini-lcd"},
	{},
};
MODULE_DEVICE_TABLE(of, reptar_lcd_table);

static struct platform_driver reptar_lcd_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DRV_NAME,
		.of_match_table = reptar_lcd_table,
	},
	.probe = reptar_lcd_probe,
	.remove = reptar_lcd_remove,
};

module_platform_driver(reptar_lcd_driver);
