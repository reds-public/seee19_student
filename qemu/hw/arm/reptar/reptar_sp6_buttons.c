/*
 * REPTAR FPGA buttons emulation
 *
 * Copyright (c) 2013-2019 HEIG-VD / REDS
 * Written by Romain Bornet
 *
 * This module provides a basic emulation for the 8 buttons of REPTAR's FPGA board.
 *
 */

#include "reptar_sp6.h"

sp6_state_t *sp6_state;

int reptar_sp6_btns_event_process(cJSON * object)
{
    /* A completer */

	return 0;
}

int reptar_sp6_btns_init(void *opaque)
{
    sp6_state = (sp6_state_t *) opaque;

    return 0;
}
