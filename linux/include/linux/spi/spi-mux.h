/*
 * SPI multiplexer core driver
 *
 * Dries Van Puymbroeck <Dries.VanPuymbroeck@barco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not,  see <http://www.gnu.org/licenses>
 */

#ifndef _LINUX_SPI_MUX_H
#define _LINUX_SPI_MUX_H

#ifdef __KERNEL__

/**
 * spi_add_mux: - create a SPI bus on a multiplexed bus segment.
 * @spi: the SPI device struct attached to the parent SPI controller
 * @mux_dev: pointer to the mux's implementation-dependent data
 * @num_chipselect: maximum number of chip selects on this mux
 * @spi_mux_select: callback function to set the mux to a specified chip select
 *
 * Description: adds a mux on the chip select of @spi
 * Return: 0 if success, err otherwise
 */
extern int spi_add_mux(struct spi_device *spi,
		       void *mux_dev,
		       u16 num_chipselect,
		       int (*spi_mux_select)(void *mux_dev, u8 chip_select));

/**
 * spi_del_mux: - delete a SPI mutliplexer previously added by spi_add_mux
 * @spi: the SPI device struct attached to the parent SPI controller
 *
 * Description: deletes the mux on the chip select of @spi
 */
extern void spi_del_mux(struct spi_device *spi);

/**
 * spi_get_mux_dev: - get pointer to the mux's implementation-dependent data
 * @spi: the SPI device struct attached to the parent SPI controller
 *
 * Return: the mux_dev pointer passed in spi_add_mux
 */
extern void *spi_get_mux_dev(struct spi_device *spi);

#endif /* __KERNEL__ */

#endif /* _LINUX_SPI_MUX_H */
