/*
 *  reptar-mini-lcd.c
 *
 *  Copyright (C) HEIG-VD / REDS 2016
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


/*
 * TODO:
 * -
 */


#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/slab.h>

#include <linux/uaccess.h>
#include <asm/uaccess.h>

#include <linux/of_platform.h>
#include <linux/of_address.h>

#define DRV_NAME "reptar_7_seg"

/* Registers */
#define REPTAR_7SEG_1			0x00
#define REPTAR_7SEG_2			0x02
#define REPTAR_7SEG_3			0x04

#define BLANK				0
#define DIGI_MASK			GENMASK(6, 0)
#define DOT				(1 << 7)

#define SEVENSEG_BUFFER_SIZE		6	/* 3 digits + points */

MODULE_AUTHOR("Florian Vaussard <florian.vaussard@heig-vd.ch>");
MODULE_DESCRIPTION("Control the 3 7-segment digits connected to the Reptar FPGA");
MODULE_LICENSE("GPL v2");

static const uint16_t digit_lut[] = {
	0x3F,
	0x06,
	0x5B,
	0x4F,
	0x66,
	0x6D,
	0x7D,
	0x07,
	0x7F,
	0x67,
};

struct reptar_device {
	struct miscdevice miscdev;
	void __iomem *base;

	bool opened;
	char buffer[SEVENSEG_BUFFER_SIZE];
	int buffer_count;
};

static inline struct device *reptar_to_dev(const struct reptar_device *reptar)
{
	return reptar->miscdev.this_device;
}

static uint16_t reptar_read(const struct reptar_device *reptar, unsigned reg)
{
	return ioread16(reptar->base + reg);
}

static void reptar_write(const struct reptar_device *reptar, unsigned reg, uint16_t value)
{
	iowrite16(value, reptar->base + reg);
}

static int reptar_7seg_show(const struct reptar_device *reptar, int digit, char c, bool dot)
{
	uint16_t code = BLANK;
	unsigned reg;

	switch (c) {
		case '0' ... '9':
			code = digit_lut[c - '0'];
			break;
	}

	if (digit == 0)
		reg = REPTAR_7SEG_1;
	else if (digit == 1)
		reg = REPTAR_7SEG_2;
	else if (digit == 2)
		reg = REPTAR_7SEG_3;
	else {
		dev_warn(reptar_to_dev(reptar), "Invalid digit number (%d)\n", digit);
		return -EINVAL;
	}

	if (dot)
		code |= DOT;

	reptar_write(reptar, reg, code);

	return 0;
}

static void reptar_7seg_blank(const struct reptar_device *reptar)
{
	int i;

	for (i = 0; i < 3; i++)
		reptar_7seg_show(reptar, i, ' ', false);
}

static int reptar_7seg_open(struct inode *inode, struct file *file)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);

	/* TODO: locking... */
	if (reptar->opened)
		return -EBUSY;

	reptar->opened = true;

	return 0;
}

static int reptar_7seg_release(struct inode *inode, struct file *file)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);

	reptar->opened = false;

	return 0;
}

static ssize_t reptar_7seg_write(struct file *file, const char *buffer, size_t count, loff_t * offset)
{
	struct reptar_device *reptar = container_of(file->private_data,
						    struct reptar_device, miscdev);
	int i;
	int n;
	char c;
	bool dot = false;

	reptar->buffer_count = min_t(size_t, count, SEVENSEG_BUFFER_SIZE);
	if(copy_from_user(reptar->buffer, buffer, reptar->buffer_count))
		return -EFAULT;

	/* for all 3 digits */
	for (n = 2, i = reptar->buffer_count; n >= 0; n--, i--) {
		if (i < 0) {
			/* no more characters, just show spaces */
			reptar_7seg_show(reptar, n, ' ', false);
			continue;
		}

		c = reptar->buffer[i];
		if ((c == '\n') | (c == 0)) {
			/* useless characters at the end of the string */
			n++;
			continue;
		}

		if (c == '.') {
			n++;
			dot = true;
			continue;
		}

		reptar_7seg_show(reptar, n, c, dot);
		dot = false;
	}

	return count;
}


struct file_operations reptar_7seg_fops = {
	.owner =   THIS_MODULE,
	.write =   reptar_7seg_write,
	.open =    reptar_7seg_open,
	.release = reptar_7seg_release,
};

static int reptar_7seg_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct reptar_device *reptar;
	void __iomem *base;
	int err = 0;

	reptar = kzalloc(sizeof(*reptar), GFP_KERNEL);
	if (reptar ==  NULL) {
		dev_err(&pdev->dev, "Unable to allocate device\n");
		return -ENOMEM;
	}

	/* Remap the registers */
	base = of_iomap(np, 0);
	if (IS_ERR(base)) {
		dev_err(&pdev->dev, "Failed to request memory region\n");
		err = PTR_ERR(base);
		goto err_free;
	}

	reptar->base = base;
	reptar->miscdev.minor = MISC_DYNAMIC_MINOR,
	reptar->miscdev.name = DRV_NAME,
	reptar->miscdev.fops = &reptar_7seg_fops,
	reptar->opened = false;

	/* Init hardware */
	reptar_7seg_blank(reptar);

	err = misc_register(&reptar->miscdev);
	if (err) {
		pr_err("%s: misc_register failed (%d)\n", __func__, err);
		goto err_unmap;
	}

	dev_info(reptar_to_dev(reptar), "Successfully registered\n");

	platform_set_drvdata(pdev, reptar);

	return 0;

err_unmap:
	iounmap(base);
err_free:
	kfree(reptar);

	return err;
}

static int reptar_7seg_remove(struct platform_device *pdev)
{
	struct reptar_device *reptar = platform_get_drvdata(pdev);

	misc_deregister(&reptar->miscdev);
	iounmap(reptar->base);
	kfree(reptar);

	return 0;
}

static struct of_device_id reptar_7seg_table[] = {
	{.compatible = "reds,reptar-7seg-lcd"},
	{},
};
MODULE_DEVICE_TABLE(of, reptar_7seg_table);

static struct platform_driver reptar_7seg_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DRV_NAME,
		.of_match_table = reptar_7seg_table,
	},
	.probe = reptar_7seg_probe,
	.remove = reptar_7seg_remove,
};

module_platform_driver(reptar_7seg_driver);
