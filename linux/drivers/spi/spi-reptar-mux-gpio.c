/*
 * SPI multiplexer driver using GPIO API for Barco Orka board
 *
 * Dries Van Puymbroeck <Dries.VanPuymbroeck@barco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not,  see <http://www.gnu.org/licenses>
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi-mux.h>

/**
 * struct reptar_spi_mux_gpio - the basic reptar_spi_mux_gpio structure
 * @gpios:		Array of GPIO numbers used to control MUX
 * @n_gpios:		Number of GPIOs used to control MUX
 */
struct reptar_spi_mux_gpio {
	unsigned int	*gpios;
	int		n_gpios;
};

static int reptar_spi_mux_gpio_select(void *mux_dev, u8 chip_select)
{
	struct reptar_spi_mux_gpio *mux =
		(struct reptar_spi_mux_gpio *)mux_dev;
	int i;

	for (i = 0; i < mux->n_gpios; i++) {
		gpio_set_value(mux->gpios[i], i == chip_select);
	}

	return 0;
}

static int reptar_spi_mux_gpio_probe(struct spi_device *spi)
{
	struct reptar_spi_mux_gpio *mux;
	struct device_node *np;
	int ret = 0, i;
	u16 num_chipselect;

	mux = devm_kzalloc(&spi->dev, sizeof(*mux), GFP_KERNEL);
	if (mux == NULL) {
		dev_err(&spi->dev, "Failed to allocate driver struct\n");
		return -ENOMEM;
	}

	np = spi->dev.of_node;
	if (!np)
		return -ENODEV;

	mux->n_gpios = of_gpio_named_count(np, "gpios");
	if (mux->n_gpios < 0) {
		dev_err(&spi->dev, "Missing gpios property in the DT.\n");
		return -EINVAL;
	}

	mux->gpios = devm_kzalloc(&spi->dev,
			     sizeof(*mux->gpios) * mux->n_gpios,
			     GFP_KERNEL);
	if (!mux->gpios) {
		dev_err(&spi->dev, "Cannot allocate gpios array");
		return -ENOMEM;
	}

	for (i = 0; i < mux->n_gpios; i++)
		mux->gpios[i] = of_get_named_gpio(np, "gpios", i);

	for (i = 0; i < mux->n_gpios; i++) {
		devm_gpio_request(&spi->dev, mux->gpios[i],
				  "barco-orka-spi-mux-gpio");
		gpio_direction_output(mux->gpios[i], 0);
	}

	/* the mux can have <nr_gpio_used_for_muxing> chip selects */
	num_chipselect = mux->n_gpios;

	ret = spi_add_mux(spi, mux, num_chipselect,
			  reptar_spi_mux_gpio_select);
	return ret;
}

static int reptar_spi_mux_gpio_remove(struct spi_device *spi)
{
	spi_del_mux(spi);
	return 0;
}

static const struct of_device_id reptar_spi_mux_gpio_of_match[] = {
	{ .compatible = "reds,reptar-spi-mux-gpio", },
	{},
};
MODULE_DEVICE_TABLE(of, reptar_spi_mux_gpio_of_match);

static struct spi_driver reptar_spi_mux_gpio_driver = {
	.probe	= reptar_spi_mux_gpio_probe,
	.remove	= reptar_spi_mux_gpio_remove,
	.driver	= {
		.owner	= THIS_MODULE,
		.name	= "reds,reptar-spi-mux-gpio",
		.of_match_table = reptar_spi_mux_gpio_of_match,
	},
};

module_spi_driver(reptar_spi_mux_gpio_driver);

MODULE_DESCRIPTION("GPIO-based SPI multiplexer driver for REDS Reptar");
MODULE_AUTHOR("Florian Vaussard <florian.vaussard@heig-vd.ch>");
MODULE_LICENSE("GPL");
MODULE_ALIAS("spi:reds,reptar-spi-mux-gpio");
